#include "game.hpp"

namespace MyGame
{
    Game::Game()
    {
        std::srand(std::time(nullptr));
        CreateFood();
        CreateEnemies();
        // e = enemies.at(8);
    }

    Game::~Game()
    {
    }

    void Game::Run()
    {
        while (isRunning)
        {
            // GetEvent();

            // UpdateEnemies here
            UpdateEnemies();

            if (XPending(gameDisplay.GetDisplay()))
            {
                XNextEvent(gameDisplay.GetDisplay(), &event);

                // Gets called when window is initialized/rezised
                if (event.type == Expose)
                {
                    XGetWindowAttributes(gameDisplay.GetDisplay(), gameDisplay.GetWindow(), &gwa);

                    // glViewport(0, 0, gwa.width, gwa.height);

                    glClearColor(1.0, 1.0, 1.0, 1.0);
                    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

                    DrawEnemiesExpose();

                    glXSwapBuffers(gameDisplay.GetDisplay(), gameDisplay.GetWindow());
                }
                /* else if (event.type == KeyPress)
                {
                    std::cout << "Hello Key Press!\n";
                    glXMakeCurrent(gameDisplay.GetDisplay(), None, NULL);
                    glXDestroyContext(gameDisplay.GetDisplay(), gameDisplay.GetGLXContext());
                    XDestroyWindow(gameDisplay.GetDisplay(), gameDisplay.GetWindow());
                    XCloseDisplay(gameDisplay.GetDisplay());

                    std::cout << "key: " << event.xkey.keycode << "\n";
                    if (event.xkey.keycode == KEY_ESCAPE)
                    {
                        isRunning = false;
                        exit(0);
                    }
                    else
                    {
                        std::cout << "Hello!\n";
                        isRunning = false;
                        exit(0);
                    }
                } */
            }

            // if (!gameOver)
            // {
            //     UpdateEnemies();
            // }

            // if (GetEvent())
            // {
            //     HandleEvent();

            //     if (!gameOver && !CheckPlayerBounds())
            //     {
            //         std::cout << "Player out of bounds!\n";
            //         gameOver = true;
            //         gameWon = false;
            //     }
            // }
        }
    }

    bool Game::GetEvent()
    {
        if (XPending(gameDisplay.GetDisplay()))
        {
            XNextEvent(gameDisplay.GetDisplay(), &event);
            std::cout << "Event: " << event.type << "\n";
            return true;
        }

        return false;
    }

    void Game::DrawPlayer()
    {
        DrawCharacter(player);
    }

    void Game::Draw()
    {
        DrawAllFood();
        DrawAllEnemies();
        DrawPlayer();
        DrawMessage();
    }

    void Game::CreateFood()
    {
        food.clear();
        food.resize(10);

        const int MAX_X = 600;
        const int MAX_Y = 600;

        for (auto &f : food)
        {
            f.position.x = (std::rand() % MAX_X) / 20 * 20;
            f.position.y = std::rand() % MAX_Y / 20 * 20;
        }
    }

    void Game::DrawSingleFood(const Food &f)
    {
        // gameDisplay.DrawRect(253, 124, 201, f.position.x, f.position.y, f.size.width, f.size.height); //DrawRect(f.color, f.position.x, f.position.y, f.size.width, f.size.height);
    }

    void Game::DrawAllFood()
    {
        for (auto &f : food)
        {
            DrawCharacter(f);
        }
    }

    void Game::CreateEnemies()
    {
        enemies.clear();
        enemies.resize(10);

        const int MAX_X = 600;
        const int MAX_Y = 600;

        for (auto &e : enemies)
        {
            e.position.x = (std::rand() % MAX_X) / 20 * 20;
            e.position.y = (std::rand() % MAX_Y) / 20 * 20;
        }
    }

    void Game::DrawAllEnemies()
    {
        for (auto &e : enemies)
        {
            DrawCharacter(e);
        }
    }

    void Game::DrawMessage()
    {
        if (!gameOver)
        {
            return;
        }

        if (gameWon)
        {
            gameDisplay.DrawText(100, 100, "You Win! Press Spacebar to restart!");
        }
        else
        {
            gameDisplay.DrawText(100, 100, "You Lose! Press Spacebar to restart!");
        }
    }

    void Game::Update()
    {
        auto iter = std::find_if(food.begin(), food.end(), [&](const Food &f)
                                 { return RectangleIntersect(player.Bounds(), f.Bounds()); });

        if (iter != food.end())
        {
            food.erase(iter);
        }

        if (food.empty())
        {
            std::cout << " Win!\n";
            gameOver = true;
            gameWon = true;
        }

        auto iterEnemies = std::find_if(enemies.begin(), enemies.end(), [&](const Enemy &e)
                                        { return RectangleIntersect(player.Bounds(), e.Bounds()); });

        if (iterEnemies != enemies.end())
        {
            gameOver = true;
            gameWon = false;
            std::cout << " Lose!\n";
        }
    }

    void Game::DrawCharacter(const Character &obj) const
    {
        // gameDisplay.DrawRect(obj.color,
        //                      obj.position.x,
        //                      obj.position.y,
        //                      obj.size.width,
        //                      obj.size.height);
    }

    // initial draw/draw when window gets resized
    void Game::DrawEnemiesExpose()
    {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        // glOrtho(-300., 300., -300., 300., 1., 20.);
        glOrtho(0., 600., 600., 0., 1., 20.);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        gluLookAt(0., 0., 10., 0., 0., 0., 0., 1., 0.);

        glBegin(GL_QUADS);
        for (auto &e : enemies)
        {
            gameDisplay.DrawRect(255, 0, 255, e.position.x, e.position.y, e.size.width, e.size.height);
        }
        glEnd();
    }

    // normal enemy Update Loop
    void Game::UpdateEnemies()
    {
        bool enemyMoved = false;

        // glMatrixMode(GL_PROJECTION);
        // glLoadIdentity();
        // glOrtho(-300., 300., -300., 300., 1., 20.);
        // glOrtho(0., 600., 600., 0., 1., 20.);

        // glMatrixMode(GL_MODELVIEW);
        // glLoadIdentity();
        // gluLookAt(0., 0., 10., 0., 0., 0., 0., 1., 0.);

        glClearColor(1.0, 1.0, 1.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glBegin(GL_QUADS);
        for (auto &e : enemies)
        {
            // std::cout << e.TimeToMove() << "Move!\n";
            if (e.TimeToMove())
            {
                // std::cout << "Can Move!\n";
                e.Move();
                gameDisplay.DrawRect(255, 0, 255, e.position.x, e.position.y, e.size.width, e.size.height);
                enemyMoved = true;
            }
        }
        glEnd();
        if (enemyMoved)
        {
            glXSwapBuffers(gameDisplay.GetDisplay(), gameDisplay.GetWindow());
            //     glXSwapBuffers(gameDisplay.GetDisplay(), gameDisplay.GetWindow());
        }

        // // for (auto &e : enemies)
        // // {
        // //     if (e.TimeToMove())
        // //     {
        // //         e.Move();
        // //         enemyMoved = true;
        // //     }
        // // }

        // // if (enemyMoved)
        // // {
        // //     gameDisplay.Redraw();
        // // }
    }

    void Game::HandleEvent()
    {
        if (event.type == Expose)
        {
            Draw();
        }

        if (event.type == KeyPress)
        {
            std::cout << "key: " << event.xkey.keycode << "\n";

            switch (event.xkey.keycode)
            {
            case KEY_UP:
                std::cout << "Up!\n";
                if (!gameOver)
                {
                    player.position.y -= 20;
                    gameDisplay.Redraw();
                }
                break;
            case KEY_DOWN:
                std::cout << "Down!\n";
                if (!gameOver)
                {
                    player.position.y += 20;
                    gameDisplay.Redraw();
                }
                break;
            case KEY_LEFT:
                std::cout << "Left!\n";
                if (!gameOver)
                {
                    player.position.x -= 20;
                    gameDisplay.Redraw();
                }
                break;
            case KEY_RIGHT:
                std::cout << "Right!\n";
                if (!gameOver)
                {
                    player.position.x += 20;
                    gameDisplay.Redraw();
                }
                break;
            case KEY_SPACE:
                std::cout << "Space!\n";
                if (gameOver)
                {
                    ResetGame();
                }
                break;
            case KEY_ESCAPE:
                std::cout << "Escape!\n";
                isRunning = false;
                break;
            default:
                break;
            }
            Update();
        }
    }

    void Game::ResetGame()
    {
        player.position = {20, 20};
        CreateFood();
        CreateEnemies();
        gameWon = false;
        gameOver = false;
    }

    bool Game::CheckPlayerBounds()
    {
        Rect windowRect = gameDisplay.GetGeometry();

        if (player.position.x < 0 || player.position.x > windowRect.width || player.position.y < 0 || player.position.y > windowRect.height)
        {
            return false;
        }

        return true;
    }
}