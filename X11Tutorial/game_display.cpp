#include "game_display.hpp"

namespace MyGame
{
    GameDisplay::GameDisplay()
    {
        display = XOpenDisplay(NULL);

        if (display == NULL)
        {
            std::cout << "\n\tcannot connect to X server\n\n";
            exit(0);
        }

        root = DefaultRootWindow(display);
        screen = DefaultScreen(display);

        vi = glXChooseVisual(display, 0, att);

        if (vi == NULL)
        {
            std::cout << "\n\tno appropriate visual found\n\n";
            exit(0);
        }
        else
        {
            std::cout << "\n\tvisual " << (void *)vi->visualid << " selected\n";
        }

        cmap = XCreateColormap(display, root, vi->visual, AllocNone);

        swa.colormap = cmap;
        swa.event_mask = ExposureMask | KeyPressMask;

        // window = XCreateSimpleWindow(
        //     display,
        //     RootWindow(display, screen),
        //     0,
        //     0,
        //     screenWidth,
        //     screenHeight,
        //     borderWidth,
        //     BlackPixel(display, screen),
        //     0x363d4d);

        window = XCreateWindow(
            display,
            root,
            0,
            0,
            screenWidth,
            screenHeight,
            borderWidth,
            vi->depth,
            InputOutput,
            vi->visual,
            CWColormap | CWEventMask,
            &swa);

        XMapWindow(display, window);
        XStoreName(display, window, "Test Application");

        glc = glXCreateContext(display, vi, NULL, GL_TRUE);
        glXMakeCurrent(display, window, glc);

        glEnable(GL_DEPTH_TEST);
        // XSelectInput(display, window, KeyPressMask | ExposureMask);
        // XMapWindow(display, window);
    }

    GameDisplay::~GameDisplay()
    {
        XCloseDisplay(display);
    }

    void GameDisplay::DrawRect(unsigned int colorR, unsigned int colorB, unsigned int colorG, int x, int y, int width, int height) const
    {
        // XSetForeground(display, DefaultGC(display, screen), color);
        // XFillRectangle(display, window, DefaultGC(display, screen), x, y, width, height);

        std::cout << "x: " << x << "y: " << y << "\n";
        // x and y is top left corner

        glColor3f((float)(colorR / 255), (float)(colorB / 255), (float)(colorG / 255));
        glVertex3f(x, y + height, 0.); // bottomLeft
        // glVertex3f(-250., -250., 0.); // bottomLeft
        // glColor3f(1., 0., 1.);
        glVertex3f(x + (width), y + height, 0.); // bottomRight
        // glVertex3f(e.position.x/2, -e.position.y/2, 0.); // bottomRight
        // glColor3f(1., 0., 1.);
        glVertex3f(x + (width), y, 0.); // topRight
        // glVertex3f(e.position.x/2, e.position.y/2, 0.); // topRight
        // glColor3f(1., 0., 1.);
        glVertex3f(x, y, 0.); // topLeft
        // glVertex3f(-e.position.x/2, e.position.y/2, 0.); // topLeft
    }

    void GameDisplay::Redraw()
    {
        XClearWindow(display, window);

        Window rootWindow;
        int x, y;
        unsigned int width, height, borderWidth, depth;

        XGetGeometry(display, window, &rootWindow, &x, &y, &width, &height, &borderWidth, &depth);

        XEvent event;
        event.xexpose.type = Expose;
        event.xexpose.display = display;
        event.xexpose.window = window;
        event.xexpose.x = x;
        event.xexpose.y = y;
        event.xexpose.width = width;
        event.xexpose.height = height;
        event.xexpose.count = 0;

        XSendEvent(display, window, false, ExposureMask, &event);
    }

    Rect GameDisplay::GetGeometry()
    {
        Window rootWindow;
        int x, y;
        unsigned int width, height, borderWidth, depth;

        XGetGeometry(display, window, &rootWindow, &x, &y, &width, &height, &borderWidth, &depth);

        Rect r;
        r.x = x;
        r.y = y;
        r.width = width;
        r.height = height;

        return r;
    }

    void GameDisplay::DrawText(int x, int y, const std::string &str) const
    {
        XDrawString(display, window, DefaultGC(display, screen), x, y, str.c_str(), str.size());
    }
}