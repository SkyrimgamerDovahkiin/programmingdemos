// Needed!!!

#include "time.hpp"

namespace MyGame
{
    Time::Time()
    {
        start = std::chrono::high_resolution_clock::now();
    }

    Time::~Time()
    {
    }

    long Time::ElapsedTime()
    {
        std::chrono::duration<long, std::nano> elap = std::chrono::high_resolution_clock::now() - start;
        return elap.count();
    }
}