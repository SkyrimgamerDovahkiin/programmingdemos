#pragma once

#include <vector>
#include <ctime>
#include <algorithm>

#include "game_display.hpp"

namespace MyGame
{
    class Game
    {
    public:
        Game();
        ~Game();

        void Run();

    private:
        GameDisplay gameDisplay;
        XEvent event;
        XWindowAttributes gwa;
        bool isRunning = true;
        bool gameOver = false;
        bool gameWon = false;
        Player player;
        // Enemy e;

        enum Keys
        {
            KEY_ESCAPE = 9,
            KEY_SPACE = 65,
            KEY_UP = 111,
            KEY_RIGHT = 114,
            KEY_DOWN = 116,
            KEY_LEFT = 113,
        };

        std::vector<Food> food;
        std::vector<Enemy> enemies;

        bool GetEvent();
        void DrawEnemiesExpose();
        void UpdateEnemies();
        void HandleEvent();
        void ResetGame();
        bool CheckPlayerBounds();
        void DrawPlayer();
        void Draw();
        void CreateFood();
        void DrawSingleFood(const Food &f);
        void DrawAllFood();
        void CreateEnemies();
        void DrawAllEnemies();
        void DrawMessage();
        void Update();
        void DrawCharacter(const Character &obj) const;
    };
}