// Needed!!!

#pragma once

#include <chrono>

namespace MyGame
{
    class Time
    {
    public:
        Time();
        ~Time();

        long ElapsedTime();

    private:
        std::chrono::high_resolution_clock::time_point start;
    };
}