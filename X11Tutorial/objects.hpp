// Needed!!!

#pragma once

#include <cstdlib>
#include <chrono>
#include "time.hpp"

struct Point
{
    int x, y;
};

struct Size
{
    int width, height;
};

struct Rect
{
    int x, y;
    int width, height;

    // Rectangle Points
    inline Point tl() const
    {
        return {std::min(x, x + width), std::min(y, y + height)};
    }

    inline Point br() const
    {
        return {std::max(x, x + width), std::max(y, y + height)};
    }

    inline Point tr() const
    {
        return {std::max(x, x + width), std::min(y, y + height)};
    }

    inline Point bl() const
    {
        return {std::min(x, x + width), std::max(y, y + height)};
    }
};

// Collision check functions
inline bool PointInRect(const Point &p, const Rect &r)
{
    return (p.x >= r.tl().x && p.x <= r.br().x && p.y >= r.tl().y && p.y <= r.br().y);
}

inline bool InRange(int i, int min_i, int max_i)
{
    return (i >= min_i && i <= max_i);
}

inline bool RectangleIntersect(const Rect &r1, const Rect &r2)
{
    // Check 1 -- Any corner inside rect
    if ((PointInRect(r1.tl(), r2) || PointInRect(r1.br(), r2)) || (PointInRect(r1.tr(), r2) || PointInRect(r1.bl(), r2)))
        return true;

    // Check 2 -- Overlapped, but all points outside
    //     +---+
    //  +--+---+----+
    //  |  |   |    |
    //  +--+---+----+
    //     +---+
    if ((InRange(r1.tl().x, r2.tl().x, r2.br().x) || InRange(r1.br().x, r2.tl().x, r2.br().x)) && r1.tl().y < r2.tl().y && r1.br().y > r2.br().y || (InRange(r1.tl().y, r2.tl().y, r2.br().y) || InRange(r1.br().y, r2.tl().y, r2.br().y)) && r1.tl().x < r2.tl().x && r1.br().x > r2.br().x)
        return true;

    return false;
}

struct Character
{
    unsigned long color = 0x07f70f;
    Point position{20, 20};
    Size size{20, 20};

    Character(unsigned long newColor, Point newPosition, Size newSize)
        : color(newColor), position(newPosition), size(newSize){};

    Rect Bounds() const
    {
        return {position.x, position.y, size.width, size.height};
    }
};

struct Player : public Character
{
    Player() : Character(0x07f70f, {20, 20}, {20, 20}){};
};

struct Food : public Character
{
    Food() : Character(0xff0000, {100, 100}, {20, 20}){};
};

struct Enemy : public Character
{
    Enemy() : Character(0xff00ff, {100, 100}, {20, 20})
    {
        timeLastMove = time.ElapsedTime();
    };

    void Move()
    {
        int direction = std::rand() % 4;
        const int moveDistance = 20;

        switch (direction)
        {
        case 0:
            position.y -= moveDistance;
            break;
        case 1:
            position.y += moveDistance;
            break;
        case 2:
            position.x -= moveDistance;
            break;
        case 3:
            position.x += moveDistance;
            break;
        default:
            break;
        }

        timeLastMove = time.ElapsedTime();
    }

    bool TimeToMove()
    {
        return ((time.ElapsedTime() - timeLastMove) >= moveTime);
    }

    long timeLastMove;
    long moveTime {250'000'000};
    MyGame::Time time;
};