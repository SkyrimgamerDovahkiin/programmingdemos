#pragma once

// X11
#include <X11/Xlib.h>
#include <GL/glx.h>
#include <GL/glu.h>

// std
#include <stdexcept>
#include <string>
#include <iostream>
#include <stdlib.h>

#include "objects.hpp"

namespace MyGame
{
    class GameDisplay
    {
    public:
        GameDisplay();
        ~GameDisplay();

        Display *GetDisplay() { return display; };
        Window GetWindow() { return window; };
        GLXContext GetGLXContext() { return glc; };
        void DrawRect(unsigned int colorR, unsigned int colorB, unsigned int colorG, int x, int y, int width, int height) const;
        void Redraw();
        Rect GetGeometry();
        void DrawText(int x, int y, const std::string &str) const;

    private:
        int screen;
        Display *display;
        Window root;
        Window window;
        XSetWindowAttributes swa;
        XVisualInfo *vi;
        GLXContext glc;
        GLint att[5] = {GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None};
        Colormap cmap;
        
        // Needed!!!
        int screenWidth = 600;
        int screenHeight = 600;
        int borderWidth = 1;
    };
}