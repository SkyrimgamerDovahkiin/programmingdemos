# Various Programming tests/demos
Repository for small Programming examples i made while learing C++/raylib/OpenGL

## What's inside of the folders

- Calculator: the Raylib Calculator Binaries, don't know if they'll work
- CMakeHelloWorld: Cmake Test with glew, glfw and glm
- CPPCalculatorTest: Raylib Calculator Test
- CppModulesTest: testing of the new modules feature in C++23
- DebianPackageTesting: a debian package compile test
- ECSTesting and ECSTesting2: i tried out to develop an ECS, does not work correctly afaik
- EngineTesting: the beginnings of a 3DEngine, does work but is very limited in function
- GLFWTestMinGW: testing compiling of a program that uses glfw with minGW
- ImGuiDemoWindowCode: a ImGui Demo Window application
- ImGuiTest(3DEngine): another 3DEngine Test/Demo
- oglesl: custom compiler for a a custom shading language, doesn't work
- OpenGLGameTest: a game made with OpenGL, don't know if it is functional
- OpenGLGameWithLeveleditor: the same game but with a level editor
- OpenGLGlfwKeyboard: glfw keyboard test/Demo
- OpenGLTutorial: code i've written while watching a OpenGL tutorial (https://www.youtube.com/watch?v=yRYHly3bl2Q)
- RaylibCalculator: the finished calculator, everything should work
- Schach: a (bad) chess game that i first developed with c for CMD/Terminal and than tried to make a OpenGL application, doesn't work correctly
- X11Tutorial: code from an X11Tutorial (https://www.youtube.com/watch?v=quJfPJqBrtI)
- OpenGLGameLayerExample: an early test with game layers for my OpenGLGame (https://gitlab.com/SkyrimgamerDovahkiin/opengl-game)
- QuadtreeExample: a Quadtree example with raylib, use the mouse to move the rectangle and query points

NOTE: i don't know if the code will compile on windows, because i use Linux/GCC
