#pragma once

#include "event.hpp"

class KeyEvent : public Event
{
public:
    inline int GetKeyCode() const { return mKeyCode; }

    virtual int GetCategoryFlags() const override { return EventCategoryKeyboard | EventCategoryInput; }
protected:
    KeyEvent(int keycode)
        : mKeyCode(keycode) {}

    int mKeyCode;
};

class KeyPressedEvent : public KeyEvent
{
public:
    KeyPressedEvent(int keycode)
        : KeyEvent(keycode) {}

    std::string ToString() const override
    {
        std::stringstream ss;
        ss << "KeyPressedEvent:\n   Key Code: " << mKeyCode << "\n";
        return ss.str();
    }

    static EventType GetStaticType() { return EventType::KeyPressed; }
    virtual EventType GetEventType() const override { return GetStaticType(); }
    virtual const char* GetName() const override { return "KeyPressed"; }
};