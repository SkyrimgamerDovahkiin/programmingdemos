#pragma once

#include <string>
#include <sstream>
#include <functional>

// base event.
// NOTE: currently, events are blocking, meaning that they have to be dealt with immediatly.
// Maybe use an event queue later.

enum class EventType
{
    None = 0, // base
    WindowClose,
    WindowResize,
    WindowMoved,
    GameUpdate,
    GameRender,
    KeyPressed,
    KeyReleased,
    MouseButtonPressed,
    MouseButtonReleased,
    MouseMoved,
    MouseScrolled
};

enum EventCategory
{
    None = 0, // base
    EventCategoryApplication,
    EventCategoryInput,
    EventCategoryKeyboard,
    EventCategoryMouse, // only for mouse movement events
    EventCategoryMouseButton,
};

class Event
{
public:
    bool Handled = false;

    virtual EventType GetEventType() const = 0;
    virtual const char *GetName() const = 0;
    virtual int GetCategoryFlags() const = 0;
    virtual std::string ToString() const { return GetName(); }

    inline bool IsInCategory(EventCategory category)
    {
        return GetCategoryFlags() & category;
    }
};

class EventDispatcher
{
public:
    EventDispatcher(Event &event)
        : m_Event(event)
    {
    }

    // F will be deduced by the compiler
    template <typename T, typename F>
    bool Dispatch(const F &func)
    {
        if (m_Event.GetEventType() == T::GetStaticType())
        {
            m_Event.Handled = func(static_cast<T &>(m_Event));
            return true;
        }
        return false;
    }

private:
    Event &m_Event;
};

inline std::ostream &operator<<(std::ostream &os, const Event &e)
{
    return os << e.ToString();
}