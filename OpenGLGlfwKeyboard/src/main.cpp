#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 640;

#include "Events/event.hpp"
#include "Events/key_event.hpp"

int main()
{
    // different function calls do need events?

    GLFWwindow *window;

    // initialize library
    if (!glfwInit())
    {
        return -1;
    }

    // create window
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Hello World", NULL, NULL);

    KeyPressedEvent event(48);
    // std::cout << (int)event.GetStaticType() << std::endl;
    // std::cout << (int)event.GetEventType() << std::endl;
    // std::cout << event.GetName() << std::endl;
    std::cout << event.ToString() << std::endl;
    // test callback with keys
    // glfwSetWindowUserPointer(window, &test);
    // glfwSetKeyCallback(window, );

    EventDispatcher dispatcher(event);
    dispatcher.Dispatch<KeyPressedEvent>(BIND_EVENT_FN(OnWindowClose));

    HZ_CORE_TRACE("{0}", e);

    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);

    float vertices[] = {
        0.0f,
        300.0f,
        0.0f,
        300.0f,
        300.0f,
        0.0f,
        300.0f,
        0.0f,
        0.0f,
        0.0f,
        0.0f,
        0.0f,
    };

    // draw quad
    glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, SCREEN_WIDTH, 0, SCREEN_HEIGHT, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    while (!glfwWindowShouldClose(window))
    {
        glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // OpenGL render stuff
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_FLOAT, 0, vertices);
        glDrawArrays(GL_QUADS, 0, 4);
        glDisableClientState(GL_VERTEX_ARRAY);

        // do event stuff

        glfwSwapBuffers(window);

        glfwPollEvents();
    }

    glfwTerminate();

    return 0;
}
