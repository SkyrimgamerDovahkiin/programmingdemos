#include "camera.hpp"

namespace OGLE
{
    Camera::Camera(int width, int height)
    {
        projection = glm::ortho(0.0f, (float)width, (float)height, 0.0f, -1.0f, 1.0f);
        view = glm::mat4(1.0f);
        position = glm::vec3(0.0f);
        update();
    }

    Camera::~Camera()
    {
    }

    glm::mat4 Camera::getViewProj()
    {
        return viewProj;
    }

    void Camera::update()
    {
        viewProj = projection * view;
    }

    void Camera::translate(glm::vec3 v)
    {
        position += v;
        view = glm::translate(view, v * -1.0f);
    }
}