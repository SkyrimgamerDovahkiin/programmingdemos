#pragma once

#include <cstdlib>
#include <chrono>

#include "defines.hpp"
#include "time.hpp"
#include "food.hpp"

using OGLE::Food;

namespace OGLE
{
    struct Enemy
    {
    public:
        Enemy() // : Character(0xff00ff, {100, 100}, {20, 20})
        {
            timeLastMove = time.ElapsedTime();
        };

        ~Enemy(){

        };

        // bool CheckCollision(Rectangle &rect, float &upDown, float &leftRight, float moveSpeed)
        void CheckCollision(int direction)
        {
            if (direction == 2)
            {
                leftRight += moveSpeed;
                rect.x += moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(leftRight, 0.0f, 0.0f));
                leftRight = 0.0f;
                // return false;
            }
            else if (direction == 3)
            {
                leftRight -= moveSpeed;
                rect.x -= moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(leftRight, 0.0f, 0.0f));
                leftRight = 0.0f;
                // return false;
            }
            else if (direction == 0)
            {
                upDown += moveSpeed;
                rect.y += moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, upDown, 0.0f));
                upDown = 0.0f;
                // return false;
            }
            else if (direction == 1)
            {
                upDown -= moveSpeed;
                rect.y -= moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, upDown, 0.0f));
                upDown = 0.0f;
                // return false;
            }
            else
            {
                // return true;
                std::cout << "EROOR!!!\n";
            }
        }

        // bool CheckScreenBounds(Rectangle &rect, float &upDown, float &leftRight, float moveSpeed)
        bool CheckScreenBounds(float screenWidth, float screenHeight)
        {
            if (rect.x < 0.0f)
            {
                leftRight += moveSpeed;
                rect.x += moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(leftRight, 0.0f, 0.0f));
                leftRight = 0.0f;
                return false;
            }
            else if (rect.x > (screenWidth - rect.width))
            {
                leftRight -= moveSpeed;
                rect.x -= moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(leftRight, 0.0f, 0.0f));
                leftRight = 0.0f;
                return false;
            }
            else if (rect.y < 0.0f)
            {
                upDown += moveSpeed;
                rect.y += moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, upDown, 0.0f));
                upDown = 0.0f;
                return false;
            }
            else if (rect.y > (screenHeight - rect.height))
            {
                upDown -= moveSpeed;
                rect.y -= moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, upDown, 0.0f));
                upDown = 0.0f;
                return false;
            }
            else
            {
                return true;
            }
        }

        void Move()
        {
            //int direction = std::rand() % 4;
            direction = std::rand() % 4;

            switch (direction)
            {
            case 0:
                upDown -= moveSpeed;
                rect.y -= moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, upDown, 0.0f));
                upDown = 0.0f;
                break;
            case 1:
                upDown += moveSpeed;
                rect.y += moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, upDown, 0.0f));
                upDown = 0.0f;
                break;
            case 2:
                leftRight -= moveSpeed;
                rect.x -= moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(leftRight, 0.0f, 0.0f));
                leftRight = 0.0f;
                break;
            case 3:
                leftRight += moveSpeed;
                rect.x += moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(leftRight, 0.0f, 0.0f));
                leftRight = 0.0f;
                break;
            default:
                break;
            }

            timeLastMove = time.ElapsedTime();
        };

        void Create(int screenWidth, int screenHeight)
        {
            float posX = (std::rand() % screenWidth) / 20 * 20;
            float posY = (std::rand() % screenHeight) / 20 * 20;

            rect = {posX, posY, 20.0f, 20.0f, glm::vec3(1.00f, 0.26f, 0.00f)};
        }

        void Draw(int drawIdx, int modelViewProjMatrixLocaction)
        {
            glUniformMatrix4fv(modelViewProjMatrixLocaction, 1, GL_FALSE, &modelViewProj[0][0]);
            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void *)(drawIdx * sizeof(GLuint)));
        }

        void Update(int drawIdx, int modelViewProjMatrixLocaction, std::vector<Food> colliders, std::vector<Enemy> enemies)
        {
            // Move
            if (TimeToMove())
            {
                Move();
            }

            // check if colliding with food
            for (size_t i = 0; i < colliders.size(); i++)
            {
                if (RectangleIntersect(rect, colliders.at(i).rect))
                {
                    CheckCollision(direction);
                }

                if (RectangleIntersect(rect, enemies.at(i).rect) && enemies.at(i).rect.x != rect.x && enemies.at(i).rect.y != rect.y)
                {
                    std::cout << " collided with enemy, dir = oldDir!\n";
                    CheckCollision(direction);
                }
            }

            // check if colliding with screen
            // CheckScreenBounds(rect, upDown, leftRight, moveSpeed);
            CheckScreenBounds(600.0f, 600.0f);

            // Draw
            Draw(drawIdx, modelViewProjMatrixLocaction);
        }

        bool TimeToMove()
        {
            return ((time.ElapsedTime() - timeLastMove) >= moveTime);
        }

        long timeLastMove;
        long moveTime{500'000'000};
        const float moveSpeed = 20.0f;
        float leftRight = 0.0f;
        float upDown = 0.0f;
        int direction = std::rand() % 4;

        OGLE::Time time;
        Rectangle rect;
        glm::mat4 model = glm::mat4(1.0f);
        glm::mat4 modelViewProj = glm::mat4(1.0f);
    };
}