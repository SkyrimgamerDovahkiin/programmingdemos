// std
#include <iostream>
#include <cmath>
#include <vector>
#include <list>
#include <fstream>
#include <ctime>
#include <algorithm>

// GLEW/SDL
// #define GLEW_STATIC
// #include <GL/glew.h>
// #define SDL_MAIN_HANDLED
// #include <SDL2/SDL.h>

// glm
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// stb
#define STB_IMAGE_IMPLEMENTATION
#include "libs/stb_image.h"

// custom
#include "defines.hpp"
#include "vertex_buffer.hpp"
#include "index_buffer.hpp"
#include "shader.hpp"
#include "camera.hpp"
// #include "create_rect.hpp"
#include "time.hpp"
#include "player.hpp"
#include "enemy.hpp"
#include "food.hpp"

// TODO: Make buffer large enough so that different text rects (vertices for text quads) can be subbuffered later.

using OGLE::VertexBuffer, OGLE::Shader, OGLE::IndexBuffer, OGLE::Camera, OGLE::Player, OGLE::Enemy, OGLE::Food;

void GLAPIENTRY OpenGLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
{
    std::cout << "[OpenGL Error] " << message << std::endl;
}

#ifdef _DEBUG

void _GLGetError(const char *file, int line, const char *call)
{
    while (GLenum error = glGetError())
    {
        std::cout << "[OpenGL Error] " << glewGetErrorString(error) << " in " << file << ":" << line << " Call: " << call << std::endl;
    }
}

#define GLCALL(call) \
    call;            \
    _GLGetError(__FILE__, __LINE__, #call)

#else

#define GLCALL(call) call;

#endif

FT_Library ft;
FT_Face face;
std::map<GLchar, CharacterGlyph> Characters;
glm::mat4 modelChar = glm::mat4(1.0f);
glm::mat4 modelViewProjChar = glm::mat4(1.0f);
glm::mat4 projectionMat = glm::ortho(0.0f, 600.0f, 0.0f, 600.0f);

void GameUpdate(std::vector<int> foodIdx)
{
    if (foodIdx.empty())
    {
        std::cout << " Win!\n";
    }

    // auto iterEnemies = std::find_if(enemies.begin(), enemies.end(), [&](const Enemy &e)
    //                                 { return RectangleIntersect(player.Bounds(), e.Bounds()); });

    // if (iterEnemies != enemies.end())
    // {
    //     gameOver = true;
    //     gameWon = false;
    //     std::cout << " Lose!\n";
    // }
}

void CreateTextTexture()
{
    if (FT_Init_FreeType(&ft))
    {
        std::cout << "ERROR::FREETYPE: Could not init FreeType Library" << std::endl;
    }

    if (FT_New_Face(ft, "fonts/arial/arial.ttf", 0, &face))
    {
        std::cout << "ERROR::FREETYPE: Failed to load font" << std::endl;
    }

    FT_Set_Pixel_Sizes(face, 0, 26);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // disable byte-alignment restriction

    for (unsigned char c = 0; c < 128; c++)
    {
        if (FT_Load_Char(face, c, FT_LOAD_RENDER))
        {
            std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
            continue;
        }

        // generate texture
        GLuint texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);

        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RED,
            face->glyph->bitmap.width,
            face->glyph->bitmap.rows,
            0,
            GL_RED,
            GL_UNSIGNED_BYTE,
            face->glyph->bitmap.buffer);

        // set texture options
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        CharacterGlyph character = {
            texture,
            glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
            glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
            static_cast<unsigned int>(face->glyph->advance.x)};
        Characters.insert(std::pair<GLchar, CharacterGlyph>(c, character));
    }
    glBindTexture(GL_TEXTURE_2D, 0);

    FT_Done_Face(face);
    FT_Done_FreeType(ft);
}

void RenderText(Shader &s, std::string text, float x, float y, float scale, glm::vec3 color, int modelViewProjMatrixLocaction, int drawIdx)
{
    // activate corresponding render state
    glUniform3f(glGetUniformLocation(s.GetShaderId(), "textColor"), color.x, color.y, color.z);
    glUniform1i(glGetUniformLocation(s.GetShaderId(), "text"), 0);

    glActiveTexture(GL_TEXTURE0);

    glm::mat4 projection = glm::ortho(0.0f, 600.0f, 600.0f, 0.0f, -1.0f, 1.0f);

    std::vector<Vertex> vertices;
    std::vector<uint32_t> indices;

    uint32_t numVertices;
    uint32_t numIndices;
    uint32_t index;

    std::string::const_iterator c;
    for (c = text.begin(); c != text.end(); c++)
    {
        // TODO: draw with a drawIdx and add 6 to it every tie for a new character
        CharacterGlyph ch = Characters[*c];

        float xpos = x + ch.Bearing.x * scale;
        float ypos = y + (ch.Size.y - ch.Bearing.y) - ch.Size.y + y * scale;

        float w = ch.Size.x * scale;
        float h = ch.Size.y * scale;

        Rectangle rect = {xpos, ypos, w, h, color};
        CreateRect(rect, vertices, indices, numVertices, numIndices, index);

        // std::cout << numVertices << " numVerts!\n";

        // render glyph texture over quad
        glBindTexture(GL_TEXTURE_2D, ch.TextureID);
        glBufferSubData(GL_ARRAY_BUFFER, 432, numVertices * sizeof(Vertex), vertices.data());
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 1728, numIndices * sizeof(indices[0]), indices.data());

        // render quad
        // glUniformMatrix4fv(modelViewProjMatrixLocaction, 1, GL_FALSE, &modelViewProjChar[0][0]);
        glUniformMatrix4fv(glGetUniformLocation(s.GetShaderId(), "projection"), 1, GL_FALSE, glm::value_ptr(projection));
        // glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void *)(18 * sizeof(GLuint)));
        // glDrawElements(GL_TRIANGLES, numVertices, GL_UNSIGNED_INT, indices.data());
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void *)(0 * sizeof(GLuint)));
        // glDrawElementsBaseVertex(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0, drawIdx);
        drawIdx += 6;

        // vertices.clear();
        // indices.clear();

        x += (ch.Advance >> 6) * scale;
    }
    glBindTexture(GL_TEXTURE_2D, 0);
}

int main(int argc, char **argv)
{
    std::vector<Vertex> vertices;
    uint32_t numVertices = 0;

    std::vector<uint32_t> indices;
    uint32_t numIndices = 0;
    uint32_t index = 0;

    std::vector<Enemy> enemies;
    std::vector<Food> foods;
    std::vector<int> foodIdx;

    uint32_t maxVertices = 1000;

    int screenWidth = 600, screenHeight = 600, maxEnemies = 10, maxFood = 10;

    std::srand(std::time(nullptr));

    Camera camera(screenWidth, screenHeight);
    camera.translate(glm::vec3(0.0f, 0.0f, 0.0f));
    camera.update();

    // create player
    Player player;
    player.Create(20.0f, 20.0f);
    player.modelViewProj = camera.getViewProj() * player.model;
    CreateRect(player.rect, vertices, indices, numVertices, numIndices, index);

    // create food
    for (int i = 0; i < 1; i++)
    {
        Food food;
        food.idx = i;
        food.Create(screenWidth, screenHeight);
        food.modelViewProj = camera.getViewProj() * food.model;
        foods.push_back(food);
        foodIdx.push_back(food.idx);
        CreateRect(food.rect, vertices, indices, numVertices, numIndices, index);
    }

    // create enemies
    for (int i = 0; i < 1; i++)
    {
        Enemy enemy;
        enemy.Create(screenWidth, screenHeight);
        enemy.modelViewProj = camera.getViewProj() * enemy.model;
        enemies.push_back(enemy);
        CreateRect(enemy.rect, vertices, indices, numVertices, numIndices, index);
    }

    // sdl init
    SDL_Window *window;
    SDL_Init(SDL_INIT_EVERYTHING);

    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetSwapInterval(1);

// Debug
#ifdef _DEBUG
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
#endif

    uint32_t flags = SDL_WINDOW_OPENGL;

    window = SDL_CreateWindow("C++ OpenGL Tutorial", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, flags);
    SDL_GLContext glContext = SDL_GL_CreateContext(window);

    GLenum err = glewInit();
    if (err != GLEW_OK)
    {
        std::cout << "Error: " << glewGetErrorString(err) << std::endl;
        std::cin.get();
        return -1;
    }

    std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;

#ifdef _DEBUG
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(OpenGLDebugCallback, 0);
#endif

    modelViewProjChar = camera.getViewProj() * modelChar;
    // texture can only be created/generated AFTER the SDLContext was created
    CreateTextTexture();

    // Create rects for freetype rendering (just for testing purposes)
    // std::string text = "CO2-neutral seit 2007";
    // std::string::const_iterator c;

    // float x = 25.0f;
    // float y = 25.0f;

    // float scale = 1.0f;

    // for (c = text.begin(); c != text.end(); c++)
    // {
    //     CharacterGlyph ch = Characters[*c];

    //     float xpos = x + ch.Bearing.x * scale;
    //     float ypos = y + (ch.Size.y - ch.Bearing.y) - ch.Size.y + y * scale;

    //     float w = ch.Size.x * scale;
    //     float h = ch.Size.y * scale;

    //     Rectangle rect = {xpos, ypos, w, h, glm::vec3(0.3f, 0.3f, 0.3f)};
    //     CreateRect(rect, vertices, indices, numVertices, numIndices, index);

    //     x += (ch.Advance >> 6) * scale;
    // }

    // create and fill Vertex buffer
    // VertexBuffer vertexBuffer(vertices.data(), numVertices);
    VertexBuffer vertexBuffer(vertices.data(), maxVertices);
    glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices * sizeof(Vertex), vertices.data());

    GLint vertexBufferSize = 0;
    glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &vertexBufferSize);

    // std::cout << "Vertex buffer size: " << vertexBufferSize << std::endl;

    // create and fill Index buffer (using Vertex buffer size to set correct index buffer size)
    // IndexBuffer indexBuffer(indices.data(), numIndices, sizeof(indices[0]));
    IndexBuffer indexBuffer(indices.data(), vertexBufferSize, sizeof(indices[0]));
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, numIndices * sizeof(indices[0]), indices.data());

    // GLint indexBufferSize = 0;
    // glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &indexBufferSize);
    // std::cout << "Index buffer size: " << indexBufferSize << std::endl;

    // create shader
    Shader shader("shaders/basic_vert.glsl", "shaders/basic_frag.glsl");

    // create text shader
    Shader textShader("shaders/text_vert.glsl", "shaders/text_frag.glsl");

    // FPS Counter
    uint64_t perfCounterFrequency = SDL_GetPerformanceFrequency();
    uint64_t lastCounter = SDL_GetPerformanceCounter();

    // normal shader model view projection matrix location
    int modelViewProjMatrixLocaction = GLCALL(glGetUniformLocation(shader.GetShaderId(), "u_modelViewProj"));

    // text shader projection matrix
    int textProjMatrixLocaction = GLCALL(glGetUniformLocation(textShader.GetShaderId(), "projection"));

    bool close = false;

    // blending for text
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    while (!close)
    {
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        camera.update();

        GameUpdate(foodIdx);

        vertexBuffer.Bind();
        indexBuffer.Bind();

        // food updates
        /*foods.at(0).Update(6, modelViewProjMatrixLocaction, player.rect, foodIdx);
        foods.at(1).Update(12, modelViewProjMatrixLocaction, player.rect, foodIdx);
        foods.at(2).Update(18, modelViewProjMatrixLocaction, player.rect, foodIdx);
        foods.at(3).Update(24, modelViewProjMatrixLocaction, player.rect, foodIdx);
        foods.at(4).Update(30, modelViewProjMatrixLocaction, player.rect, foodIdx);
        foods.at(5).Update(36, modelViewProjMatrixLocaction, player.rect, foodIdx);
        foods.at(6).Update(42, modelViewProjMatrixLocaction, player.rect, foodIdx);
        foods.at(7).Update(48, modelViewProjMatrixLocaction, player.rect, foodIdx);
        foods.at(8).Update(54, modelViewProjMatrixLocaction, player.rect, foodIdx);
        foods.at(9).Update(60, modelViewProjMatrixLocaction, player.rect, foodIdx);

        // enemy updates
        enemies.at(0).Update(66, modelViewProjMatrixLocaction, foods, enemies);
        enemies.at(1).Update(72, modelViewProjMatrixLocaction, foods, enemies);
        enemies.at(2).Update(78, modelViewProjMatrixLocaction, foods, enemies);
        enemies.at(3).Update(84, modelViewProjMatrixLocaction, foods, enemies);
        enemies.at(4).Update(90, modelViewProjMatrixLocaction, foods, enemies);
        enemies.at(5).Update(96, modelViewProjMatrixLocaction, foods, enemies);
        enemies.at(6).Update(102, modelViewProjMatrixLocaction, foods, enemies);
        enemies.at(7).Update(108, modelViewProjMatrixLocaction, foods, enemies);
        enemies.at(8).Update(114, modelViewProjMatrixLocaction, foods, enemies);
        enemies.at(9).Update(120, modelViewProjMatrixLocaction, foods, enemies);*/

        shader.Bind();
        player.Update(enemies);
        player.Draw(0, modelViewProjMatrixLocaction);
        shader.Unbind();

        textShader.Bind();
        // RenderText(textShader, text, glm::vec3(0.5f, 0.8f, 0.2f), textProjMatrixLocaction, 18);
        RenderText(textShader, "CO2-neutral seit 2007", 25.0f, 25.0f, 1.0f, glm::vec3(0.5f, 0.8f, 0.2f), textProjMatrixLocaction, 1728);
        textShader.Unbind();

        indexBuffer.Unbind();
        vertexBuffer.Unbind();

        SDL_GL_SwapWindow(window);

        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
            {
                close = true;
            }
            if (event.type == SDL_KEYDOWN)
            {
                player.Move(event);
            }
        }

        // // FPS Counter, maybe print to text later
        // uint64_t endCounter = SDL_GetPerformanceCounter();
        // uint64_t counterElapsed = endCounter - lastCounter;
        // delta = ((float)counterElapsed) / (float)perfCounterFrequency;
        // uint32_t FPS = (uint32_t)((float)perfCounterFrequency / (float)counterElapsed);
        // // std::cout << FPS << std::endl;
        // lastCounter = endCounter;
    }

    return 0;
}
