#version 330 core
// in vec2 TexCoords;

layout(location = 0) out vec4 f_color;

in vec4 v_color;
in vec2 v_texCoord;
uniform sampler2D text;

uniform vec3 textColor;

void main()
{    
    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, v_texCoord).r);
    f_color = sampled * vec4(textColor, 1.0); // vec4(textColor, 1.0) * sampled;
}