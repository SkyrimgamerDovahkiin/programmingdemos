#pragma once

// // std
#include <vector>
#include <map>

// GLEW/SDL
#define GLEW_STATIC
#include <GL/glew.h>
#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>

// Freetype
#include <ft2build.h>
#include FT_FREETYPE_H

// custom
#include "defines.hpp"
#include "camera.hpp"
#include "enemy.hpp"
#include "shader.hpp"
#include "create_rect.hpp"

using OGLE::Camera, OGLE::Enemy, OGLE::Shader;

namespace OGLE
{
    struct Player
    {
    public:
        Player() // : Character(0xff00ff, {100, 100}, {20, 20})
            {};

        ~Player(){

        };

        bool CheckScreenBounds(float screenWidth, float screenHeight)
        {
            if (rect.x < 0.0f)
            {
                leftRight += moveSpeed;
                rect.x += moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(leftRight, 0.0f, 0.0f));
                leftRight = 0.0f;
                return false;
            }
            else if (rect.x > (screenWidth - rect.width))
            {
                leftRight -= moveSpeed;
                rect.x -= moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(leftRight, 0.0f, 0.0f));
                leftRight = 0.0f;
                return false;
            }
            else if (rect.y < 0.0f)
            {
                upDown += moveSpeed;
                rect.y += moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, upDown, 0.0f));
                upDown = 0.0f;
                return false;
            }
            else if (rect.y > (screenHeight - rect.height))
            {
                upDown -= moveSpeed;
                rect.y -= moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, upDown, 0.0f));
                upDown = 0.0f;
                return false;
            }
            else
            {
                return true;
            }
        }

        bool CheckCollision(Rectangle collider)
        {
            return RectangleIntersect(collider, rect);
        };

        void Move(SDL_Event event)
        {
            if (died)
            {
                return;
            }

            switch (event.key.keysym.sym)
            {
            case SDLK_w:
                upDown -= moveSpeed;
                rect.y -= moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, upDown, 0.0f));
                upDown = 0.0f;
                break;
            case SDLK_a:
                leftRight -= moveSpeed;
                rect.x -= moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(leftRight, 0.0f, 0.0f));
                leftRight = 0.0f;
                break;
            case SDLK_s:
                upDown += moveSpeed;
                rect.y += moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, upDown, 0.0f));
                upDown = 0.0f;
                break;
            case SDLK_d:
                leftRight += moveSpeed;
                rect.x += moveSpeed;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(leftRight, 0.0f, 0.0f));
                leftRight = 0.0f;
                break;
            default:
                break;
            }
        };

        void Create(float width, float height)
        {
            rect = {20.0f, 20.0f, width, height, glm::vec3(0.00f, 1.00f, 0.00f)};
        }

        void Draw(int drawIdx, int modelViewProjMatrixLocaction)
        {
            glUniformMatrix4fv(modelViewProjMatrixLocaction, 1, GL_FALSE, &modelViewProj[0][0]);
            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void *)(drawIdx * sizeof(GLuint)));
        }

        void Update(std::vector<Enemy> colliders)
        {
            if (died)
            {
                return;
            }

            CheckScreenBounds(600.0f, 600.0f);
            // playerModelViewProj = camera.getViewProj() * model;
            // playerModelViewProj = glm::translate(playerModelViewProj, glm::vec3(playerLeftRight, playerUpDown, 0.0f));

            for (size_t i = 0; i < colliders.size(); i++)
            {
                if (RectangleIntersect(rect, colliders.at(i).rect))
                {
                    // std::cout << " player has died!\n";
                    // died = true;
                }
            }

            // Draw(drawIdx, modelViewProjMatrixLocaction);
        }

        const float moveSpeed = 20.0f;
        float leftRight = 0.0f;
        float upDown = 0.0f;
        bool died = false;

        Rectangle rect;
        glm::mat4 model = glm::mat4(1.0f);
        glm::mat4 modelViewProj = glm::mat4(1.0f);

        glm::mat4 modelChar = glm::mat4(1.0f);
        glm::mat4 modelViewProjChar = glm::mat4(1.0f);
        // glm::mat4 projectionMat = glm::ortho(0.0f, 600.0f, 600.0f, 0.0f);
        glm::mat4 projectionMat = glm::ortho(0.0f, 600.0f, 0.0f, 600.0f);

        // std::vector<Vertex> verticesChar;
        // std::vector<uint32_t> indicesChar;
        // uint32_t numVertices = 0;
        // uint32_t numIndices = 0;
        // uint32_t index = 0;

        // FT_Library ft;
        // FT_Face face;
        // std::map<GLchar, CharacterGlyph> Characters;
    };
}