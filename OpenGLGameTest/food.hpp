#pragma once

#include <cstdlib>
#include <chrono>

#include "defines.hpp"
#include "time.hpp"

namespace OGLE
{
    struct Food
    {
    public:
        Food() // : Character(0xff00ff, {100, 100}, {20, 20})
            {};

        ~Food(){

        };

        void Create(int screenWidth, int screenHeight)
        {
            canUpdate = true;

            float posX = (std::rand() % screenWidth) / 20 * 20;
            float posY = (std::rand() % screenHeight) / 20 * 20;

            // TODO: Check if position is on same position as Entity
            rect = {posX, posY, 20.0f, 20.0f, glm::vec3(0.00f, 0.98f, 0.98f)};
        }

        void Draw(int drawIdx, int modelViewProjMatrixLocaction)
        {
            glUniformMatrix4fv(modelViewProjMatrixLocaction, 1, GL_FALSE, &modelViewProj[0][0]);
            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void *)(drawIdx * sizeof(GLuint)));
        }

        void Update(int drawIdx, int modelViewProjMatrixLocaction, Rectangle collider, std::vector<int> &foodIdx)
        {
            if (!canUpdate) { return; }

            if (RectangleIntersect(collider, rect))
            {
                std::cout << "food got erased!\n";
                canUpdate = false;

                auto obj = std::find(foodIdx.begin(), foodIdx.end(), idx);
                if (obj != foodIdx.end())
                {
                    foodIdx.erase(obj);
                }
            }

            Draw(drawIdx, modelViewProjMatrixLocaction);
        }

        Rectangle rect;
        glm::mat4 model = glm::mat4(1.0f);
        glm::mat4 modelViewProj = glm::mat4(1.0f);
        bool canUpdate;
        int idx;
    };
}