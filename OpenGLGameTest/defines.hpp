#pragma once

#include <iostream>
#include <vector>

#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

struct Vector3
{
    glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
    glm::vec3 down = glm::vec3(0.0f, -1.0f, 0.0f);
    glm::vec3 forward = glm::vec3(0.0f, 0.0f, 1.0f);
    glm::vec3 back = glm::vec3(0.0f, 0.0f, -1.0f);
    glm::vec3 right = glm::vec3(1.0f, 0.0f, 0.0f);
    glm::vec3 left = glm::vec3(-1.0f, 0.0f, 0.0f);
    glm::vec3 zero = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 one = glm::vec3(1.0f, 1.0f, 1.0f);
};

struct Vertex
{
    float x;
    float y;
    float z;

    float u;
    float v;

    float r;
    float g;
    float b;
    float a;
};

struct Point
{
    float x, y;
};

struct Size
{
    float width, height;
};

// Defines a Rectangle. Values are x and y pos and width and height.
// x and y positions are the origin (top-left corner)
struct Rectangle
{
    float x;      // Rectangle position x
    float y;      // Rectangle position y
    float width;  // Rectangle width
    float height; // Rectangle height
    glm::vec3 color; // Rectangle Color

    // Rectangle Points
    inline Point tl() const
    {
        return {std::min(x, x + width), std::min(y, y + height)};
    }

    inline Point br() const
    {
        return {std::max(x, x + width), std::max(y, y + height)};
    }

    inline Point tr() const
    {
        return {std::max(x, x + width), std::min(y, y + height)};
    }

    inline Point bl() const
    {
        return {std::min(x, x + width), std::max(y, y + height)};
    }
};

// Collision check functions
inline bool PointInRect(const Point &p, const Rectangle &r)
{
    return (p.x >= r.tl().x && p.x <= r.br().x && p.y >= r.tl().y && p.y <= r.br().y);
}

inline bool InRange(int i, int minI, int maxI)
{
    return (i >= minI && i <= maxI);
}

inline bool RectangleIntersect(const Rectangle &r1, const Rectangle &r2)
{
    // Check 1 -- Any corner inside rect
    if ((PointInRect(r1.tl(), r2) || PointInRect(r1.br(), r2)) || (PointInRect(r1.tr(), r2) || PointInRect(r1.bl(), r2)))
        return true;

    // Check 2 -- Overlapped, but all points outside
    //     +---+
    //  +--+---+----+
    //  |  |   |    |
    //  +--+---+----+
    //     +---+
    if ((InRange(r1.tl().x, r2.tl().x, r2.br().x) || InRange(r1.br().x, r2.tl().x, r2.br().x)) && r1.tl().y < r2.tl().y && r1.br().y > r2.br().y || (InRange(r1.tl().y, r2.tl().y, r2.br().y) || InRange(r1.br().y, r2.tl().y, r2.br().y)) && r1.tl().x < r2.tl().x && r1.br().x > r2.br().x)
        return true;

    return false;
}

struct CharacterGlyph {
    unsigned int TextureID;  // ID handle of the glyph texture
    glm::ivec2   Size;       // Size of glyph
    glm::ivec2   Bearing;    // Offset from baseline to left/top of glyph
    unsigned int Advance;    // Offset to advance to next glyph
};

// // Defines a Enemy.
// struct Enemy
// {
//     Rectangle rect;
//     glm::vec3 color;
// };