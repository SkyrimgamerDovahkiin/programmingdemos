#include "time.hpp"

namespace OGLE
{
    Time::Time()
    {
        start = std::chrono::high_resolution_clock::now();
    }

    Time::~Time()
    {
    }

    long Time::ElapsedTime()
    {
        std::chrono::duration<long, std::nano> elap = std::chrono::high_resolution_clock::now() - start;
        return elap.count();
    }
}