// std
#include <iostream>
#include <cmath>
#include <vector>
#include <list>
#include <fstream>
#include <ctime>
#include <algorithm>

// GLEW/SDL
// #define GLEW_STATIC
// #include <GL/glew.h>
// #define SDL_MAIN_HANDLED
// #include <SDL2/SDL.h>

// glm
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// stb
#define STB_IMAGE_IMPLEMENTATION
#include "libs/stb_image.h"

// custom
#include "defines.hpp"
#include "vertex_buffer.hpp"
#include "index_buffer.hpp"
#include "shader.hpp"
#include "camera.hpp"
// #include "create_rect.hpp"
#include "time.hpp"
#include "player.hpp"
#include "enemy.hpp"
#include "food.hpp"

using OGLE::VertexBuffer, OGLE::Shader, OGLE::IndexBuffer, OGLE::Camera, OGLE::Player, OGLE::Enemy, OGLE::Food;

void GLAPIENTRY OpenGLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
{
    std::cout << "[OpenGL Error] " << message << std::endl;
}

#ifdef _DEBUG

void _GLGetError(const char *file, int line, const char *call)
{
    while (GLenum error = glGetError())
    {
        std::cout << "[OpenGL Error] " << glewGetErrorString(error) << " in " << file << ":" << line << " Call: " << call << std::endl;
    }
}

#define GLCALL(call) \
    call;            \
    _GLGetError(__FILE__, __LINE__, #call)

#else

#define GLCALL(call) call;

#endif

/* void GameUpdate(std::vector<int> foodIdx)
{
    if (foodIdx.empty())
    {
        std::cout << " Win!\n";
    }

    // auto iterEnemies = std::find_if(enemies.begin(), enemies.end(), [&](const Enemy &e)
    //                                 { return RectangleIntersect(player.Bounds(), e.Bounds()); });

    // if (iterEnemies != enemies.end())
    // {
    //     gameOver = true;
    //     gameWon = false;
    //     std::cout << " Lose!\n";
    // }
} */

void RenderText(Shader &shader, std::string text, float x, float y, float scale, glm::vec3 color, std::map<GLchar, CharacterGlyph> &Characters);

int main(int argc, char **argv)
{
    std::vector<Vertex> vertices;
    uint32_t numVertices = 0;

    std::vector<uint32_t> indices;
    uint32_t numIndices = 0;
    uint32_t index = 0;

    std::map<GLchar, CharacterGlyph> Characters;

    // std::vector<Enemy> enemies;
    // std::vector<Food> foods;
    // std::vector<int> foodIdx;

    // int screenWidth = 600, screenHeight = 600, maxEnemies = 10, maxFood = 10;
    int screenWidth = 600, screenHeight = 600;

    glm::mat4 projectionMat = glm::ortho(0.0f, static_cast<float>(screenWidth), 0.0f, static_cast<float>(screenHeight));

    std::srand(std::time(nullptr));

    // Camera camera(screenWidth, screenHeight);
    // camera.translate(glm::vec3(0.0f, 0.0f, 0.0f));
    // camera.update();

    // create player

    // TODO: move player create AFTER context creation!!!!

    Player player;
    player.Create(20.0f, 20.0f);
    // player.modelViewProj = camera.getViewProj() * player.model;
    // player.modelViewProjChar = camera.getViewProj() * player.modelChar;
    CreateRect(player.rect, vertices, indices, numVertices, numIndices, index);

    // create food
    /* for (int i = 0; i < maxFood; i++)
    {
        Food food;
        food.idx = i;
        food.Create(screenWidth, screenHeight);
        food.modelViewProj = camera.getViewProj() * food.model;
        foods.push_back(food);
        foodIdx.push_back(food.idx);
        CreateRect(food.rect, vertices, indices, numVertices, numIndices, index);
    }

    // create enemies
    for (int i = 0; i < maxEnemies; i++)
    {
        Enemy enemy;
        enemy.Create(screenWidth, screenHeight);
        enemy.modelViewProj = camera.getViewProj() * enemy.model;
        enemies.push_back(enemy);
        CreateRect(enemy.rect, vertices, indices, numVertices, numIndices, index);
    } */

    // sdl init
    SDL_Window *window;
    SDL_Init(SDL_INIT_EVERYTHING);

    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetSwapInterval(1);

// Debug
#ifdef _DEBUG
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
#endif

    uint32_t flags = SDL_WINDOW_OPENGL;

    window = SDL_CreateWindow("C++ OpenGL Tutorial", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, flags);
    SDL_GLContext glContext = SDL_GL_CreateContext(window);

    GLenum err = glewInit();
    if (err != GLEW_OK)
    {
        std::cout << "Error: " << glewGetErrorString(err) << std::endl;
        std::cin.get();
        return -1;
    }

    std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;

#ifdef _DEBUG
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(OpenGLDebugCallback, 0);
#endif

    // create Index and Vertex buffer
    IndexBuffer indexBuffer(indices.data(), numIndices, sizeof(indices[0]));
    VertexBuffer vertexBuffer(vertices.data(), numVertices);
    // vertexBuffer.Unbind();

    // create shader
    // Shader shader("shaders/basic_vert.glsl", "shaders/basic_frag.glsl");

    // create text shader
    Shader textShader("shaders/text_vert.glsl", "shaders/text_frag.glsl");
    textShader.Bind();

    // FPS Counter
    uint64_t perfCounterFrequency = SDL_GetPerformanceFrequency();
    uint64_t lastCounter = SDL_GetPerformanceCounter();

    // normal shader model view projection matrix location
    // int modelViewProjMatrixLocaction = GLCALL(glGetUniformLocation(shader.GetShaderId(), "u_modelViewProj"));

    // text shader projection matrix location
    int textProjMatrixLocaction = GLCALL(glGetUniformLocation(textShader.GetShaderId(), "projection"));

    // time
    // Uint64 timeNow = SDL_GetPerformanceCounter();
    // Uint64 timeLast = 0.0f;
    // double deltaTime = 0.0f;

    bool close = false;

    // player.CreateTextTexture();

    // blending for text
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glUniformMatrix4fv(textProjMatrixLocaction, 1, GL_FALSE, glm::value_ptr(projectionMat));

    // FreeType
    // --------
    FT_Library ft;
    // All functions return a value different than 0 whenever an error occurred
    if (FT_Init_FreeType(&ft))
    {
        std::cout << "ERROR::FREETYPE: Could not init FreeType Library" << std::endl;
        return -1;
    }

    // load font as face
    FT_Face face;
    if (FT_New_Face(ft, "fonts/antonio/Antonio-Bold.ttf", 0, &face))
    {
        std::cout << "ERROR::FREETYPE: Failed to load font" << std::endl;
        return -1;
    }
    else
    {
        // set size to load glyphs as
        FT_Set_Pixel_Sizes(face, 0, 48);

        // disable byte-alignment restriction
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

        // load first 128 characters of ASCII set
        for (unsigned char c = 0; c < 128; c++)
        {
            // Load character glyph
            if (FT_Load_Char(face, c, FT_LOAD_RENDER))
            {
                std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
                continue;
            }
            // generate texture
            unsigned int texture;
            glGenTextures(1, &texture);
            glBindTexture(GL_TEXTURE_2D, texture);
            glTexImage2D(
                GL_TEXTURE_2D,
                0,
                GL_RED,
                face->glyph->bitmap.width,
                face->glyph->bitmap.rows,
                0,
                GL_RED,
                GL_UNSIGNED_BYTE,
                face->glyph->bitmap.buffer);
            // set texture options
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            // now store character for later use
            CharacterGlyph character = {
                texture,
                glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
                glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
                static_cast<unsigned int>(face->glyph->advance.x)};
            Characters.insert(std::pair<char, CharacterGlyph>(c, character));
        }
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    // destroy FreeType once we're finished
    FT_Done_Face(face);
    FT_Done_FreeType(ft);

    while (!close)
    {
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        // timeLast = timeNow;
        // timeNow = SDL_GetPerformanceCounter();
        // deltaTime = (double)((timeNow - timeLast) / (double)SDL_GetPerformanceFrequency());

        // camera.update();

        // GameUpdate(foodIdx);

        vertexBuffer.Bind();
        indexBuffer.Bind();

        // glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), vertices.data(), GL_DYNAMIC_DRAW);

        /*foods.at(0).Update(6, modelViewProjMatrixLocaction, player.rect, foodIdx);
        foods.at(1).Update(12, modelViewProjMatrixLocaction, player.rect, foodIdx);
        foods.at(2).Update(18, modelViewProjMatrixLocaction, player.rect, foodIdx);
        foods.at(3).Update(24, modelViewProjMatrixLocaction, player.rect, foodIdx);
        foods.at(4).Update(30, modelViewProjMatrixLocaction, player.rect, foodIdx);
        foods.at(5).Update(36, modelViewProjMatrixLocaction, player.rect, foodIdx);
        foods.at(6).Update(42, modelViewProjMatrixLocaction, player.rect, foodIdx);
        foods.at(7).Update(48, modelViewProjMatrixLocaction, player.rect, foodIdx);
        foods.at(8).Update(54, modelViewProjMatrixLocaction, player.rect, foodIdx);
        foods.at(9).Update(60, modelViewProjMatrixLocaction, player.rect, foodIdx);

        // enemy updates
        enemies.at(0).Update(66, modelViewProjMatrixLocaction, foods, enemies);
        enemies.at(1).Update(72, modelViewProjMatrixLocaction, foods, enemies);
        enemies.at(2).Update(78, modelViewProjMatrixLocaction, foods, enemies);
        enemies.at(3).Update(84, modelViewProjMatrixLocaction, foods, enemies);
        enemies.at(4).Update(90, modelViewProjMatrixLocaction, foods, enemies);
        enemies.at(5).Update(96, modelViewProjMatrixLocaction, foods, enemies);
        enemies.at(6).Update(102, modelViewProjMatrixLocaction, foods, enemies);
        enemies.at(7).Update(108, modelViewProjMatrixLocaction, foods, enemies);
        enemies.at(8).Update(114, modelViewProjMatrixLocaction, foods, enemies);
        enemies.at(9).Update(120, modelViewProjMatrixLocaction, foods, enemies);*/

        // shader.Bind();
        // player.Update(enemies);
        // player.Draw(0, modelViewProjMatrixLocaction);
        // shader.Unbind();

        glBufferData(GL_ELEMENT_ARRAY_BUFFER, 0 * sizeof(indices[0]), nullptr, GL_DYNAMIC_DRAW);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), nullptr, GL_DYNAMIC_DRAW);

        textShader.Bind();
        RenderText(textShader, "text", 540.0f, 570.0f, 0.5f, glm::vec3(0.3, 0.7f, 0.9f), Characters);
        // player.RenderText(textShader, "text", 25.0f, 25.0f, 1.0f, glm::vec3(0.3f, 0.3f, 0.3f), camera, textProjMatrixLocaction);
        textShader.Unbind();

        indexBuffer.Unbind();
        vertexBuffer.Unbind();

        SDL_GL_SwapWindow(window);

        /* SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
            {
                close = true;
            }
            if (event.type == SDL_KEYDOWN)
            {
                player.Move(event);
            }
        } */

        // // FPS Counter, maybe print to text later
        // uint64_t endCounter = SDL_GetPerformanceCounter();
        // uint64_t counterElapsed = endCounter - lastCounter;
        // delta = ((float)counterElapsed) / (float)perfCounterFrequency;
        // uint32_t FPS = (uint32_t)((float)perfCounterFrequency / (float)counterElapsed);
        // // std::cout << FPS << std::endl;
        // lastCounter = endCounter;
    }

    return 0;
}

// render line of text
// -------------------
void RenderText(Shader &shader, std::string text, float x, float y, float scale, glm::vec3 color, std::map<GLchar, CharacterGlyph> &Characters)
{
    // activate corresponding render state
    shader.Bind();
    glUniform3f(glGetUniformLocation(shader.GetShaderId(), "textColor"), color.x, color.y, color.z);
    glActiveTexture(GL_TEXTURE0);

    // iterate through all characters
    std::string::const_iterator c;
    for (c = text.begin(); c != text.end(); c++)
    {
        CharacterGlyph ch = Characters[*c];

        float xpos = x + ch.Bearing.x * scale;
        float ypos = y - (ch.Size.y - ch.Bearing.y) * scale;

        float w = ch.Size.x * scale;
        float h = ch.Size.y * scale;
        // update VBO for each character
        Vertex vertices[6][7] = {
            //     {xpos, ypos + h, 0.0f, 0.0f},
            //     {xpos, ypos, 0.0f, 1.0f},
            //     {xpos + w, ypos, 1.0f, 1.0f},

            //     {xpos, ypos + h, 0.0f, 0.0f},
            //     {xpos + w, ypos, 1.0f, 1.0f},
            //     {xpos + w, ypos + h, 1.0f, 0.0f}};

            Vertex{xpos, ypos + h, 0.0f, color.x, color.y, color.z, 1.0f}, // bottomLeft
            Vertex{xpos, ypos, 0.0f, color.x, color.y, color.z, 1.0f},     // topLeft
            Vertex{xpos + w, ypos, 0.0f, color.x, color.y, color.z, 1.0f}, // topRight

            Vertex{xpos, ypos + h, 0.0f, color.x, color.y, color.z, 1.0f},      // bottomLeft
            Vertex{xpos + w, ypos, 0.0f, color.x, color.y, color.z, 1.0f},      // topRight
            Vertex{xpos + w, ypos + h, 0.0f, color.x, color.y, color.z, 1.0f}}; // bottomRight

        // render glyph texture over quad
        glBindTexture(GL_TEXTURE_2D, ch.TextureID);
        // update content of VBO memory
        // glBindBuffer(GL_ARRAY_BUFFER, VBO);
        // glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices) * sizeof(float), vertices); // be sure to use glBufferSubData and not glBufferData
        // glBufferSubData(GL_ARRAY_BUFFER, 0, 6 * sizeof(Vertex), vertices); // be sure to use glBufferSubData and not glBufferData


        glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(Vertex), vertices, GL_DYNAMIC_DRAW);
        // glBufferSubData(GL_ARRAY_BUFFER, 0, 3 * sizeof(Vertex), vertices);
        // glBindBuffer(GL_ARRAY_BUFFER, 0);

        // render quad
        glDrawArrays(GL_TRIANGLES, 0, 6);
        // now advance cursors for next glyph (note that advance is number of 1/64 pixels)
        x += (ch.Advance >> 6) * scale; // bitshift by 6 to get value in pixels (2^6 = 64 (divide amount of 1/64th pixels by 64 to get amount of pixels))
    }
    glBindTexture(GL_TEXTURE_2D, 0);
}