#include "index_buffer.hpp"

namespace OGLE
{
    IndexBuffer::IndexBuffer(void *data, uint32_t numIndices, uint8_t elementSize)
    {
        glGenBuffers(1, &bufferId);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferId);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * elementSize, NULL, GL_DYNAMIC_DRAW);
        // glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * elementSize, data, GL_DYNAMIC_DRAW);
    }

    IndexBuffer::~IndexBuffer()
    {
        glDeleteBuffers(1, &bufferId);
    }

    void IndexBuffer::Bind()
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferId);
    }

    void IndexBuffer::Unbind()
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
}