#include <iostream>
#include <random>
#include <vector>

#include "raylib.h"

struct QuadTree
{
    // NOTE: maybe need to change width and height for subdivision
    // because with raylib, width and height are from the origin and
    // not from the center (at least i think it works like that)

    Rectangle boundary;
    int capacity = 4;
    std::vector<Vector2> points;
    bool divided = false;

    QuadTree *topLeft;
    QuadTree *topRight;
    QuadTree *bottomLeft;
    QuadTree *bottomRight;

    std::vector<Vector2> Query(const Rectangle& range)
    {
        std::vector<Vector2> found;

        if (!CheckCollisionRecs(boundary, range))
        {
            return found;
        }
        else
        {
            for (const Vector2 &p : points)
            {
                // CheckCollisionPointRec(p, range);
                // if (range.contains(p))
                if (CheckCollisionPointRec(p, range))
                {
                    found.push_back(p);
                }
            }

            if (divided)
            {
                std::vector<Vector2> temp = topRight->Query(range);
                found.insert(std::end(found), std::begin(temp), std::end(temp));

                temp = topLeft->Query(range);
                found.insert(std::end(found), std::begin(temp), std::end(temp));

                temp = bottomRight->Query(range);
                found.insert(std::end(found), std::begin(temp), std::end(temp));

                temp = bottomLeft->Query(range);
                found.insert(std::end(found), std::begin(temp), std::end(temp));
            }

            return found;
        }
    }

    void Show()
    {
        DrawRectangleLines(boundary.x, boundary.y, boundary.width, boundary.height, ORANGE);
        // DrawRectangle(boundary.x, boundary.y, boundary.width, boundary.height, YELLOW);

        if (divided)
        {
            topLeft->Show();
            topRight->Show();
            bottomLeft->Show();
            bottomRight->Show();
        }

        for (size_t i = 0; i < points.size(); ++i)
        {
            DrawCircle(points.at(i).x, points.at(i).y, 3, DARKBROWN);
        }
    }

    void Subdivide()
    {

        Rectangle tl = {boundary.x, boundary.y, boundary.width / 2, boundary.height / 2};
        topLeft = new QuadTree();
        topLeft->boundary = tl;
        topLeft->capacity = 4;

        Rectangle tr = {boundary.x + boundary.width / 2, boundary.y, boundary.width / 2, boundary.height / 2};
        topRight = new QuadTree();
        topRight->boundary = tr;
        topRight->capacity = 4;

        Rectangle bl = {boundary.x, boundary.y + boundary.height / 2, boundary.width / 2, boundary.height / 2};
        bottomLeft = new QuadTree();
        bottomLeft->boundary = bl;
        bottomLeft->capacity = 4;

        Rectangle br = {boundary.x + boundary.width / 2, boundary.y + boundary.height / 2, boundary.width / 2, boundary.height / 2};
        bottomRight = new QuadTree();
        bottomRight->boundary = br;
        bottomRight->capacity = 4;

        divided = true;
    }

    bool Insert(Vector2 point)
    {
        if (!CheckCollisionPointRec(point, boundary))
        {
            return false;
        }

        if (points.size() < capacity)
        {
            points.push_back(point);
            return true;
        }
        else
        {
            if (!divided)
            {
                Subdivide();
            }

            if (topLeft->Insert(point))
            {
                return true;
            }
            else if (topRight->Insert(point))
            {
                return true;
            }
            else if (bottomLeft->Insert(point))
            {
                return true;
            }
            else if (bottomRight->Insert(point))
            {
                return true;
            }
        }
    }
};

Rectangle boundary = {200, 200, 400, 400};
QuadTree tree = {.boundary = boundary};
// tree.boundary = boundary;

void Draw()
{
    if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT))
    {
        // Vector2 point = {GetMousePosition().x, GetMousePosition().y};
        // tree.Insert(point);
    }

    tree.Show();
}

void Update(Rectangle& range, std::vector<Vector2> &points)
{
    range = {GetMousePosition().x - 50, GetMousePosition().y - 50, 100, 100};
    points = tree.Query(range);

    std::cout << "points: " << points.size() << std::endl;
}

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    const int screenWidth = 1000;
    const int screenHeight = 1000;

    InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");

    // Create a random device to seed the random number generator
    std::random_device rd;

    // Use the random device to seed a Mersenne Twister random number generator
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distr_width(200, 600);
    std::uniform_int_distribution<> distr_height(200, 600);

    for (size_t i = 0; i < 300; i++)
    {
        int randomX = distr_width(gen);
        int randomY = distr_height(gen);

        Vector2 point = {randomX, randomY};
        tree.Insert(point);
    }

    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------

    while (!WindowShouldClose())
    {
        // Update
        //----------------------------------------------------------------------------------
        // TODO: Update your variables here

        Rectangle range;
        std::vector<Vector2> points;
        Update(range, points);
        //----------------------------------------------------------------------------------

        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

        ClearBackground(RAYWHITE);

        DrawRectangleLines(range.x, range.y, range.width, range.height, DARKBLUE);
        // DrawRectangle(range.x, range.y, range.width, range.height, YELLOW);
        Draw();
        for (const Vector2 &p : points)
        {
            DrawCircle(p.x, p.y, 3, GREEN);
        }
        // tree.Show();
        // DrawText("Congrats! You created your first window!", 300, 450, 20, LIGHTGRAY);

        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();
    //--------------------------------------------------------------------------------------

    return 0;
}
