#include "raylib.h"
#include <iostream>
#include <cstring>
#include <array>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <deque>
#include "../Program/calculator.hpp"

using namespace Calculator;
using std::cout, std::string, std::deque;

// TODO: Bugfixing/overhauling
// Better Comments
// Seperate collision detection and drawing of the buttons
// Add buttons to array and draw them

// NOTE: Sometimes buggy with long numbers and commas (rounding errors)

// array with all available expressions
const std::array<char, 20> expressionsArray{
    // numbers
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '0',
    // operators
    '*',
    '/',
    '-',
    '+',
    '(',
    ')',
    // other
    '=', // equals, program should just print result
    'D', // delete, program should delete last char
    ',', // comma, program should read everything after and before as one number
    '.', // dot, program should read everything after and before as one number
};

// Button class
class Button
{
private:
    bool btnAction;
    bool mouseOverBtn;
    int expressionInt;
    calculator calculator_class;

public:
    void DetectCollision(std::vector<Rectangle> buttons, int &letterCount, std::string &expression, int &posX,
                         bool &showFirstResult, bool &showSecondResult, bool &showThirdResult, bool &showFourthResult,
                         std::string &expressionFirstResult, std::string &expressionSecondResult, std::string &expressionThirdResult, std::string &expressionFourthResult)
    {
        Vector2 mousePoint = GetMousePosition();
        mouseOverBtn = false;
        btnAction = false;

        for (int i = 0; i < 18; i++)
        {
            // calculate new rect because the rects get moved while drawing
            Rectangle rect = buttons.at(i);
            Rectangle newRect = rect;
            newRect.x = rect.x - rect.width / 2 + 10.0f;
            newRect.y = rect.y - rect.height / 2 + 295.0f;

            // Check button state
            if (CheckCollisionPointRec(mousePoint, newRect))
            {
                mouseOverBtn = true;
                expressionInt = i;

                if (IsMouseButtonReleased(MOUSE_BUTTON_LEFT))
                {
                    btnAction = true;
                }
                break;
            }
            else
                mouseOverBtn = false;
        }

        if (btnAction)
        {
            if (expressionsArray.at(expressionInt) == 'D' && letterCount < 15)
            {
                letterCount--;
                if (letterCount <= 0)
                    letterCount = 0;

                if (expression.size() > 0)
                {
                    expression.pop_back();
                }
            }
            else if (expressionsArray.at(expressionInt) == 'D' && letterCount >= 15)
            {
                letterCount--;
                if (letterCount < 0)
                    letterCount = 0;

                // calculate last char length so that the last char is removed but the expression doesn't move
                string str{expression[letterCount]};
                const char *expressionPtr = str.c_str();
                posX = posX + 4 + MeasureText(expressionPtr, 40);

                if (expression.size() > 0)
                {
                    expression.pop_back();
                }
            }

            if (expressionsArray.at(expressionInt) == '=')
            {
                // disassembles the expression and calculates
                string expressionForResultStoring = expression;
                std::cout << " Calculate!\n";

                deque<double> numbers;
                deque<char> operators;
                calculator_class.DissasembleExpression(expression, numbers, operators);
                calculator_class.Calculate(numbers, operators);

                // needed: the expression, the "equals" symbol and the result
                double result = 0.0;
                if (numbers.size() > 0)
                {
                    result = numbers.at(0);
                }
                operators.clear();
                numbers.clear();

                // using ostringstream because the result is more precise
                std::ostringstream streamObj;
                streamObj << result;
                std::string strObj = streamObj.str();

                // copying the contents of the string to char array
                string showInResult = " ";
                if (showFirstResult)
                {
                    showInResult = expressionForResultStoring + "=" + strObj + "\0";
                    expressionFirstResult = showInResult;
                    showSecondResult = true;
                    showFirstResult = false;
                }
                else if (showSecondResult)
                {
                    showInResult = expressionForResultStoring + "=" + strObj + "\0";
                    expressionSecondResult = showInResult;
                    showThirdResult = true;
                    showSecondResult = false;
                }
                else if (showThirdResult)
                {
                    showInResult = expressionForResultStoring + "=" + strObj + "\0";
                    expressionThirdResult = showInResult;
                    showFourthResult = true;
                    showThirdResult = false;
                }
                else if (showFourthResult)
                {
                    showInResult = expressionForResultStoring + "=" + strObj + "\0";
                    expressionFourthResult = showInResult;
                    showFirstResult = false;
                    showSecondResult = false;
                    showThirdResult = false;
                    showFourthResult = false;
                }

                // resets letter count and clears array
                letterCount = 0;

                // copying the contents of the string to char array
                letterCount = strObj.size();
                expression = strObj;
            }
            else if (expressionsArray.at(expressionInt) != 'D' && expressionsArray.at(expressionInt) != '=' && letterCount < 15)
            {
                posX = 0;
                expression.push_back(expressionsArray.at(expressionInt));
                letterCount++;
            }
            else if (expressionsArray.at(expressionInt) != 'D' && expressionsArray.at(expressionInt) != '=' && letterCount >= 15)
            {
                expression.push_back(expressionsArray.at(expressionInt));

                // calculate last char length so that the last char is visible
                string str{expression.at(letterCount)};
                const char *expressionPtr = str.c_str();
                posX = posX - 4 - MeasureText(expressionPtr, 40);
                letterCount++;
            }
            btnAction = false;
        }
    }

    // Draws the Button
    void DrawButton(std::vector<Rectangle> buttons, Texture2D button)
    {
        // Draw Buttons with text
        for (int i = 0; i < 18; i++)
        {
            Rectangle rect{
                buttons.at(i).x - (buttons.at(i).width / 2) + 10.0f,
                buttons.at(i).y - (buttons.at(i).height / 2) + 295.0f,
                buttons.at(i).width,
                buttons.at(i).height};

            Rectangle sourceRec = {0.0f, 0.0f, rect.width, rect.height};

            // Draw control
            // convert the char to a const char *
            string str{expressionsArray.at(i)};
            const char *expressionPtr = str.c_str();

            // text offset so the text is in the middle of the rectangle
            auto textOffsetX = MeasureTextEx(GetFontDefault(), expressionPtr, 20, 0).x / 2;
            auto textOffsetY = MeasureTextEx(GetFontDefault(), expressionPtr, 20, 0).y / 2;

            if (mouseOverBtn)
            {
                Rectangle newRect = rect;
                newRect.x = buttons.at(expressionInt).x - buttons.at(expressionInt).width / 2 + 10.0f;
                newRect.y = buttons.at(expressionInt).y - buttons.at(expressionInt).height / 2 + 295.0f;

                string strMouseOver{expressionsArray.at(expressionInt)};
                const char *expressionPtrMouseOver = strMouseOver.c_str();

                auto textOffsetXMouseOver = MeasureTextEx(GetFontDefault(), expressionPtrMouseOver, 20, 0).x / 2;
                auto textOffsetYMouseOver = MeasureTextEx(GetFontDefault(), expressionPtrMouseOver, 20, 0).y / 2;

                DrawTextureRec(button, sourceRec, {rect.x, rect.y}, WHITE);
                DrawText(expressionPtr, rect.x + (rect.width / 2 - textOffsetX), rect.y + (rect.height / 2 - textOffsetY), 20, WHITE);
                DrawTextureRec(button, sourceRec, {newRect.x, newRect.y}, GRAY);
                DrawText(strMouseOver.c_str(), newRect.x + (buttons.at(expressionInt).width / 2 - textOffsetXMouseOver), newRect.y + (buttons.at(expressionInt).height / 2 - textOffsetYMouseOver), 20, WHITE);
            }
            else
            {
                DrawTextureRec(button, sourceRec, {rect.x, rect.y}, WHITE);
                DrawText(expressionPtr, rect.x + (rect.width / 2 - textOffsetX), rect.y + (rect.height / 2 - textOffsetY), 20, WHITE);
            }
        }
    }
};

int main()
{
    const int screenWidth = 430;
    const int screenHeight = 460;
    calculator calculator_class;

    // maybe get changed later. the number is not the actual button number, but one less
    // because the first button gets drawn before the others
    const int numButtons = 17;

    int counter = 0;

    // the offset/spacing for between the buttons
    const float offset = 10.0f;

    // the initial button pos
    const Vector2 initButtonPos = {10.0f, 295.0f};

    int MaxRecsX = 6;
    int MaxRecsY = 3;

    InitWindow(screenWidth, screenHeight, "Calculator");

    std::string expression;
    std::string expressionFirstResult;
    std::string expressionSecondResult;
    std::string expressionThirdResult;
    std::string expressionFourthResult;

    int letterCount = 0;

    Rectangle textBox = {0, 230, screenWidth, 50};
    Rectangle firstResultBox = {0, 175, screenWidth, 50};
    Rectangle secondResultBox = {0, 120, screenWidth, 50};
    Rectangle thirdResultBox = {0, 65, screenWidth, 50};
    Rectangle fourthResultBox = {0, 10, screenWidth, 50};
    bool mouseOnText = false;
    bool mouseOnFirstBox = false;
    bool mouseOnSecondBox = false;
    bool mouseOnThirdBox = false;
    bool mouseOnFourthBox = false;
    bool showFirstResult = true;
    bool showSecondResult = false;
    bool showThirdResult = false;
    bool showFourthResult = false;

    int framesCounter = 0;

    auto workDir = GetApplicationDirectory();
    string spriteLocation = workDir + (string) "/Sprites/ButtonBackground.png";
    Texture2D buttonTex = LoadTexture(spriteLocation.c_str()); // Load button texture

    bool btnAction = false; // Button action should be activated

    Vector2 mousePoint = {0.0f, 0.0f};

    // the xPosition for the scrolling text
    int posX = 0;

    std::vector<Rectangle> buttons;

    for (size_t i = 0; i <= numButtons; i++)
    {
        Rectangle rect;
        buttons.push_back(rect);
    }

    for (int y = 0; y < MaxRecsY; y++)
    {
        for (int x = 0; x < MaxRecsX; x++)
        {
            buttons.at(y * MaxRecsX + x).x = ((float)buttonTex.width / 2.0f) + (float)buttonTex.width * x + (offset * x);
            buttons.at(y * MaxRecsX + x).y = ((float)buttonTex.height / 2.0f) + (float)buttonTex.height * y + (offset * y);
            buttons.at(y * MaxRecsX + x).width = (float)buttonTex.width;
            buttons.at(y * MaxRecsX + x).height = (float)buttonTex.height;
        }
    }

    SetTargetFPS(60);

    Button button;

    // Main game loop
    while (!WindowShouldClose()) // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        mousePoint = GetMousePosition();
        btnAction = false;
        counter = 0;

        button.DetectCollision(buttons, letterCount, expression, posX, showFirstResult, showSecondResult, showThirdResult, showFourthResult, expressionFirstResult, expressionSecondResult, expressionThirdResult, expressionFourthResult);
        //----------------------------------------------------------------------------------

        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

        ClearBackground(RAYWHITE);

        // Draw Buttons
        button.DrawButton(buttons, buttonTex);
        counter++;

        if (CheckCollisionPointRec(GetMousePosition(), textBox))
            mouseOnText = true;
        else
            mouseOnText = false;

        if (CheckCollisionPointRec(GetMousePosition(), firstResultBox))
            mouseOnFirstBox = true;
        else
            mouseOnFirstBox = false;

        if (CheckCollisionPointRec(GetMousePosition(), secondResultBox))
            mouseOnSecondBox = true;
        else
            mouseOnSecondBox = false;

        if (CheckCollisionPointRec(GetMousePosition(), thirdResultBox))
            mouseOnThirdBox = true;
        else
            mouseOnThirdBox = false;

        if (CheckCollisionPointRec(GetMousePosition(), fourthResultBox))
            mouseOnFourthBox = true;
        else
            mouseOnFourthBox = false;

        if (mouseOnText)
        {
            // Draw input line
            // Set the window's cursor to the I-Beam
            SetMouseCursor(MOUSE_CURSOR_IBEAM);
        }
        else
        {
            SetMouseCursor(MOUSE_CURSOR_DEFAULT);
        }
        // Get char pressed (unicode character) on the queue
        int key = GetCharPressed();

        // Check if more characters have been pressed on the same frame
        while (key > 0)
        {
            // Only allow keys that are in the expressionsArray
            for (size_t i = 0; i < expressionsArray.size(); i++)
            {
                if (key == expressionsArray.at(i) && (letterCount < 15))
                {
                    posX = 0;
                    expression.push_back((char)key);
                    letterCount++;
                }
                else if (key == expressionsArray.at(i) && (letterCount >= 15))
                {
                    expression.push_back((char)key);

                    // calculate last char length so that the last char is visible
                    string str{expression.at(letterCount)};
                    const char *expressionPtr = str.c_str();
                    posX = posX - 4 - MeasureText(expressionPtr, 40);
                    letterCount++;
                }

                if (key == ',' || key == '.')
                {
                    key = '.';
                }
                else if (key == 'D' || key == '=')
                {
                    key = '\0';
                }
            }

            key = GetCharPressed(); // Check next character in the queue
        }

        if (IsKeyPressed(KEY_ENTER))
        {
            // disassembles the expression and calculates
            string expressionForResultStoring = expression;

            deque<double> numbers;
            deque<char> operators;
            calculator_class.DissasembleExpression(expression, numbers, operators);
            calculator_class.Calculate(numbers, operators);

            double result = 0.0;
            if (numbers.size() > 0)
            {
                result = numbers.at(0);
            }
            operators.clear();
            numbers.clear();

            // using ostringstream because the result is more precise
            std::ostringstream streamObj;
            streamObj << result;
            std::string strObj = streamObj.str();

            // copying the contents of the string to char array
            string showInResult = " ";
            if (showFirstResult)
            {
                showInResult = expressionForResultStoring + "=" + strObj + "\0";
                expressionFirstResult = showInResult;
                showSecondResult = true;
                showFirstResult = false;
            }
            else if (showSecondResult)
            {
                showInResult = expressionForResultStoring + "=" + strObj + "\0";
                expressionSecondResult = showInResult;
                showThirdResult = true;
                showSecondResult = false;
            }
            else if (showThirdResult)
            {
                showInResult = expressionForResultStoring + "=" + strObj + "\0";
                expressionThirdResult = showInResult;
                showFourthResult = true;
                showThirdResult = false;
            }
            else if (showFourthResult)
            {
                showInResult = expressionForResultStoring + "=" + strObj + "\0";
                expressionFourthResult = showInResult;
                showFirstResult = false;
                showSecondResult = false;
                showThirdResult = false;
                showFourthResult = false;
            }

            // resets letter count and clears array
            letterCount = 0;

            // copying the contents of the string to char array
            letterCount = strObj.size();
            expression = strObj;
        }

        if (IsKeyPressed(KEY_BACKSPACE) && letterCount < 15)
        {
            letterCount--;
            if (letterCount <= 0)
                letterCount = 0;

            if (expression.size() > 0)
            {
                expression.pop_back();
            }
        }
        else if (IsKeyPressed(KEY_BACKSPACE) && letterCount >= 15)
        {
            letterCount--;
            if (letterCount < 0)
                letterCount = 0;

            // calculate last char length so that the last char is removed but the expression doesn't move
            string str{expression.at(letterCount)};
            const char *expressionPtr = str.c_str();
            posX = posX + 4 + MeasureText(expressionPtr, 40);

            if (expression.size() > 0)
            {
                expression.pop_back();
            }
        }

        framesCounter++;

        // NOTE: need to find a better solution for resetting the counter
        if (framesCounter > 100000)
            framesCounter = 0;

        // draws the text box and the "name" text
        // when letter count >= 15, text get drawn at posX because otherwise the
        // last char wouldn't be visible
        DrawRectangleRec(textBox, BLACK);

        // Draws the Rectangles for the result Boxes
        DrawRectangleRec(firstResultBox, RAYWHITE);
        DrawRectangleRec(secondResultBox, RAYWHITE);
        DrawRectangleRec(thirdResultBox, RAYWHITE);
        DrawRectangleRec(fourthResultBox, RAYWHITE);

        if (letterCount < 15)
        {
            DrawText(expression.c_str(), (int)textBox.x + 5, (int)textBox.y + 8, 40, RAYWHITE);
            if (((framesCounter / 20) % 2) == 0)
                DrawText("_", (int)textBox.x + 8 + MeasureText(expression.c_str(), 40), (int)textBox.y + 12, 40, RAYWHITE);
        }

        else if (letterCount >= 15)
        {
            DrawText(expression.c_str(), posX, (int)textBox.y + 8, 40, RAYWHITE);
            if (((framesCounter / 20) % 2) == 0)
                DrawText("_", posX + 8 + MeasureText(expression.c_str(), 40), (int)textBox.y + 12, 40, RAYWHITE);
        }

        // if mouse over a result box, "overlay" it with a gray rectangle
        if (mouseOnFirstBox && strlen(expressionFirstResult.c_str()) != 0)
        {
            DrawRectangleRec(firstResultBox, GRAY);
        }
        else if (mouseOnSecondBox && strlen(expressionSecondResult.c_str()) != 0)
        {
            DrawRectangleRec(secondResultBox, GRAY);
        }
        else if (mouseOnThirdBox && strlen(expressionThirdResult.c_str()) != 0)
        {
            DrawRectangleRec(thirdResultBox, GRAY);
        }
        else if (mouseOnFourthBox && strlen(expressionFourthResult.c_str()) != 0)
        {
            DrawRectangleRec(fourthResultBox, GRAY);
        }

        // Draws the result boxes text. if the mouse is over one and the mouse button is pressed, it copies the text before the "=" into the array
        DrawText(expressionFirstResult.c_str(), (int)firstResultBox.x + 5, (int)firstResultBox.y + 8, 40, BLACK);
        DrawText(expressionSecondResult.c_str(), (int)secondResultBox.x + 5, (int)secondResultBox.y + 8, 40, BLACK);
        DrawText(expressionThirdResult.c_str(), (int)thirdResultBox.x + 5, (int)thirdResultBox.y + 8, 40, BLACK);
        DrawText(expressionFourthResult.c_str(), (int)fourthResultBox.x + 5, (int)fourthResultBox.y + 8, 40, BLACK);

        if (mouseOnFirstBox && IsMouseButtonPressed(MOUSE_BUTTON_LEFT) && expressionFirstResult.size() > 0)
        {
            letterCount = 0;

            expression.clear();

            for (size_t i = 0; i < expressionFirstResult.size(); i++)
            {
                expression.push_back(expressionFirstResult.at(i));
                if (expressionFirstResult.at(i + 1) == '=')
                {
                    letterCount = (int)i;
                    break;
                }
            }
        }
        else if (mouseOnSecondBox && IsMouseButtonPressed(MOUSE_BUTTON_LEFT) && expressionSecondResult.size() > 0)
        {
            letterCount = 0;

            expression.clear();

            for (size_t i = 0; i < expressionSecondResult.size(); i++)
            {
                expression.push_back(expressionSecondResult.at(i));
                if (expressionSecondResult.at(i + 1) == '=')
                {
                    letterCount = (int)i;
                    break;
                }
            }
        }
        else if (mouseOnThirdBox && IsMouseButtonPressed(MOUSE_BUTTON_LEFT) && expressionThirdResult.size() > 0)
        {
            letterCount = 0;

            expression.clear();

            for (size_t i = 0; i < expressionThirdResult.size(); i++)
            {
                expression.push_back(expressionThirdResult.at(i));
                if (expressionThirdResult.at(i + 1) == '=')
                {
                    letterCount = (int)i;
                    break;
                }
            }
        }
        else if (mouseOnFourthBox && IsMouseButtonPressed(MOUSE_BUTTON_LEFT) && expressionFourthResult.size() > 0)
        {
            letterCount = 0;

            expression.clear();

            for (size_t i = 0; i < expressionFourthResult.size(); i++)
            {
                expression.push_back(expressionFourthResult.at(i));
                if (expressionFourthResult.at(i + 1) == '=')
                {
                    letterCount = (int)i;
                    break;
                }
            }
        }
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    UnloadTexture(buttonTex); // Unload button texture
    // buttons.clear();

    CloseWindow(); // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}