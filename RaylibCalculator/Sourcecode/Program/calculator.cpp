#include "calculator.hpp" // header in local directory
#include <iostream>     // header in standard library
#include <vector>
#include <deque>
#include <algorithm>
#include <math.h>

using namespace Calculator;
using namespace std;

double calculator::Solve(double num1, char op, double num2)
{
    switch (op)
    {
    case '+':
        return (num1 + num2);
    case '-':
        return (num1 - num2);
    case '*':
        return (num1 * num2);
    case '/':
        return (num1 / num2);
    default:
        return (0.0);
    }
}

void calculator::DissasembleExpression(string &expression, deque<double> &numbers, deque<char> &operators)
{
    // variables
    vector<double> digits;
    unsigned int i, k, h;
    double temp = 0;

    // if the expression at i is between 0 and 9, the chars after get tested to see if there's a comma.
    // after that it gets added to the digits vector
    // if the expression at i is a valid operator, it gets added to the operators deque
    // if the expression at i is anything invalid, an error gets shown
    // after that, every digit in the digit vector is added to temp and then temp is added to the numbers deque
    // the power of 10 is needed so that the numbers get added correctly
    // the digits vector is needed because we can't add immediately to numbers deque because we would get "out of range"

    for (i = 0; i <= expression.size(); i++)
    {
        if (expression[i] >= '0' && expression[i] <= '9')
        {
            double result = 0.0;
            string temp = "";

            h = i;
            // for (int h = 0; h < expression.size(); h++)
            while (!expression.empty())
            {
                if (expression[h] == '+' || expression[h] == '-' || expression[h] == '*' || expression[h] == '/' || expression[h] == '(' || expression[h] == ')' || expression[h] == '\0')
                {
                    result = stod(temp);
                    digits.push_back(result);
                    i = h - 1;
                    break;
                }
                temp += expression[h];
                h++;
            }
        }

        else if (expression[i] == '+' || expression[i] <= '-' || expression[i] == '*' || expression[i] <= '/')
        {
            operators.push_back(expression[i]);
            for (k = 0; k < digits.size(); k++)
                temp += digits.at(k) * pow(10, (digits.size() - 1 - k));
            numbers.push_back(temp);
            digits.clear();
            temp = 0;
        }
        else if (expression[i] == '(' || expression[i] <= ')')
        {
            operators.push_back(expression[i]);
        }
        else
        {
            cout << "Error! one or more operators or numbers are not correct! \n";
            operators.clear();
            numbers.clear();
            return;
        }
    }
    operators.erase(operators.end());
    return;
}

void calculator::Calculate(deque<double> &numbers, deque<char> &operators)
{
    double temp;
    unsigned int i, j, k, left_parenthesis_location, right_parenthesis_location;
    // calculator calculator_class;

    for (i = 0; i < operators.size(); i++)
    {
        // if (operators.at(i) == '+' || operators.at(i) == '-' || operators.at(i) == '*' || operators.at(i) == '/' || operators.at(i) == '(' || operators.at(i) == ')')
        //{
        temp = 0.0;

        if (operators.size() == 1)
        {
            //for (unsigned int u = 0; u < numbers.size(); u++)
            //{
              //  cout << u << " " << numbers.at(u) << " number!\n";
            //}
            // unsigned int test1 = 3333333;
            // double test2 = 4.5;
            // double tester = test1 - test2;
            // cout << (double)tester << " number!\n";

            temp = Solve(numbers.at(i), operators.at(i), numbers.at(i + 1));
            operators.clear();
            numbers.at(0) = temp;
            return;
        }
        else if (operators.size() > 1)
        {
            if (adjacent_find(operators.begin(), operators.end(), not_equal_to<>()) == operators.end())
            {
                temp = Solve(numbers.at(i), operators.at(i), numbers.at(i + 1));
                operators.erase(operators.begin() + i);
                numbers.erase(numbers.begin() + i + 1);
                numbers.at(i) = temp;
                --i;
            }
            else
            {
                if (operators.at(i) == '(')
                {
                    // number get erased because when the operator is a parenthesis, the number is zero
                    left_parenthesis_location = i;
                    numbers.erase(numbers.begin() + i);
                    // cout << "left parenthesis" << endl;
                }
                if (operators.at(i) == ')')
                {
                    // number get erased because when the operator is a parenthesis, the number is zero
                    right_parenthesis_location = i;
                    numbers.erase(numbers.begin() + i);
                    // cout << "right parenthesis" << endl;
                    // cout << numbers.at(0) << endl;

                    // auto first = operators.begin() + left_parenthesis_location + 1;
                    // auto tester = i - 1;
                    // cout << tester << " tester!\n" << endl;
                    // auto last = operators.begin() + 1 + (i - 1);
                    // auto test = adjacent_find(first, last, not_equal_to<>()) == last;
                    // cout << test << " bool!\n" << endl;

                    // Examine from operator after left parenthesis to operator before right parenthesis. "+ 1" is needed because begin dosn't give the first element
                    if (adjacent_find(operators.begin() + left_parenthesis_location + 1, operators.begin() + 1 + (i - 1), not_equal_to<>()) == operators.begin() + 1 + (i - 1))
                    {
                        // cout << "Hello from equal!\n";
                        for (j = left_parenthesis_location; j < i - 1; j++)
                        {
                            temp = Solve(numbers.at(j), operators.at(j + 1), numbers.at(j + 1));
                            operators.erase(operators.begin() + (j + 1));
                            numbers.erase(numbers.begin() + (j + 1));
                            numbers.at(j) = temp;
                            --j;
                            --i;
                        }
                        operators.erase(operators.begin() + left_parenthesis_location);
                        operators.erase(operators.begin() + i - 1);
                        --i;
                        --i;
                    }
                    else
                    {
                        for (j = left_parenthesis_location; j < i - 1; j++)
                        {
                            if (operators.at(j + 1) == '*' || operators.at(j + 1) == '/')
                            {
                                // cout << "Hello from multiply/divide not equal!\n";
                                temp = Solve(numbers.at(j), operators.at(j + 1), numbers.at(j + 1));
                                operators.erase(operators.begin() + j + 1);
                                numbers.erase(numbers.begin() + j + 1);
                                numbers.at(j) = temp;
                                --j;
                                --i;
                                --right_parenthesis_location;
                            }

                            auto first = operators.begin() + left_parenthesis_location;
                            auto last = operators.begin() + right_parenthesis_location;
                            deque<char>::iterator multiply = find(first, last, '*');
                            deque<char>::iterator divide = find(first, last, '/');

                            while (multiply == last && divide == last && !operators.empty())
                            {
                                deque<char>::iterator add = find(first, last, '+');
                                deque<char>::iterator subtract = find(first, last, '-');

                                if (add == last && subtract == last)
                                {
                                    operators.erase(operators.begin() + left_parenthesis_location);
                                    operators.erase(operators.begin() + left_parenthesis_location);
                                    --i;
                                    --i;
                                    --j;
                                    --j;
                                    // cout << "parenthesis done!" << endl;
                                    break;
                                }

                                for (k = left_parenthesis_location; k < i - 1; j++)
                                {
                                    // Need to search for add/subtract operator INSIDE while because the loop
                                    // isn't left because no multiply/divide is always true at this point

                                    // left parenthesis location is deleted two times because when the left parenthesis
                                    // is deleted, the right one takes the position where the left was on

                                    // TODO: more tests needed!
                                    deque<char>::iterator add = find(first, last, '+');
                                    deque<char>::iterator subtract = find(first, last, '-');

                                    if (add == last && subtract == last)
                                    {
                                        // cout << numbers.at(0) << endl;
                                        break;
                                    }

                                    temp = Solve(numbers.at(k), operators.at(k + 1), numbers.at(k + 1));
                                    operators.erase(operators.begin() + k + 1);
                                    numbers.erase(numbers.begin() + k + 1);
                                    numbers.at(k) = temp;
                                    // cout << numbers.at(k) << " operator!" << endl;

                                    --k;
                                    --i;
                                    --right_parenthesis_location;
                                }
                                --j;
                            }
                        }
                    }
                }

                deque<char>::iterator left_parenthesis = find(operators.begin(), operators.end(), '(');
                deque<char>::iterator right_parenthesis = find(operators.begin(), operators.end(), ')');

                while (left_parenthesis == operators.end() && right_parenthesis == operators.end() && !operators.empty())
                {
                    for (j = 0; j < operators.size(); j++)
                    {
                        if (operators.at(j) == '*' || operators.at(j) == '/')
                        {
                            // cout << "is correct!" << endl;
                            temp = Solve(numbers.at(j), operators.at(j), numbers.at(j + 1));
                            numbers.at(j) = temp;
                            operators.erase(operators.begin() + j);
                            numbers.erase(numbers.begin() + j + 1);
                            --j;
                        }
                    }

                    // debug
                    //  for (j = 0; j < operators.size(); j++)
                    //  {
                    //      cout << j << " " << operators.at(j) << endl;
                    //  }
                    // debug end

                    deque<char>::iterator multiply = find(operators.begin(), operators.end(), '*');
                    deque<char>::iterator divide = find(operators.begin(), operators.end(), '/');

                    while (multiply == operators.end() && divide == operators.end() && !operators.empty())
                    {
                        // cout << "addition!" << endl;
                        temp = Solve(numbers.at(0), operators.at(0), numbers.at(1));
                        operators.erase(operators.begin());
                        numbers.erase(numbers.begin());
                        numbers.at(0) = temp;
                        // cout << numbers.at(0) << endl;
                        --i;
                    }
                }
            }
        }
        //}
    }
}