#include <iostream>
#include <string>
#include <vector>
#include <deque>
#include <algorithm>

#include <math.h>
#include "../Program/calculator.hpp"

using namespace Calculator;
using std::cout, std::cin, std::string, std::endl, std::deque;

/* THIS IS THE TERMINAL/CMD CALCULATOR */

// TODO: Spaces between operators
// parenthesis (does maybe not fully work yet. more tests needed!)
// Comments

// global variables
string expression;
deque<double> numbers;
deque<char> operators;

int main()
{
    //calculator class to use the calculate functions
    calculator calculator_class;

    // calculator introduction
    cout << "Welcome to the Calculator!\nFirst, you need to input what you want to do!\nThen press enter and you'll get the result! \n\n";
    cout << "'+' to add\n'-' to subtract\n'*' to multiply\n'/' to divide\n\n";
    cout << "Enter your expression:\n";

    // let the user put in the expression
    cin >> expression;
    cout << "\n";

    //dissasemble, calculate and print out the expression
    calculator_class.DissasembleExpression(expression, numbers, operators);
    calculator_class.Calculate(numbers, operators);
    if (numbers.size() > 0)
    {
        cout << "Result: " << numbers.at(0) << endl;
    }
    operators.clear();
    numbers.clear();
    return 0;
}