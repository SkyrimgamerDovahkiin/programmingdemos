#include <iostream>
#include <vector>
#include <memory>
#include <map>

using Entity = std::uint32_t;
using ComponentType = std::uint32_t;

struct Component
{
    int index;
};

struct Health : public Component
{
    int health;
};

struct Position : public Component
{
    int x;
    int y;
    int z;
};

// every Entity "has" one of these
struct ComponentData
{
    std::vector<std::unique_ptr<Component>> entityComponents;
};

std::map<Entity, ComponentData> entityMap;

// NEED to be sure that the "index"/component type and the entity that is given as an argument does exist, error checking is needed.
template <typename T>
T GetComponentData(Entity entity, ComponentType type)
{
    auto componentPtr = entityMap.at(entity).entityComponents.at(type).get();
    auto componentPtr2 = *(T *)componentPtr;
    return componentPtr2;
}

int main()
{
    for (int i = 0; i < 100000; i++)
    {
        ComponentData data;
        entityMap.insert({i, std::move(data)});
        entityMap.at(i).entityComponents.emplace_back(new Health{0, i});
        entityMap.at(i).entityComponents.emplace_back(new Position{1, 1 + i, 2 + i, 3 + i});
    }

    Entity e = 50;
    Health healthRetrieved = GetComponentData<Health>(e, 0);
    Position positionRetrieved = GetComponentData<Position>(e, 1);

    // std::cout << healthRetrieved.health << "\n";
    // std::cout << "Position: x: " << positionRetrieved.x << " y: " << positionRetrieved.y << " z: " << positionRetrieved.z << "\n";

    return 0;
}