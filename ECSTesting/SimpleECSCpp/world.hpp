#pragma once

#include <iostream>
#include <vector>
#include <memory>

//#include "health_component_manager.hpp"
//#include "name_component_manager.hpp"
#include "entity_manager.hpp"

namespace OGLE
{
    class World
    {
    public:
        World();
        ~World();

        // HealthComponentManager healthComponentManager;
        // NameComponentManager nameComponentManager;
        EntityManager *entityManager;
    };
}