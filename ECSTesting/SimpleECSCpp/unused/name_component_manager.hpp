#pragma once

#include <iostream>
#include <vector>
#include <memory>

#include "components.hpp"

namespace OGLE
{
    class NameComponentManager
    {
    public:
        NameComponentManager();
        ~NameComponentManager();

        std::vector<Name> name_components;
    };
}