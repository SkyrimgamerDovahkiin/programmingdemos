#pragma once

#include <iostream>
#include <vector>
#include <memory>

#include "components.hpp"
#include "defines.hpp"

namespace OGLE
{
    class HealthComponentManager
    {
    public:
        HealthComponentManager();
        ~HealthComponentManager();

        std::vector<Health> health_components;

        // Create an "entity" array/vector (normal indexes starting at 0)
        // then add the number (id) of an entity to that array/vector everytime "add component" in the Enitity Manager is called
        std::vector<Entity> entities;
    };
}