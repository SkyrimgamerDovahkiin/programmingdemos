#pragma once

#include <iostream>

using Entity = std::uint32_t;
using ComponentType = std::uint32_t;
using ComponentInstance = std::uint32_t;