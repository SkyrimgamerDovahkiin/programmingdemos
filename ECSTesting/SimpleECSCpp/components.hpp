#pragma once

#include <iostream>
#include <string>
#include <memory>
#include <vector>

struct Component
{

};

struct Health : public Component
{
    uint32_t health;
};

struct Name : Component
{
    std::string name;
};

struct ComponentCounter
{
    static int counter;
};

//template <typename ComponentType>

// Each Entity can only "have" one of these.
// the "Component" vector stores which Components (Health, Position, etc. for example) the entity has
struct ComponentData
{
    // std::uint32_t size = 1;
    // std::array<ComponentType, 1024> data;

    // std::vector<std::unique_ptr<Component>> entityComponents;
    std::vector<Health> entityComponents;
};