#include "entity_manager.hpp"

#include <algorithm>
#include <utility>

namespace OGLE
{
    EntityManager::EntityManager()
    {
    }

    EntityManager::~EntityManager()
    {
    }

    Entity EntityManager::CreateEntity()
    {
        Entity entity;
        entity = id;

        if (entities.size() != 0)
        {
            id = entities.back() + 1;
            entity = id;
        }

        entities.push_back(entity);
        return entity;
    }

    void EntityManager::DestroyEntity(Entity entity)
    {
        if (entity > 0)
        {
            entities.erase(entities.begin() + entity - 1);
        }
        else
        {
            entities.erase(entities.begin() + entity);
        }
    }

    Component EntityManager::GetComponent(Entity entity)
    {
        // TODO: needs to return given Type. For example, if given the Type "Health", it needs to search to the component data vector
        //  of the Entity and needs to return the health Component. for now, just return the zeroth element of the EntityComponents Vector.
        //auto component = componentManager->entityMap.at(entity).entityComponents.at(0).get();

        Component component;
        return component;
    }

    // Prints the Entities
    void EntityManager::PrintEntities()
    {
        for (size_t i = 0; i < entities.size(); i++)
        {
            std::cout << entities[i] << std::endl;
        }
    }
}