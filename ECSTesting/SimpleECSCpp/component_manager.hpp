#pragma once

#include <iostream>
#include <vector>
#include <memory>
#include <map>

#include "components.hpp"
#include "defines.hpp"

namespace OGLE
{
    class ComponentManager
    {
    public:
        ComponentManager();
        ~ComponentManager();
        ComponentManager(const ComponentManager&) = delete;

        // Component Types needed
        //ComponentData<ComponentType> componentData;
        //std::map<Entity, ComponentInstance> entityMap;
        std::map<Entity, ComponentData> entityMap;
    };
}