#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include <stdlib.h>

#include "world.hpp"
#include "components.hpp"
#include "defines.hpp"
#include "entity_manager.hpp"
#include "component_manager.hpp"

using OGLE::World, OGLE::EntityManager, OGLE::ComponentManager;

// void PrintHealthSystem(World world);
// void PrintNameSystem(World world);

// TODO: Figure out a way to pass variable arguments/ no Component as a Argument
// "Generalize" Component Manager

int main()
{
    World world;
    EntityManager entityManager;
    world.entityManager = &entityManager;
    ComponentManager componentManager;
    entityManager.componentManager = &componentManager;

    std::cout << "\n";
    Entity entity = entityManager.CreateEntity();

    Health health;
    health.health = 100;
    
    ComponentType componentType;

    entityManager.AddComponent(entity, health);
    //entityManager.GetComponents(entity2);
    //auto test = entityManager.lookup(entity2, health);
    //std::cout << test << std::endl;

    // std::cout << entityManager.healthComponentManager.entities.size() << " Size!\n\n";

    // world.entityManager->PrintEntities();

    //std::cout << "\n";

    // PrintHealthSystem(world);
    // PrintNameSystem(world);
}

// void PrintHealthSystem(World world)
// {
//     for (auto x : world.health_components)
//     {
//         std::cout << x.health << std::endl;
//     }
// }

// void PrintNameSystem(World world)
// {
//     for (auto x : world.name_components)
//     {
//         std::cout << "I am " << x.name << "!" << std::endl;
//     }
// }