#pragma once

#include <iostream>
#include <vector>
#include <memory>

#include "defines.hpp"
// #include "health_component_manager.hpp"
#include "component_manager.hpp"
#include "components.hpp"

namespace OGLE
{
    class EntityManager
    {
    public:
        EntityManager();
        ~EntityManager();

        Entity CreateEntity();
        void DestroyEntity(Entity entity);

        //template<typename ComponentType>
        void AddComponent(Entity entity, Component component)
        {
            // NOTE: What happens:

            

            // Each Entity get a "ComponentData" assigned. This thing is unique for every entity;
            // Then the Component that has been put in gets added to the "data" array inside of the "Component Data"
            // Then the entity with the component Data gets added to a map so the Component Manager knows what entity has which Components

            // Needs to accept a Type which is a component (derived from "Base" Component maybe?)
            // Need to check if component is of same type. if yes, then dont add

            ComponentData componentData;
            

            // std::unique_ptr<Component> ptr(&component);
            // componentData.entityComponents.push_back(std::move(ptr));
            // Health health;
            // componentData.entityComponents.push_back(health);
            // componentManager->entityMap.insert({entity, componentData});
        }

        Component GetComponent(Entity entity);

        void PrintEntities();

        ComponentManager *componentManager;
        std::vector<Entity> entities;

    private:
        std::uint32_t id = 0;
    };
}