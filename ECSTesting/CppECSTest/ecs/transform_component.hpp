# pragma once

// glm
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// #include "component_base.hpp"

// Defines a Transform Component with Position, Rotation and Scale.
struct Transform //: public Component
{
    glm::vec3 position;   // Position
    glm::vec3 rotation;   // Rotation
    glm::vec3 scale;      // Scale
};