#include "entity_manager.hpp"
// #include "component_base.hpp"

// #include <map>

namespace OGLE
{
    EntityManager::EntityManager()
    {
    }

    EntityManager::~EntityManager()
    {
    }

    Entity EntityManager::CreateEntity()
    {
        Entity entity;
        entity.id = id;

        if (entities.size() != 0)
        {
            id = entities.back().id + 1;
            entity.id = id;
        }

        entities.push_back(entity);
        return entity;
    }

    void EntityManager::DestroyEntity(Entity entity)
    {
        if (entity.id > 0)
        {
            entities.erase(entities.begin() + entity.id - 1);
        }
        else
        {
            entities.erase(entities.begin() + entity.id);
        }
    }

    // void EntityManager::AddComponent(Entity entity, Component component, uint32_t index)
    // {
    // }

    // Prints the Entities
    void EntityManager::PrintEntities()
    {
        for (size_t i = 0; i < entities.size(); i++)
        {
            std::cout << entities[i].id << std::endl;
        }
    }
}