#pragma once

// glm
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// #include "component_base.hpp"

// Defines a Rectangle. Values are x and y pos and width and height.
// x and y positions are the middle
struct Rectangle //: public Component
{
    float x;      // Rectangle position x
    float y;      // Rectangle position y
    float width;  // Rectangle width
    float height; // Rectangle height
    glm::vec3 color; // Rectangle Color
};