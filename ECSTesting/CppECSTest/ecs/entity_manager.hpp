#pragma once

#include <iostream>
#include <vector>
#include <memory>

#include "../defines.hpp"
#include "entity.hpp"


namespace OGLE
{
    class EntityManager
    {
    public:
        EntityManager();
        ~EntityManager();

        Entity CreateEntity();
        void DestroyEntity(Entity entity);
        // void AddComponent(Entity entity, Component component, uint32_t index);
        void PrintEntities();

        // Add and remove components from entity

        std::vector<Entity> entities;
    private:
        std::uint32_t id = 0;
    };
}