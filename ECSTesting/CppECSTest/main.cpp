// std
#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include <map>
#include <utility>

// glm
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// custom
#include "defines.hpp"
#include "ecs/transform_component.hpp"
#include "ecs/rectangle_component.hpp"
#include "ecs/entity_manager.hpp"
#include "ecs/entity.hpp"

using OGLE::EntityManager, OGLE::Entity;

int main(int argc, char **argv)
{
    EntityManager entityManager;

    Transform transform;
    Rectangle rect;
    
    std::cout << transform.id << " Hello!\n";
    std::cout << rect.id << " Hello!\n";

    Entity e = entityManager.CreateEntity();
    Entity f = entityManager.CreateEntity();
    // Entity g = entityManager.CreateEntity();
    // Entity h = entityManager.CreateEntity();
    // Entity i = entityManager.CreateEntity();
    // Entity j = entityManager.CreateEntity();

    for (size_t i = 0; i <= 10; i++)
    {
        entityManager.CreateEntity();
    }

    entityManager.DestroyEntity(e);
    entityManager.DestroyEntity(f);

    entityManager.PrintEntities();
}