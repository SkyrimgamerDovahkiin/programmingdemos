#include "parser.hpp"

Parser::Parser(std::vector<Token> tokens) : mTokens(std::move(tokens))
{
}

Parser::~Parser()
{
}

std::optional<NodeExpression> Parser::ParseExpression()
{
    if (Peek().has_value() && Peek().value().type == TokenType::_int_lit)
    {
        return NodeExpression{.var = NodeExpressionIntLit{.int_lit = Consume()}};
    }
    else if (Peek().has_value() && Peek().value().type == TokenType::_identifier)
    {
        return NodeExpression{.var = NodeExpressionIdentifier{.ident = Consume()}};
    }
    else
    {
        return {};
    }
}

std::optional<NodeStatement> Parser::ParseStatement()
{
    if (Peek().value().type == TokenType::_exit && Peek(1).has_value() && Peek(1).value().type == TokenType::_open_paren)
    {
        Consume();
        Consume();

        NodeStatementExit statementExit;

        if (auto nodeExpr = ParseExpression())
        {
            statementExit = {.expression = nodeExpr.value()};
        }
        else
        {
            std::cerr << "Invalid expression" << std::endl;
        }
        if (Peek().has_value() && Peek().value().type == TokenType::_close_paren)
        {
            Consume();
        }
        else
        {
            std::cerr << "Expected )" << std::endl;
        }
        if (Peek().has_value() && Peek().value().type == TokenType::_semi)
        {
            Consume();
        }
        else
        {
            std::cerr << "Expected semicolon" << std::endl;
        }
        return NodeStatement{.var = statementExit};
    }
    else if (Peek().has_value() && Peek().value().type == TokenType::_let &&
             Peek(1).has_value() && Peek(1).value().type == TokenType::_identifier &&
             Peek(2).has_value() && Peek(2).value().type == TokenType::_equals)
    {
        Consume();
        NodeStatementLet statementLet{.identifier = Consume()};
        Consume();
        if (auto expr = ParseExpression())
        {
            statementLet.expression = expr.value();
        }
        else
        {
            std::cerr << "Invalid expression" << std::endl;
        }
        if (Peek().has_value() && Peek().value().type == TokenType::_semi)
        {
            Consume();
        }
        else
        {
            std::cerr << "Expected ';'" << std::endl;
        }
        return NodeStatement{.var = statementLet};
    }
    else
    {
        return {};
    }
}

std::optional<NodeProgram> Parser::ParseProgram()
{
    NodeProgram program;

    while (Peek().has_value())
    {
        if (auto stmt = ParseStatement())
        {
            program.statements.push_back(stmt.value());
        }
        else
        {
            std::cerr << "Invalid statement" << std::endl;
        }
    }
    return program;
}

std::optional<Token> Parser::Peek(int offset) const
{
    if (mCurIdx + offset >= mTokens.size())
    {
        return {};
    }
    else
    {
        return mTokens.at(mCurIdx + offset);
    }
}

Token Parser::Consume()
{
    return mTokens.at(mCurIdx++);
}