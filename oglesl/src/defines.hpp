#pragma once

// std
#include <variant>
#include <optional>
#include <string>
#include <iostream>
#include <vector>
#include <sstream>
#include <unordered_map>

enum class TokenType
{
    _exit,
    _int_lit,
    _semi,
    _open_paren,
    _close_paren,
    _identifier,
    _let,
    _equals,
};

struct Token
{
    TokenType type;
    std::optional<std::string> value{};
};

struct NodeExpressionIntLit
{
    Token int_lit;
};

struct NodeExpressionIdentifier
{
    Token ident;
};

struct NodeExpression
{
    std::variant<NodeExpressionIntLit, NodeExpressionIdentifier> var;
};

struct NodeStatementExit
{
    NodeExpression expression;
};

struct NodeStatementLet
{
    Token identifier;
    NodeExpression expression;
};

struct NodeStatement
{
    std::variant<NodeStatementExit, NodeStatementLet> var;
};

struct NodeProgram
{
    std::vector<NodeStatement> statements;
};