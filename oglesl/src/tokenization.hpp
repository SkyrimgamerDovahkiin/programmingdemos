#pragma once

//custom
#include "defines.hpp"

class Tokenizer
{
public:
    Tokenizer(const std::string src);
    ~Tokenizer();

    std::vector<Token> Tokenize();

private:
    [[nodiscard]] std::optional<char> Peek(int offset = 0) const;
    char Consume();
    const std::string mSrc;
    size_t mCurIdx = 0;
};