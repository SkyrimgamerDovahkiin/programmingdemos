#include "generation.hpp"

Generator::Generator(NodeProgram prog) : mProgram(std::move(prog))
{
}

Generator::~Generator()
{
}

void Generator::GenerateExpression(const NodeExpression &expression)
{
    struct ExpressionVisitor
    {
        Generator *gen;
        void operator()(const NodeExpressionIntLit &expressionIntLit)
        {
            gen->mOutput << "    mov rax, " << expressionIntLit.int_lit.value.value() << "\n";
            gen->Push("rax");
        }
        void operator()(const NodeExpressionIdentifier &expressionIdentifier)
        {
            if (!gen->mVars.contains(expressionIdentifier.ident.value.value()))
            {
                std::cerr << "Undeclared identifier: " << expressionIdentifier.ident.value.value() << "\n";
            }
            else
            {
                const auto &var = gen->mVars.at(expressionIdentifier.ident.value.value());
                std::stringstream offset;
                offset << "QWORD [rsp + " << (gen->mStackSize - var.stackLocation - 1) * 8 << "]\n";
                gen->Push(offset.str());
            }
        }
    };

    ExpressionVisitor visitor{.gen = this};
    std::visit(visitor, expression.var);
}

void Generator::GenerateStatement(const NodeStatement &statement)
{
    struct StatementVisitor
    {
        Generator *gen;
        void operator()(const NodeStatementExit &statementExit) const
        {
            gen->GenerateExpression(statementExit.expression);
            gen->mOutput << "    mov rax, 60\n";
            gen->Pop("rdi");
            gen->mOutput << "    syscall\n";
        }
        void operator()(const NodeStatementLet &statementLet) const
        {
            if (gen->mVars.contains(statementLet.identifier.value.value()))
            {
                std::cerr << "Identifier already used: " << statementLet.identifier.value.value() << std::endl;
            }
            else
            {
                gen->mVars.insert({statementLet.identifier.value.value(), Variable {.stackLocation = gen->mStackSize}});
                gen->GenerateExpression(statementLet.expression);
            }
        }
    };

    StatementVisitor visitor{.gen = this};
    std::visit(visitor, statement.var);
}

std::string Generator::GenerateProgram()
{
    mOutput << "global _start\n_start:\n";

    for (const NodeStatement &statement : mProgram.statements)
    {
        GenerateStatement(statement);
    }

    mOutput << "    mov rax, 60\n";
    mOutput << "    mov rdi, 0\n";
    mOutput << "    syscall\n";

    return mOutput.str();
}

void Generator::Push(const std::string &_register)
{
    mOutput << "    push " << _register << "\n";
    mStackSize++;
}

void Generator::Pop(const std::string &_register)
{
    mOutput << "    pop " << _register << "\n";
    mStackSize--;
}