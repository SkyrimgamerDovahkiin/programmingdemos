#pragma once

#include "tokenization.hpp"

class Parser
{
public:
    Parser(std::vector<Token> tokens);
    ~Parser();

    std::optional<NodeExpression> ParseExpression();
    std::optional<NodeStatement> ParseStatement();
    std::optional<NodeProgram> ParseProgram();

private:
    [[nodiscard]] std::optional<Token> Peek(int offset = 0) const;
    Token Consume();
    const std::vector<Token> mTokens;
    size_t mCurIdx = 0;
};