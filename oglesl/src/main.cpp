// std
#include <fstream>

// custom
#include "generation.hpp"

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "Usage: " << argv[0] << " <input.oglesh>" << std::endl;
        return 1;
    }

    // read file to stringstream
    std::ifstream input(argv[1], std::ifstream::in);
    std::stringstream contentsStream;
    contentsStream << input.rdbuf();
    input.close();

    // put stringstream into string contents
    std::string contents = contentsStream.str();

    Tokenizer tokenizer(std::move(contents));
    std::vector<Token> tokens = tokenizer.Tokenize();
    
    Parser parser(std::move(tokens));
    std::optional<NodeProgram> program = parser.ParseProgram();
    if (!program.has_value())
    {
        std::cerr << "Invalid Program" << std::endl;
        return 1;
    }
    
    
    Generator generator(program.value());

    // print contents
    {
        std::fstream output("out.asm", std::ios::out);
        output << generator.GenerateProgram();
        output.close();
    }

    system("nasm -felf64 out.asm");
    system("ld out.o -o out");

    return 0;
}