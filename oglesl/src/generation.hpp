#pragma once

#include "parser.hpp"

class Generator
{
public:
    Generator(NodeProgram prog);
    ~Generator();

    void GenerateExpression(const NodeExpression &expression);
    void GenerateStatement(const NodeStatement &statement);
    std::string GenerateProgram();

private:
    void Push(const std::string &_register);
    void Pop(const std::string &_register);

    struct Variable
    {
        size_t stackLocation;
    };

    const NodeProgram mProgram;
    std::stringstream mOutput;
    size_t mStackSize = 0;
    std::unordered_map<std::string, Variable> mVars {};
};