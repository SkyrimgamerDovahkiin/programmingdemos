#include "tokenization.hpp"

Tokenizer::Tokenizer(const std::string src) : mSrc(std::move(src))
{
}

Tokenizer::~Tokenizer()
{
}

std::vector<Token> Tokenizer::Tokenize()
{
    std::vector<Token> tokens;
    std::string buffer;

    while (Peek().has_value())
    {
        if (std::isalpha(Peek().value()))
        {
            buffer.push_back(Consume());
            while (Peek().has_value() && std::isalnum(Peek().value()))
            {
                buffer.push_back(Consume());
            }
            if (buffer == "exit")
            {
                tokens.push_back({.type = TokenType::_exit});
                buffer.clear();
                continue;
            }
            else if (buffer == "let")
            {
                tokens.push_back({.type = TokenType::_let});
                buffer.clear();
                continue;
            }
            else
            {
                tokens.push_back({.type = TokenType::_identifier, .value = buffer});
                buffer.clear();
                continue;
            }
        }
        else if (std::isdigit(Peek().value()))
        {
            buffer.push_back(Consume());
            while (Peek().has_value() && std::isdigit(Peek().value()))
            {
                buffer.push_back(Consume());
            }
            tokens.push_back({.type = TokenType::_int_lit, .value = buffer});
            buffer.clear();
            continue;
        }
        else if (Peek().value() == '(')
        {
            Consume();
            tokens.push_back({.type = TokenType::_open_paren});
            continue;
        }
        else if (Peek().value() == ')')
        {
            Consume();
            tokens.push_back({.type = TokenType::_close_paren});
            continue;
        }
        else if (Peek().value() == ';')
        {
            Consume();
            tokens.push_back({.type = TokenType::_semi});
            continue;
        }
        else if (Peek().value() == '=')
        {
            Consume();
            tokens.push_back({.type = TokenType::_equals});
            continue;
        }
        else if (std::isspace(Peek().value()))
        {
            Consume();
            continue;
        }
        else
        {
            std::cout << "Wrong code" << std::endl;
        }
    }

    mCurIdx = 0;
    return tokens;
}

std::optional<char> Tokenizer::Peek(int offset) const
{
    if (mCurIdx + offset >= mSrc.length())
    {
        return {};
    }
    else
    {
        return mSrc.at(mCurIdx + offset);
    }
}

char Tokenizer::Consume()
{
    return mSrc.at(mCurIdx++);
}