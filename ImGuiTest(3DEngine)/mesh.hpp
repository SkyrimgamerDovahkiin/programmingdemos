#pragma once

// std
#include <string>
#include <fstream>

// glm
#include <glm/glm.hpp>

// glew
#include <GL/glew.h>

// custom
#include "defines.hpp"
#include "shader.hpp"

namespace OGLE
{

    class Mesh
    {
    public:
        // Mesh(const char *filename, Material *material, Shader *shader);
        Mesh(Shader *shader);
        ~Mesh();

        void LoadMaterial(Material *material);
        void ChangeMaterialLocation(std::string materialLocation);
        void Render();

        Material *material;
        std::string materialLocation;

        glm::mat4 model = glm::mat4(1.0f);
        glm::mat4 modelViewProj = glm::mat4(1.0f);

        // uint64_t numVertices = 0;
        uint64_t numIndices = 0;
        uint64_t numIndicesOffset = 0;

    private:
        // VertexBuffer *vertexBuffer; // change this later so that a vertex buffer can be passed to the constructor and used for rendering
        // IndexBuffer *indexBuffer;   // change this later so that a index buffer can be passed to the constructor and used for rendering
        // Shader *shader;

        // uint64_t numIndices = 0;

        int diffuseLocation;
        int specularLocation;
        int emissiveLocation;
        int shininessLocation;
    };
}