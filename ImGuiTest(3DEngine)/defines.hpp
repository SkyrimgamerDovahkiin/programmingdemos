#pragma once

#include <iostream>
#include <vector>
#include <fstream>

// glm
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// vector 3 definitions
struct Vector3
{
    glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
    glm::vec3 down = glm::vec3(0.0f, -1.0f, 0.0f);
    glm::vec3 forward = glm::vec3(0.0f, 0.0f, 1.0f);
    glm::vec3 back = glm::vec3(0.0f, 0.0f, -1.0f);
    glm::vec3 right = glm::vec3(1.0f, 0.0f, 0.0f);
    glm::vec3 left = glm::vec3(-1.0f, 0.0f, 0.0f);
    glm::vec3 zero = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 one = glm::vec3(1.0f, 1.0f, 1.0f);
};

// the vertex
struct Vertex
{
    glm::vec3 positions;
    glm::vec3 normals;
    glm::vec2 uvs;
    glm::vec4 colors;
};

// the material
struct Material
{
    GLuint shaderID; // the shader the material uses

    // the colors for each channel and the shininess
    glm::vec3 diffuse;
    glm::vec3 specular;
    glm::vec3 emissive;
    float shininess;

    // the different texture maps
    /*GLuint diffuseMap = 0;
    const char *diffuseMapLocation;
    glm::ivec2 diffuseMapSize;

    GLuint normalMap;
    char *normalMapLocation;
    glm::ivec2 normalMapSize;*/
};

std::ofstream &operator<<(std::ofstream &stream, Material &material);
std::ifstream &operator>>(std::ifstream &stream, Material &material);

// std::ofstream &operator<<(std::ofstream &stream, Material &material)
// {
//     stream.write(reinterpret_cast<char *>(&material.diffuse.x), sizeof(float));
//     stream.write(reinterpret_cast<char *>(&material.diffuse.y), sizeof(float));
//     stream.write(reinterpret_cast<char *>(&material.diffuse.z), sizeof(float));

//     stream.write(reinterpret_cast<char *>(&material.specular.x), sizeof(float));
//     stream.write(reinterpret_cast<char *>(&material.specular.y), sizeof(float));
//     stream.write(reinterpret_cast<char *>(&material.specular.z), sizeof(float));

//     stream.write(reinterpret_cast<char *>(&material.emissive.x), sizeof(float));
//     stream.write(reinterpret_cast<char *>(&material.emissive.y), sizeof(float));
//     stream.write(reinterpret_cast<char *>(&material.emissive.z), sizeof(float));

//     stream.write(reinterpret_cast<char *>(&material.shininess), sizeof(float));

//     return stream;
// }

// // overwrite operator for reading material
// std::ifstream &operator>>(std::ifstream &stream, Material &material)
// {
//     stream.read(reinterpret_cast<char *>(&material.diffuse.x), sizeof(float));
//     stream.read(reinterpret_cast<char *>(&material.diffuse.y), sizeof(float));
//     stream.read(reinterpret_cast<char *>(&material.diffuse.z), sizeof(float));

//     stream.read(reinterpret_cast<char *>(&material.specular.x), sizeof(float));
//     stream.read(reinterpret_cast<char *>(&material.specular.y), sizeof(float));
//     stream.read(reinterpret_cast<char *>(&material.specular.z), sizeof(float));

//     stream.read(reinterpret_cast<char *>(&material.emissive.x), sizeof(float));
//     stream.read(reinterpret_cast<char *>(&material.emissive.y), sizeof(float));
//     stream.read(reinterpret_cast<char *>(&material.emissive.z), sizeof(float));

//     stream.read(reinterpret_cast<char *>(&material.shininess), sizeof(float));

//     return stream;
// }

// CharacterGlyph for text rendering (later)
struct CharacterGlyph
{
    glm::vec2 Size;       // Size of glyph
    glm::vec2 Bearing;    // Offset from baseline to left/top of glyph
    unsigned int Advance; // Offset to advance to next glyph
    float tx;             // x offset of glyph in texture coordinates
};