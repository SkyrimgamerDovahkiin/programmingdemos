// std
#include <iostream>
#include <cmath>
#include <vector>
#include <list>
#include <fstream>
#include <ctime>
#include <algorithm>
#include <deque>
#include <string>
#include <stdio.h>

// GLEW/SDL
#define GLEW_STATIC
#include <GL/glew.h>
#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>

// STB
#define STB_IMAGE_IMPLEMENTATION
#include "libs/stb/stb_image.h"

// custom
#include "defines.hpp"
#include "vertex_buffer.hpp"
#include "index_buffer.hpp"
#include "shader.hpp"
#include "mesh.hpp"
#include "floating_camera.hpp"
#include "tools/modelexporter.hpp"

#include "libs/imgui/imgui.h"
#include "libs/imgui/imgui_impl_sdl2.h"
#include "libs/imgui/imgui_impl_opengl3.h"
#include "libs/imgui/imgui_stdlib.h"

#define IMGUI_PAYLOAD_TYPE_DND_MATERIAL_LOCATION "DND_MATERIAL_LOCATION"

using OGLE::VertexBuffer, OGLE::IndexBuffer, OGLE::Shader, OGLE::FloatingCamera, OGLE::Mesh;

void GLAPIENTRY OpenGLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
{
    std::cout << "[OpenGL Error] " << message << std::endl;
}

#ifdef _DEBUG

void _GLGetError(const char *file, int line, const char *call)
{
    while (GLenum error = glGetError())
    {
        std::cout << "[OpenGL Error] " << glewGetErrorString(error) << " in " << file << ":" << line << " Call: " << call << std::endl;
    }
}

#endif

std::vector<Vertex> vertices;
std::vector<uint32_t> indices;

uint64_t numVertices = 0;
uint64_t numIndices = 0;

uint64_t maxVertices = 16000000;

ImVec4 rectColor = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

int screenWidth = 1920, screenHeight = 1080;

// GLuint texID;
FloatingCamera camera(90.0f, screenWidth, screenHeight); // the camera

std::string modelName;
std::string modelOutputName;
std::string modelLoadingName;

// the curDir for the content explorer
std::filesystem::path curDir = "Assets";

char *lastTextureName;

GLuint boundTexture;

// needed for text input window to show up
bool textInputPopup = false;

// needed for disableing movement when in imgui window
bool canMoveInScene = true;

// the shaders that can exist
std::vector<Shader*> shaders;

// the materials for editing
std::vector<std::pair<std::string, Material>> materials;

// the meshes for editing
std::vector<std::pair<std::string, Mesh>> meshes;

// the materialIndex to find the material in the vector
int materialIndex = 0;

// the meshIndex to find the mesh in the vector
int meshIndex = 0;

std::filesystem::path selected;

// drag and drop file path
std::string dragAndDropPath;

uint64_t lastModelIndex = 0;
uint64_t offsetZ = 0;

// load a texture, currently disabled
/*void LoadTexture(const char *filename, Material &material)
{
    // int textureWidth = 0;
    // int textureHeight = 0;
    // int bitsPerPixel = 0;

    // auto textureBuffer = stbi_load(filename, &textureWidth, &textureHeight, &bitsPerPixel, 4);

    // set the bound texture to the diffuse map of the cur material
    boundTexture = material.diffuseMap;

    // glBindTexture(GL_TEXTURE_2D, material.diffuseMap);
    // glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, textureWidth, textureHeight,
    // GL_RGBA, GL_UNSIGNED_BYTE, textureBuffer);
}*/

// create a texture for the material, currently disabled
/*void CreateTexture(const char *filename, Material &material)
{
    int textureWidth = 0;
    int textureHeight = 0;
    int bitsPerPixel = 0;

    // stbi_set_flip_vertically_on_load(true);
    auto textureBuffer = stbi_load(filename, &textureWidth, &textureHeight, &bitsPerPixel, 4);

    glGenTextures(1, &material.diffuseMap);

    glBindTexture(GL_TEXTURE_2D, material.diffuseMap);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, textureWidth, textureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureBuffer);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);

    if (textureBuffer)
    {
        stbi_image_free(textureBuffer);
    }
}*/

// method to create material in custom file format on hard drive and push it back into materials list
void CreateMaterial(std::string saveLocation, std::string name, GLuint shaderID)
{
    // std::cout << saveLocation << "/" << name << std::endl;

    Material material;
    material.shaderID = shaderID;
    material.diffuse = glm::vec3(1.0f);
    material.specular = glm::vec3(1.0f);
    material.emissive = glm::vec3(1.0f);
    material.shininess = 1.0f;

    std::ofstream outfile(saveLocation + "/" + name + ".mat");
    outfile << saveLocation + "/" + name + ".mat" << std::endl;
    outfile << material << std::endl;
    outfile.close();

    // when new material is created, push it back into materials list
    std::pair<std::string, Material> pair{saveLocation + "/" + name + ".mat", material};
    materials.push_back(pair);
}

// compare operators needed for material pair
bool operator==(const Material &lhs, const Material &rhs)
{
    if (lhs.diffuse == rhs.diffuse && lhs.specular == rhs.specular && lhs.emissive == rhs.emissive && lhs.shininess == rhs.shininess)
    {
        return true;
    }
    return false;
}

bool operator==(const std::pair<std::string, Material> &lhs, const std::pair<std::string, Material> &rhs)
{
    if (lhs.first == rhs.first && lhs.second == rhs.second)
    {
        return true;
    }
    return false;
}

// How this works:
// 1. Every material has its location saved in its own file
// 2. This location gets read and used for searching
// 3. when correct one found, the materialIndex gets set to the location
void LoadMaterial(std::string saveLocation, std::string name)
{
    std::string path = saveLocation + "/" + name + ".mat";

    selected = path;

    // set material index to the correct material for editing
    for (size_t i = 0; i < materials.size(); i++)
    {
        if (materials[i].first == path)
        {
            materialIndex = i;
        }
    }
}

// load all materials at startup
void LoadMaterials()
{
    for (const auto &dirEntry : std::filesystem::recursive_directory_iterator("Assets"))
    {
        if (dirEntry.path().extension() == ".mat")
        {
            // std::cout << dirEntry.path() << " material\n";
            Material material;
            std::string chosenLocation;

            std::ifstream infile;
            infile.open(dirEntry.path());
            if (infile.is_open())
            {
                getline(infile, chosenLocation);
                infile >> material;

                infile.close();
            }

            std::pair<std::string, Material> pair{chosenLocation, material};
            materials.push_back(pair);
            // Debug
            // for (size_t i = 0; i < tempMaterials.size(); i++)
            // {
            //     std::cout << tempMaterials[i].first << " " << tempMaterials[i].second.diffuse.x << std::endl;
            // }
        }
    }
}

// save material to disk
void SaveMaterial(std::string location, Material material)
{
    std::ofstream outfile(location);
    outfile << location << std::endl;
    outfile << material << std::endl;
    outfile.close();
}

// NOTE: should only be done AFTER vertex and index buffer was created
// NOTE: change later to choose a vertex buffer/index buffer or create a new one if it's a different model
// NOTE: shader should be in the material later
void CreateMesh(glm::vec3 position, std::string path, Shader *shader)
{
    // get the current path when clicked
    std::string curDir = std::filesystem::current_path();

    uint64_t numVerticesTemp = 0;
    uint64_t numIndicesTemp = 0;
    // uint64_t index = 0;

    std::vector<Vertex> verticesTemp;
    std::vector<uint32_t> indicesTemp;

    verticesTemp.clear();
    indicesTemp.clear();

    selected = path;

    // read model
    std::ifstream input = std::ifstream(path, std::ios::in | std::ios::binary);
    if (!input.is_open())
    {
        std::cout << "Error reading model file!" << std::endl;
    }
    input.read((char *)&numVerticesTemp, sizeof(uint64_t));
    input.read((char *)&numIndicesTemp, sizeof(uint64_t));

    for (uint64_t i = 0; i < numVerticesTemp; i++)
    {
        Vertex vertex;
        input.read((char *)&vertex.positions.x, sizeof(float));
        input.read((char *)&vertex.positions.y, sizeof(float));
        input.read((char *)&vertex.positions.z, sizeof(float));

        input.read((char *)&vertex.normals.x, sizeof(float));
        input.read((char *)&vertex.normals.y, sizeof(float));
        input.read((char *)&vertex.normals.z, sizeof(float));

        // my code
        input.read((char *)&vertex.uvs.x, sizeof(float));
        input.read((char *)&vertex.uvs.y, sizeof(float));

        vertex.colors = glm::vec4(1.0f);

        verticesTemp.push_back(vertex);
    }

    for (uint64_t i = 0; i < numIndicesTemp; i++)
    {
        uint32_t index;
        input.read((char *)&index, sizeof(uint32_t));
        indicesTemp.push_back(index + lastModelIndex);
    }

    lastModelIndex = numVertices + numVerticesTemp; // set last model index to number of vertices + verticesTemp.size() so that the correct index is added to the indices array

    Mesh mesh(shader);
    mesh.numIndices = numIndicesTemp; // how many indices the mesh has, needed for rendering

    mesh.numIndicesOffset = numIndices;

    position = glm::vec3(position.x, position.y, position.z);

    mesh.LoadMaterial(&materials.at(0).second);        // load a temporary material
    mesh.model = glm::translate(mesh.model, position); // move model to specified position
    mesh.modelViewProj = camera.GetViewProj() * mesh.model;
    std::pair<std::string, Mesh> pair{path, mesh};
    meshes.push_back(pair);

    meshIndex = meshes.size() - 1; // set mesh index

    glBufferSubData(GL_ARRAY_BUFFER, numVertices * sizeof(Vertex), numVerticesTemp * sizeof(Vertex), verticesTemp.data());
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(indicesTemp[0]), numIndicesTemp * sizeof(indicesTemp[0]), indicesTemp.data());

    numVertices += numVerticesTemp;
    numIndices += numIndicesTemp;
}

// loads a mesh (currently there isn't a method to create and load and objects aren't clickable/selectable)
void LoadMesh(std::filesystem::path selectedModel)
{
    for (size_t i = 0; i < meshes.size(); i++)
    {
        // std::cout << meshes[i].first << "\n";
        if (meshes[i].first == selectedModel)
        {
            selected = selectedModel;
            meshIndex = i;
        }
    }
}

// the file explorer
// shader gets passed to material, currently just for testing
void ShowFileExplorerWindow(Shader *shader)
{
    std::filesystem::path assetsDir = "Assets"; // change later

    ImGui::Begin("Content Browser");

    if (curDir != assetsDir)
    {
        if (ImGui::Button("<-"))
        {
            curDir = curDir.parent_path();
        }
    }

    for (auto &it : std::filesystem::directory_iterator(curDir))
    {
        const auto &path = it.path();
        auto relativePath = std::filesystem::relative(path, assetsDir);
        std::string filenameString = relativePath.filename().string();

        if (it.is_directory())
        {
            if (ImGui::Button(filenameString.c_str()))
            {
                curDir /= path.filename();
            }
        }
        else
        {
            if (ImGui::Button(filenameString.c_str()))
            {
                if (path.extension() == ".ogle")
                {
                    // std::cout << " loading " << filenameString << std::endl;
                    // LoadMesh(path.stem());
                    CreateMesh(glm::vec3(0.0f, 0.0f, offsetZ += 5), path, shader);
                }
                if (path.extension() == ".mat")
                {
                    LoadMaterial(curDir, path.stem());
                }
            }

            if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None))
            {
                dragAndDropPath = path;
                // Set payload to carry the index of our item (could be anything)
                ImGui::SetDragDropPayload(IMGUI_PAYLOAD_TYPE_DND_MATERIAL_LOCATION, &dragAndDropPath, dragAndDropPath.size());

                // Display preview (could be anything, e.g. when dragging an image we could decide to display
                // the filename and a small preview of the image, etc.)
                ImGui::Text("Copy %s", dragAndDropPath.c_str());

                ImGui::EndDragDropSource();
            }
        }
    }

    if (ImGui::BeginPopupContextWindow())
    {
        if (ImGui::MenuItem("New Material"))
        {
            textInputPopup = true;
        }
        ImGui::EndPopup();
    }

    static std::string name;
    if (textInputPopup)
    {
        ImGui::InputText("##1", &name);
        if (ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_Enter)))
        {
            CreateMaterial(curDir, name, 1);
            textInputPopup = false;
        }
    }

    ImGui::End();
}

// Show inspector window for objects/materials/etc.
void ShowInspectorWindow()
{
    ImGui::SetNextWindowSize(ImVec2(500.0f, (float)screenHeight), ImGuiCond_Once);
    ImGui::SetNextWindowPos(ImVec2(screenWidth - 500.0f, 0.0f), ImGuiCond_Once);

    ImGui::Begin("Inspector");

    // when nothing selected, show welcome screen
    if (selected.extension() == ".ogle")
    {
        ImGui::TextUnformatted(selected.stem().c_str());

        // std::cout << meshes.at(meshIndex).second.materialLocation;
        // std::cout << meshIndex << std::endl;
        // LoadMesh(selected);

        // if (ImGui::InputText("g", &meshes.at(meshIndex).second.materialLocation))
        // {
        //     for (int i = 0; i < materials.size(); i++)
        //     {
        //         if (materials[i].first == meshes.at(meshIndex).second.materialLocation)
        //         {
        //             meshes.at(meshIndex).second.LoadMaterial(&materials[i].second);
        //         }
        //     }
        // }
        ImGui::InputText("##MaterialInput", &meshes.at(meshIndex).second.materialLocation);
        if (ImGui::BeginDragDropTarget())
        {
            if (const ImGuiPayload *payload = ImGui::AcceptDragDropPayload(IMGUI_PAYLOAD_TYPE_DND_MATERIAL_LOCATION))
            {
                // std::cout << payload->DataSize << " hfhgddg\n";
                // std::cout << dragAndDropPath.size() << " hfhgddg\n";
                IM_ASSERT(payload->DataSize == dragAndDropPath.size());
                std::string payload_n = *(const std::string *)payload->Data;

                meshes.at(meshIndex).second.materialLocation = payload_n;
                for (int i = 0; i < materials.size(); i++)
                {
                    if (materials[i].first == meshes.at(meshIndex).second.materialLocation)
                    {
                        meshes.at(meshIndex).second.LoadMaterial(&materials[i].second);
                    }
                }
            }
            ImGui::EndDragDropTarget();
        }
    }
    else if (selected.extension() == ".mat")
    {
        std::string name = selected.stem();
        name.append(" (Material)");

        ImGui::TextUnformatted(name.c_str());
        if (ImGui::TreeNode("Maps"))
        {
            ImGui::ColorEdit3("Diffuse Color", (float *)&materials.at(materialIndex).second.diffuse);
            ImGui::ColorEdit3("Emissive Color", (float *)&materials.at(materialIndex).second.emissive);
            ImGui::ColorEdit3("Specular Color", (float *)&materials.at(materialIndex).second.specular);
            ImGui::InputFloat("Shininess", &materials.at(materialIndex).second.shininess);

            ImGui::TreePop();
        }
    }
    else
    {
        ImGui::TextUnformatted("select something");
    }

    ImGui::End();
}

// TODO: select object
void ClickedOnObject(int x, int y)
{
    glm::vec3 currentRay;

    glm::mat4 projectionMatrix = camera.GetProjection();
    glm::mat4 viewMatrix = camera.GetView();

    // get mouse position is x and y argument and convert them to Normalized Device Coords (aka OpenGL screen coordinate system)
    float ndcX = (2.0f * x) / screenWidth - 1.0f;
    float ndcY = (2.0f * y) / screenHeight - 1.0f;

    glm::vec2 normalizedDeviceCoords = glm::vec2(ndcX, -ndcY);

    // convert to homogeneous clip space
    glm::vec4 clipCoords = glm::vec4(normalizedDeviceCoords.x, normalizedDeviceCoords.y, -1.0f, 1.0f);

    // convert to eye space
    glm::mat4 invertedProjection = glm::inverse(projectionMatrix);
    glm::vec4 eyeCoords = invertedProjection * clipCoords;
    eyeCoords = glm::vec4(eyeCoords.x, eyeCoords.y, -1.0f, 0.0f);

    // convert to world space
    glm::vec4 inversedView = glm::inverse(viewMatrix) * eyeCoords;
    glm::vec3 worldCoords = glm::vec3(inversedView.x, inversedView.y, inversedView.z);
    // normalize world coords
    worldCoords = glm::normalize(worldCoords);

    currentRay = worldCoords;

    std::cout << worldCoords.x << " " << worldCoords.y << " \n";
}

void ShowModelConverterWindow(ModelExporter modelExporter)
{
    // Window for material access
    {
        ImGui::SetNextWindowSize(ImVec2(500.0f, 250.0f), ImGuiCond_Once);
        ImGui::SetNextWindowPos(ImVec2(screenWidth - 1000.0f, screenHeight - 250.0f), ImGuiCond_Once);

        ImGui::Begin("Model Converter"); // Create window with name

        ImGui::InputText("Model location", &modelName);
        ImGui::SetItemTooltip("Put in the model name. The model location must be in Assets/models");
        // ImGui::SameLine();
        ImGui::InputText("Model output location", &modelOutputName);
        ImGui::SetItemTooltip("Put in the desired model name without file extension. The model will be saved in Assets/models");
        // ImGui::SameLine();
        if (ImGui::Button("Convert", ImVec2(60.0f, 20.0f)))
        {
            modelExporter.Convert(modelName, modelOutputName);
        }

        static std::string path;
        ImGui::InputText("OGLE Model location", &path);
        if (ImGui::Button("Load Model", ImVec2(60.0f, 20.0f)))
        {
            LoadMesh(path);
        }

        // ImGui::InputText("Model loading location", &modelLoadingName);
        // ImGui::SetItemTooltip("Put in the desired model name without file extension. The model location must be in Assets/models");

        // if (ImGui::Button("Load Model", ImVec2(100.0f, 20.0f)))
        // {
        //     LoadModel(modelLoadingName);
        // }

        ImGui::End();
    }
}

// Saves the materials in the tempMaterials vector to their files
void SaveMaterialsToFile()
{
    for (size_t i = 0; i < materials.size(); i++)
    {
        std::pair<std::string, Material> pair = materials.at(i);
        SaveMaterial(pair.first, pair.second);
    }
}

// Main code
int main(int, char **)
{
    // load materials
    materials.clear();
    LoadMaterials();

    // FloatingCamera camera(90.0f, screenWidth, screenHeight);
    camera.Translate(glm::vec3(0.0f, 0.0f, 0.0f));
    camera.Update();

    // std::cout << camera.GetPosition().z << std::endl;

    // sdl init
    SDL_Window *window;

    {
        SDL_Init(SDL_INIT_EVERYTHING);

        SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetSwapInterval(1);
    }

    // Debug
#ifdef _DEBUG
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
#endif

    uint32_t flags = SDL_WINDOW_OPENGL;
    window = SDL_CreateWindow("OpenGL Game", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, flags);
    SDL_GLContext glContext = SDL_GL_CreateContext(window);

    GLenum err = glewInit();
    if (err != GLEW_OK)
    {
        std::cout << "Error: " << glewGetErrorString(err) << std::endl;
        std::cin.get();
        return -1;
    }

    std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;

#ifdef _DEBUG
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(OpenGLDebugCallback, 0);
#endif

    // setup
    // create Vertex buffer
    VertexBuffer vertexBuffer(vertices.data(), maxVertices);

    // create Index buffer (size = maxVertices * 4)
    IndexBuffer indexBuffer(indices.data(), maxVertices * 4);

    // create shader
    // Shader shader("shaders/basic_vert.glsl", "shaders/basic_frag.glsl");

    // create texture shader
    Shader textureShader("shaders/texture_vert.glsl", "shaders/texture_frag.glsl");
    shaders.push_back(&textureShader);

    // normal shader model view projection matrix location
    // int colorUniformLocation = glGetUniformLocation(shader.GetShaderId(), "u_color");
    // int modelViewProjMatrixLocaction = glGetUniformLocation(shader.GetShaderId(), "u_modelViewProj");

    // texture shader locations
    int textureUniformLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_texture");
    int textureScaleUniformLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_texScale");
    int textureViewProjMatrixUniformLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_modelViewProj");
    int textureModelViewLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_modelView");
    int textureInvModelViewLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_invModelView");

    // light stuff
    // int diffuseLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_diffuse");
    // int specularLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_specular");
    // int emissiveLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_emissive");
    // int shininessLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_shininess");

    // delta time stuff
    uint64_t perfCounterFrequency = SDL_GetPerformanceFrequency();
    uint64_t lastCounter = SDL_GetPerformanceCounter();
    float delta = 0.0f;

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO &io = ImGui::GetIO();
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; // Enable Keyboard Controls
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;  // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    // ImGui::StyleColorsLight();

    // Setup Platform/Renderer backends
    ImGui_ImplSDL2_InitForOpenGL(window, glContext);
    ImGui_ImplOpenGL3_Init();

    // Our state
    bool show_demo_window = false;
    bool showInspectorWindow = true;
    bool showModelConverterWindow = true;

    // camera speed
    float cameraSpeed = 6.0f;

    // time
    float time = 0.0f;

    // close
    bool close = false;

    // enable dropping for files
    SDL_EventState(SDL_DROPFILE, SDL_ENABLE);

    // input bools
    bool buttonW = false;
    bool buttonS = false;
    bool buttonA = false;
    bool buttonD = false;
    bool buttonSpace = false;
    bool buttonShift = false;

    // create material for testing
    // Material material;
    // Mesh mesh("Assets/materials/gfgd.mat", &textureShader);
    // Mesh mesh("Assets/materials/Golem.mat", &textureShader);
    // CreateMesh("golem", &textureShader);

    // model exporter
    ModelExporter modelExporter;

    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);

    // Main loop
    while (!close)
    {
        // input
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            ImGui_ImplSDL2_ProcessEvent(&event); // Forward your event to backend

            if (event.type == SDL_QUIT)
            {
                SaveMaterialsToFile();

                close = true;
            }
            else if (event.type == SDL_DROPFILE)
            {
                // In case if dropped file
                // material.diffuseMapLocation = event.drop.file;
                // TODO: change texture of the currently selected material (later)
            }
            else if (event.type == SDL_KEYDOWN && canMoveInScene)
            {
                switch (event.key.keysym.sym)
                {
                case SDLK_w:
                    buttonW = true;
                    break;
                case SDLK_s:
                    buttonS = true;
                    break;
                case SDLK_a:
                    buttonA = true;
                    break;
                case SDLK_d:
                    buttonD = true;
                    break;
                case SDLK_SPACE:
                    buttonSpace = true;
                    break;
                case SDLK_LSHIFT:
                    buttonShift = true;
                    break;
                }
            }
            else if (event.type == SDL_KEYUP && canMoveInScene)
            {
                switch (event.key.keysym.sym)
                {
                case SDLK_w:
                    buttonW = false;
                    break;
                case SDLK_s:
                    buttonS = false;
                    break;
                case SDLK_a:
                    buttonA = false;
                    break;
                case SDLK_d:
                    buttonD = false;
                    break;
                case SDLK_SPACE:
                    buttonSpace = false;
                    break;
                case SDLK_LSHIFT:
                    buttonShift = false;
                    break;
                }
            }

            else if (event.type == SDL_MOUSEBUTTONDOWN)
            {
                if (event.button.button == SDL_BUTTON_LEFT)
                {
                    ClickedOnObject(event.motion.x, event.motion.y);
                }
            }

            // Mouse movement, currently disabled
            // else if (event.type == SDL_MOUSEMOTION && SDL_GetRelativeMouseMode() && canMoveInScene)
            // {
            //     camera.OnMouseMoved(event.motion.xrel, event.motion.yrel);
            // }
            // else if (event.type == SDL_MOUSEBUTTONDOWN && canMoveInScene)
            // {
            //     if (event.button.button == SDL_BUTTON_LEFT && canMoveInScene)
            //     {
            //         SDL_SetRelativeMouseMode(SDL_TRUE);
            //     }
            //     if (event.button.button == SDL_BUTTON_RIGHT && canMoveInScene)
            //     {
            //         SDL_SetRelativeMouseMode(SDL_FALSE);
            //     }
            // }
        }

        // camera movement
        {
            if (buttonW)
            {
                camera.MoveFront(delta * cameraSpeed);
            }
            if (buttonS)
            {
                camera.MoveFront(-delta * cameraSpeed);
            }
            if (buttonA)
            {
                camera.MoveSideways(-delta * cameraSpeed);
            }
            if (buttonD)
            {
                camera.MoveSideways(delta * cameraSpeed);
            }
            if (buttonSpace)
            {
                camera.MoveUp(delta * cameraSpeed);
            }
            if (buttonShift)
            {
                camera.MoveUp(-delta * cameraSpeed);
            }
        }

        time += delta;
        // NOTE: first call new imgui frame, then create window(s), then render

        // (After event loop)
        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplSDL2_NewFrame();
        ImGui::NewFrame();

        if (ImGui::IsWindowFocused() || ImGui::IsWindowHovered() || ImGui::IsAnyItemActive() || ImGui::IsAnyItemFocused())
        {
            canMoveInScene = false;
        }
        else
        {
            canMoveInScene = true;
        }

        // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
        if (show_demo_window)
        {
            ImGui::ShowDemoWindow(&show_demo_window);
        }

        // toggleable bools window
        {
            ImGui::Begin("Bools");

            ImGui::Checkbox("Show Demo Window", &show_demo_window);
            ImGui::Checkbox("Show Inspector Window", &showInspectorWindow);
            ImGui::Checkbox("Show Model Importer Window", &showModelConverterWindow);

            ImGui::End();
        }

        if (showInspectorWindow)
        {
            ShowInspectorWindow();
        }
        if (showModelConverterWindow)
        {
            ShowModelConverterWindow(modelExporter);
        }

        ShowFileExplorerWindow(&textureShader);

        // set clear color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // render/do other opengl stuff
        vertexBuffer.Bind();
        indexBuffer.Bind();
        textureShader.Bind();

        camera.Update();

        // do with model
        // levelModel = glm::rotate(levelModel, 1.0f * delta, glm::vec3(0, 1, 0));
        // levelModelViewProj = camera.GetViewProj() * levelModel;

        for (size_t i = 0; i < meshes.size(); i++)
        {
            // meshes.at(i).second.model = glm::rotate(meshes.at(i).second.model, 1.0f * delta, glm::vec3(0, 1, 0));
            meshes.at(i).second.modelViewProj = camera.GetViewProj() * meshes.at(i).second.model;

            glm::mat4 modelView = camera.GetView() * meshes.at(i).second.model;
            glm::mat4 invModelView = glm::transpose(glm::inverse(modelView));

            glUniformMatrix4fv(textureModelViewLocation, 1, GL_FALSE, &modelView[0][0]);
            glUniformMatrix4fv(textureInvModelViewLocation, 1, GL_FALSE, &invModelView[0][0]);
            glUniformMatrix4fv(textureViewProjMatrixUniformLocation, 1, GL_FALSE, &meshes.at(i).second.modelViewProj[0][0]);

            /*glUniform3fv(diffuseLocation, 1, &mesh.material.diffuse[0]);
            glUniform3fv(specularLocation, 1, &mesh.material.specular[0]);
            glUniform3fv(emissiveLocation, 1, &mesh.material.emissive[0]);
            glUniform1f(shininessLocation, mesh.material.shininess);*/

            // glUniform1i(textureUniformLocation, 0);

            // glActiveTexture(GL_TEXTURE0);
            // glBindTexture(GL_TEXTURE_2D, boundTexture);

            // glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, 0);

            // for (size_t i = 0; i < meshes.size(); i++)
            // {
            //     meshes.at(i).second.Render();
            // }

            meshes.at(i).second.Render();
            // meshes.at(i).second.Render(meshes.at(i).second.numIndices, 37);
            // std::cout << (void *)(36 * sizeof(uint64_t)) << "\n";
            // glDrawElements(GL_TRIANGLES, meshes.at(i).second.numIndices, GL_UNSIGNED_INT, (void *)(meshes.at(i).second.numIndicesOffset * sizeof(GLuint)));
        }

        textureShader.Unbind();
        indexBuffer.Unbind();
        vertexBuffer.Unbind();

        // imgui rendering
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        SDL_GL_SwapWindow(window);

        // FPS
        uint64_t endCounter = SDL_GetPerformanceCounter();
        uint64_t counterElapsed = endCounter - lastCounter;
        delta = ((float)counterElapsed) / (float)perfCounterFrequency;
        uint32_t FPS = (uint32_t)((float)perfCounterFrequency / (float)counterElapsed);
        lastCounter = endCounter;
    }

    // glDeleteTextures(1, &materialGrid.diffuseMap);

    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();

    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
