#pragma once

#include "camera.hpp"

namespace OGLE
{
    class FloatingCamera : public Camera
    {
    public:
        FloatingCamera(float fov, float width, float height);
        ~FloatingCamera();

        void Update() override;
        void OnMouseMoved(float xRel, float yRel);
        void MoveFront(float amount);
        void MoveSideways(float amount);
        void MoveUp(float amount);

    protected:
        float yaw;
        float pitch;
        glm::vec3 lookAt;
        const float mouseSensitivity = 0.3f;
        glm::vec3 up;
    };
}