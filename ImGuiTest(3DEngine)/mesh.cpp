#include "mesh.hpp"

namespace OGLE
{
    Mesh::Mesh(Shader *shader)
    {
        // std::cout << saveLocation << "/" << name << std::endl;
        // this->materialLocation = materialLocation;
        diffuseLocation = glGetUniformLocation(shader->GetShaderId(), "u_diffuse");
        specularLocation = glGetUniformLocation(shader->GetShaderId(), "u_specular");
        emissiveLocation = glGetUniformLocation(shader->GetShaderId(), "u_emissive");
        shininessLocation = glGetUniformLocation(shader->GetShaderId(), "u_shininess");
    }

    Mesh::~Mesh()
    {
    }

    void Mesh::LoadMaterial(Material *material)
    {
        this->material = material;
            // std::pair<std::string, Material> pair{chosenLocation, material};
            // materials.push_back(pair);
            // Debug
            // for (size_t i = 0; i < tempMaterials.size(); i++)
            // {
            //     std::cout << tempMaterials[i].first << " " << tempMaterials[i].second.diffuse.x << std::endl;
            // }
    }

    // just for testing
    void Mesh::ChangeMaterialLocation(std::string materialLocation)
    {
        this->materialLocation = materialLocation;
    }

    // NOTE: the correct shader needs to be bound before rendering. the mesh does'nt take care of that
    void Mesh::Render()
    {
        glUniform3fv(diffuseLocation, 1, &material->diffuse[0]);
        glUniform3fv(specularLocation, 1, &material->specular[0]);
        glUniform3fv(emissiveLocation, 1, &material->emissive[0]);
        glUniform1f(shininessLocation, material->shininess);

        // glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, (void *)(offset * sizeof(GLuint)));
        glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, (void *)(numIndicesOffset * sizeof(GLuint)));
    }
}