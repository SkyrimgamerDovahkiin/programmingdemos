#include "floating_camera.hpp"

namespace OGLE
{
    FloatingCamera::FloatingCamera(float fov, float width, float height) : Camera(fov, width, height)
    {
        up = glm::vec3(0.0f, 1.0f, 0.0f);
        yaw = -90.0f;
        pitch = 0.0f;
        OnMouseMoved(0.0f, 0.0f);
        Update();
    }

    FloatingCamera::~FloatingCamera()
    {
    }

    void FloatingCamera::OnMouseMoved(float xRel, float yRel)
    {
        yaw += xRel * mouseSensitivity;
        pitch -= yRel * mouseSensitivity;
        if (pitch > 89.0f)
            pitch = 89.0f;
        if (pitch < -89.0f)
            pitch = -89.0f;

        glm::vec3 front;
        front.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
        front.y = sin(glm::radians(pitch));
        front.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
        lookAt = glm::normalize(front);
        Update();
    }

    void FloatingCamera::Update()
    {
        view = glm::lookAt(position, position + lookAt, up);
        viewProj = projection * view;
    }

    void FloatingCamera::MoveFront(float amount)
    {
        Translate(glm::normalize(glm::vec3(1.0f, 0.0f, 1.0f) * lookAt) * amount);
        Update();
    }

    void FloatingCamera::MoveSideways(float amount)
    {
        Translate(glm::normalize(glm::cross(lookAt, up)) * amount);
        Update();
    }

    void FloatingCamera::MoveUp(float amount)
    {
        Translate(up * amount);
    }
}