#version 330 core

layout(location = 0) out vec4 f_color;

in vec3 v_position;
in vec3 v_normal;
in vec2 v_texCoord;
in vec4 v_color;

uniform sampler2D u_texture;
uniform vec2 u_texScale;

uniform vec3 u_diffuse;
uniform vec3 u_specular;
uniform vec3 u_emissive;
uniform float u_shininess;

void main()
{
    // Vector from fragment to camera
    vec3 view = normalize(-v_position);

    // light vector, make value changeable with imgui later
    vec3 light = normalize(vec3(1.0f, 1.0f, 1.0f));

    // normalized normal vector
    vec3 normal = normalize(v_normal);

    // reflection vector
    vec3 reflection = reflect(-light, normal);

    // light components
    vec3 ambient = u_diffuse * 0.2f;
    vec3 diffuse = max(dot(normal, light), 0.0f) * u_diffuse;
    vec3 specular = pow(max(dot(reflection, view), 0.0f), u_shininess) * u_specular;

    // multiply texture with scale, currently not used
    vec2 customTexCoords = vec2(v_texCoord.x * u_texScale.x, v_texCoord.y * u_texScale.y);
    vec4 texColor = texture(u_texture, customTexCoords);

    f_color = vec4(ambient + diffuse + specular + u_emissive, 1.0f);

    // multiply texture color with light, should work, currently not used
    // f_color = texColor * vec4(ambient + diffuse + specular, 1.0f);
}