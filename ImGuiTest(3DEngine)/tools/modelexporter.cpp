#include "modelexporter.hpp"

ModelExporter::ModelExporter()
{
}

ModelExporter::~ModelExporter()
{
}

void ModelExporter::ProcessMesh(aiMesh *mesh, const aiScene *scene)
{
    for (unsigned int i = 0; i < mesh->mNumVertices; i++)
    {
        Position position;
        position.x = mesh->mVertices[i].x;
        position.y = mesh->mVertices[i].y;
        position.z = mesh->mVertices[i].z;
        positions.push_back(position);

        Position normal;
        normal.x = mesh->mNormals[i].x;
        normal.y = mesh->mNormals[i].y;
        normal.z = mesh->mNormals[i].z;
        normals.push_back(normal);

        // my code
        UV uv;
        assert(mesh->mNumUVComponents > (unsigned int *)0);
        uv.x = mesh->mTextureCoords[0][i].x;
        uv.y = mesh->mTextureCoords[0][i].y;
        uvs.push_back(uv);
    }

    for (unsigned int i = 0; i < mesh->mNumFaces; i++)
    {
        aiFace face = mesh->mFaces[i];
        assert(face.mNumIndices == 3);

        for (unsigned int j = 0; j < face.mNumIndices; j++)
        {
            indices.push_back(face.mIndices[j]);
        }
    }
}

void ModelExporter::ProcessNode(aiNode *node, const aiScene *scene)
{
    for (unsigned int i = 0; i < node->mNumMeshes; i++)
    {
        aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
        ProcessMesh(mesh, scene);
    }

    for (unsigned int i = 0; i < node->mNumChildren; i++)
    {
        ProcessNode(node->mChildren[i], scene);
    }
}

void ModelExporter::Convert(std::string modelName, std::string outputName)
{
    // select the correct directory
    std::string curDir = std::filesystem::current_path();
    std::string modelDir = curDir + "/Assets/models/" + modelName;
    std::string outputDir = curDir + "/Assets/models/" + outputName + ".ogle";

    // std::cout << modelDir << "\n";
    // std::cout << outputDir << "\n";

    // when no arguments, give error and return
    if (modelName.empty() || outputName.empty())
    {
        std::cout << "No model directory or output directory specified!" << std::endl;
        return;
    }

    uint flags = aiProcess_Triangulate |
                 aiProcess_GenNormals |
                 aiProcess_OptimizeMeshes |
                 aiProcess_OptimizeGraph |
                 aiProcess_JoinIdenticalVertices |
                 aiProcess_ImproveCacheLocality;

    Assimp::Importer importer;

    const aiScene *scene = importer.ReadFile(modelDir, flags);

    if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
    {
        std::cout << "Error while loading model with assimp: " << importer.GetErrorString() << std::endl;
    }

    ProcessNode(scene->mRootNode, scene);

    std::ofstream output(outputDir, std::ios::out | std::ios::binary);
    std::cout << "Writing ogle file..." << std::endl; // maybe make progressbar with imgui?

    uint64_t numVertices = positions.size();
    uint64_t numIndices = indices.size();

    output.write((char *)&numVertices, sizeof(uint64_t));
    output.write((char *)&numIndices, sizeof(uint64_t));

    for (uint64_t i = 0; i < numVertices; i++)
    {
        output.write((char *)&positions[i].x, sizeof(float));
        output.write((char *)&positions[i].y, sizeof(float));
        output.write((char *)&positions[i].z, sizeof(float));

        output.write((char *)&normals[i].x, sizeof(float));
        output.write((char *)&normals[i].y, sizeof(float));
        output.write((char *)&normals[i].z, sizeof(float));

        // my code
        output.write((char *)&uvs[i].x, sizeof(float));
        output.write((char *)&uvs[i].y, sizeof(float));
    }

    for (uint64_t i = 0; i < numIndices; i++)
    {
        output.write((char *)&indices[i], sizeof(uint32_t));
    }

    output.close();
}