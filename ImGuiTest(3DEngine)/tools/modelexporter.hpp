#include <iostream>
#include <string>
#include <vector>
#include <cassert>
#include <filesystem>
#include <fstream>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

struct Position
{
    float x, y, z;
};

// my code
struct UV
{
    float x, y;
};

class ModelExporter
{
public:
    ModelExporter();
    ~ModelExporter();
    void Convert(std::string modelDir, std::string outputDir);

private:
    std::vector<Position> positions;
    std::vector<Position> normals;
    std::vector<uint32_t> indices;
    std::vector<UV> uvs;
    void ProcessMesh(aiMesh *mesh, const aiScene *scene);
    void ProcessNode(aiNode *node, const aiScene *scene);
};
