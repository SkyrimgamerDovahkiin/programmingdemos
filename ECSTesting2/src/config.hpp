#pragma once

// std
#include <iostream>
#include <cmath>
#include <vector>
#include <list>
#include <fstream>
#include <ctime>
#include <algorithm>
#include <deque>
#include <optional>

// glm
#include <glm/glm.hpp>