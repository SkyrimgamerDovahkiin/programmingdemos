#include "config.hpp"
#include "structs2ndNormalForm.hpp"

std::unordered_map<Weapon, Weapon> weapons;

std::string PrintVector2(glm::vec2 v)
{
    return std::to_string(v.x) + " v.x\n" + std::to_string(v.y) + " v.y\n";
}

void CreateWeapon(WeaponType t, WeaponQuality q, uint32_t d, WeaponDamageType dt)
{
    Weapon w;
    w.type = t;
    w.quality = q;
    w.damage = d;
    w.damageType = dt;
    auto p = std::pair<Weapon, Weapon>(w, w);
    weapons.emplace(p);
}

std::string PrintWeapon(Weapon w)
{
    auto it = weapons.find(w);
    if (it != weapons.end())
    {
        std::string s;
        s += "Weapon:\n";
        s += std::to_string(it->second.type)
        + " weapon type\n"
        + std::to_string(it->second.quality)
        + " quality\n"
        + std::to_string(it->second.damage)
        + " damage\n"
        + std::to_string((WeaponDamageType)it->second.damageType)
        + " damage type\n";
        return s;
    }
    return std::string();
}

int main(int argc, char **argv)
{
    CreateWeapon(WeaponType::AXE, WeaponQuality::USELESS, 1, WeaponDamageType::BLADE);
    CreateWeapon(WeaponType::AXE, WeaponQuality::AVERAGE, 1, WeaponDamageType::BLADE);
    CreateWeapon(WeaponType::WARHAMMER, WeaponQuality::AVERAGE, 5, WeaponDamageType::BLUNT);
    CreateWeapon(WeaponType::SWORD, WeaponQuality::MASTER, 10, WeaponDamageType::BLADE);
    CreateWeapon(WeaponType::BOW, WeaponQuality::LEGENDARY, 20, WeaponDamageType::RANGED);

    // debug test
    Weapon w;
    w.type = WeaponType::AXE;
    w.quality = WeaponQuality::AVERAGE;
    std::cout << PrintWeapon(w) << std::endl;

    return 0;
}