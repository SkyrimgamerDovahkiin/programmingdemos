#include "config.hpp"

enum WeaponType
{
    SWORD,
    HAMMER,
    WARHAMMER,
    AXE,
    WARAXE,
    BOW,
};

enum WeaponQuality
{
    USELESS,
    AVERAGE,
    MASTER,
    LEGENDARY,
};

enum WeaponDamageType
{
    BLADE,
    BLUNT,
    RANGED,
};

struct Weapon
{
    WeaponType type;
    WeaponQuality quality;
    uint32_t damage;
    WeaponDamageType damageType;

    bool operator==(const Weapon &other) const
    {
        return other.type == type && other.quality == quality;
    }
};

template <>
struct std::hash<Weapon>
{
    std::size_t operator()(const Weapon &k) const
    {
        using std::hash;
        using std::size_t;
        using std::string;

        auto t = std::to_string(k.type) + std::to_string(k.quality);

        return std::hash<std::string>()(t);
    }
};