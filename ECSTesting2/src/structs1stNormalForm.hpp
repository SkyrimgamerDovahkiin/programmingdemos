#include "config.hpp"

struct Mesh
{
    unsigned int meshID;
    std::string meshName;
};

struct Texture
{
    unsigned int textureID;
    std::string textureName;
};

enum PickupType
{
    KEY,
    POTION,
    ARMOR,
};

enum ColorTint
{
    NIL, // default
    RED,
    GREEN,
    BLUE,
};

struct Pickup
{
    unsigned int pickupID;
    unsigned int meshID;
    unsigned int textureID;
    PickupType pickupType;
};

struct PickupTint
{
    ColorTint colorTint = ColorTint::RED;
};

struct Room
{
    unsigned int roomID;
    unsigned int meshID;
    unsigned int textureID;
    glm::vec2 position;
    bool isStart;
    bool isExit;
};

struct PickupInstance
{
    unsigned int roomID;
    unsigned int pickupID;

    bool operator==(const PickupInstance &other) const
    {
        return other.roomID == roomID && other.pickupID == pickupID;
    }
};

struct Door
{
    unsigned int fromRoomID;
    unsigned int toRoomID;

    bool operator==(const Door &other) const
    {
        return other.fromRoomID == fromRoomID && other.toRoomID == toRoomID;
    }
};

struct LockedDoor
{
    unsigned int fromRoomID;
    unsigned int toRoomID;
    unsigned int keyPickupID; // needs to be unique

    bool operator==(const LockedDoor &other) const
    {
        return other.fromRoomID == fromRoomID && other.toRoomID == toRoomID;
    }
};

template <>
struct std::hash<Door>
{
    std::size_t operator()(const Door &k) const
    {
        using std::hash;
        using std::size_t;
        using std::string;

        auto t = std::to_string(k.fromRoomID) + std::to_string(k.toRoomID);

        return std::hash<std::string>()(t);
    }
};

template <>
struct std::hash<PickupInstance>
{
    std::size_t operator()(const PickupInstance &k) const
    {
        using std::hash;
        using std::size_t;
        using std::string;

        auto t = std::to_string(k.pickupID) + std::to_string(k.roomID);

        return std::hash<std::string>()(t);
    }
};

template <>
struct std::hash<LockedDoor>
{
    std::size_t operator()(const LockedDoor &k) const
    {
        using std::hash;
        using std::size_t;
        using std::string;

        auto t = std::to_string(k.fromRoomID) + std::to_string(k.toRoomID);

        return std::hash<std::string>()(t);
    }
};