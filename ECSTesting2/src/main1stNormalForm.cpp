#include "config.hpp"
#include "structs1stNormalForm.hpp"

std::unordered_map<unsigned int, Mesh> meshes;
std::unordered_map<unsigned int, Texture> textures;
std::unordered_map<unsigned int, Pickup> pickups;
std::unordered_map<unsigned int, PickupTint> pickupTints;
std::unordered_map<unsigned int, Room> rooms;
std::unordered_map<PickupInstance, PickupInstance> pickupInstances;
std::unordered_map<Door, Door> doors;
std::unordered_map<LockedDoor, LockedDoor> lockedDoors;

std::string PrintVector2(glm::vec2 v)
{
    return std::to_string(v.x) + " v.x\n" + std::to_string(v.y) + " v.y\n";
}

std::string PrintPickupVector(std::vector<Pickup> v)
{
    std::string s;

    for (auto &p : v)
    {
        s += "Pickup:\n";
        s += std::to_string(p.pickupID) + " pickupID\n" + std::to_string(p.meshID) + " meshID\n" + std::to_string(p.textureID) + " textureID\n" + std::to_string(p.pickupType) + " pickupType\n"
             // + std::to_string(p.colorTint)
             + " colorTint\n\n";
    }
    return s;
}

std::string PrintRoomVector(int id)
{
    auto it = rooms.find(id);
    if (it != rooms.end())
    {
        std::string s;
        s += "Room:\n";
        s += std::to_string(it->second.roomID) + " roomID\n" + std::to_string(it->second.meshID) + " meshID\n" + std::to_string(it->second.textureID) + " textureID\n" + PrintVector2(it->second.position) + "\n" + std::to_string(it->second.isStart) + " isStart\n" + std::to_string(it->second.isExit) + " isExit\n";
        // + "\nHas Pickup(s):\n"
        // + PrintPickupVector(r.pickups) + "\n";
        return s;
    }
    return std::string();
}

void CreateMesh(const unsigned int id, const std::string name)
{
    Mesh mesh;
    mesh.meshID = id;
    mesh.meshName = name;
    auto p = std::pair<unsigned int, Mesh>(mesh.meshID, mesh);
    meshes.emplace(p);
}

void CreateTexture(const unsigned int id, const std::string name)
{
    Texture tex;
    tex.textureID = id;
    tex.textureName = name;
    auto p = std::pair<unsigned int, Texture>(tex.textureID, tex);
    textures.emplace(p);
}

void CreatePickup(uint32_t id, uint32_t meshID, uint32_t textureID, PickupType type, std::optional<ColorTint> tint)
{
    Pickup pickup;
    pickup.pickupID = id;
    pickup.meshID = meshID;
    pickup.textureID = textureID;
    pickup.pickupType = type;

    auto p = std::pair<unsigned int, Pickup>(pickup.pickupID, pickup);
    pickups.emplace(p);

    // when tint has value, we create a pickup tint
    if (tint.has_value())
    {
        PickupTint t;
        t.colorTint = tint.value();
        auto p = std::pair<unsigned int, PickupTint>(pickup.pickupID, t);
        pickupTints.emplace(p);
    }
}

void CreateRoom(uint32_t id, uint32_t meshID, uint32_t textureID, glm::vec2 pos, bool isStart, bool isExit)
{
    Room room;
    room.roomID = id;
    room.meshID = meshID;
    room.textureID = textureID;
    room.position = pos;
    room.isStart = isStart;
    room.isExit = isExit;
    auto p = std::pair<unsigned int, Room>(room.roomID, room);
    rooms.emplace(p);
}

void CreatePickupInstance(uint32_t roomID, uint32_t pickupID)
{
    PickupInstance pi;
    pi.roomID = roomID;
    pi.pickupID = pickupID;
    auto p = std::pair<PickupInstance, PickupInstance>(pi, pi);
    pickupInstances.emplace(p);
}

void CreateDoor(uint32_t fromRoomID, uint32_t toRoomID)
{
    Door door;
    door.fromRoomID = fromRoomID;
    door.toRoomID = toRoomID;
    auto p = std::pair<Door, Door>(door, door);
    doors.emplace(p);
}

void CreateLockedDoor(uint32_t fromRoomID, uint32_t toRoomID, uint32_t keyID)
{
    LockedDoor door;
    door.fromRoomID = fromRoomID;
    door.toRoomID = toRoomID;
    door.keyPickupID = keyID;
    auto p = std::pair<LockedDoor, LockedDoor>(door, door);
    lockedDoors.emplace(p);
}

int main(int argc, char **argv)
{
    CreateMesh(0, "meshtest");
    CreateMesh(1, "meshgreat");
    CreateMesh(2, "meshgreater");
    CreateMesh(3, "meshgreaterest");

    CreateTexture(0, "texturetest");
    CreateTexture(1, "texturegreat");
    CreateTexture(2, "texturegreater");
    CreateTexture(3, "texturegreaterest");

    CreatePickup(0, 0, 0, PickupType::POTION, ColorTint::RED);
    CreatePickup(1, 1, 1, PickupType::ARMOR, ColorTint::GREEN);
    CreatePickup(2, 2, 3, PickupType::ARMOR, ColorTint::BLUE);
    CreatePickup(3, 3, 3, PickupType::KEY, std::nullopt);

    CreateRoom(0, 3, 3, glm::vec2(0.0f), true, false); // start
    CreateRoom(1, 3, 3, glm::vec2(20.0f, 0.0f), false, false);
    CreateRoom(2, 3, 3, glm::vec2(40.0f, 0.0f), false, false);
    CreateRoom(3, 3, 3, glm::vec2(60.0f, 0.0f), false, true); // exit

    CreatePickupInstance(0, 1); // room 0 with armor with green tint
    CreatePickupInstance(0, 0); // room 0 with potion with red tint
    CreatePickupInstance(1, 2); // room 1 with armor with blue tint
    CreatePickupInstance(2, 3); // room 2 with key with no tint

    CreateDoor(0, 1);
    CreateDoor(0, 2); // goes to room with key
    CreateLockedDoor(2, 3, 3); // goes to exit room but needs pickup 3 (aka the key) to open

    std::cout << PrintRoomVector(1) << std::endl;

    LockedDoor door;
    door.fromRoomID = 2;
    door.toRoomID = 3;

    auto it = lockedDoors.find(door);
    if (it != lockedDoors.end())
    {
        std::cout << lockedDoors.size() << "found!\n";
    }

    return 0;
}