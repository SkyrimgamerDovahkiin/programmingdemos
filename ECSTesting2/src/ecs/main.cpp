#include "config.hpp"

int main()
{
    // basic SDL window
    SDL_Init(SDL_INIT_VIDEO);
    SDL_Window *window = SDL_CreateWindow("Hello", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 600, 600, SDL_WINDOW_SHOWN);
    SDL_Event e;

    // delta time stuff
    uint64_t perfCounterFrequency = SDL_GetPerformanceFrequency();
    uint64_t lastCounter = SDL_GetPerformanceCounter();
    float delta = 0.0f;
    float time = 0.0f;
    uint32_t FPS = 0;

    bool close = false;

    while (!close)
    {
        while (SDL_PollEvent(&e))
        {
            if (e.type == SDL_QUIT || (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE))
                close = true;
        }

        time += delta;

        // Add test systems here
        // physicsSystem->Update(delta);

        uint64_t endCounter = SDL_GetPerformanceCounter();
        uint64_t counterElapsed = endCounter - lastCounter;
        delta = ((float)counterElapsed) / (float)perfCounterFrequency;
        FPS = (uint32_t)((float)perfCounterFrequency / (float)counterElapsed);
        lastCounter = endCounter;

        // do other stuff
    }

    {
        SDL_DestroyWindow(window);
        SDL_Quit();
    }
    return 0;
}