#pragma once

#include "config.hpp"

class EntityManager
{
public:
    EntityManager()
    {
    }

    Entity CreateEntity()
    {
        curEntities++;
        return curEntities;
    }

    void SetSignature(Entity e, Signature signature)
    {
        // Put this entity's signature into the map
        mSignatures.emplace(e, signature);
    }

    Signature GetSignature(Entity e)
    {
        // Get this entity's signature from the array
        return mSignatures[e];
    }

private:
    Entity curEntities = -1;

    // unordered map of signatures where the index corresponds to the entity ID
    std::unordered_map<Entity, Signature> mSignatures;
};