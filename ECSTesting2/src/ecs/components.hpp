#pragma once

#include "config.hpp"

struct NameComponent
{
    std::string name;
};

struct TransformComponent
{
    glm::vec3 position;
    glm::vec3 rotationEulers;
    glm::vec3 scale;
};

struct AttributesComponent
{
    uint16_t health;
    uint16_t stamina;
    uint16_t mana;
};