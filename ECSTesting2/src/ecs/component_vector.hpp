#pragma once

#include "config.hpp"

class IComponentVector
{
public:
    virtual ~IComponentVector() = default;
    // virtual void EntityDestroyed(Entity entity) = 0;
};

template <typename T>
class ComponentVector : public IComponentVector
{
public:
    void InsertData(Entity e, T component)
    {
        assert(mEntityToIndexMap.find(e) == mEntityToIndexMap.end() && "Component added to same entity more than once.");

        // Put new entry at end and update the maps
        size_t newIndex = mSize;
        mEntityToIndexMap.emplace(e, newIndex);
        mIndexToEntityMap.emplace(newIndex, e);
        mComponentMap.emplace(newIndex, component);
        ++mSize;
    }

    void RemoveData(Entity e)
    {
        assert(mEntityToIndexMap.find(e) != mEntityToIndexMap.end() && "Removing non-existent component.");

        // Copy element at end into deleted element's place to maintain density
        size_t indexOfRemovedEntity = mEntityToIndexMap[e];
        size_t indexOfLastElement = mSize - 1;
        mComponentMap[indexOfRemovedEntity] = mComponentMap[indexOfLastElement];

        // Update map to point to moved spot
        Entity entityOfLastElement = mIndexToEntityMap[indexOfLastElement];
        mEntityToIndexMap[entityOfLastElement] = indexOfRemovedEntity;
        mIndexToEntityMap[indexOfRemovedEntity] = entityOfLastElement;

        mEntityToIndexMap.erase(e);
        mIndexToEntityMap.erase(indexOfLastElement);

        --mSize;
    }

    T &GetData(Entity e)
    {
        assert(mEntityToIndexMap.find(e) != mEntityToIndexMap.end() && "Retrieving non-existent component.");

        // Return a reference to the entity's component
        return mComponentArray[mEntityToIndexMap[e]];
    }

private:
    // The packed unordered map of components (of generic type T),
    // can only be max number of entities size
    std::unordered_map<size_t, T> mComponentMap;

    // Map from an entity ID to an array index.
    std::unordered_map<Entity, size_t> mEntityToIndexMap;

    // Map from an array index to an entity ID.
    std::unordered_map<size_t, Entity> mIndexToEntityMap;

    // Total size of valid entries in the array.
    size_t mSize;
};