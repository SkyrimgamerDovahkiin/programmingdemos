#pragma once

// std
#include <iostream>
#include <cmath>
#include <vector>
#include <list>
#include <fstream>
#include <ctime>
#include <algorithm>
#include <deque>
#include <optional>
#include <unordered_set>
#include <bitset>
#include <memory>
#include <set>
#include <random>

// GLEW/SDL
#include <GL/glew.h>
#include <SDL2/SDL.h>

// glm
#include <glm/glm.hpp>

typedef uint32_t Entity; // an unique id for a Entity
typedef uint32_t ComponentType; // an unique id for a Component

const ComponentType MAX_COMPONENTS = 256; // used because we are not using hundreds of components for now
typedef std::bitset<MAX_COMPONENTS> Signature;