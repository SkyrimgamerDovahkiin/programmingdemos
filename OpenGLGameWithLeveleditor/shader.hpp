#pragma once
#include <GL/glew.h>
#include <string>

#include "defines.hpp"

namespace OGLE
{
    class Shader
    {
    public:
        Shader(const char* vertexShaderFilename, const char* fragmentShaderFilename);
        ~Shader();
        void Bind();
        void Unbind();
        GLuint GetShaderId();

    private:
        GLuint Compile(std::string shaderSource, GLenum type);
        std::string Parse(const char* filename);
        GLuint CreateShader(const char* vertexShaderFilename, const char* fragmentShaderFilename);

        GLuint shaderId;
    };
}