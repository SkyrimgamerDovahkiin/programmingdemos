#pragma once
#include <GL/glew.h>

#include "defines.hpp"

namespace OGLE
{
    class IndexBuffer
    {
    public:
        IndexBuffer(void *data, uint32_t numIndices, uint8_t elementSize);
        ~IndexBuffer();
        void Bind();
        void Unbind();

    private:
        GLuint bufferId;
    };
}