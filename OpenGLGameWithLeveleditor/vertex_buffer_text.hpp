#pragma once
#include <GL/glew.h>

#include "defines.hpp"

namespace OGLE
{
    class VertexBufferText
    {
    public:
        VertexBufferText();
        ~VertexBufferText();
        void Bind();
        void Unbind();

    private:
        GLuint bufferId;
        GLuint vao;
        uint32_t capacity = 20;
    };
}