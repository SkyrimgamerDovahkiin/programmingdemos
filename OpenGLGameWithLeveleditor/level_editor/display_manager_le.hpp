#pragma once

#include "config_le.hpp"

namespace LevelEditor
{
    // OpenGL Debug stuff
    void GLAPIENTRY OpenGLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam);
    
    class DisplayManager
    {
    public:
        DisplayManager();
        ~DisplayManager();

        SDL_Window *GetWindow() { return mWindow; };

        void Init(int screenWidth, int screenHeight, uint32_t flags = SDL_WINDOW_OPENGL, const char *name = "Level Editor");

    private:
        SDL_Window *mWindow;
        SDL_GLContext mGlContext;
    };
}