#include "defines_le.hpp"

std::vector<Tile> tiles = {
    {0, SubTexture{glm::vec2(0.0f, 0.0f), glm::vec2(20.0f, 20.0f), glm::vec2(600.0f, 0.0f)}},
    {1, SubTexture{glm::vec2(20.0f, 0.0f), glm::vec2(20.0f, 20.0f), glm::vec2(600.0f, 0.0f)}},
    {2, SubTexture{glm::vec2(40.0f, 0.0f), glm::vec2(20.0f, 20.0f), glm::vec2(600.0f, 0.0f)}},
    {3, SubTexture{glm::vec2(60.0f, 0.0f), glm::vec2(20.0f, 20.0f), glm::vec2(600.0f, 0.0f)}},
    {4, SubTexture{glm::vec2(80.0f, 0.0f), glm::vec2(20.0f, 20.0f), glm::vec2(600.0f, 0.0f)}},
    {5, SubTexture{glm::vec2(100.0f, 0.0f), glm::vec2(20.0f, 20.0f), glm::vec2(600.0f, 0.0f)}},
    {6, SubTexture{glm::vec2(120.0f, 0.0f), glm::vec2(20.0f, 20.0f), glm::vec2(600.0f, 0.0f)}},
    {7, SubTexture{glm::vec2(140.0f, 0.0f), glm::vec2(20.0f, 20.0f), glm::vec2(600.0f, 0.0f)}},
    {8, SubTexture{glm::vec2(160.0f, 0.0f), glm::vec2(20.0f, 20.0f), glm::vec2(600.0f, 0.0f)}},
    {9, SubTexture{glm::vec2(180.0f, 0.0f), glm::vec2(20.0f, 20.0f), glm::vec2(600.0f, 0.0f)}},
    {10, SubTexture{glm::vec2(200.0f, 0.0f), glm::vec2(20.0f, 20.0f), glm::vec2(600.0f, 0.0f)}},
    {11, SubTexture{glm::vec2(220.0f, 0.0f), glm::vec2(20.0f, 20.0f), glm::vec2(600.0f, 0.0f)}},
    {12, SubTexture{glm::vec2(240.0f, 0.0f), glm::vec2(20.0f, 20.0f), glm::vec2(600.0f, 0.0f)}},
};

// The reflected tile types
std::vector<std::string> tileNames =
    {
        "CORNER_TR",
        "CORNER_TL",
        "CORNER_BR",
        "CORNER_BL",
        "WALL_LEFT",
        "WALL_RIGHT",
        "WALL_BOTTOM",
        "WALL_TOP",
        "PLAYER",
        "ENEMY",
        "FOOD",
        "WAYPOINT",
        "ERASER"};