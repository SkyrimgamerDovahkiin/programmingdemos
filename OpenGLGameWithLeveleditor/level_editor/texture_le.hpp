#pragma once

#include "config_le.hpp"

namespace LevelEditor
{
    class Texture
    {
    public:
        GLuint texID;
        
        Texture(std::string location);
        ~Texture();

        void CreateTexture();

    private:
        std::string mLocation;
    };
}