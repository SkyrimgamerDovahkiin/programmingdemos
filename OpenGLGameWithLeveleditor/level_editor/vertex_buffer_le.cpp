#include "vertex_buffer_le.hpp"

namespace LevelEditor
{
    VertexBuffer::VertexBuffer(void *data, uint32_t numVertices)
    {
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);

        glGenBuffers(1, &bufferId);
        glBindBuffer(GL_ARRAY_BUFFER, bufferId);
        // glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(Vertex), NULL, GL_DYNAMIC_DRAW);
        glBufferData(GL_ARRAY_BUFFER, numVertices, NULL, GL_DYNAMIC_DRAW);
        // glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(Vertex), data, GL_DYNAMIC_DRAW);

        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(struct Vertex, position));
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(struct Vertex, texCoords));
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(struct Vertex, color));

        glBindVertexArray(0);
    }

    VertexBuffer::~VertexBuffer()
    {
        glDeleteBuffers(1, &bufferId);
        glDeleteVertexArrays(1, &vao);
    }

    void VertexBuffer::Bind()
    {
        glBindVertexArray(vao);
        glBindBuffer(GL_ARRAY_BUFFER, bufferId);
    }

    void VertexBuffer::Unbind()
    {
        glBindVertexArray(0);
    }
}