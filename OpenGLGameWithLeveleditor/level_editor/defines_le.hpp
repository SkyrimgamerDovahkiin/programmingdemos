#pragma once

#include "config_le.hpp"

// a vertex
struct Vertex
{
    glm::vec3 position;
    glm::vec2 texCoords;
    glm::vec4 color;
};

// The subtexture, aka a rect from the texture atlas
struct SubTexture
{
    glm::vec2 position = {0, 0};
    glm::vec2 spriteSize = {20, 20};
    glm::vec2 textureSize = {600, 20};
};

struct Tile
{
    int tileIdx = 0;
    SubTexture subTex;
};

// The different tile sprites
extern std::vector<Tile> tiles;

// The reflected tile types
extern std::vector<std::string> tileNames;

struct Rectangle
{
    int x;           // Rectangle position x
    int y;           // Rectangle position y
    int width;       // Rectangle width
    int height;      // Rectangle height
    glm::vec3 color; // Rectangle Color

    // the four cornes of the rect for collision checking
    glm::vec2 tl = {x, y};
    glm::vec2 br = {x + width, y + height};
    glm::vec2 tr = {x + width, y};
    glm::vec2 bl = {x, y + height};
};

// Collision check for cursor
inline bool RectangleIntersect(const Rectangle &r1, const Rectangle &r2)
{
    if (r1.tl == r2.tl && r1.br == r2.br && r1.tr == r2.tr && r1.bl == r2.bl)
    {
        return true;
    }
    return false;
}