#pragma once

#include "config_le.hpp"

namespace LevelEditor
{
    class IndexBuffer
    {
    public:
        IndexBuffer() = default;
        IndexBuffer(void *data, uint32_t numIndices, uint8_t elementSize);
        ~IndexBuffer();
        void Bind();
        void Unbind();

    private:
        GLuint bufferId;
    };
}