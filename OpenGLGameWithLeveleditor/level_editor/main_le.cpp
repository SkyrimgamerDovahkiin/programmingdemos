// custom
#include "config_le.hpp" // everything else
#include "display_manager_le.hpp"
#include "renderer_le.hpp"
#include "vertex_buffer_le.hpp"
#include "index_buffer_le.hpp"
#include "shader_le.hpp"
#include "camera_le.hpp"
#include "create_rect_le.hpp"
#include "texture_le.hpp"

using LevelEditor::Camera,
    LevelEditor::DisplayManager,
    LevelEditor::Renderer,
    LevelEditor::VertexBuffer,
    LevelEditor::IndexBuffer,
    LevelEditor::Shader,
    LevelEditor::Texture;

// void GLAPIENTRY OpenGLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
// {
//     std::cout << "[OpenGL Error] " << message << std::endl;
// }

// vertices and indices stuff
std::vector<Vertex> vertices;
uint32_t numVertices = 0;
std::vector<uint32_t> indices;
uint32_t numIndices = 0;
uint32_t indicesIndex = 0;
uint32_t maxVertices = 16000000; // 16 MB Vertex Buffer size

// cursor
glm::mat4 cursorModel = glm::mat4(1.0f);
glm::mat4 cursorModelViewProj = glm::mat4(1.0f);

// level
glm::mat4 levelModel = glm::mat4(1.0f);
glm::mat4 levelModelViewProj = glm::mat4(1.0f);

int screenWidth = 600, screenHeight = 600;
int levelWidth = 2400, levelHeight = 2400;

Tile curTile = tiles.at(0);
int lastTileType = curTile.tileIdx;
int curTileType = curTile.tileIdx;

// the rect that will be placed and pushed back in the "placedRects" vector
Rectangle rectToPlace = {0, 0, 20, 20, glm::vec3(1.0f, 1.0f, 1.0f)};

// the placed rects aka the level
std::vector<Rectangle> placedRects;

// the tile indices
std::vector<uint32_t> tileIndices;

void MoveScreen(Camera &camera, Rectangle &cursor, SDL_Event &event)
{
    // camera translation
    if (camera.GetPosition().x != levelWidth && event.key.keysym.sym == SDLK_d) // right side of screen
    {
        std::cout << " moving Right!\n";
        camera.Translate(glm::vec3(20.0f, 0.0f, 0.0f));
        camera.Update();
    }
    else if (camera.GetPosition().x != 0.0f && event.key.keysym.sym == SDLK_a) // left side of screen
    {
        std::cout << " moving Left!\n";
        camera.Translate(glm::vec3(-20.0f, 0.0f, 0.0f));
        camera.Update();
    }
    else if (camera.GetPosition().y != 0.0f && event.key.keysym.sym == SDLK_w) // top of screen
    {
        std::cout << " moving Up!\n";
        camera.Translate(glm::vec3(0.0f, -20.0f, 0.0f));
        camera.Update();
    }
    else if (camera.GetPosition().y != levelHeight && event.key.keysym.sym == SDLK_s) // bottom of screen
    {
        std::cout << " moving Down!\n";
        camera.Translate(glm::vec3(0.0f, 20.0f, 0.0f));
        camera.Update();
    }

    // calculation needed so the cursor moves on grid
    int remainderX = cursor.x % 20;
    int realX = cursor.x + 20 - remainderX - 20;

    int remainderY = cursor.y % 20;
    int realY = cursor.y + 20 - remainderY - 20;

    // cursor translation
    cursorModelViewProj = camera.GetViewProj() * cursorModel;
    cursorModelViewProj = glm::translate(cursorModelViewProj, glm::vec3(realX + camera.GetPosition().x, realY + camera.GetPosition().y, 0.0f));

    levelModelViewProj = camera.GetViewProj() * levelModel;
}

bool canPlace(Rectangle realCursor)
{
    for (size_t i = 0; i < placedRects.size(); i++)
    {
        if (RectangleIntersect(realCursor, placedRects.at(i)))
        {
            return false;
        }
    }
    return true;
}

void PlaceTile(Tile curTile, Rectangle cursor, Camera &camera)
{
    std::vector<Vertex> verticesToPlace;
    std::vector<uint32_t> indicesToPlace;

    uint32_t numVerticesToPlace = 0;
    uint32_t numIndicesToPlace = 0;

    verticesToPlace.clear();
    indicesToPlace.clear();

    // need to add the cameraPos here so that collision checking works
    // the "normal" cursor only goes between 0 and 600 (screen width/height)
    // and does not take the camera position into account
    int remainderX = cursor.x % 20;
    int realX = cursor.x + camera.GetPosition().x + 20 - remainderX - 20;

    int remainderY = cursor.y % 20;
    int realY = cursor.y + camera.GetPosition().y + 20 - remainderY - 20;

    // collision check to check if cursor is intersecting with rect. if it is, create a new rectangle
    Rectangle realCursor = {realX, realY, 20, 20, cursor.color};
    for (size_t i = 0; i < placedRects.size(); i++)
    {
        // when the rectangle is intersecting, create a new rectangle with correct cursor color and buffer it over the old one
        if (RectangleIntersect(realCursor, placedRects.at(i)))
        {
            // std::cout << " intersecting!\n";
            // NOTE: don't add the camCoords here because they were originally added when rect was placed
            // rectToPlace = {placedRects.at(i).x, placedRects.at(i).y, cursor.width, cursor.height, curTile.color};
            rectToPlace = {placedRects.at(i).x, placedRects.at(i).y, cursor.width, cursor.height, glm::vec3(1.0f)};
            placedRects.at(i) = rectToPlace;
            levelModelViewProj = camera.GetViewProj() * levelModel;

            std::vector<Vertex> vertices = {
                Vertex{
                    glm::vec3(rectToPlace.x, rectToPlace.y + rectToPlace.height, 0.0f),
                    glm::vec2(curTileType * 20.0f / 600.0f, (0.0f + 20.0f) / 20.0f),
                    glm::vec4(rectToPlace.color.x, rectToPlace.color.y, rectToPlace.color.z, 1.0f)}, // bottomLeft
                Vertex{
                    glm::vec3(rectToPlace.x, rectToPlace.y, 0.0f),
                    glm::vec2(curTileType * 20.0f / 600.0f, 0.0f / 20.0f),
                    glm::vec4(rectToPlace.color.x, rectToPlace.color.y, rectToPlace.color.z, 1.0f)}, // topLeft
                Vertex{
                    glm::vec3(rectToPlace.x + rectToPlace.width, rectToPlace.y + rectToPlace.height, 0.0f),
                    glm::vec2((curTileType * 20.0f + 20.0f) / 600.0f, (0.0f + 20.0f) / 20.0f),
                    glm::vec4(rectToPlace.color.x, rectToPlace.color.y, rectToPlace.color.z, 1.0f)}, // bottomRight
                Vertex{
                    glm::vec3(rectToPlace.x + rectToPlace.width, rectToPlace.y, 0.0f),
                    glm::vec2((curTileType * 20.0f + 20.0f) / 600.0f, 0.0f / 20.0f),
                    glm::vec4(rectToPlace.color.x, rectToPlace.color.y, rectToPlace.color.z, 1.0f)} // topRight
            };

            std::vector<uint32_t> indices = {
                (uint32_t)i * 4,
                (uint32_t)i * 4 + 1,
                (uint32_t)i * 4 + 2,
                (uint32_t)i * 4 + 1,
                (uint32_t)i * 4 + 2,
                (uint32_t)i * 4 + 3,
            };

            if (tileIndices.size() != 0)
            {
                tileIndices.at(i - 1) = curTileType;
            }

            glBufferSubData(GL_ARRAY_BUFFER, (i * 4) * sizeof(Vertex), 4 * sizeof(Vertex), vertices.data());
            glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, (i * 6) * sizeof(indices[0]), 6 * sizeof(indices[0]), indices.data());

            continue;
        }
    }
    if (canPlace(realCursor))
    {
        // rectToPlace = {realX, realY, cursor.width, cursor.height, curTile.color};glm::vec3(1.0f)
        rectToPlace = {realX, realY, cursor.width, cursor.height, glm::vec3(1.0f)};
        levelModelViewProj = camera.GetViewProj() * levelModel;
        placedRects.push_back(rectToPlace);
        CreateRect(rectToPlace, verticesToPlace, indicesToPlace, numVerticesToPlace, numIndicesToPlace, indicesIndex, curTileType * 20.0f, 0.0f, 600.0f, 20.0f, 20.0f, 20.0f);

        // subbuffer the rect
        glBufferSubData(GL_ARRAY_BUFFER, numVertices * sizeof(Vertex), 4 * sizeof(Vertex), verticesToPlace.data());
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(indicesToPlace[0]), 6 * sizeof(indicesToPlace[0]), indicesToPlace.data());

        numVertices = numVertices + numVerticesToPlace;
        numIndices = numIndices + numIndicesToPlace;

        tileIndices.push_back(curTileType);
    }

    for (size_t i = 0; i < verticesToPlace.size(); i++)
    {
        vertices.push_back(verticesToPlace.at(i));
    }

    for (size_t i = 0; i < indicesToPlace.size(); i++)
    {
        indices.push_back(indicesToPlace.at(i));
    }
}

// remove the last placed tile
void RemoveTile(Camera &camera)
{
    if (placedRects.size() > 1)
    {
        placedRects.pop_back();

        auto size = placedRects.size() - 1;

        levelModelViewProj = camera.GetViewProj() * levelModel;

        glBufferSubData(GL_ARRAY_BUFFER, (size * 4) * sizeof(Vertex), 4 * sizeof(Vertex), NULL);
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, (size * 6) * sizeof(indices[0]), 6 * sizeof(indices[0]), NULL);

        indicesIndex -= 4;
        numVertices -= 4;
        numIndices -= 6;
    }
}

// needed for comparison between 2 Vertices
bool operator==(const Vertex &v1, const Vertex &v2)
{
    // if (v1.position == v2.position && v1.color == v2.color && v1.texCoords == v2.texCoords)
    if (v1.position == v2.position)
    {
        return true;
    }
    return false;
}

// remove any tile when clicked on it
void RemoveAnyTile(Camera &camera, Rectangle cursor)
{
    // need to add the cameraPos here so that collision checking works
    // the "normal" cursor only goes between 0 and 600 (screen width/height)
    // and does not take the camera position into account
    int remainderX = cursor.x % 20;
    int realX = cursor.x + camera.GetPosition().x + 20 - remainderX - 20;

    int remainderY = cursor.y % 20;
    int realY = cursor.y + camera.GetPosition().y + 20 - remainderY - 20;

    Rectangle realCursor = {realX, realY, 20, 20, cursor.color};
    if (placedRects.size() > 1)
    {
        for (size_t i = 0; i < placedRects.size(); i++)
        {
            // when the rectangle is intersecting, remove it from everything
            if (RectangleIntersect(realCursor, placedRects.at(i)))
            {
                // debug cout for printing indices and vertices to console before the deletion
                /*for (size_t i = 0; i < indices.size(); i++)
                {
                    std::cout << indices.at(i) << " indices before!\n";
                    if (i % 6 == 5)
                    {
                        std::cout << "\n";
                    }
                }
                std::cout << "\n";
                for (size_t i = 0; i < vertices.size(); i++)
                {
                    std::cout << vertices.at(i).position.x << " " << vertices.at(i).position.y << " vertPos before\n";
                    if (i % 4 == 3)
                    {
                        std::cout << "\n";
                    }
                }
                std::cout << "\n";*/

                // look for the correct vertices and indices to delete
                Vertex vertexKey = {
                    glm::vec3(placedRects.at(i).x, placedRects.at(i).y + placedRects.at(i).height, 0.0f),
                    glm::vec2(0.0f * 20.0f / 600.0f, (0.0f + 20.0f) / 20.0f),
                    glm::vec4(placedRects.at(i).color.x, placedRects.at(i).color.y, placedRects.at(i).color.z, 1.0f)};

                auto it = std::find(vertices.begin(), vertices.end(), vertexKey);

                int vertexIndex = -1;

                // If vertex was found (this should be the case every time), erase the vertex and the 3 following vertices
                if (it != vertices.end())
                {
                    vertexIndex = it - vertices.begin();

                    // debug output
                    // std::cout << vertices.at(vertexIndex).position.x << " " << vertices.at(vertexIndex).position.y << " \n";
                    // std::cout << vertices.at(vertexIndex + 1).position.x << " " << vertices.at(vertexIndex + 1).position.y << " \n";
                    // std::cout << vertices.at(vertexIndex + 2).position.x << " " << vertices.at(vertexIndex + 2).position.y << " \n";
                    // std::cout << vertices.at(vertexIndex + 3).position.x << " " << vertices.at(vertexIndex + 3).position.y << " \n";
                    // std::cout << "\n";

                    // NOTE: this needs to be only begin + index because the other vertices that came after the deleted will move one space up
                    vertices.erase(vertices.begin() + i * 4);
                    vertices.erase(vertices.begin() + i * 4);
                    vertices.erase(vertices.begin() + i * 4);
                    vertices.erase(vertices.begin() + i * 4);
                }

                // erase the according indices
                for (size_t j = 0; j < 6; j++)
                {
                    // std::cout << i << " the i!\n";
                    // NOTE: this needs to be only i * 6 because the other indices that came after the deleted will move one space up
                    // std::cout << indices.at(i * 6) << " indices at i * 6!\n";
                    indices.erase(indices.begin() + i * 6);
                }
                // std::cout << "\n";

                // erase the rect from placedRects
                placedRects.erase(placedRects.begin() + i);
                tileIndices.erase(tileIndices.begin() + (i - 1));

                // recalculate levelModelViewProj
                levelModelViewProj = camera.GetViewProj() * levelModel;

                // recalculate indicesIndex, numVertices, numIndices
                indicesIndex -= 4;
                numVertices -= 4;
                numIndices -= 6;

                // renumber indices so they fit the correct vertices
                for (size_t j = i * 6; j < indices.size(); j++)
                {
                    indices.at(j) -= 4;
                }

                // debug cout for printing indices and vertices to console
                /*for (size_t i = 0; i < indices.size(); i++)
                {
                    std::cout << indices.at(i) << " indices after!\n";
                    if (i % 6 == 5)
                    {
                        std::cout << "\n";
                    }
                }
                std::cout << "\n";

                for (size_t i = 0; i < vertices.size(); i++)
                {
                    std::cout << vertices.at(i).position.x << " " << vertices.at(i).position.y << " vertPos after\n";
                    if (i % 4 == 3)
                    {
                        std::cout << "\n";
                    }
                }
                std::cout << "\n";*/

                // rebuffer whole level
                glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices * sizeof(Vertex), vertices.data());
                glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, numIndices * sizeof(indices[0]), indices.data());

                continue;
            }
        }
    }
}

// needed for vertex output to file
std::ostream &operator<<(std::ostream &outs, const Vertex &A)
{
    return outs
           << A.position.x << " " << A.position.y << " " << A.position.z << "\n"
           << A.texCoords.x << " " << A.texCoords.y << "\n"
           << A.color.x << " " << A.color.y << " " << A.color.z << " " << A.color.w << "\n";
}

void LoadLevel(Camera &camera)
{
    // reset variables
    vertices.clear();
    indices.clear();
    numVertices = 0;
    numIndices = 0;
    indicesIndex = 0;
    placedRects.clear();
    tileIndices.clear();

    Rectangle rectToPlace = {0, 0, 20, 20, glm::vec3(1.0f, 1.0f, 1.0f)};
    placedRects.push_back(rectToPlace);

    // loading
    // the file to read from, indices file at the beginning
    std::ifstream infile("saved_level/indices.txt");

    // floats to read from file
    float a, b, c, d, e, f, g, h, i;

    // read indices
    while (infile >> a >> b >> c >> d >> e >> f)
    {
        indices.push_back((uint32_t)a);
        indices.push_back((uint32_t)b);
        indices.push_back((uint32_t)c);
        indices.push_back((uint32_t)d);
        indices.push_back((uint32_t)e);
        indices.push_back((uint32_t)f);

        // setting indicesIndex to correct value
        indicesIndex = f + 1;
    }

    numIndices = indices.size();

    // read vertices
    infile = std::ifstream("saved_level/vertices.txt");
    while (infile >> a >> b >> c >> d >> e >> f >> g >> h >> i)
    {
        Vertex vertex;

        vertex.position = glm::vec3(a, b, c);
        vertex.texCoords = glm::vec2(d, e);
        vertex.color = glm::vec4(f, g, h, i);

        vertices.push_back(vertex);
    }

    numVertices = vertices.size();

    // read tile type and create level
    int tileIdx = 1;
    infile = std::ifstream("saved_level/tiles.txt");

    while (infile >> a)
    {
        Rectangle rect = {(int)vertices.at(tileIdx * 4 + 1).position.x, (int)vertices.at(tileIdx * 4 + 1).position.y,
                          20, 20,
                          vertices.at(tileIdx * 4 + 1).color};
        placedRects.push_back(rect);

        tileIndices.push_back(a);

        tileIdx++;
    }

    // TODO: maybe set cursor rect to correct position

    cursorModelViewProj = camera.GetViewProj() * cursorModel;
    levelModelViewProj = camera.GetViewProj() * levelModel;

    // subbuffer data
    glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices * sizeof(Vertex), vertices.data());
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, numIndices * sizeof(indices[0]), indices.data());
}

void SaveLevel()
{
    std::cout << "Saving level...\n";

    // save tile types so enemies/player can be found
    std::ofstream outputFileTiles("saved_level/tiles.txt");
    for (int i = 0; i < tileIndices.size(); i++)
    {
        outputFileTiles << tileIndices[i] << " \n";
    }

    std::ofstream outputFileIndices("saved_level/indices.txt");
    for (int i = 0; i < indices.size(); i++)
    {
        outputFileIndices << indices[i] << " ";
        if (i % 6 == 5)
        {
            outputFileIndices << "\n";
        }
    }

    std::ofstream outputFileVertex("saved_level/vertices.txt");
    std::ostream_iterator<Vertex> outputIteratorVertex(outputFileVertex, "\n");
    std::copy(std::begin(vertices), std::end(vertices), outputIteratorVertex);

    std::cout << "Done saving level!\n";
}

int main(int argc, char **argv)
{
    // needed so the "placedRects" vector has a size per default (also useful for later calculations)
    Rectangle rect = {-20, -20, 2, 2, glm::vec3(0.0f, 0.0f, 0.0f)};
    placedRects.push_back(rect);

    Camera camera(screenWidth, screenHeight);

    Rectangle cursor = {0, 0, 20, 20, glm::vec3(1.0f, 1.0f, 1.0f)};
    cursorModelViewProj = camera.GetViewProj() * cursorModel;
    CreateRect(cursor, vertices, indices, numVertices, numIndices, indicesIndex, 0.0f, 0.0f, 600.0f, 20.0f, 20.0f, 20.0f);

    {
        // // sdl init
        // SDL_Window *window;
        // SDL_Init(SDL_INIT_EVERYTHING);

        // SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
        // SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
        // SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
        // SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
        // SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
        // SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        // SDL_GL_SetSwapInterval(1);

        //     // Debug
        // #ifdef _DEBUG
        //     SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
        // #endif

        // uint32_t flags = SDL_WINDOW_OPENGL;
        // window = SDL_CreateWindow("Level Editor", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, flags);
        // SDL_GLContext glContext = SDL_GL_CreateContext(window);

        // GLenum err = glewInit();
        // if (err != GLEW_OK)
        // {
        //     std::cout << "Error: " << glewGetErrorString(err) << std::endl;
        //     std::cin.get();
        //     return -1;
        // }

        // std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;

        // #ifdef _DEBUG
        //     glEnable(GL_DEBUG_OUTPUT);
        //     glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        //     glDebugMessageCallback(OpenGLDebugCallback, 0);
        // #endif
    }

    DisplayManager displayManager;
    displayManager.Init(screenWidth, screenHeight);

    Renderer renderer(displayManager, maxVertices, sizeof(indices[0]));
    renderer.Init(maxVertices, numVertices, numIndices, indices, vertices);

    renderer.AddTexture("WallSprites", "graphics/WallSprites.png");

    // Texture texture("graphics/WallSprites.png"); // create texture

    // FPS Counter stuff, move to renderer
    // uint64_t now = SDL_GetPerformanceCounter();
    // uint64_t last = 0;
    // double deltaTime = 0.0f;
    // double moveSeconds = 0.5f;
    // double lastTimeMoved = 0.0f;

    // create and fill Vertex buffer
    // VertexBuffer vertexBuffer(vertices.data(), maxVertices);
    // glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices * sizeof(Vertex), vertices.data());

    // create and fill Index buffer
    // IndexBuffer indexBuffer(indices.data(), maxVertices, sizeof(indices[0]));
    // glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, numIndices * sizeof(indices[0]), indices.data());

    renderer.CreateShader("cursorShader", "level_editor/shaders/cursor_vert.glsl", "level_editor/shaders/cursor_frag.glsl");
    // renderer.CreateShader("textureShader", "level_editor/shaders/texture_vert.glsl", "level_editor/shaders/texture_frag.glsl");

    // create shader
    // Shader cursorShader("level_editor/shaders/cursor_vert.glsl", "level_editor/shaders/cursor_frag.glsl");

    // create texture shader
    // Shader textureShader("level_editor/shaders/texture_vert.glsl", "level_editor/shaders/texture_frag.glsl");

    // create level shader
    // Shader levelShader("level_editor/shaders/level_vert.glsl", "level_editor/shaders/level_frag.glsl");

    // cursor shader locations
    // int modelViewProjMatrixLocaction = glGetUniformLocation(cursorShader.GetShaderId(), "u_modelViewProj");
    // int basicColorUniformLocation = glGetUniformLocation(cursorShader.GetShaderId(), "u_color");
    // int cursorTextureUniformLocation = glGetUniformLocation(cursorShader.GetShaderId(), "u_texture");
    // int cursorTexValuesUniformLocation = glGetUniformLocation(cursorShader.GetShaderId(), "u_texValues");

    // texture shader locations
    // int textureUniformLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_texture");
    // int texValuesUniformLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_texValues");
    // int texViewProjMatrixUniformLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_modelViewProj");

    // level shader model view projection matrix location
    // int levelViewProjMatrixUniformLocation = glGetUniformLocation(levelShader.GetShaderId(), "u_modelViewProj");

    // the texture offsets for moving the texture with shader (for cursor)
    // The order is startX, endX, startY, endY
    glm::vec4 textureOffsets(0.0f / 600.0f, 20.0f / 600.0f, 0.0f / 600.0f, 20.0f / 600.0f);

    bool close = false;
    bool eraseMode = false; // bool for toggling tile erase mode, initially false

    // texture.CreateTexture();
    renderer.CreateTexture("WallSprites");

    // glEnable(GL_BLEND);
    // glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    // setting clear color
    // glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    while (!close)
    {
        // // delta time calculation
        // last = now;
        // now = SDL_GetPerformanceCounter();
        // deltaTime = (double)((now - last) / (double)SDL_GetPerformanceFrequency());
        // // lastTimeMoved += deltaTime;

        // glClear(GL_COLOR_BUFFER_BIT);

        // vertexBuffer.Bind();
        // indexBuffer.Bind();

        // glActiveTexture(GL_TEXTURE0);
        // glBindTexture(GL_TEXTURE_2D, texture.texID);

        // cursor drawing
        // cursorShader.Bind();

        // glUniform4f(cursorTexValuesUniformLocation, textureOffsets.x, textureOffsets.y, textureOffsets.z, textureOffsets.w);
        // glUniform1i(cursorTextureUniformLocation, 0);

        // glUniformMatrix4fv(modelViewProjMatrixLocaction, 1, GL_FALSE, &cursorModelViewProj[0][0]);
        // glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void *)(0 * sizeof(GLuint)));

        // cursorShader.Unbind();

        // texture drawing
        // textureShader.Bind();

        // level drawing (numIndices - 6 because cursor gets drawn before)
        // glUniform1i(textureUniformLocation, 0);
        // glUniformMatrix4fv(texViewProjMatrixUniformLocation, 1, GL_FALSE, &levelModelViewProj[0][0]);
        // glDrawElements(GL_TRIANGLES, numIndices - 6, GL_UNSIGNED_INT, (void *)(6 * sizeof(GLuint)));

        // draw blinking cursor, currently disabled
        // if (lastTimeMoved < moveSeconds)
        // {
        //     glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        // }

        // if (lastTimeMoved > moveSeconds + 0.5f)
        // {
        //     // std::cout << " stop!\n";
        //     lastTimeMoved = 0.0;
        // }

        // textureShader.Unbind();

        // indexBuffer.Unbind();
        // vertexBuffer.Unbind();

        // SDL_GL_SwapWindow(displayManager.GetWindow());

        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
            {
                close = true;
            }

            // toggle eraseMode
            else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_LSHIFT)
            {
                eraseMode = !eraseMode;

                if (eraseMode)
                {
                    textureOffsets.x = 240.0f / 600.0f;
                    textureOffsets.y = 260.0f / 600.0f;
                    textureOffsets.z = 0.0f / 600.0f;
                    textureOffsets.w = 20.0f / 600.0f;

                    lastTileType = curTileType;
                    curTileType = tiles.back().tileIdx;
                }
                else if (!eraseMode)
                {
                    textureOffsets.x = (lastTileType * 20.0f) / 600.0f;
                    textureOffsets.y = (lastTileType * 20.0f + 20.0f) / 600.0f;
                    textureOffsets.z = 0.0f / 600.0f;
                    textureOffsets.w = 20.0f / 600.0f;

                    curTileType = lastTileType;
                    lastTileType = curTileType;
                }

                SDL_SetWindowTitle(displayManager.GetWindow(), tileNames.at(curTileType).c_str());
            }
            else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_1)
            {
                SaveLevel();
            }
            else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_2)
            {
                LoadLevel(camera);
            }

            if (event.type == SDL_MOUSEMOTION)
            {
                cursor.x = event.motion.x;
                cursor.y = event.motion.y;

                // calculation needed so the cursor moves like it's on a grid
                int remainderX = cursor.x % 20;
                int realX = cursor.x + 20 - remainderX - 20;

                int remainderY = cursor.y % 20;
                int realY = cursor.y + 20 - remainderY - 20;

                cursorModelViewProj = camera.GetViewProj() * cursorModel;
                cursorModelViewProj = glm::translate(cursorModelViewProj, glm::vec3(realX + camera.GetPosition().x, realY + camera.GetPosition().y, 0.0f));
            }
            if (event.type == SDL_MOUSEWHEEL && !eraseMode)
            {
                // Wheel up
                if (event.wheel.y > 0)
                {
                    lastTileType = curTileType;
                    if (curTileType == tiles.front().tileIdx)
                    {
                        lastTileType = curTileType;
                        curTileType = tiles.back().tileIdx;
                    }

                    // std::cout << lastTileType << " !\n";
                    --curTileType;
                    curTile = tiles.at(curTileType);

                    textureOffsets.x = curTile.subTex.position.x / 600.0f;
                    textureOffsets.y = curTile.subTex.position.x + 20.0f / 600.0f;
                    textureOffsets.z = curTile.subTex.position.y / 600.0f;
                    textureOffsets.w = curTile.subTex.position.y + 20.0f / 600.0f;

                    SDL_SetWindowTitle(displayManager.GetWindow(), tileNames.at(curTileType).c_str());
                }
                // Wheel down
                else
                {
                    lastTileType = curTileType;

                    if (curTileType == tiles.back().tileIdx - 1)
                    {
                        lastTileType = curTileType;
                        curTileType = -1;
                    }

                    // std::cout << lastTileType << " !\n";
                    ++curTileType;
                    curTile = tiles.at(curTileType);

                    textureOffsets.x = curTile.subTex.position.x / 600.0f;
                    textureOffsets.y = curTile.subTex.position.x + 20.0f / 600.0f;
                    textureOffsets.z = curTile.subTex.position.y / 600.0f;
                    textureOffsets.w = curTile.subTex.position.y + 20.0f / 600.0f;

                    SDL_SetWindowTitle(displayManager.GetWindow(), tileNames.at(curTileType).c_str());
                }
            }
            // Place tile as long as the lmb is held and not in erase Mode
            else if (event.button.button == SDL_BUTTON_LEFT && !eraseMode)
            {
                PlaceTile(curTile, cursor, camera);
            }
            // Remove tile on left click and when in erase Mode
            else if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT && eraseMode)
            {
                RemoveAnyTile(camera, cursor);
            }

            if (event.type == SDL_KEYDOWN)
            {
                MoveScreen(camera, cursor, event);
            }
        }
    }

    return 0;
}
