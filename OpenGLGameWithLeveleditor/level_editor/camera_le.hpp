#pragma once

#include "config_le.hpp"

namespace LevelEditor
{
    class Camera
    {
    public:
        Camera(int width, int height);
        ~Camera();

        glm::mat4 GetViewProj();
        glm::vec3 GetPosition();
        virtual void Update();
        virtual void Translate(glm::vec3 v);

    protected:
        glm::vec3 position;
        glm::mat4 projection;
        glm::mat4 view;
        glm::mat4 viewProj;
    };
}