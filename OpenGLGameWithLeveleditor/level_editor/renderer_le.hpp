#pragma once

#include "config_le.hpp"
#include "display_manager_le.hpp"
#include "vertex_buffer_le.hpp"
#include "index_buffer_le.hpp"
#include "shader_le.hpp"
#include "texture_le.hpp"

namespace LevelEditor
{
    class Renderer
    {
    public:
        Renderer(DisplayManager dm, uint32_t maxVertices, size_t elementSize);
        ~Renderer();

        void Init(uint32_t maxVertices,
                  uint32_t numVertices,
                  uint32_t numIndices,
                  std::vector<uint32_t> indices,
                  std::vector<Vertex> vertices);

        // Adds a texture to the list of available textures
        void AddTexture(std::string textureName, std::string location);

        // Creates a texture (does the texture loading and all that)
        void CreateTexture(std::string textureName);

        // Adds a shader to the list of available textures
        void CreateShader(std::string shaderName, const char *vertexShaderFilename, const char *fragmentShaderFilename);

        void Render();

    private:
        std::unique_ptr<DisplayManager> mDisplayManager;
        // std::unordered_map<Shader, std::vector<int>> mShadersWithLocations; //std::vector<Shader> mShaders;

        // std::vector<Shader> mShaders;

        // shaders with their names
        std::unordered_map<std::string, Shader> mShaders;

        // textures with their names
        std::unordered_map<std::string, Texture> mTextures;

        // Vertex and Index Buffer
        VertexBuffer mVertexBuffer;
        IndexBuffer mIndexBuffer;

        // FPS Counter stuff
        double deltaTime = 0.0f;
        uint64_t now = 0;
        uint64_t last = 0;
    };
}