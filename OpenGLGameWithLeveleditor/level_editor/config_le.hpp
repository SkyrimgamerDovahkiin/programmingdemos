// std
#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>
#include <fstream>
#include <cstdio>
#include <memory>

// glew/SDL
#include <GL/glew.h>
#include <SDL2/SDL.h>

// glm
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// STB
#include "../libs/stb_image.h"