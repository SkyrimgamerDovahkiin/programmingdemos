#include "display_manager_le.hpp"

namespace LevelEditor
{
    void GLAPIENTRY OpenGLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
    {
        std::cout << "[OpenGL Error] " << message << std::endl;
    }

    DisplayManager::DisplayManager()
    {
    }

    DisplayManager::~DisplayManager()
    {
    }

    void DisplayManager::Init(int screenWidth, int screenHeight, uint32_t flags, const char *name)
    {
        // sdl init
        SDL_Init(SDL_INIT_EVERYTHING);
        SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

        // If debug enabled, enable sdl debug
#ifdef _DEBUG
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
#endif

        // create window and context
        mWindow = SDL_CreateWindow(name, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, flags);
        mGlContext = SDL_GL_CreateContext(mWindow);

        SDL_GL_SetSwapInterval(1); // enable vsync

        // initialize glew
        GLenum err = glewInit();
        if (err != GLEW_OK)
        {
            std::cout << "Error: " << glewGetErrorString(err) << std::endl;
            std::cin.get();
            return;
        }

        // print out OpenGL version
        std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;

        // If debug enabled, enable OpenGL debug
#ifdef _DEBUG
        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(OpenGLDebugCallback, 0);
#endif
    }
}