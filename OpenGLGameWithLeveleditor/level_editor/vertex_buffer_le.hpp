#pragma once

#include "config_le.hpp"
#include "defines_le.hpp"

namespace LevelEditor
{
    class VertexBuffer
    {
    public:
        VertexBuffer() = default;
        VertexBuffer(void* data, uint32_t numVertices);
        ~VertexBuffer();
        void Bind();
        void Unbind();

    private:
        GLuint bufferId;
        GLuint vao;
    };
}