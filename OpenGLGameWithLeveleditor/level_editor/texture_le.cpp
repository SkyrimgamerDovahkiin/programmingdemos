#include "texture_le.hpp"

namespace LevelEditor
{
    Texture::Texture(std::string location)
        : mLocation(location)
    {
    }

    Texture::~Texture()
    {
        glDeleteTextures(1, &texID);
    }

    void Texture::CreateTexture()
    {
        int textureWidth = 0;
        int textureHeight = 0;
        int bitsPerPixel = 0;

        // stbi_set_flip_vertically_on_load(true);
        auto textureBuffer = stbi_load(mLocation.c_str(), &textureWidth, &textureHeight, &bitsPerPixel, 4);

        glGenTextures(1, &texID);
        glBindTexture(GL_TEXTURE_2D, texID);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, textureWidth, textureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureBuffer);
        glBindTexture(GL_TEXTURE_2D, 0);

        if (textureBuffer)
        {
            stbi_image_free(textureBuffer);
        }
    }
}