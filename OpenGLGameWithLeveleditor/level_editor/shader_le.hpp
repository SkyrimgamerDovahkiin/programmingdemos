#pragma once

#include "config_le.hpp"

// code from https://github.com/codingminecraft/MinecraftCloneForYoutube
struct ShaderVariable
{
    // std::string name;
    GLint location;
    // uint32_t shaderProgramId; //maybe not needed?
    GLenum dataType;

    bool operator==(const ShaderVariable &other) const
    {
        // return other.shaderProgramId == shaderProgramId && other.name == name;
        return other.location == location;
    }
};

namespace LevelEditor
{
    class Shader
    {
    public:
        Shader(const char *vertexShaderFilename, const char *fragmentShaderFilename);
        ~Shader();
        void Bind();
        void Unbind();
        GLuint GetShaderId();

    private:
        GLuint Compile(std::string shaderSource, GLenum type);
        std::string Parse(const char *filename);
        GLuint CreateShader(const char *vertexShaderFilename, const char *fragmentShaderFilename);

        GLuint shaderId;

        std::unordered_map<std::string, ShaderVariable> shaderVariables;
    };
}