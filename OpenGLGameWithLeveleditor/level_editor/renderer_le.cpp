#include "renderer_le.hpp"

namespace LevelEditor
{
    Renderer::Renderer(DisplayManager dm, uint32_t maxVertices, size_t elementSize)
        : mVertexBuffer(VertexBuffer(0, maxVertices)),
          mIndexBuffer(IndexBuffer(0, maxVertices, elementSize))
    {
        mDisplayManager = std::make_unique<DisplayManager>(dm);
    }

    Renderer::~Renderer()
    {
        mTextures.clear();
    }

    void Renderer::Init(uint32_t maxVertices,
                        uint32_t numVertices,
                        uint32_t numIndices,
                        std::vector<uint32_t> indices,
                        std::vector<Vertex> vertices)
    {
        // FPS Counter stuff
        now = SDL_GetPerformanceCounter();

        // create and fill Vertex buffer
        // vertexBuffer = VertexBuffer(0, maxVertices);
        glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices * sizeof(Vertex), vertices.data());

        // create and fill Index buffer
        // indexBuffer = IndexBuffer(0, maxVertices, sizeof(indices[0]));
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, numIndices * sizeof(indices[0]), indices.data());

        // set OpenGL params
        // glEnable(GL_BLEND);
        // glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        // setting clear color
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    }

    void Renderer::AddTexture(std::string textureName, std::string location)
    {
        mTextures.emplace(textureName, Texture(location));
    }

    void Renderer::CreateTexture(std::string textureName)
    {
        mTextures.at(textureName).CreateTexture();
    }

    void Renderer::CreateShader(std::string shaderName, const char *vertexShaderFilename, const char *fragmentShaderFilename)
    {
        Shader shader(vertexShaderFilename, fragmentShaderFilename);
        // mShaders.push_back(shader);
        mShaders.emplace(shaderName, shader);
    }

    void Renderer::Render()
    {
        // delta time calculation
        last = now;
        now = SDL_GetPerformanceCounter();
        deltaTime = (double)((now - last) / (double)SDL_GetPerformanceFrequency());
        // lastTimeMoved += deltaTime;

        glClear(GL_COLOR_BUFFER_BIT);

        mVertexBuffer.Bind();
        mIndexBuffer.Bind();

        glActiveTexture(GL_TEXTURE0);

        // currently we only have that wall sprites texture
        glBindTexture(GL_TEXTURE_2D, mTextures.at("WallSprites").texID);

        // cursor drawing
        mShaders.at("cursorShader").Bind();

        // draw

        mShaders.at("cursorShader").Unbind();

        mShaders.at("textureShader").Bind();

        // draw

        mShaders.at("textureShader").Unbind();

        mIndexBuffer.Unbind();
        mVertexBuffer.Unbind();

        SDL_GL_SwapWindow(mDisplayManager->GetWindow());
    }
}