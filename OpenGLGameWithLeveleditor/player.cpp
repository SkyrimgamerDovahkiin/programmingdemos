#include "player.hpp"

namespace OGLE
{
    Player::Player(){};
    Player::~Player(){};

    void Player::CheckLevelBounds(float xMax, float yMax)
    {
        if (rect.x < 0.0f) // left side of screen
        {
            rect.x += moveDistance;
            modelViewProj = glm::translate(modelViewProj, glm::vec3(20.0f, 0.0f, 0.0f));
        }
        else if (rect.x > (xMax - rect.width)) // right side of screen
        {
            rect.x -= moveDistance;
            modelViewProj = glm::translate(modelViewProj, glm::vec3(-20.0f, 0.0f, 0.0f));
        }
        else if (rect.y < 0.0f) // top of screen
        {
            rect.y += moveDistance;
            modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, 20.0f, 0.0f));
        }
        else if (rect.y > (yMax - rect.height)) // bottom of screen
        {
            rect.y -= moveDistance;
            modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, -20.0f, 0.0f));
        }
    }

    void Player::CheckCollision(std::vector<Rectangle> level)
    {
        for (size_t i = 0; i < level.size(); i++)
        {
            if (RectangleIntersect(rect, level[i]))
            {
                // std::cout << " is colliding with level!\n";
                if (direction == 1)
                {
                    rect.y += moveDistance;
                    modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, 20.0f, 0.0f));
                }
                else if (direction == 2)
                {
                    rect.y -= moveDistance;
                    modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, -20.0f, 0.0f));
                }
                else if (direction == 3)
                {
                    rect.x += moveDistance;
                    modelViewProj = glm::translate(modelViewProj, glm::vec3(20.0f, 0.0f, 0.0f));
                }
                else if (direction == 4)
                {
                    rect.x -= moveDistance;
                    modelViewProj = glm::translate(modelViewProj, glm::vec3(-20.0f, 0.0f, 0.0f));
                }
            }
        }
    };

    void Player::Move(SDL_Event event)
    {
        // if (died)
        // {
        //     return;
        // }
        switch (event.key.keysym.sym)
        {
        case SDLK_w:
            rect.y -= moveDistance;
            modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, -20.0f, 0.0f));
            direction = 1;
            break;
        case SDLK_a:
            rect.x -= moveDistance;
            modelViewProj = glm::translate(modelViewProj, glm::vec3(-20.0f, 0.0f, 0.0f));
            direction = 3;
            break;
        case SDLK_s:
            rect.y += moveDistance;
            modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, 20.0f, 0.0f));
            direction = 2;
            break;
        case SDLK_d:
            rect.x += moveDistance;
            modelViewProj = glm::translate(modelViewProj, glm::vec3(20.0f, 0.0f, 0.0f));
            direction = 4;
            break;
        default:
            break;
        }
    };

    void Player::Create(float x, float y)
    {
        startX = x;
        startY = y;
        rect = {x, y, 20.0f, 20.0f, glm::vec3(0.00f, 1.00f, 0.00f)};
    }

    void Player::Draw(int drawIdx, int modelViewProjMatrixLocaction)
    {
        glUniformMatrix4fv(modelViewProjMatrixLocaction, 1, GL_FALSE, &modelViewProj[0][0]);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void *)(drawIdx * sizeof(GLuint)));
    }

    void Player::Update(double deltaTime, std::vector<Rectangle> level, std::vector<Enemy> enemies)
    {
        if (died)
        {
            return;
        }

        CheckCollision(level);

        // CheckScreenBounds(600.0f, 600.0f);

        // check if player is colliding with enemy
        for (size_t i = 0; i < enemies.size(); i++)
        {
            if (RectangleIntersect(rect, enemies.at(i).rect))
            {
                died = true;
            }
        }
    }

    void Player::Reset()
    {
        died = false;
    }
}