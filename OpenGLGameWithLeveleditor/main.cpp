// std
#include <iostream>
#include <cmath>
#include <vector>
#include <list>
#include <fstream>
#include <ctime>
#include <algorithm>
#include <deque>

// GLEW/SDL
// #define GLEW_STATIC
// #include <GL/glew.h>
// #define SDL_MAIN_HANDLED
// #include <SDL2/SDL.h>

// STB
#define STB_IMAGE_IMPLEMENTATION
#include "libs/stb_image.h"

// custom
#include "defines.hpp"
#include "vertex_buffer.hpp"
#include "vertex_buffer_text.hpp"
#include "index_buffer.hpp"
#include "shader.hpp"
#include "camera.hpp"
#include "time.hpp"
#include "player.hpp"
#include "enemy.hpp"
#include "food.hpp"
#include "text.hpp"
#include "create_rect.hpp"

using OGLE::VertexBuffer, OGLE::VertexBufferText, OGLE::IndexBuffer, OGLE::Shader, OGLE::Camera, OGLE::Player, OGLE::Enemy, OGLE::Food, OGLE::Text;

void GLAPIENTRY OpenGLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
{
    std::cout << "[OpenGL Error] " << message << std::endl;
}

#ifdef _DEBUG

void _GLGetError(const char *file, int line, const char *call)
{
    while (GLenum error = glGetError())
    {
        std::cout << "[OpenGL Error] " << glewGetErrorString(error) << " in " << file << ":" << line << " Call: " << call << std::endl;
    }
}

#define GLCALL(call) \
    call;            \
    _GLGetError(__FILE__, __LINE__, #call)

#else

#define GLCALL(call) call;

#endif

// vertices and indices stuff
std::vector<Vertex> vertices;
uint32_t numVertices = 0;
std::vector<uint32_t> indices;
uint32_t numIndices = 0;
uint32_t indicesIndex = 0;
uint32_t maxVertices = 16000000; // 16 MB Vertex Buffer size, indices size gets determined from that

// foods/enemies setup
std::vector<Enemy> enemies;
std::vector<Food> foods;
std::vector<int> foodIdxs;

// the level model and level MVP
glm::mat4 levelModel = glm::mat4(1.0f);
glm::mat4 levelModelViewProj = glm::mat4(1.0f);

// ints
int screenWidth = 600, screenHeight = 600, score = 0, highscore = 0, levelInt = 0;

GLuint texId = 0;

std::vector<Rectangle> level;

std::vector<uint32_t> levelDrawIdx;
std::vector<uint32_t> waypointDrawIdx;
std::vector<uint32_t> enemyDrawIdx;
std::vector<uint32_t> foodDrawIdx;
int playerDrawIdx = 0;
int foodIdx = 0;

// function to move the screen when player reaches an edge, xMax and yMax need to be the maximum minus 600.0f, because the cam isn't at the right edge/bottom edge
void ScrollScreen(Camera &camera, Player &player, float xMax, float yMax)
{
    // camera translation
    if (camera.GetPosition().x != xMax && player.rect.x >= (screenWidth + camera.GetPosition().x)) // right side of screen
    {
        std::cout << " moving Right!\n";
        camera.Translate(glm::vec3(600.0f, 0.0f, 0.0f));
        camera.Update();
    }
    else if (camera.GetPosition().x != 0.0f && player.rect.x <= camera.GetPosition().x - 20.0f) // left side of screen
    {
        std::cout << " moving Left!\n";
        camera.Translate(glm::vec3(-600.0f, 0.0f, 0.0f));
        camera.Update();
    }
    else if (camera.GetPosition().y != 0.0f && player.rect.y <= camera.GetPosition().y - 20.0f) // top of screen
    {
        std::cout << " moving Up!\n";
        camera.Translate(glm::vec3(0.0f, -600.0f, 0.0f));
        camera.Update();
    }
    else if (camera.GetPosition().y != yMax && player.rect.y >= (screenWidth + camera.GetPosition().y)) // bottom of screen
    {
        std::cout << " moving Down!\n";
        camera.Translate(glm::vec3(0.0f, 600.0f, 0.0f));
        camera.Update();
    }

    // player and enemies translation
    // NOTE: need to subtract the orig Pos of the player/enemies.
    // So for example, if the player is at game creation positioned at 20.0f, 20.0f, 0.0f, this need to be subtracted
    player.modelViewProj = camera.GetViewProj() * player.model;
    player.modelViewProj = glm::translate(player.modelViewProj, glm::vec3(player.rect.x - player.startX, player.rect.y - player.startY, 0.0f));

    for (size_t i = 0; i < enemies.size(); i++)
    {
        enemies.at(i).modelViewProj = camera.GetViewProj() * enemies.at(i).model;
        enemies.at(i).modelViewProj = glm::translate(enemies.at(i).modelViewProj, glm::vec3(enemies.at(i).rect.x - enemies.at(i).startPosX, enemies.at(i).rect.y - enemies.at(i).startPosY, 0.0f));
    }

    for (size_t i = 0; i < foods.size(); i++)
    {
        foods.at(i).modelViewProj = camera.GetViewProj() * foods.at(i).model;
        foods.at(i).modelViewProj = glm::translate(foods.at(i).modelViewProj, glm::vec3(foods.at(i).rect.x - foods.at(i).startPosX, foods.at(i).rect.y - foods.at(i).startPosY, 0.0f));
    }

    levelModelViewProj = camera.GetViewProj() * levelModel;
}

void LoadTexture()
{
    int textureWidth = 0;
    int textureHeight = 0;
    int bitsPerPixel = 0;

    // stbi_set_flip_vertically_on_load(true);
    auto textureBuffer = stbi_load("graphics/WallSprites.png", &textureWidth, &textureHeight, &bitsPerPixel, 4);

    GLCALL(glGenTextures(1, &texId));
    GLCALL(glBindTexture(GL_TEXTURE_2D, texId));

    GLCALL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
    GLCALL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
    GLCALL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
    GLCALL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));

    GLCALL(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, textureWidth, textureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureBuffer));
    GLCALL(glBindTexture(GL_TEXTURE_2D, 0));

    if (textureBuffer)
    {
        stbi_image_free(textureBuffer);
    }
}

void CreateLevel(Player &player, Camera &camera)
{
    // the file to read from, indices file at the beginning
    std::ifstream infile("saved_level/indices.txt");

    // floats to read from file
    float a, b, c, d, e, f, g, h, i;

    // read indices
    while (infile >> a >> b >> c >> d >> e >> f)
    {
        indices.push_back((uint32_t)a);
        indices.push_back((uint32_t)b);
        indices.push_back((uint32_t)c);
        indices.push_back((uint32_t)d);
        indices.push_back((uint32_t)e);
        indices.push_back((uint32_t)f);
    }

    // Debug
    /*for (size_t i = 0; i < indices.size(); i++)
    {
        std::cout << indices[i] << " the indices!\n";
        if (i % 6 == 5)
        {
            std::cout << "\n";
        }
    }*/

    numIndices = indices.size();

    // read vertices
    infile = std::ifstream("saved_level/vertices.txt");
    while (infile >> a >> b >> c >> d >> e >> f >> g >> h >> i)
    {
        Vertex vertex;

        vertex.x = a;
        vertex.y = b;
        vertex.z = c;

        vertex.u = d;
        vertex.v = e;

        vertex.r = f;
        vertex.g = g;
        vertex.b = h;
        vertex.a = i;

        vertices.push_back(vertex);
    }

    // Debug
    /*for (size_t i = 0; i < vertices.size(); i++)
    {
        std::cout << vertices[i].x << " " << vertices[i].y << " " << vertices[i].z << "\n";
        if (i % 4 == 3)
        {
            std::cout << "\n";
        }
    }*/

    numVertices = vertices.size();

    // read tile type and create level
    int tileIdx = 1;
    infile = std::ifstream("saved_level/tiles.txt");

    while (infile >> a)
    {
        if (a >= 0 && a < 8)
        {
            // std::cout << "processing level... \n";
            Rectangle rect = {vertices.at(tileIdx * 4 + 1).x, vertices.at(tileIdx * 4 + 1).y, 20.0f, 20.0f, glm::vec3(vertices.at(tileIdx * 4 + 1).r, vertices.at(tileIdx * 4 + 1).g, vertices.at(tileIdx * 4 + 1).b)};
            level.push_back(rect);
            levelDrawIdx.push_back(tileIdx);
        }
        else if (a == 8)
        {
            // std::cout << tileIdx << "!\n";
            // create player
            {
                player.Create(vertices.at(tileIdx * 4 + 1).x, vertices.at(tileIdx * 4 + 1).y);
                player.rect.color = glm::vec3(vertices.at(tileIdx * 4 + 1).r, vertices.at(tileIdx * 4 + 1).g, vertices.at(tileIdx * 4 + 1).b);
                player.modelViewProj = camera.GetViewProj() * player.model;
            }
            playerDrawIdx = tileIdx;
        }
        else if (a == 9)
        {
            // create enemy
            Enemy enemy;

            // enemy.Create(screenWidth, screenHeight, 640.0f, 640.0f);
            enemy.Create(vertices.at(tileIdx * 4 + 1).x, vertices.at(tileIdx * 4 + 1).y);
            enemy.modelViewProj = camera.GetViewProj() * enemy.model;

            // create waypoints and grid for enemy
            enemy.CreateWaypoints(vertices.at(tileIdx * 4 + 1).x - 40.0f, vertices.at(tileIdx * 4 + 1).y - 40.0f, vertices.at(tileIdx * 4 + 1).x + 40.0f, vertices.at(tileIdx * 4 + 1).y + 40.0f);

            // create enemy for drawing on screen
            enemies.push_back(enemy);

            enemyDrawIdx.push_back(tileIdx);
        }
        else if (a == 10)
        {
            // std::cout << "a food!\n";
            // create food
            Food food;
            food.idx = foodIdx;
            foodIdx++;
            food.Create(vertices.at(tileIdx * 4 + 1).x, vertices.at(tileIdx * 4 + 1).y);
            food.modelViewProj = camera.GetViewProj() * food.model;
            foods.push_back(food);
            foodIdxs.push_back(food.idx);
            foodDrawIdx.push_back(tileIdx);
        }
        else if (a == 11)
        {
            // std::cout << "a waypoint!\n";
            // create waypoint
            waypointDrawIdx.push_back(tileIdx);
        }
        tileIdx++;
    }

    levelModelViewProj = camera.GetViewProj() * levelModel;
}

void GameUpdateText(std::vector<int> foodIdx, Shader &s, int textProjMatrixLocaction, Player &player, Text &text, VertexBufferText &vertexBuffer)
{
    if (highscore < score)
    {
        highscore = score;
    }

    text.DrawString(20.0f, 40.0f, 1.0f, "Level: " + std::to_string(levelInt), &s, glm::vec3(0.0f, 1.0f, 0.0f), vertexBuffer, textProjMatrixLocaction);
    text.DrawString(300.0f, 20.0f, 1.0f, "Highscore: " + std::to_string(highscore), &s, glm::vec3(0.0f, 1.0f, 0.0f), vertexBuffer, textProjMatrixLocaction);
    text.DrawString(20.0f, 20.0f, 1.0f, "Score: " + std::to_string(score), &s, glm::vec3(0.0f, 1.0f, 0.0f), vertexBuffer, textProjMatrixLocaction);

    if (foodIdx.empty())
    {
        text.DrawString(10.0f, 150.0f, 0.9f, "You Win! Press L to go to next level!", &s, glm::vec3(0.0f, 1.0f, 0.0f), vertexBuffer, textProjMatrixLocaction);
    }

    else if (player.died)
    {
        text.DrawString(60.0f, 150.0f, 1.0f, "You Lost! Press L to restart!", &s, glm::vec3(1.0f, 0.0f, 0.0f), vertexBuffer, textProjMatrixLocaction);

        // reset score and level
        score = 0;
        levelInt = 0;
    }
}

void GameUpdate(Player &player, Camera &camera, double deltaTime, int texViewProjMatrixUniformLocation, bool close)
{
    // draw level
    for (int i = 0; i < levelDrawIdx.size(); i++)
    {
        glUniformMatrix4fv(texViewProjMatrixUniformLocation, 1, GL_FALSE, &levelModelViewProj[0][0]);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void *)((levelDrawIdx.at(i) * 6) * sizeof(GLuint)));
    }

    // draw waypoints
    for (int i = 0; i < waypointDrawIdx.size(); i++)
    {
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void *)((waypointDrawIdx.at(i) * 6) * sizeof(GLuint)));
    }

    // update foods
    for (size_t i = 0; i < foodDrawIdx.size(); i++)
    {
        foods.at(i).Update(foodDrawIdx.at(i) * 6, texViewProjMatrixUniformLocation, player.rect, foodIdxs, score);
    }

    // update enemies
    for (size_t i = 0; i < enemyDrawIdx.size(); i++)
    {
        enemies.at(i).Update(enemyDrawIdx.at(i) * 6, texViewProjMatrixUniformLocation, enemies, deltaTime);
    }

    // player update and draw
    ScrollScreen(camera, player, 1800.0f, 1800.0f);
    player.Update(deltaTime, level, enemies);
    player.CheckLevelBounds(2400.0f, 2400.0f);
    player.Draw(playerDrawIdx * 6, texViewProjMatrixUniformLocation);
}

int main(int argc, char **argv)
{
    std::srand(std::time(nullptr));

    Camera camera(screenWidth, screenHeight);
    Player player;

    // create level
    CreateLevel(player, camera);

    // sdl init
    SDL_Window *window;
    SDL_Init(SDL_INIT_EVERYTHING);

    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetSwapInterval(1);

// Debug
#ifdef _DEBUG
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
#endif

    uint32_t flags = SDL_WINDOW_OPENGL;
    window = SDL_CreateWindow("OpenGL Game", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, flags);
    SDL_GLContext glContext = SDL_GL_CreateContext(window);

    GLenum err = glewInit();
    if (err != GLEW_OK)
    {
        std::cout << "Error: " << glewGetErrorString(err) << std::endl;
        std::cin.get();
        return -1;
    }

    std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;

#ifdef _DEBUG
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(OpenGLDebugCallback, 0);
#endif

    // Text can only be created/generated AFTER the SDLContext was created because of glGenTextures
    Text text;
    text.InitText("fonts/arial/arial.ttf");

    // create and fill Vertex buffer
    VertexBuffer vertexBuffer(vertices.data(), maxVertices);
    glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices * sizeof(Vertex), vertices.data());

    GLint vertexBufferSize = 0;
    glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &vertexBufferSize);

    // create and fill Index buffer (using Vertex buffer size to set correct index buffer size)
    IndexBuffer indexBuffer(indices.data(), vertexBufferSize, sizeof(indices[0]));
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, numIndices * sizeof(indices[0]), indices.data());

    // text vertex buffer
    VertexBufferText vertexBufferText;

    // create shader
    Shader shader("shaders/basic_vert.glsl", "shaders/basic_frag.glsl");

    // create texture shader
    Shader texShader("shaders/texture_vert.glsl", "shaders/texture_frag.glsl");

    // create text shader
    Shader textShader("shaders/text_vert.glsl", "shaders/text_frag.glsl");

    // FPS Counter
    uint64_t now = SDL_GetPerformanceCounter();
    uint64_t last = 0;
    double deltaTime = 0.0f;

    // normal shader model view projection matrix location
    int modelViewProjMatrixLocaction = GLCALL(glGetUniformLocation(shader.GetShaderId(), "u_modelViewProj"));

    // texture shader locations
    int colorUniformLocation = GLCALL(glGetUniformLocation(texShader.GetShaderId(), "u_color"));
    int textureUniformLocation = GLCALL(glGetUniformLocation(texShader.GetShaderId(), "u_texture"));
    int texViewProjMatrixUniformLocation = GLCALL(glGetUniformLocation(texShader.GetShaderId(), "u_modelViewProj"));

    // text shader projection matrix location
    int textProjMatrixLocaction = GLCALL(glGetUniformLocation(textShader.GetShaderId(), "u_modelViewProj"));

    bool close = false;

    LoadTexture(); // load the sprites

    // blending for text
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // main game loop
    while (!close)
    {
        // delta time calculation
        last = now;
        now = SDL_GetPerformanceCounter();
        deltaTime = (double)((now - last) / (double)SDL_GetPerformanceFrequency());
        // std::cout << deltaTime << " deltaTime!\n";

        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        vertexBuffer.Bind();
        indexBuffer.Bind();
        texShader.Bind();

        GLCALL(glUniform4f(colorUniformLocation, 1.0f, 1.0f, 1.0f, 1.0f));
        GLCALL(glUniform1i(textureUniformLocation, 0));

        GLCALL(glActiveTexture(GL_TEXTURE0));
        GLCALL(glBindTexture(GL_TEXTURE_2D, texId));

        // normal game update, currently disabled
        GameUpdate(player, camera, deltaTime, texViewProjMatrixUniformLocation, close);

        texShader.Unbind();
        indexBuffer.Unbind();
        vertexBuffer.Unbind();

        // text drawing
        vertexBufferText.Bind();
        textShader.Bind();

        // text game update
        GameUpdateText(foodIdxs, textShader, textProjMatrixLocaction, player, text, vertexBufferText);

        textShader.Unbind();
        vertexBufferText.Unbind();

        SDL_GL_SwapWindow(window);

        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
            {
                close = true;
            }
            if (event.type == SDL_KEYDOWN && !foodIdxs.empty() && !player.died)
            {
                player.Move(event);
            }
            if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_l && (foodIdxs.empty() || player.died))
            {
                // reset level
                std::cout << "resetting level!\n";

                // reset vertex and index buffer
                glBufferData(GL_ARRAY_BUFFER, maxVertices, NULL, GL_DYNAMIC_DRAW);

                GLint vertexBufferSize = 0;
                glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &vertexBufferSize);

                glBufferData(GL_ELEMENT_ARRAY_BUFFER, vertexBufferSize * sizeof(indices[0]), NULL, GL_DYNAMIC_DRAW);

                // clear vectors and reset variables
                vertices.clear();
                indices.clear();
                numVertices = 0;
                numIndices = 0;
                indicesIndex = 0;
                enemies.clear();
                foods.clear();
                foodIdxs.clear();

                level.clear();
                levelDrawIdx.clear();
                waypointDrawIdx.clear();
                enemyDrawIdx.clear();
                foodDrawIdx.clear();
                playerDrawIdx = 0;
                foodIdx = 0;

                player.Reset();

                // create new Game Objects
                CreateLevel(player, camera);

                // subbuffer the new rect vertices
                glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices * sizeof(Vertex), vertices.data());

                // subbuffer the new rect indices
                glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, numIndices * sizeof(indices[0]), indices.data());
            }
        }
    }

    glDeleteTextures(1, &texId);

    return 0;
}

/*{
    // NOTE: Pathfinding Code starts here!!!
    // The code from the bfs tutorial

    // graph constructing
    // Initialize an empty adjacency list that can hold up to n nodes.
    // std::vector<std::vector<Edge>> CreateEmptyGraph(int n)
    // {
    //     std::vector<std::vector<Edge>> graph;
    //     for (int i = 0; i < n; i++)
    //         graph.push_back(std::vector<Edge>());
    //     return graph;
    // }

    // int main()
    // {
    //     // std::vector<int> dirs;                        // the directions the enemy can take in ints;
    //     // std::vector<int> prev;                        // the previous locations the enemy can move to in nodes/ints
    //     // std::vector<std::vector<glm::vec2>> gridVec2; // The glm::vec2 grid where positions get stored, so the enemy knows which node is which pos

    //     // // the start and end pos of the vec2 grid based on the waypoints
    //     // glm::vec2 WaypointPosStart(0.0f, 0.0f);
    //     // glm::vec2 WaypointPosEnd(20.0f, 40.0f);

    //     // // the curPos of the enemy and the waypoint he should walk to
    //     // glm::vec2 currentPos(0.0f, 0.0f);
    //     // glm::vec2 WaypointToWalk(20.0f, 40.0f);

    //     // // start and end pos of the enemy in the int/node grid
    //     // int start = 0, end = 0;

    //     // calculate the grid aka "transform" the vec2 to ints so that pathfinding works with adjacency list/matrix
    //     // start and end get changed to the curPos index and waypoint index on the int grid, respectively
    //     // std::vector<std::vector<int>> grid = GridCalculation(WaypointPosStart, WaypointPosEnd, start, end, currentPos, WaypointToWalk, gridVec2);

    //     // debug cout for grid and gridVec2
    //     // for (size_t i = 0; i < grid.size(); i++)
    //     // {
    //     //     for (size_t j = 0; j < grid.at(i).size(); j++)
    //     //     {
    //     //         std::cout << grid[i][j] << " ";
    //     //     }
    //     //     std::cout << "\n";
    //     // }
    //     // std::cout << "\n";

    //     // for (size_t i = 0; i < gridVec2.size(); i++)
    //     // {
    //     //     for (size_t j = 0; j < gridVec2.at(i).size(); j++)
    //     //     {
    //     //         std::cout << gridVec2[i][j].x << " " << gridVec2[i][j].y << " ";
    //     //     }
    //     //     std::cout << "\n";
    //     // }
    //     // std::cout << "\n";

    //     // reconstruct the path
    //     // std::vector<glm::vec2> positions = ReconstructPath(prev, grid, dirs, start, end);

    //     // debug cout for dirs vector
    //     // for (size_t i = 0; i < dirs.size(); i++)
    //     // {
    //     //     std::cout << " " << dirs.at(i) << " dirs at i\n";
    //     // }
    //     // std::cout << "\n";

    //     // // debug cout for positions vector
    //     // std::cout << "The PositionsVector is " << positions.size() << " nodes long\n";
    //     // for (size_t i = 0; i < positions.size(); i++)
    //     // {
    //     //     std::cout << positions.at(i).x << " x " << positions.at(i).y << " y\n";
    //     // }
    //     // std::cout << "\n";

    //     return 0;
    // }
}*/