#include "defines.hpp"

// needed for comparison between 2 Points
bool operator==(const Point &p1, const Point &p2)
{
    // if (v1.position == v2.position && v1.color == v2.color && v1.texCoords == v2.texCoords)
    if (p1.x == p2.x && p1.y == p2.y)
    {
        return true;
    }
    return false;
}