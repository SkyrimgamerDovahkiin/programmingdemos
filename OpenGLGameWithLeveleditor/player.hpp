#pragma once

// std
#include <map>

// GLEW/SDL
#define GLEW_STATIC
#include <GL/glew.h>
#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>

// Freetype
#include <ft2build.h>
#include FT_FREETYPE_H

// custom
#include "enemy.hpp"

using OGLE::Enemy;

namespace OGLE
{
    struct Player
    {
    public:
        Player();
        ~Player();

        void CheckLevelBounds(float xMax, float yMax);
        void CheckCollision(std::vector<Rectangle> level);
        void Move(SDL_Event event);
        void Create(float x, float y);
        void Draw(int drawIdx, int modelViewProjMatrixLocaction);
        void Update(double deltaTime, std::vector<Rectangle> level, std::vector<Enemy> enemies);
        void Reset();

        bool died = false;
        Rectangle rect;
        glm::mat4 model = glm::mat4(1.0f);
        glm::mat4 modelViewProj = glm::mat4(1.0f);
        
        // the start positions
        float startX;
        float startY;

    private:
        const float moveDistance = 20.0f;
        float leftRight = 0.0f;
        float upDown = 0.0f;

        // the direction of the player, needed for collision
        float direction = 0;
    };
}