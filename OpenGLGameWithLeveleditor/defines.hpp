#pragma once

#include <iostream>
#include <vector>

// glm
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

struct Vector3
{
    glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
    glm::vec3 down = glm::vec3(0.0f, -1.0f, 0.0f);
    glm::vec3 forward = glm::vec3(0.0f, 0.0f, 1.0f);
    glm::vec3 back = glm::vec3(0.0f, 0.0f, -1.0f);
    glm::vec3 right = glm::vec3(1.0f, 0.0f, 0.0f);
    glm::vec3 left = glm::vec3(-1.0f, 0.0f, 0.0f);
    glm::vec3 zero = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 one = glm::vec3(1.0f, 1.0f, 1.0f);
};

struct Vertex
{
    float x;
    float y;
    float z;

    float u;
    float v;

    float r;
    float g;
    float b;
    float a;
};

struct TextVertex
{
    glm::vec2 position;
    glm::vec2 texCoords;
    glm::vec3 color;
};

// the edge that will be added to the adjacency list for enemy pathfinding
struct Edge
{
    int from = 0, to = 0, cost = 1;
    // the direction that the enemy should take because the glm::vec2 get converted to nodes/ints.
    // Not to be confused with the direction of a directional graph, which isn't used here.
    int direction = 0;
};

struct Point
{
    float x, y;
};

struct Cell
{
    float x = 0, y = 0; // the x and y coords for the positions, so the enemy can move to them
    int index = 0;      // the index the cell gets assigned for pathfinding
};

// Defines a Rectangle. Values are x and y pos and width and height.
// x and y positions are the origin (top-left corner)
struct Rectangle
{
    float x;         // Rectangle position x
    float y;         // Rectangle position y
    float width;     // Rectangle width
    float height;    // Rectangle height
    glm::vec3 color; // Rectangle Color

    // Rectangle Points
    // glm::vec2 tl = {std::min(x, x + width), std::min(y, y + height)};
    // glm::vec2 br = {std::max(x, x + width), std::max(y, y + height)};
    // glm::vec2 tr = {std::max(x, x + width), std::min(y, y + height)};
    // glm::vec2 bl = {std::min(x, x + width), std::max(y, y + height)};
    inline Point tl() const
    {
        return {std::min(x, x + width), std::min(y, y + height)};
    }

    inline Point br() const
    {
        return {std::max(x, x + width), std::max(y, y + height)};
    }

    inline Point tr() const
    {
        return {std::max(x, x + width), std::min(y, y + height)};
    }

    inline Point bl() const
    {
        return {std::min(x, x + width), std::max(y, y + height)};
    }
};

// Collision check functions
// inline bool PointInRect(const Point &p, const Rectangle &r)
// {
//     return (p.x >= r.tl().x && p.x <= r.br().x && p.y >= r.tl().y && p.y <= r.br().y);
// }

// inline bool InRange(int i, int minI, int maxI)
// {
//     return (i >= minI && i <= maxI);
// }

// needed for comparison between 2 Points
bool operator==(const Point &p1, const Point &p2);

inline bool RectangleIntersect(const Rectangle &r1, const Rectangle &r2)
{
    if (r1.tl() == r2.tl() && r1.br() == r2.br() && r1.tr() == r2.tr() && r1.bl() == r2.bl())
    {
        return true;
    }
    return false;
    /*// Check 1 -- Any corner inside rect
    if ((PointInRect(r1.tl(), r2) || PointInRect(r1.br(), r2)) || (PointInRect(r1.tr(), r2) || PointInRect(r1.bl(), r2)))
        return true;

    // Check 2 -- Overlapped, but all points outside
    //     +---+
    //  +--+---+----+
    //  |  |   |    |
    //  +--+---+----+
    //     +---+
    if ((InRange(r1.tl().x, r2.tl().x, r2.br().x) || InRange(r1.br().x, r2.tl().x, r2.br().x)) && r1.tl().y < r2.tl().y && r1.br().y > r2.br().y || (InRange(r1.tl().y, r2.tl().y, r2.br().y) || InRange(r1.br().y, r2.tl().y, r2.br().y)) && r1.tl().x < r2.tl().x && r1.br().x > r2.br().x)
        return true;

    return false;*/
}

struct CharacterGlyph
{
    glm::vec2 Size;       // Size of glyph
    glm::vec2 Bearing;    // Offset from baseline to left/top of glyph
    unsigned int Advance; // Offset to advance to next glyph
    float tx;             // x offset of glyph in texture coordinates
};

/*void PositionTranslation()
{
    // fn convert(pos
    //            : f32, bound_window
    //            : f32, bound_game
    //            : f32)
    //     ->f32
    // {
    //     let tile_size = bound_window / bound_game;
    //     pos / bound_game *bound_window - (bound_window / 2.) + (tile_size / 2.)
    // }
    // for (pos, mut transform)
    //     in q.iter_mut()
    //     {
    //         transform.translation = Vec3::new (
    //             convert(pos.x as f32, WINDOW_WIDTH as f32, ARENA_WIDTH as f32),
    //             convert(pos.y as f32, WINDOW_HEIGHT as f32, ARENA_HEIGHT as f32),
    //             0.0, );
    //     }
}*/