#version 330 core
layout(location = 0) in vec3 a_position;
layout(location = 1) in vec2 a_tex_coord;
layout(location = 2) in vec4 a_color;

out vec4 v_color;
out vec2 v_tex_coord;

uniform mat4 u_modelViewProj;

void main()
{
    gl_Position = u_modelViewProj * vec4(a_position, 1.0f);
    v_tex_coord = a_tex_coord;
    v_color = a_color;
}