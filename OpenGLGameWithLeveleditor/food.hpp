#pragma once

// GLEW
#include <GL/glew.h>

// std
#include <cstdlib>
#include <algorithm>

// custom
#include "defines.hpp"

namespace OGLE
{
    struct Food
    {
    public:
        Food();
        ~Food();

        void Create(float posX, float posY);
        void Draw(int drawIdx, int modelViewProjMatrixLocaction);
        void Update(int drawIdx, int modelViewProjMatrixLocaction, Rectangle collider, std::vector<int> &foodIdx, int &score);

        Rectangle rect;
        glm::mat4 model = glm::mat4(1.0f);
        glm::mat4 modelViewProj = glm::mat4(1.0f);
        int idx;

        // needed so the foods can be moved correctly when camera is moved, because the start pos need to be subtracted
        float startPosX = 0.0f;
        float startPosY = 0.0f;

    private:
        bool canUpdate;
        int score = 10;
    };
}