#pragma once

// GLEW
#include <GL/glew.h>

// custom
#include "defines.hpp"
#include "time.hpp"
#include "food.hpp"

// std
#include <random>
#include <deque>
#include <algorithm>

using OGLE::Food;

namespace OGLE
{
    struct Enemy
    {
    public:
        Enemy();
        ~Enemy();

        void CheckCollision(int direction);
        bool CheckScreenBounds(float screenWidth, float screenHeight);
        void Move();
        void Patrol();
        void Create(float posX, float posY);
        void CreateWaypoints(float startX, float startY, float endX, float endY);
        void Draw(int drawIdx, int modelViewProjMatrixLocaction);
        void Reset();
        // void Update(int drawIdx, int modelViewProjMatrixLocaction, std::vector<Food> &colliders, std::vector<Enemy> enemies);
        void Update(int drawIdx, int modelViewProjMatrixLocaction, std::vector<Enemy> enemies, double deltaTime);

        // pathfinding
        void GridCalculation();
        void AddEdge(int u, int v, std::vector<std::vector<int>> &adjMat, std::vector<std::vector<Edge>> &adjList);
        void Bfs();
        void ReconstructPath();
        void CalculateRandomWaypoint();

        Rectangle rect;
        glm::mat4 model = glm::mat4(1.0f);
        glm::mat4 modelViewProj = glm::mat4(1.0f);

        // pathfinding
        std::vector<glm::vec2> waypoints; // the waypoints

        // the current waypoint (aka the waypoint the enemy should move to) and the last waypoint where he was
        glm::vec2 curWaypoint = glm::vec2(0.0f, 0.0f);
        glm::vec2 lastWaypoint = glm::vec2(0.0f, 0.0f);

        // needed so the enemies can be moved correctly when camera is moved, because the start pos need to be subtracted
        float startPosX = 0.0f;
        float startPosY = 0.0f;

    private:
        int direction = std::rand() % 4;
        const float moveSpeed = 20.0f;
        float leftRight = 0.0f;
        float upDown = 0.0f;
        double lastTimeMoved = 0.0; // the last time the enemy moved, initially 0
        double moveSeconds = 0.5;   // the time that should at least pass between every move in seconds
        std::vector<glm::vec2> out;

        // pathfinding
        std::vector<glm::vec2> dirs;         // the directions the enemy can take in vec2;
        std::vector<Cell> prev;              // the previous locations the enemy can move to in nodes/ints
        std::vector<std::vector<Cell>> grid; // the cell grid

        // the start and end pos of the vec2 grid based on the waypoints
        glm::vec2 WaypointPosStart = glm::vec2(0.0f, 0.0f);
        glm::vec2 WaypointPosEnd = glm::vec2(20.0f, 40.0f);

        // start and end pos of the enemy in the int/node grid
        int startIdx = 0, endIdx = 0;
        int testCounter = 0;

        // time
        OGLE::Time time;
    };
}