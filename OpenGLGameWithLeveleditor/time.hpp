#pragma once

#include <chrono>

namespace OGLE
{
    class Time
    {
    public:
        Time();
        ~Time();

        long ElapsedTime();

        std::chrono::high_resolution_clock::time_point start;
    private:
    };
}