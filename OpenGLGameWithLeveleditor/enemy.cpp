#include "enemy.hpp"

namespace OGLE
{
    Enemy::Enemy()
    {
        // timeLastMove = time.ElapsedTime();
    }

    Enemy::~Enemy()
    {
    }

    void Enemy::CheckCollision(int direction)
    {
        if (direction == 2)
        {
            leftRight += moveSpeed;
            rect.x += moveSpeed;
            modelViewProj = glm::translate(modelViewProj, glm::vec3(leftRight, 0.0f, 0.0f));
            leftRight = 0.0f;
        }
        else if (direction == 3)
        {
            leftRight -= moveSpeed;
            rect.x -= moveSpeed;
            modelViewProj = glm::translate(modelViewProj, glm::vec3(leftRight, 0.0f, 0.0f));
            leftRight = 0.0f;
        }
        else if (direction == 0)
        {
            upDown += moveSpeed;
            rect.y += moveSpeed;
            modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, upDown, 0.0f));
            upDown = 0.0f;
        }
        else if (direction == 1)
        {
            upDown -= moveSpeed;
            rect.y -= moveSpeed;
            modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, upDown, 0.0f));
            upDown = 0.0f;
        }
        else
        {
            std::cout << "Error, direction is wrong!\n";
        }
    }

    bool Enemy::CheckScreenBounds(float screenWidth, float screenHeight)
    {
        if (rect.x < 0.0f)
        {
            leftRight += moveSpeed;
            rect.x += moveSpeed;
            modelViewProj = glm::translate(modelViewProj, glm::vec3(leftRight, 0.0f, 0.0f));
            leftRight = 0.0f;
            return false;
        }
        else if (rect.x > (screenWidth - rect.width))
        {
            leftRight -= moveSpeed;
            rect.x -= moveSpeed;
            modelViewProj = glm::translate(modelViewProj, glm::vec3(leftRight, 0.0f, 0.0f));
            leftRight = 0.0f;
            return false;
        }
        else if (rect.y < 0.0f)
        {
            upDown += moveSpeed;
            rect.y += moveSpeed;
            modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, upDown, 0.0f));
            upDown = 0.0f;
            return false;
        }
        else if (rect.y > (screenHeight - rect.height))
        {
            upDown -= moveSpeed;
            rect.y -= moveSpeed;
            modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, upDown, 0.0f));
            upDown = 0.0f;
            return false;
        }
        else
        {
            return true;
        }
    }

    // pathfinding
    // grid calculation
    // method for converting from glm::vec2 to a grid adjacency array/matrix
    // TODO: need to use the first and last waypoint of the waypoint vector as calculation.
    // this method currently only works from 0, 0 to whatever the last waypoint coords are
    void Enemy::GridCalculation()
    {
        // start and end need to be always the first and last waypoint, respectively
        glm::vec2 start = waypoints.at(0);
        glm::vec2 end = waypoints.back();

        int row = 0, column = 0; // rows and columns for grid pathfinding
        int idx = 0;             // the cell index
        int pushCounter = 0;     // the push counter so the cells get pushed back correctly because start isn't always 0,0

        // fill grid
        {
            // for (size_t y = 0; y < end.y + 20; y += 20)
            for (size_t y = start.y; y < end.y + 20; y += 20)
            {
                grid.push_back(std::vector<Cell>());
                for (size_t x = start.x; x < end.x + 20; x += 20)
                // for (size_t x = 0; x < end.x + 20; x += 20)
                {
                    Cell cell;
                    cell.x = x;
                    cell.y = y;
                    cell.index = idx;

                    // set the start and end idx respectively
                    if (x == rect.x && y == rect.y)
                    {
                        startIdx = cell.index;
                    }
                    else if (x == end.x && y == end.y)
                    {
                        endIdx = cell.index;
                    }
                    idx++;

                    // grid.at(y / 20).push_back(cell);
                    grid.at(pushCounter).push_back(cell);
                }
                pushCounter++;
            }
        }

        // calculate rows and columns
        for (size_t i = 0; i < grid.size(); i++)
        {
            row++;
        }
        // using grid at 0 because a row always has the same number of columns (at least for now)
        for (size_t j = 0; j < grid.at(0).size(); j++)
        {
            column++;
        }

        // debug cout for grid
        // for (size_t i = 0; i < grid.size(); i++)
        // {
        //     for (size_t j = 0; j < grid.at(0).size(); j++)
        //     {
        //         std::cout << grid[i][j].index << " ";
        //     }
        //     std::cout << "\n";
        // }
    }

    // function to add edge into the adjacency matrix and adjacency list
    void Enemy::AddEdge(int u, int v, std::vector<std::vector<int>> &adjMat, std::vector<std::vector<Edge>> &adjList)
    {
        adjMat.at(u).at(v) = 1;
        adjMat.at(v).at(u) = 1;

        Edge edge;
        edge.from = u;
        edge.to = v;

        // Setting the edge direction used for moving the enemy.
        if (v > u && !(v > u + 1))
        {
            edge.direction = 1; // right
        }
        else if (v < u && !(v < u - 1))
        {
            edge.direction = -1; // left
        }
        else if (v > u + 1)
        {
            edge.direction = -2; // down
        }
        else if (v < u - 1)
        {
            edge.direction = 2; // up
        }

        adjList.at(u).push_back(edge);
        adjList.at(u).push_back(edge);
    }

    // path calculation
    void Enemy::Bfs()
    {
        int n = grid.back().back().index;                                     // using the last element idx in grid to determine adjacency matrix/list size
        std::vector<std::vector<int>> adjMat(n + 1, std::vector<int>(n + 1)); // adjacency matrix
        std::vector<std::vector<Edge>> graph;                                 // adjacency list

        // fill graph with empty values for n loops
        for (int i = 0; i < n + 1; i++)
            graph.push_back(std::vector<Edge>());

        // fill adjacency matrix and adjacency list with correct values
        {
            for (size_t i = 0; i < grid.size(); i++)
            {
                for (size_t j = 0; j < grid.at(i).size(); j++)
                {
                    if (i == 0)
                    {
                        if (j == 0)
                        {
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index + 1, adjMat, graph);
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index + grid.at(i).size(), adjMat, graph);
                        }
                        else if (j == grid.at(i).size() - 1)
                        {
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index - 1, adjMat, graph);
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index + grid.at(i).size(), adjMat, graph);
                        }
                        else
                        {
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index + 1, adjMat, graph);
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index - 1, adjMat, graph);
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index + grid.at(i).size(), adjMat, graph);
                        }
                    }
                    else if (i == grid.size() - 1)
                    {
                        if (j == 0)
                        {
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index + 1, adjMat, graph);
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index - grid.at(i).size(), adjMat, graph);
                        }
                        else if (j == grid.at(i).size() - 1)
                        {
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index - 1, adjMat, graph);
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index - grid.at(i).size(), adjMat, graph);
                        }
                        else
                        {
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index + 1, adjMat, graph);
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index - 1, adjMat, graph);
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index - grid.at(i).size(), adjMat, graph);
                        }
                    }
                    else
                    {
                        if (j == 0)
                        {
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index + 1, adjMat, graph);
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index - grid.at(i).size(), adjMat, graph);
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index + grid.at(i).size(), adjMat, graph);
                        }
                        else if (j == grid.at(i).size() - 1)
                        {
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index - 1, adjMat, graph);
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index - grid.at(i).size(), adjMat, graph);
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index + grid.at(i).size(), adjMat, graph);
                        }
                        else
                        {
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index + 1, adjMat, graph);
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index - 1, adjMat, graph);
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index - grid.at(i).size(), adjMat, graph);
                            AddEdge(grid.at(i).at(j).index, grid.at(i).at(j).index + grid.at(i).size(), adjMat, graph);
                        }
                    }
                }
            }
        }

        // Debug stuff to print the content of the graph to console
        /*std::cout << "The graph/adjacency list: \n";
        for (size_t i = 0; i < graph.size(); i++)
        {
            for (size_t j = 0; j < graph.at(i).size(); j++)
            {
                std::cout << graph[i][j].from << " from " << graph[i][j].to << " to\n";
            }
            std::cout << "\n";
        }*/

        prev.resize(graph.size());

        // actual pathfinding
        std::vector<bool> visited(graph.size(), false);
        std::deque<int> queue;

        // Start by visiting the 'startIdx' node and add it to the queue.
        queue.push_back(startIdx);
        visited.at(startIdx) = true;

        // Continue until the BFS is done.
        while (!queue.empty())
        {
            int node = queue.front();
            queue.pop_front();

            std::vector<Edge> edges = graph.at(node);

            // Loop through all edges attached to this node. Mark nodes as visited once they're
            // in the queue. This will prevent having duplicate nodes in the queue and speedup the BFS.
            for (Edge edge : edges)
            {
                if (!visited.at(edge.to))
                {
                    visited.at(edge.to) = true;
                    prev.at(edge.to).index = node;
                    queue.push_back(edge.to);
                }
            }
        }
    }

    // path reconstruction
    void Enemy::ReconstructPath()
    {
        // std::vector<int> path; // the node path. currently not needed beacuse we only need the directions
        // calculate path
        Bfs();

        for (int at = endIdx; at != startIdx; at = prev.at(at).index)
        {
            // path.push_back(at);
            // calculate directions and push them back to the dir vector
            if (at > prev.at(at).index && !(at > prev.at(at).index + 1))
            {
                // dirs.push_back(1); // right
                dirs.push_back(glm::vec2(20.0f, 0.0f)); // right
            }
            else if (at < prev.at(at).index && !(at < prev.at(at).index - 1))
            {
                // dirs.push_back(-1); // left
                dirs.push_back(glm::vec2(-20.0f, 0.0f)); // left
            }
            if (at > prev.at(at).index + 1)
            {
                // dirs.push_back(-2); // down
                dirs.push_back(glm::vec2(0.0f, 20.0f)); // down
            }
            else if (at < prev.at(at).index - 1)
            {
                // dirs.push_back(2); // up
                dirs.push_back(glm::vec2(0.0f, -20.0f)); // up
            }
        }
        // path.push_back(start);

        // std::reverse(path.begin(), path.end());
        std::reverse(dirs.begin(), dirs.end());

        // path.clear();
    }

    void Enemy::CalculateRandomWaypoint()
    {
        lastWaypoint = curWaypoint; // the current waypoint gets saved as the last for later

        // random selection of waypoint
        size_t nelems = 1;
        std::sample(
            waypoints.begin(),
            waypoints.end(),
            std::back_inserter(out),
            nelems,
            std::mt19937{std::random_device{}()});
        curWaypoint = out.at(0);

        // setting start and end index of enemy
        for (size_t i = 0; i < grid.size(); i++)
        {
            for (size_t j = 0; j < grid.at(i).size(); j++)
            {
                if (grid.at(i).at(j).x == rect.x && grid.at(i).at(j).y == rect.y)
                {
                    startIdx = grid.at(i).at(j).index;
                }
                if (grid.at(i).at(j).x == curWaypoint.x && grid.at(i).at(j).y == curWaypoint.y)
                {
                    endIdx = grid.at(i).at(j).index;
                }
            }
        }
    }

    void Enemy::Reset()
    {
        dirs.clear();
        out.clear();
        prev.clear();
        CalculateRandomWaypoint();

        // only do actual pathfinding if the startPos isn't the endPos
        if (startIdx != endIdx)
        {
            ReconstructPath();
        }
    }

    // patrolling
    void Enemy::Patrol()
    {
        if (dirs.empty())
        {
            dirs.clear();
            out.clear();
            prev.clear();
            CalculateRandomWaypoint();

            // only do actual pathfinding if the startPos isn't the endPos
            if (startIdx != endIdx)
            {
                ReconstructPath();
            }

            // for (size_t i = 0; i < dirs.size(); i++)
            // {
            //     std::cout << dirs.at(i).x << " " << dirs.at(i).y << " \n";
            // }
            // std::cout << "\n";
            // std::cout << startIdx << " " << endIdx << " \n\n";
        }

        for (size_t i = 0; i < dirs.size(); i++)
        {
            if (lastTimeMoved < moveSeconds)
            {
                return;
            }
            if (dirs.at(i) == glm::vec2(20.0f, 0.0f))
            {
                // right
                leftRight += dirs.at(i).x;
                rect.x += dirs.at(i).x;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(leftRight, 0.0f, 0.0f));
                leftRight = 0.0f;
            }
            else if (dirs.at(i) == glm::vec2(-20.0f, 0.0f))
            {
                // left. need to use "+=" because dirs.at(i).x already has the correct sign
                leftRight += dirs.at(i).x;
                rect.x += dirs.at(i).x;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(leftRight, 0.0f, 0.0f));
                leftRight = 0.0f;
            }
            else if (dirs.at(i) == glm::vec2(0.0f, 20.0f))
            {
                // down
                upDown += dirs.at(i).y;
                rect.y += dirs.at(i).y;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, upDown, 0.0f));
                upDown = 0.0f;
            }
            else if (dirs.at(i) == glm::vec2(0.0f, -20.0f))
            {
                // up. need to use "+=" because dirs.at(i).y already has the correct sign
                upDown += dirs.at(i).y;
                rect.y += dirs.at(i).y;
                modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, upDown, 0.0f));
                upDown = 0.0f;
            }

            dirs.erase(dirs.begin() + i);
            if (lastTimeMoved > moveSeconds)
            {
                lastTimeMoved = 0.0;
            }
        }
    }

    void Enemy::Move()
    {
        // if (lastTimeMoved < moveSeconds)
        // {
        //     return;
        // }
        // direction = std::rand() % 4;

        Patrol();

        // switch (direction)
        // {
        // case 0:
        // upDown -= moveSpeed;
        // rect.y -= moveSpeed;
        // modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, upDown, 0.0f));
        // upDown = 0.0f;
        // break;
        // case 1:
        // else if (glm::vec2(rect.x, rect.y) != curWaypoint)
        // {
        //     upDown += moveSpeed;
        //     rect.y += moveSpeed;
        //     modelViewProj = glm::translate(modelViewProj, glm::vec3(0.0f, upDown, 0.0f));
        //     upDown = 0.0f;
        // }
        // std::cout << "Current Waypoint: " << curWaypoint.x << " x " << curWaypoint.y << " y\n";

        // break;
        // case 2:
        //     leftRight -= moveSpeed;
        //     rect.x -= moveSpeed;
        //     modelViewProj = glm::translate(modelViewProj, glm::vec3(leftRight, 0.0f, 0.0f));
        //     leftRight = 0.0f;
        //     break;
        // case 3:
        //     leftRight += moveSpeed;
        //     rect.x += moveSpeed;
        //     modelViewProj = glm::translate(modelViewProj, glm::vec3(leftRight, 0.0f, 0.0f));
        //     leftRight = 0.0f;
        //     break;
        // default:
        //     break;
        // }
        // if (lastTimeMoved > moveSeconds)
        // {
        //     lastTimeMoved = 0.0;
        // }

        // timeLastMove = time.ElapsedTime();
    }

    void Enemy::Create(float posX, float posY)
    {
        // clear and reset everything to make sure path calculation works
        out.clear();
        dirs.clear();
        prev.clear();
        grid.clear();
        startIdx = 0;
        endIdx = 0;

        // spawn protection for player so the enemies don't spawn next to/inside of the player (old)
        // if (posX <= 60 && posY <= 60)
        // {
        //     posX = (std::rand() % screenWidth) / 20 * 20;
        //     posY = (std::rand() % screenHeight) / 20 * 20;
        // }

        startPosX = posX;
        startPosY = posY;

        rect = {posX, posY, 20.0f, 20.0f, glm::vec3(1.00f, 0.26f, 0.00f)};
    }

    void Enemy::CreateWaypoints(float startX, float startY, float endX, float endY)
    {
        waypoints.push_back(glm::vec2(startX, startY)); // topLeft
        waypoints.push_back(glm::vec2(endX, startY));   // topRight
        waypoints.push_back(glm::vec2(startX, endY));   // bottomLeft
        waypoints.push_back(glm::vec2(endX, endY));     // bottomRight

        // Debug
        // std::cout << "The waypoints:\n";
        // for (size_t i = 0; i < waypoints.size(); i++)
        // {
        //     std::cout << waypoints.at(i).x << " " << waypoints.at(i).y << " \n";
        // }
        // std::cout << "\n";

        curWaypoint = waypoints.back();
        lastWaypoint = curWaypoint;
        GridCalculation();
        ReconstructPath();
    }

    void Enemy::Draw(int drawIdx, int modelViewProjMatrixLocaction)
    {
        glUniformMatrix4fv(modelViewProjMatrixLocaction, 1, GL_FALSE, &modelViewProj[0][0]);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void *)(drawIdx * sizeof(GLuint)));
    }

    // void Enemy::Update(int drawIdx, int modelViewProjMatrixLocaction, std::vector<Food> &colliders, std::vector<Enemy> enemies)
    void Enemy::Update(int drawIdx, int modelViewProjMatrixLocaction, std::vector<Enemy> enemies, double deltaTime)
    {
        lastTimeMoved += deltaTime;
        // Move
        // if (TimeToMove())
        // {
        Move();
        // }

        // check if colliding with food
        /*for (size_t i = 0; i < colliders.size(); i++)
        {
            if (RectangleIntersect(rect, colliders.at(i).rect))
            {
                CheckCollision(direction);
            }
        }*/

        // check if colliding with another enemy
        /*for (size_t i = 0; i < enemies.size(); i++)
        {
            if (RectangleIntersect(rect, enemies.at(i).rect) && enemies.at(i).rect.x != rect.x && enemies.at(i).rect.y != rect.y)
            {
                CheckCollision(direction);
            }
        }*/

        // check if colliding with screen
        // CheckScreenBounds(600.0f, 600.0f);

        // Draw
        Draw(drawIdx, modelViewProjMatrixLocaction);
    }

    // bool Enemy::TimeToMove()
    // {
    //     return ((time.ElapsedTime() - timeLastMove) >= moveTime);
    // }
}