#pragma once

// std
#include <iostream>
#include <algorithm>
#include <string>
#include <fstream>
#include <map>

// GLEW
// #include <GL/glew.h>

// Freetype
#include <ft2build.h>
#include FT_FREETYPE_H

// custom
#include "shader.hpp"
#include "vertex_buffer_text.hpp"

namespace OGLE
{
    struct Text
    {
        Text();
        ~Text();

        void InitText(std::string filename);                                                                                                                        // initialize Text, needs to be AFTER the SDLContext was created because of glGenTextures
        void DrawString(float x, float y, float scale, std::string text, Shader *shader, glm::vec3 color, VertexBufferText &vertexBuffer, int modelViewProjMatrix); // draw text
        void CreateTextRect(Rectangle rect, TextVertex *&vData, uint32_t &numVertices, CharacterGlyph ch);

    private:
        glm::mat4 projectionMat = glm::ortho(0.0f, 600.0f, 600.0f, 0.0f);
        std::map<GLchar, CharacterGlyph> Characters;
        FT_Library ft;
        FT_Face face;
        FT_Bitmap bitmap;
        TextVertex *textVertexBufferData = 0;
        uint32_t textVertexBufferCapacity = 20;
        GLuint fontTexture;
        int atlasWidth, atlasHeight;
    };
}