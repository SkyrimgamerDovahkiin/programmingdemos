#include "vertex_buffer_text.hpp"

namespace OGLE
{
    VertexBufferText::VertexBufferText()
    {
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);

        glGenBuffers(1, &bufferId);
        glBindBuffer(GL_ARRAY_BUFFER, bufferId);

        glBufferData(GL_ARRAY_BUFFER, sizeof(TextVertex) * 6 * capacity, NULL, GL_DYNAMIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(TextVertex), (const void *)offsetof(TextVertex, position));
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(TextVertex), (const void *)offsetof(TextVertex, texCoords));
        glBindVertexArray(0);
    }

    VertexBufferText::~VertexBufferText()
    {
        glDeleteBuffers(1, &bufferId);
        glDeleteVertexArrays(1, &vao);
    }

    void VertexBufferText::Bind()
    {
        glBindVertexArray(vao);
        glBindBuffer(GL_ARRAY_BUFFER, bufferId);
    }

    void VertexBufferText::Unbind()
    {
        glBindVertexArray(0);
    }
}