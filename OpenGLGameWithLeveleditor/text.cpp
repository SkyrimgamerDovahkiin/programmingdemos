#include "text.hpp"

namespace OGLE
{
    Text::Text()
    {
        textVertexBufferData = new TextVertex[textVertexBufferCapacity * 6];
    }

    Text::~Text()
    {
        if (textVertexBufferData)
        {
            delete[] textVertexBufferData;
        }
    }

    void Text::InitText(std::string filename) // initialize Text, needs to be AFTER the SDLContext was created because of glGenTextures
    {
        if (FT_Init_FreeType(&ft))
        {
            std::cout << "ERROR::FREETYPE: Could not init FreeType Library" << std::endl;
        }

        if (FT_New_Face(ft, "fonts/arial/arial.ttf", 0, &face))
        {
            std::cout << "ERROR::FREETYPE: Failed to load font" << std::endl;
        }

        FT_Set_Pixel_Sizes(face, 0, 40);

        int w = 0;
        int h = 0;

        for (unsigned char c = 32; c < 128; c++)
        {
            if (FT_Load_Char(face, c, FT_LOAD_RENDER))
            {
                std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
                continue;
            }

            w += face->glyph->bitmap.width;
            h = std::max((unsigned int)h, face->glyph->bitmap.rows);
        }

        atlasWidth = w;
        atlasHeight = h;

        // create texture and initialize content to 0
        glActiveTexture(GL_TEXTURE0);
        glGenTextures(1, &fontTexture);
        glBindTexture(GL_TEXTURE_2D, fontTexture);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, w, h, 0, GL_RED, GL_UNSIGNED_BYTE, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        // fill texture
        int x = 0;
        for (unsigned char c = 32; c < 128; c++)
        {
            if (FT_Load_Char(face, c, FT_LOAD_RENDER))
            {
                std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
                continue;
            }

            CharacterGlyph character = {
                glm::vec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
                glm::vec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
                static_cast<unsigned int>(face->glyph->advance.x),
                (float)x / w};
            Characters.insert(std::pair<GLchar, CharacterGlyph>(c, character));

            glTexSubImage2D(GL_TEXTURE_2D, 0, x, 0, face->glyph->bitmap.width, face->glyph->bitmap.rows, GL_RED, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer);
            x += face->glyph->bitmap.width;
        }
        glBindTexture(GL_TEXTURE_2D, 0);

        FT_Done_Face(face);
        FT_Done_FreeType(ft);
    }

    void Text::DrawString(float x, float y, float scale, std::string text, Shader *shader, glm::vec3 color, VertexBufferText &vertexBuffer, int modelViewProjMatrix)
    {
        // set uniforms
        glUniform3f(glGetUniformLocation(shader->GetShaderId(), "text_color"), color.x, color.y, color.z);
        glUniform1i(glGetUniformLocation(shader->GetShaderId(), "text"), 0);

        // set vertex buffer capacity if not big enough
        uint32_t len = text.size();
        if (textVertexBufferCapacity < len)
        {
            textVertexBufferCapacity = len;
            glBufferData(GL_ARRAY_BUFFER, sizeof(TextVertex) * 6 * textVertexBufferCapacity, 0, GL_DYNAMIC_DRAW);
            delete[] textVertexBufferData;
            textVertexBufferData = new TextVertex[textVertexBufferCapacity * 6];
        }

        // activate and bind texture
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, fontTexture);

        // loop over text, create the rects and set the correct coords
        TextVertex *vData = textVertexBufferData;
        uint32_t numVertices = 0;

        std::string::const_iterator c;
        for (c = text.begin(); c != text.end(); c++)
        {
            if (*c >= 32 && *c < 128)
            {
                CharacterGlyph ch = Characters[*c];

                float xpos = x + ch.Bearing.x * scale;
                float ypos = y + ((ch.Size.y - ch.Bearing.y) - ch.Size.y + y) * scale;

                float w = ch.Size.x * scale;
                float h = ch.Size.y * scale;

                Rectangle rect = {xpos, ypos, w, h, color};
                CreateTextRect(rect, vData, numVertices, ch);

                x += (ch.Advance >> 6) * scale;
            }
        }

        // render glyph texture over quad
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(TextVertex) * numVertices, textVertexBufferData);
        glUniformMatrix4fv(modelViewProjMatrix, 1, GL_FALSE, glm::value_ptr(projectionMat));
        glDrawArrays(GL_TRIANGLES, 0, numVertices);

        glBindTexture(GL_TEXTURE_2D, 0);
    }

    void Text::CreateTextRect(Rectangle rect, TextVertex *&vData, uint32_t &numVertices, CharacterGlyph ch)
    {
        vData[0].position = glm::vec2(rect.x, rect.y);
        vData[0].texCoords = glm::vec2(ch.tx, 0);
        vData[0].color = rect.color;

        vData[1].position = glm::vec2(rect.x + rect.width, rect.y);
        vData[1].texCoords = glm::vec2(ch.tx + ch.Size.x / atlasWidth, 0);
        vData[1].color = rect.color;

        vData[2].position = glm::vec2(rect.x + rect.width, rect.y + rect.height);
        vData[2].texCoords = glm::vec2(ch.tx + ch.Size.x / atlasWidth, ch.Size.y / atlasHeight);
        vData[2].color = rect.color;

        vData[3].position = glm::vec2(rect.x, rect.y + rect.height);
        vData[3].texCoords = glm::vec2(ch.tx, ch.Size.y / atlasHeight);
        vData[3].color = rect.color;

        vData[4].position = glm::vec2(rect.x, rect.y);
        vData[4].texCoords = glm::vec2(ch.tx, 0);
        vData[4].color = rect.color;

        vData[5].position = glm::vec2(rect.x + rect.width, rect.y + rect.height);
        vData[5].texCoords = glm::vec2(ch.tx + ch.Size.x / atlasWidth, ch.Size.y / atlasHeight);
        vData[5].color = rect.color;

        vData += 6;
        numVertices += 6;
    };
}