#include "food.hpp"

namespace OGLE
{
    Food::Food(){};
    Food::~Food(){};

    void Food::Create(float posX, float posY)
    {
        canUpdate = true;

        startPosX = posX;
        startPosY = posY;

        rect = {posX, posY, 20.0f, 20.0f, glm::vec3(1.0f, 1.0f, 1.0f)};
    }

    void Food::Draw(int drawIdx, int modelViewProjMatrixLocaction)
    {
        glUniformMatrix4fv(modelViewProjMatrixLocaction, 1, GL_FALSE, &modelViewProj[0][0]);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void *)(drawIdx * sizeof(GLuint)));
    }

    void Food::Update(int drawIdx, int modelViewProjMatrixLocaction, Rectangle collider, std::vector<int> &foodIdx, int &score)
    {
        if (!canUpdate)
        {
            return;
        }

        if (RectangleIntersect(collider, rect))
        {
            canUpdate = false;

            auto obj = std::find(foodIdx.begin(), foodIdx.end(), idx);
            if (obj != foodIdx.end())
            {
                score += this->score;
                foodIdx.erase(obj);
            }
        }

        Food::Draw(drawIdx, modelViewProjMatrixLocaction);
    }
}