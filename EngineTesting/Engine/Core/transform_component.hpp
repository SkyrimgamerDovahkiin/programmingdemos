#pragma once

#include "../Core/config.hpp"

#include "component.hpp"

namespace OGLE::Core
{
    class Transform : public Component
    {
    public:
        Transform(std::shared_ptr<GameObject> gameObject);
        ~Transform();

        void Translate(glm::vec3 translation); // moves the transform by translation
        void Scale(glm::vec3 scale);           // scales the scale

        void SetPosition(glm::vec3 position); // sets the position
        void SetScale(glm::vec3 scale);       // sets the scale

        glm::vec3 GetPosition() { return mPosition; };
        glm::vec3 GetScale() { return mScale; };

    private:
        glm::vec3 mPosition;
        glm::vec3 mRotationEulers;
        glm::vec3 mScale;
    };
}