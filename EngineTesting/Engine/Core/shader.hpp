#pragma once

#include "config.hpp"

// custom
#include "../../defines.hpp"

// code from https://github.com/codingminecraft/MinecraftCloneForYoutube
struct ShaderVariable
{
    std::string name;
    GLint varLocation;
    uint32_t shaderProgramId;
    GLSLDataType dataType;

    bool operator==(const ShaderVariable &other) const
    {
        return other.shaderProgramId == shaderProgramId && other.name == name;
    }
};

template <>
struct std::hash<ShaderVariable>
{
    std::size_t operator()(const ShaderVariable &k) const
    {
        using std::hash;
        using std::size_t;
        using std::string;

        // Compute individual hash values for first,
        // second and third and combine them using XOR
        // and bit shifting:

        return (hash<string>()(k.name));
    }
};

namespace OGLE
{
    namespace Core
    {
        class Shader
        {
        public:
            Shader(std::string location);
            ~Shader();

            void Bind();
            void Unbind();
            GLuint GetShaderId();
            GLuint *GetShaderIdPointer();

            // searches the variables for the varName and shaderID and returns the location of a uniform if found
            GLint GetVariableLocation(Shader &shader, const std::string varName);

            void UploadFloat(const std::string varName, const float &_float);
            void UploadVec2(const std::string varName, const glm::vec2 &_vec2);
            void UploadVec3(const std::string varName, const glm::vec3 &_vec3);
            void UploadVec4(const std::string varName, const glm::vec4 &_vec4);
            void UploadMat3(const std::string varName, const glm::mat3 &_mat3);
            void UploadMat4(const std::string varName, const glm::mat4 &_mat4);
            // void uploadIVec4(const char *varName, const glm::ivec4 &vec4) const;
            // void uploadIVec3(const char *varName, const glm::ivec3 &vec3) const;
            // void uploadIVec2(const char *varName, const glm::ivec2 &vec2) const;
            // void uploadInt(const char *varName, int value) const;
            // void uploadIntArray(const char *varName, int length, const int *array) const;
            // void uploadUInt(const char *varName, uint32_t value) const;
            // void uploadBool(const char *varName, bool value) const;

            // the variables of the shader
            std::unordered_map<std::string, ShaderVariable> shaderVariables;

            std::string shaderName; // the name of the shader
            bool useNormalMaps; // bool to check if the shader allows using normal maps

        private:
            GLuint Compile(std::string shaderSource, GLenum type);
            void Parse(std::string filename);
            GLuint CreateShader();

            GLuint shaderId; // the shader id the shader has after compilation
            std::string outputFilePrefix;
            std::string vertexShaderFilename;
            std::string fragmentShaderFilename;
        };
    }
}