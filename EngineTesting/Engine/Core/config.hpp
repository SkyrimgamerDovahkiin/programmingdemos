// file for including (almost) every needed thing
#pragma once

// std
#include <iostream>
#include <cassert>
#include <filesystem>
#include <cmath>
#include <vector>
#include <variant>
#include <optional>
#include <list>
#include <fstream>
#include <sstream>
#include <ctime>
#include <algorithm>
#include <unordered_map>
#include <variant>
#include <deque>
#include <string>
#include <stdio.h>
#include <cstddef>
#include <any>
#include <memory>
#include <type_traits>

// glm
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// GLEW/SDL
#include <GL/glew.h>
#include <SDL2/SDL.h>

// assimp
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

// STB
#include "../../libs/stb/stb_image.h"

// ImGui
#include "../../libs/imgui/imgui.h"
#include "../../libs/imgui/imgui_impl_sdl2.h"
#include "../../libs/imgui/imgui_impl_opengl3.h"
#include "../../libs/imgui/imgui_stdlib.h"