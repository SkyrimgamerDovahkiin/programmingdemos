#pragma once
#include "config.hpp"

#include "../../defines.hpp"

namespace OGLE
{
    class VertexBuffer
    {
    public:
        VertexBuffer(void* data, uint32_t size);
        ~VertexBuffer();

        GLuint GetBufferID() { return bufferId; };
        GLuint GetBufferVAO() { return vao; };

        void Bind();
        void Unbind();

    private:
        GLuint bufferId;
        GLuint vao;
    };
}