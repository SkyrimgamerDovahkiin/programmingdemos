#pragma once

#include "../Core/config.hpp"

#include "component.hpp"
#include "material.hpp"
#include "../../defines.hpp"
#include "../Rendering/floating_camera.hpp"

using OGLE::FloatingCamera, OGLE::Core::Material;

namespace OGLE::Core
{
    class MeshRenderer : public Component
    {
    public:
        MeshRenderer(std::shared_ptr<FloatingCamera> camera,
        std::vector<Vertex> vertices,
        std::vector<uint32_t> indices,
        uint64_t numIndices,
        uint64_t numIndicesOffset,
        GLuint vertexBufferID,
        GLuint vertexBufferVAO,
        GLuint indexBufferID,
        std::shared_ptr<Shader>bbShader,
        std::shared_ptr<GameObject> gameObject,
        bool drawBB = false);

        ~MeshRenderer();

        void CalculateBoundingBox();

        // called when the camera is switched (first person to third person for example)
        void SetCamera(std::shared_ptr<FloatingCamera> camera) { mCamera = camera; };

        void SetMaterial(std::shared_ptr<Material> material) { mMaterial = material; };
        Material *GetMaterial() { return mMaterial.get(); };

        void SetModel(glm::mat4 model) { mModel = model; };
        glm::mat4 GetModel() { return mModel; };
        void SetModelViewProj(glm::mat4 modelViewProj) { mModelViewProj = modelViewProj; };
        glm::mat4 GetModelViewProj() { return mModelViewProj; };

        void Render();
        void DrawBoundingBox();


    private:
        std::shared_ptr<FloatingCamera> mCamera; // the camera the mesh should use

        glm::mat4 mModel = glm::mat4(1.0f);
        glm::mat4 mModelViewProj = glm::mat4(1.0f);
        glm::mat4 mBbModel = glm::mat4(1.0f);
        glm::mat4 mBbModelViewProj = glm::mat4(1.0f);

        glm::vec3 mBbMin; // min size of mesh bounding box;
        glm::vec3 mBbMax; // max size of mesh bounding box;

        std::vector<Vertex> mVertices;
        std::vector<uint32_t> mIndices;
        uint64_t mNumIndices = 0;       // the number of indices in total;
        uint64_t mNumIndicesOffset = 0; // the offset into the indices array where the mesh starts

        GLuint mVertexBufferID = 0;  // the id of the vertex buffer which the object should use for rendering;
        GLuint mVertexBufferVAO = 0; // the vao which the object should use for rendering;
        GLuint mIndexBufferID = 0;   // the the id of the index buffer which the object should use for rendering;

        std::shared_ptr<Material> mMaterial; // the material the mesh should use
        std::shared_ptr<Shader>mBbShader; // the bounding box shader the mesh should use

        bool mDrawBoundingBox = false; // if the mesh should draw its bounding box
    };
}