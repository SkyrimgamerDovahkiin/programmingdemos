#include "shader.hpp"

namespace OGLE
{
    namespace Core
    {
        Shader::Shader(std::string location)
        {
            Parse(location);           // create shader files
            shaderId = CreateShader(); // create shader program
        }

        Shader::~Shader()
        {
            glDeleteProgram(shaderId);
        }

        void Shader::Bind()
        {
            glUseProgram(shaderId);
        }

        void Shader::Unbind()
        {
            glUseProgram(0);
        }

        GLuint Shader::GetShaderId()
        {
            return shaderId;
        }

        GLuint *Shader::GetShaderIdPointer()
        {
            return &shaderId;
        }

        GLint Shader::GetVariableLocation(Shader &shader, const std::string varName)
        {
            auto iter = shaderVariables.find(varName);
            if (iter != shaderVariables.end())
            {
                return iter->second.varLocation;
            }

            return -1;
        }

        void Shader::UploadFloat(const std::string varName, const float &_float)
        {
            int varLocation = GetVariableLocation(*this, varName);
            glUniform1f(varLocation, _float);
        }

        void Shader::UploadVec2(const std::string varName, const glm::vec2 &_vec2)
        {
            int varLocation = GetVariableLocation(*this, varName);
            glUniform2f(varLocation, _vec2.x, _vec2.y);
        }

        void Shader::UploadVec3(const std::string varName, const glm::vec3 &_vec3)
        {
            int varLocation = GetVariableLocation(*this, varName);
            glUniform3f(varLocation, _vec3.x, _vec3.y, _vec3.z);
        }

        void Shader::UploadVec4(const std::string varName, const glm::vec4 &_vec4)
        {
            int varLocation = GetVariableLocation(*this, varName);
            glUniform4f(varLocation, _vec4.x, _vec4.y, _vec4.z, _vec4.w);
        }

        void Shader::UploadMat3(const std::string varName, const glm::mat3 &_mat3)
        {
            int varLocation = GetVariableLocation(*this, varName);
            glUniformMatrix3fv(varLocation, 1, GL_FALSE, glm::value_ptr(_mat3));
        }

        void Shader::UploadMat4(const std::string varName, const glm::mat4 &_mat4)
        {
            int varLocation = GetVariableLocation(*this, varName);
            glUniformMatrix4fv(varLocation, 1, GL_FALSE, glm::value_ptr(_mat4));
        }

        GLuint Shader::Compile(std::string shaderSource, GLenum type)
        {
            GLuint id = glCreateShader(type);
            const char *src = shaderSource.c_str();
            glShaderSource(id, 1, &src, 0);
            glCompileShader(id);

            int result;
            glGetShaderiv(id, GL_COMPILE_STATUS, &result);
            if (result != GL_TRUE)
            {
                int length = 0;
                glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
                char *message = new char[length];
                glGetShaderInfoLog(id, length, &length, message);
                std::cerr << "Shader compilation error: " << message << std::endl;
                delete[] message;
                return 0;
            }
            return id;
        }

        void Shader::Parse(std::string filename)
        {
            // read shader
            std::ifstream input = std::ifstream(filename, std::ios::in);

            // calculate outputFilePrefix
            outputFilePrefix = filename;

            for (size_t i = 0; i < 11; i++) // 11 because it's the length of the .OGLEShader suffix
            {
                outputFilePrefix.erase(outputFilePrefix.end() - 1);
            }

            // std::cout << outputFilePrefix << std::endl;

            // vertex and fragment output stream
            std::ofstream outputVertex = std::ofstream(outputFilePrefix + "_vert.glsl", std::ios::out);
            std::ofstream outputFragment = std::ofstream(outputFilePrefix + "_frag.glsl", std::ios::out);
            outputFragment.close();

            if (!input.is_open())
            {
                std::cout << "Error reading shader file!" << std::endl;
            }

            // read the first line and save the name of the shader
            input.ignore(std::numeric_limits<std::streamsize>::max(), '\"'); // ignore up until qoutation mark
            getline(input, shaderName);                                      // read first line
            shaderName.erase(shaderName.end() - 1);                          // erase the last char, aka the qoutation mark that comes after the name

            std::cout << shaderName << std::endl;

            // read rest of the file and save the shaders
            char b;
            std::string buffer;
            bool canInput = false;

            while (input.get(b))
            {
                buffer.push_back(b);
                if (b == '\n' && canInput)
                {
                    if (outputVertex.is_open())
                    {
                        outputVertex << buffer;
                    }
                    if (outputFragment.is_open())
                    {
                        outputFragment << buffer;
                    }
                    buffer.clear();
                }
                else if (b == '\n' && !canInput)
                {
                    // just clear buffer when input disabled
                    buffer.clear();
                }
                else if (b == '/' && input.peek() == '/') // when comment, ignore
                {
                    buffer.clear();
                    input.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                }
                if (buffer == "#start GLSLVERT")
                {
                    canInput = true;
                    buffer.clear(); // clear buffer because we don't want the start vertex inside of the vertex shader
                }
                if (buffer == "#start GLSLFRAG")
                {
                    outputFragment.open(outputFilePrefix + "_frag.glsl"); // open fragment shader output
                    buffer.clear();                                       // clear buffer because we don't want the start vertex inside of the vertex shader
                }
                if (buffer == "#end GLSLFRAG")
                {
                    canInput = false;
                    outputFragment.close(); // close fragment shader output
                    buffer.clear();         // clear buffer because we don't want the start vertex inside of the vertex shader
                }
                if (buffer == "#end GLSLVERT")
                {
                    outputVertex.close(); // close vertex shader output
                    buffer.clear();       // clear buffer because we don't want the start vertex inside of the vertex shader
                }
                // std::cout << buffer << std::endl;
            }
            input.close();

            // set vertex and fragment shader output name for creating shader
            vertexShaderFilename = outputFilePrefix + "_vert.glsl";
            fragmentShaderFilename = outputFilePrefix + "_frag.glsl";
        }

        // GLuint Shader::CreateShader(const char *vertexShaderFilename, const char *fragmentShaderFilename)
        GLuint Shader::CreateShader()
        {
            // std::string vertexShaderSource = Parse(vertexShaderFilename);
            // std::string fragmentShaderSource = Parse(fragmentShaderFilename);

            std::string vertexShaderSource;
            std::string fragmentShaderSource;
            std::stringstream ss;

            std::ifstream inputVertex = std::ifstream(vertexShaderFilename, std::ios::in);
            std::ifstream inputFragment = std::ifstream(fragmentShaderFilename, std::ios::in);

            // read vertex shader into string
            char b;
            while (inputVertex.get(b))
            {
                vertexShaderSource.push_back(b);
            }
            inputVertex.close();
            // std::cout << vertexShaderSource << std::endl;

            // read fragment shader into string
            while (inputFragment.get(b))
            {
                fragmentShaderSource.push_back(b);
            }
            inputFragment.close();
            // std::cout << fragmentShaderSource << std::endl;

            GLuint program = glCreateProgram();
            GLuint vs = Compile(vertexShaderSource, GL_VERTEX_SHADER);
            GLuint fs = Compile(fragmentShaderSource, GL_FRAGMENT_SHADER);

            glAttachShader(program, vs);
            glAttachShader(program, fs);
            glLinkProgram(program);

#ifdef _RELEASE
            glDetachShader(program, vs);
            glDetachShader(program, fs);

            glDeleteShader(vs);
            glDeleteShader(fs);
#endif

            // should't be needed, but we do it anyway
            outputFilePrefix.clear();
            vertexShaderSource.clear();
            fragmentShaderSource.clear();

            // get uniforms
            int numUniforms;
            glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &numUniforms);

            int maxCharLength;
            glGetProgramiv(program, GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxCharLength);
            if (numUniforms > 0) // only do this when uniforms exist
            {
                char *buffer = (char *)malloc(sizeof(char) * maxCharLength);

                for (size_t i = 0; i < numUniforms; i++)
                {
                    int length, size;
                    GLenum dataType;
                    glGetActiveUniform(program, (GLuint)i, maxCharLength, &length, &size, &dataType, buffer);
                    GLint varLocation = glGetUniformLocation(program, buffer);

                    ShaderVariable shaderVariable;
                    shaderVariable.name = std::string(buffer, length);
                    shaderVariable.varLocation = varLocation;
                    shaderVariable.shaderProgramId = program;

                    if (dataType == GL_FLOAT)
                    {
                        shaderVariable.dataType = GLSLDataType::DATATYPE_FLOAT;
                        // std::cout << " float\n";
                    }
                    else if (dataType == GL_FLOAT_VEC2)
                    {
                        shaderVariable.dataType = GLSLDataType::DATATYPE_VEC2;
                        // std::cout << " vec 2\n";
                    }
                    else if (dataType == GL_FLOAT_VEC3)
                    {
                        shaderVariable.dataType = GLSLDataType::DATATYPE_VEC3;
                        // std::cout << " vec 3\n";
                    }
                    else if (dataType == GL_FLOAT_VEC4)
                    {
                        shaderVariable.dataType = GLSLDataType::DATATYPE_VEC4;
                        // std::cout << " vec 4\n";
                    }
                    else if (dataType == GL_FLOAT_MAT4)
                    {
                        shaderVariable.dataType = GLSLDataType::DATATYPE_MAT4;
                        // std::cout << " mat 4\n";
                    }
                    // shaderVariables.emplace(std::pair(shaderVariable, shaderVariable.name));
                    shaderVariables.emplace(std::pair(shaderVariable.name, shaderVariable));

                    // std::cout << "Name: " << buffer << " Location: " << varLocation << " Used Shader: " << program << "\n";
                }
                free(buffer);
            }

            return program;
        }
    }
}