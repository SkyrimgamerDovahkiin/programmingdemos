#include "transform_component.hpp"

namespace OGLE::Core
{
    Transform::Transform(std::shared_ptr<GameObject> gameObject) : Component(gameObject)
    {
    }

    Transform::~Transform()
    {
    }

    void Transform::Translate(glm::vec3 translation)
    {
        mPosition += translation;
    }

    void Transform::Scale(glm::vec3 scale)
    {
        mScale += scale;
    }

    void Transform::SetPosition(glm::vec3 position)
    {
        mPosition = position;
    }

    void Transform::SetScale(glm::vec3 scale)
    {
        mScale = scale;
    }
}