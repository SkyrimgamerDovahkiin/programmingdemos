#pragma once

#include "config.hpp"

#include "../../defines.hpp"

namespace OGLE
{
    class FrameBuffer
    {
    public:
        FrameBuffer();
        ~FrameBuffer();

        void Bind();
        void Unbind();

        void ResizeTexture(int width, int height);

        GLuint textureColor;
        GLuint textureDepth;
    private:
        GLuint bufferId;
        GLuint fbo;
    };
}