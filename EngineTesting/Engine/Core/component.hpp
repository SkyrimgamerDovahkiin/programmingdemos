#pragma once

#include "../Core/config.hpp"
#include "game_object.hpp"

using OGLE::Core::GameObject;

namespace OGLE::Core
{
    class Component
    {
    public:
        Component(std::shared_ptr<GameObject> gameObject);
        virtual ~Component();
        
        std::shared_ptr<GameObject> mGameObject;
    private:
    };
}