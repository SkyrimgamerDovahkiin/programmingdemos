#pragma once

#include "../Core/config.hpp"

#include "transform_component.hpp"
#include "mesh_renderer_component.hpp"

namespace OGLE::Core
{
    enum ComponentType
    {
        CT_Transform,
        CT_MeshRenderer
    };

    class GameObject
    {
    public:
        GameObject(std::shared_ptr<Transform> transform);
        ~GameObject();

        Transform GetTransform(); // returns the transform of the game object

        void AddMeshRenderer(std::shared_ptr<MeshRenderer> meshRenderer) { mMeshRenderer = meshRenderer; };

        // should usually be done before the main game loop because it is relatively slow
        template <typename T>
        void AddComponent(T *t)
        {
            if (std::is_same<T, Transform>())
            {
                std::pair<ComponentType, Component *> test(CT_Transform, t);
                mComponents.emplace(test);
            }
            else if (std::is_same<T, MeshRenderer>())
            {
                std::pair<ComponentType, Component *> test(CT_MeshRenderer, t);
                mComponents.emplace(test);
            }
        }

        // should usually be done before the main game loop because it is relatively slow
        template <typename T>
        T *GetComponent()
        {
            if (std::is_same<T, Transform>())
            {
                ComponentType type;
                type = ComponentType::CT_Transform;
                auto it = mComponents.find(type);
                if (it != mComponents.end())
                {
                    return dynamic_cast<T *>(it->second);
                }
                else
                {
                    return nullptr;
                }
            }
            else if (std::is_same<T, MeshRenderer>())
            {
                ComponentType type;
                type = ComponentType::CT_MeshRenderer;
                auto it = mComponents.find(type);
                if (it != mComponents.end())
                {
                    return dynamic_cast<T *>(it->second);
                }
                else
                {
                    return nullptr;
                }
            }
            else
            {
                return nullptr;
            }
        }

        // name of the GO
        std::string name;
    private:
        std::shared_ptr<Transform> mTransform;                                 // every game object can only have one transform and also needs one
        std::shared_ptr<MeshRenderer> mMeshRenderer = nullptr; // the mesh renderer, initially nullptr
    };
}