#include "component.hpp"

namespace OGLE::Core
{
    Component::Component(std::shared_ptr<GameObject> gameObject)
    {
        mGameObject = gameObject;
    }

    Component::~Component()
    {
    }
}