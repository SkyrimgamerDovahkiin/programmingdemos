#include "game_object.hpp"

namespace OGLE::Core
{
    GameObject::GameObject(std::shared_ptr<Transform> transform)
    {
        mTransform = transform;
    }

    GameObject::~GameObject()
    {
    }

    Transform GameObject::GetTransform()
    {
        return *mTransform;
    }
}