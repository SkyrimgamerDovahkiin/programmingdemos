#pragma once

#include "config.hpp"

// custom
#include "shader.hpp"

// #include "../../defines.hpp"

struct Uniform
{
    std::string uniformName;
    GLSLDataType dataType;
    std::any value;

    bool operator==(const Uniform &other) const
    {
        return other.uniformName == uniformName;
    }
};

namespace OGLE
{
    namespace Core
    {
        class Material
        {
        public:
            Material(std::shared_ptr<Shader> shader);
            Material(const Material &other);
            ~Material();

            void SetShader(std::shared_ptr<Shader> shader); // sets the shader the material should use
            Shader *GetShader() { return mShader.get(); }

            std::string GetShaderName() { return mShader->shaderName; }
            GLuint GetShaderID() { return mShader->GetShaderId(); }

            std::unordered_map<std::string, int>* GetMUniformInts() { return &mUniformInts; }
            std::unordered_map<std::string, float>* GetMUniformFloats() { return &mUniformFloats; }
            std::unordered_map<std::string, glm::vec2>* GetMUniformFloat2s() { return &mUniformFloat2s; }
            std::unordered_map<std::string, glm::vec3>* GetMUniformFloat3s() { return &mUniformFloat3s; }
            std::unordered_map<std::string, glm::vec4>* GetMUniformFloat4s() { return &mUniformFloat4s; }
            std::unordered_map<std::string, glm::mat3>* GetMUniformMat3s() { return &mUniformMat3s; }
            std::unordered_map<std::string, glm::mat4>* GetMUniformMat4s() { return &mUniformMat4s; }

            void UpdateShaderUniforms();

#define GETUNIFORMVALUE(mapName, defaultReturn) \
    const auto &it = mapName.find(name);        \
    if (it != mapName.end())                    \
    {                                           \
        return it->second;                      \
    }                                           \
    return defaultReturn;

            template <typename T>
            T GetUniformValue(const std::string &name) const
            {
                if constexpr (std::is_same<T, int>())
                {
                    GETUNIFORMVALUE(mUniformInts, 0);
                }
                else if constexpr (std::is_same<T, float>())
                {
                    GETUNIFORMVALUE(mUniformFloats, 0.f);
                }
                else if constexpr (std::is_same<T, glm::vec2>())
                {
                    GETUNIFORMVALUE(mUniformFloat2s, glm::vec2(0.f));
                }
                else if constexpr (std::is_same<T, glm::vec3>())
                {
                    GETUNIFORMVALUE(mUniformFloat3s, glm::vec3(0.f));
                }
                else if constexpr (std::is_same<T, glm::vec4>())
                {
                    GETUNIFORMVALUE(mUniformFloat4s, glm::vec4(0.f));
                }
                else if constexpr (std::is_same<T, glm::mat3>())
                {
                    GETUNIFORMVALUE(mUniformMat3s, glm::mat3(1.f));
                }
                else if constexpr (std::is_same<T, glm::mat4>())
                {
                    GETUNIFORMVALUE(mUniformMat4s, glm::mat4(1.f));
                }
                else
                {
                    std::cerr << "Unsupported data type in Material::GetUniformValue()" << std::endl;
                    // static_assert(false, "Unsupported data type in Material::GetUniformValue()");
                }
            }

#undef GETUNIFORMVALUE

            template <typename T>
            void SetUniformValue(const std::string &name, const T &val)
            {
                if constexpr (std::is_same<T, int>())
                {
                    mUniformInts.at(name) = val;
                }
                else if constexpr (std::is_same<T, float>())
                {
                    mUniformFloats.at(name) = val;
                }
                else if constexpr (std::is_same<T, glm::vec2>())
                {
                    mUniformFloat2s.at(name) = val;
                }
                else if constexpr (std::is_same<T, glm::vec3>())
                {
                    mUniformFloat3s.at(name) = val;
                }
                else if constexpr (std::is_same<T, glm::vec4>())
                {
                    mUniformFloat4s.at(name) = val;
                }
                else if constexpr (std::is_same<T, glm::mat3>())
                {
                    mUniformMat3s.at(name) = val;
                }
                else if constexpr (std::is_same<T, glm::mat4>())
                {
                    mUniformMat4s.at(name) = val;
                }
                else
                {
                    std::cerr << "Unsupported data type in Material::SetUniformValue()" << std::endl;
                    // static_assert(false, "Unsupported data type in Material::SetUniformValue()");
                }
            }

        private:
            std::shared_ptr<Shader> mShader;

            // Data
            std::unordered_map<std::string, int> mUniformInts;
            std::unordered_map<std::string, float> mUniformFloats;
            std::unordered_map<std::string, glm::vec2> mUniformFloat2s;
            std::unordered_map<std::string, glm::vec3> mUniformFloat3s;
            std::unordered_map<std::string, glm::vec4> mUniformFloat4s;
            std::unordered_map<std::string, glm::mat3> mUniformMat3s;
            std::unordered_map<std::string, glm::mat4> mUniformMat4s;
        };
    }
}