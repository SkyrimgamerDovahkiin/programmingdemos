#include "material.hpp"

namespace OGLE
{
    namespace Core
    {
        Material::Material(std::shared_ptr<Shader> shader) : mShader(shader)
        {
            if (mShader == nullptr)
            {
                assert("shader is null");
            }

            for (const auto &it : mShader->shaderVariables)
            {
                if (it.first == "u_modelViewProj" || it.first == "u_modelView" || it.first == "u_invModelView")
                {
                    continue;
                }

                if (it.second.dataType == GLSLDataType::DATATYPE_FLOAT)
                {
                    mUniformFloats.emplace(std::pair(it.first, 1.0f));
                    // std::cout << " float\n";
                }
                else if (it.second.dataType == GLSLDataType::DATATYPE_VEC2)
                {
                    mUniformFloat2s.emplace(std::pair(it.first, glm::vec2(1.0f)));
                    // std::cout << " vec 2\n";
                }
                else if (it.second.dataType == GLSLDataType::DATATYPE_VEC3)
                {
                    mUniformFloat3s.emplace(std::pair(it.first, glm::vec3(1.0f)));
                    // std::cout << " vec 3\n";
                }
                else if (it.second.dataType == GLSLDataType::DATATYPE_VEC4)
                {
                    mUniformFloat4s.emplace(std::pair(it.first, glm::vec4(1.0f)));
                    // std::cout << " vec 4\n";
                }
                else if (it.second.dataType == GLSLDataType::DATATYPE_MAT4)
                {
                    mUniformMat4s.emplace(std::pair(it.first, glm::mat4(1.0f)));
                    // std::cout << " mat 4\n";
                }
            }
        }

        Material::Material(const Material &other)
        {
            mShader = other.mShader;

            mUniformInts = other.mUniformInts;
            mUniformFloats = other.mUniformFloats;
            mUniformFloat2s = other.mUniformFloat2s;
            mUniformFloat3s = other.mUniformFloat3s;
            mUniformFloat4s = other.mUniformFloat4s;
            mUniformMat3s = other.mUniformMat3s;
            mUniformMat4s = other.mUniformMat4s;
        }

        Material::~Material() {}

        void Material::SetShader(std::shared_ptr<Shader> shader)
        {
            if (shader == nullptr)
            {
                assert("shader is null");
            }
            else
            {
                this->mShader = shader;
                mUniformInts.clear();
                mUniformFloats.clear();
                mUniformFloat2s.clear();
                mUniformFloat3s.clear();
                mUniformFloat4s.clear();
                mUniformMat3s.clear();
                mUniformMat4s.clear();

                for (const auto &it : shader->shaderVariables)
                {
                    if (it.first == "u_modelViewProj" || it.first == "u_modelView" || it.first == "u_invModelView")
                    {
                        continue;
                    }

                    if (it.second.dataType == GLSLDataType::DATATYPE_FLOAT)
                    {
                        mUniformFloats.emplace(std::pair(it.first, 1.0f));
                        // std::cout << " float\n";
                    }
                    else if (it.second.dataType == GLSLDataType::DATATYPE_VEC2)
                    {
                        mUniformFloat2s.emplace(std::pair(it.first, glm::vec2(1.0f)));
                        // std::cout << " vec 2\n";
                    }
                    else if (it.second.dataType == GLSLDataType::DATATYPE_VEC3)
                    {
                        mUniformFloat3s.emplace(std::pair(it.first, glm::vec3(1.0f)));
                        // std::cout << " vec 3\n";
                    }
                    else if (it.second.dataType == GLSLDataType::DATATYPE_VEC4)
                    {
                        mUniformFloat4s.emplace(std::pair(it.first, glm::vec4(1.0f)));
                        // std::cout << " vec 4\n";
                    }
                    else if (it.second.dataType == GLSLDataType::DATATYPE_MAT4)
                    {
                        mUniformMat4s.emplace(std::pair(it.first, glm::mat4(1.0f)));
                        // std::cout << " mat 4\n";
                    }
                }
            }
        }

        void Material::UpdateShaderUniforms()
        {
            if (mShader)
            {
                for (const auto &it : mUniformFloats)
                {
                    mShader->UploadFloat(it.first, it.second);
                }
                for (const auto &it : mUniformFloat2s)
                {
                    mShader->UploadVec2(it.first, it.second);
                }
                for (const auto &it : mUniformFloat3s)
                {
                    mShader->UploadVec3(it.first, it.second);
                }
                for (const auto &it : mUniformFloat4s)
                {
                    mShader->UploadVec4(it.first, it.second);
                }
                for (const auto &it : mUniformMat4s)
                {
                    mShader->UploadMat4(it.first, it.second);
                }
            }
        }
    }
}