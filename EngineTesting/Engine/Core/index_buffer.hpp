#pragma once
#include "config.hpp"

#include "../../defines.hpp"

namespace OGLE
{
    class IndexBuffer
    {
    public:
        IndexBuffer(void *data, uint32_t size);
        ~IndexBuffer();

        GLuint GetBufferID() { return bufferId; };

        void Bind();
        void Unbind();

    private:
        GLuint bufferId;
    };
}