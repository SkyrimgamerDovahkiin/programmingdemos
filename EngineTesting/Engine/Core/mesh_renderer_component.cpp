#include "mesh_renderer_component.hpp"

namespace OGLE::Core
{
    MeshRenderer::MeshRenderer(std::shared_ptr<FloatingCamera> camera,
                               std::vector<Vertex> vertices,
                               std::vector<uint32_t> indices,
                               uint64_t numIndices,
                               uint64_t numIndicesOffset,
                               GLuint vertexBufferID,
                               GLuint vertexBufferVAO,
                               GLuint indexBufferID,
                               std::shared_ptr<Shader> bbShader,
                               std::shared_ptr<GameObject> gameObject,
                               bool drawBB)
        : Component(gameObject)
    {
        mCamera = camera;
        mVertices = vertices;
        mIndices = indices;
        mNumIndices = numIndices;
        mNumIndicesOffset = numIndicesOffset;
        mVertexBufferID = vertexBufferID;
        mVertexBufferVAO = vertexBufferVAO;
        mIndexBufferID = indexBufferID;
        mBbShader = bbShader;
        mDrawBoundingBox = drawBB;

        CalculateBoundingBox();
    }

    MeshRenderer::~MeshRenderer()
    {
    }

    void MeshRenderer::CalculateBoundingBox()
    {
        // when mesh has no vertices, return
        if (mVertices.size() == 0)
        {
            std::cerr << "Mesh vertices vector size is 0!" << std::endl;
            return;
        }

        GLfloat minX, maxX, minY, maxY, minZ, maxZ;

        minX = maxX = mVertices[0].positions.x;
        minY = maxY = mVertices[0].positions.y;
        minZ = maxZ = mVertices[0].positions.z;

        for (int i = 0; i < mVertices.size(); i++)
        {
            if (mVertices[i].positions.x < minX)
                minX = mVertices[i].positions.x;
            if (mVertices[i].positions.x > maxX)
                maxX = mVertices[i].positions.x;
            if (mVertices[i].positions.y < minY)
                minY = mVertices[i].positions.y;
            if (mVertices[i].positions.y > maxY)
                maxY = mVertices[i].positions.y;
            if (mVertices[i].positions.z < minZ)
                minZ = mVertices[i].positions.z;
            if (mVertices[i].positions.z > maxZ)
                maxZ = mVertices[i].positions.z;
        }

        glm::vec3 size = glm::vec3(maxX - minX, maxY - minY, maxZ - minZ);
        glm::vec3 center = glm::vec3((minX + maxX) / 2, (minY + maxY) / 2, (minZ + maxZ) / 2);
        glm::mat4 transform = glm::translate(glm::mat4(1), center) * glm::scale(glm::mat4(1), size);

        mBbModelViewProj = mModelViewProj * transform;

        mBbMin = glm::vec3(minX, minY, minZ); // set min size of mesh bb;
        mBbMax = glm::vec3(maxX, maxY, maxZ); // set max size of mesh bb;
    }

    void MeshRenderer::Render()
    {
        mModelViewProj = mCamera->GetViewProj() * mModel;

        // should only be done if we want to use normal maps
        if (mMaterial->GetShader()->useNormalMaps)
        {
            glm::mat4 modelView = mCamera->GetView() * mModel;
            glm::mat4 invModelView = glm::transpose(glm::inverse(modelView));

            mMaterial->GetShader()->UploadMat4("u_modelView", modelView);
            mMaterial->GetShader()->UploadMat4("u_invModelView", invModelView);
        }

        // do always because there should always be a camera position uniform (if not, this gives an error anyway)
        mMaterial->GetShader()->UploadMat4("u_modelViewProj", mModelViewProj);

        mMaterial->UpdateShaderUniforms();
        glDrawElements(GL_TRIANGLES, mNumIndices, GL_UNSIGNED_INT, (void *)(mNumIndicesOffset * sizeof(GLuint)));
    }

    void MeshRenderer::DrawBoundingBox()
    {
        glm::vec3 size = glm::vec3(mBbMax.x - mBbMin.x, mBbMax.y - mBbMin.y, mBbMax.z - mBbMin.z);
        glm::vec3 center = glm::vec3((mBbMin.x + mBbMax.x) / 2, (mBbMin.y + mBbMax.y) / 2, (mBbMin.z + mBbMax.z) / 2);
        glm::mat4 transform = glm::translate(glm::mat4(1), center) * glm::scale(glm::mat4(1), size);

        mBbModel = mModel * transform;
        // /* multiply object's modelViewProj matrix with transform to get bounding box */
        mBbModelViewProj = mModelViewProj * transform;

        if (!mDrawBoundingBox)
        {
            return;
        }

        mBbShader->Bind();
        mBbShader->UploadVec4("u_color", glm::vec4(0.075f, 0.6f, 0.0f, 1.0f));
        mBbShader->UploadMat4("u_modelViewProj", mBbModelViewProj);
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, 0);
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid *)(4 * sizeof(GLuint)));
        glDrawElements(GL_LINES, 8, GL_UNSIGNED_INT, (GLvoid *)(8 * sizeof(GLuint)));
        mBbShader->Unbind();
    }
}