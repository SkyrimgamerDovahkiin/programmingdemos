#include "input_manager.hpp"

namespace OGLE::Input
{
    InputManager::InputManager(bool *close)
    {
        mClose = close;
    }

    InputManager::~InputManager()
    {
    }

    void InputManager::HandleInput()
    {
        while (SDL_PollEvent(&mEvent))
        {
            ImGui_ImplSDL2_ProcessEvent(&mEvent); // Forward your event to backend
            if (mEvent.type == SDL_QUIT)
            {
                *mClose = true;
            }
        }
    }
}