#pragma once

#include "../Core/config.hpp"

namespace OGLE::Input
{
    class InputManager
    {
    public:
        InputManager(bool *close);
        ~InputManager();

        void HandleInput();

    private:
        SDL_Event mEvent;
        const Uint8 *mKeystate = SDL_GetKeyboardState(NULL); // for continuous key presses
        bool *mClose;
        int mMouseX, mMouseY; // needed for object selection
    };
}