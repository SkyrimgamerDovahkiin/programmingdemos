#pragma once

#include "../Core/config.hpp"

// custom
#include "display_manager.hpp"
#include "floating_camera.hpp"
#include "../../mesh.hpp"
#include "../Core/frame_buffer.hpp"

// ImGUI
#include "../../libs/imgui/imgui.h"
#include "../../libs/imgui/imgui_impl_sdl2.h"
#include "../../libs/imgui/imgui_impl_opengl3.h"
#include "../../libs/imgui/imgui_stdlib.h"

using OGLE::FrameBuffer, OGLE::Mesh, OGLE::FloatingCamera, OGLE::Rendering::DisplayManager;

namespace OGLE::Rendering
{
    // code based on http://www.opengl-tutorial.org/miscellaneous/clicking-on-objects/picking-with-custom-ray-obb-function/
    bool TestRayOBBIntersection(
        glm::vec3 rayOrigin,         // Ray origin, in world space
        glm::vec3 rayDirection,      // Ray direction (NOT target position!), in world space. Must be normalize()'d.
        glm::vec3 aabbMin,           // Minimum X,Y,Z coords of the mesh when not transformed at all.
        glm::vec3 aabbMax,           // Maximum X,Y,Z coords. Often aabbMin*-1 if your mesh is centered, but it's not always the case.
        glm::mat4 modelMatrix,       // Transformation applied to the mesh (which will thus be also applied to its bounding box)
        float &intersectionDistance, // Output : distance between rayOrigin and the intersection with the OBB
        glm::vec3 &intersectionPoint // the point where the ray intersects the mesh
    );

    class Renderer
    {
    public:
        Renderer();
        ~Renderer();

        void Init(std::shared_ptr<FloatingCamera> camera, std::shared_ptr<DisplayManager> displayManager);
        void Render(); // TODO: needs to choose which window to render to

        void SetMeshRenderers(std::vector<std::shared_ptr<MeshRenderer>> meshRenderers)
        {
            mMeshRenderers = meshRenderers;
        }

        std::shared_ptr<FloatingCamera> GetCamera()
        {
            return mCamera;
        }

        void BindVertexBuffer(GLuint vao, GLuint bufferId)
        {
            glBindVertexArray(vao);
            glBindBuffer(GL_ARRAY_BUFFER, bufferId);
        }

        void UnbindVertexBuffer()
        {
            glBindVertexArray(0);
        }

        void BindIndexBuffer(GLuint bufferId)
        {
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferId);
        }

        void UnbindIndexBuffer()
        {
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        }

        // takes mouse position and converts it to world coordinate space and returns them
        // code is based on https://www.youtube.com/watch?v=DLKN0jExRIM
        // and on https://antongerdelan.net/opengl/raycasting.html
        glm::vec3 ClickObject(int x, int y);

        // needs to be separate in case we want to do stuff before (collision checking etc.)
        void SwapWindow();
        ImVec2 wsize; // the size of the window the texture should be rendered to

    private:
        std::shared_ptr<DisplayManager> mDisplayManager;
        std::shared_ptr<FloatingCamera> mCamera;
        std::vector<std::shared_ptr<MeshRenderer>> mMeshRenderers;
        FrameBuffer frameBuffer;
        bool mCanSelectObject = true;
        int mMeshIndex = -1;
        
    };
}