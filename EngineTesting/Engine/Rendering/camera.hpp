#pragma once

#include "../Core/config.hpp"

namespace OGLE
{
    class Camera
    {
    public:
        Camera(float fov, float width, float height);
        ~Camera();

        glm::mat4 GetView();
        glm::mat4 GetViewProj();
        glm::vec3 GetPosition();
        glm::mat4 GetProjection();
        virtual void Update();
        virtual void Translate(glm::vec3 v);

    protected:
        glm::vec3 position;
        glm::mat4 projection;
        glm::mat4 view;
        glm::mat4 viewProj;
    };
}