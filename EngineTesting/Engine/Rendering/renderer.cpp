#include "renderer.hpp"

namespace OGLE::Rendering
{
    // code based on http://www.opengl-tutorial.org/miscellaneous/clicking-on-objects/picking-with-custom-ray-obb-function/
    bool TestRayOBBIntersection(
        glm::vec3 rayOrigin,         // Ray origin, in world space
        glm::vec3 rayDirection,      // Ray direction (NOT target position!), in world space. Must be normalize()'d.
        glm::vec3 aabbMin,           // Minimum X,Y,Z coords of the mesh when not transformed at all.
        glm::vec3 aabbMax,           // Maximum X,Y,Z coords. Often aabbMin*-1 if your mesh is centered, but it's not always the case.
        glm::mat4 modelMatrix,       // Transformation applied to the mesh (which will thus be also applied to its bounding box)
        float &intersectionDistance, // Output : distance between rayOrigin and the intersection with the OBB
        glm::vec3 &intersectionPoint // the point where the ray intersects the mesh
    )
    {

        // Intersection method from Real-Time Rendering and Essential Mathematics for Games
        float tMin = 0.0f;
        float tMax = 100000.0f;

        glm::vec3 OBBposition_worldspace(modelMatrix[3].x, modelMatrix[3].y, modelMatrix[3].z);

        glm::vec3 delta = OBBposition_worldspace - rayOrigin;

        // Test intersection with the 2 planes perpendicular to the OBB's X axis
        {
            glm::vec3 xaxis(modelMatrix[0].x, modelMatrix[0].y, modelMatrix[0].z);
            float e = glm::dot(xaxis, delta);
            float f = glm::dot(rayDirection, xaxis);

            if (fabs(f) > 0.001f)
            { // Standard case

                float t1 = (e + aabbMin.x) / f; // Intersection with the "left" plane
                float t2 = (e + aabbMax.x) / f; // Intersection with the "right" plane
                // t1 and t2 now contain distances betwen ray origin and ray-plane intersections

                // We want t1 to represent the nearest intersection,
                // so if it's not the case, invert t1 and t2
                if (t1 > t2)
                {
                    float w = t1;
                    t1 = t2;
                    t2 = w; // swap t1 and t2
                }

                // tMax is the nearest "far" intersection (amongst the X,Y and Z planes pairs)
                if (t2 < tMax)
                    tMax = t2;
                // tMin is the farthest "near" intersection (amongst the X,Y and Z planes pairs)
                if (t1 > tMin)
                    tMin = t1;

                // And here's the trick :
                // If "far" is closer than "near", then there is NO intersection.
                // See the images in the tutorials for the visual explanation.
                if (tMax < tMin)
                    return false;
            }
            else
            { // Rare case : the ray is almost parallel to the planes, so they don't have any "intersection"
                if (-e + aabbMin.x > 0.0f || -e + aabbMax.x < 0.0f)
                    return false;
            }
        }

        intersectionPoint.x = tMin;

        // Test intersection with the 2 planes perpendicular to the OBB's Y axis
        // Exactly the same thing than above.
        {
            glm::vec3 yaxis(modelMatrix[1].x, modelMatrix[1].y, modelMatrix[1].z);
            float e = glm::dot(yaxis, delta);
            float f = glm::dot(rayDirection, yaxis);

            if (fabs(f) > 0.001f)
            {

                float t1 = (e + aabbMin.y) / f;
                float t2 = (e + aabbMax.y) / f;

                if (t1 > t2)
                {
                    float w = t1;
                    t1 = t2;
                    t2 = w;
                }

                if (t2 < tMax)
                    tMax = t2;
                if (t1 > tMin)
                    tMin = t1;
                if (tMin > tMax)
                    return false;
            }
            else
            {
                if (-e + aabbMin.y > 0.0f || -e + aabbMax.y < 0.0f)
                    return false;
            }
        }

        intersectionPoint.y = tMin;

        // Test intersection with the 2 planes perpendicular to the OBB's Z axis
        // Exactly the same thing than above.
        {
            glm::vec3 zaxis(modelMatrix[2].x, modelMatrix[2].y, modelMatrix[2].z);
            float e = glm::dot(zaxis, delta);
            float f = glm::dot(rayDirection, zaxis);

            if (fabs(f) > 0.001f)
            {

                float t1 = (e + aabbMin.z) / f;
                float t2 = (e + aabbMax.z) / f;

                if (t1 > t2)
                {
                    float w = t1;
                    t1 = t2;
                    t2 = w;
                }

                if (t2 < tMax)
                    tMax = t2;
                if (t1 > tMin)
                    tMin = t1;
                if (tMin > tMax)
                    return false;
            }
            else
            {
                if (-e + aabbMin.z > 0.0f || -e + aabbMax.z < 0.0f)
                    return false;
            }
        }

        intersectionPoint.z = tMin;

        intersectionDistance = tMin;
        // intersectionPoint = glm::vec3(tMin);
        return true;
    }

    Renderer::Renderer()
    {
    }
    Renderer::~Renderer()
    {
    }

    void Renderer::Init(std::shared_ptr<FloatingCamera> camera, std::shared_ptr<DisplayManager> displayManager)
    {
        mDisplayManager = displayManager;

#ifdef _IMGUI
        // Dear ImGui Setup
        {
            // Setup Dear ImGui context
            IMGUI_CHECKVERSION();
            ImGui::CreateContext();
            ImGuiIO &io = ImGui::GetIO();
            io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; // Enable Keyboard Controls
            io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;  // Enable Gamepad Controls

            // Setup Dear ImGui style
            ImGui::StyleColorsDark();
            // ImGui::StyleColorsLight();

            // Setup Platform/Renderer backends
            ImGui_ImplSDL2_InitForOpenGL(mDisplayManager->GetWindow(), mDisplayManager->GetContext());
            ImGui_ImplOpenGL3_Init();
        }
#endif

        mCamera = camera;
        mCamera->Translate(glm::vec3(0.0f, 0.0f, 5.0f));
        mCamera->Update();
    }

    void Renderer::Render()
    {
        // NOTE: first call new imgui frame, then "create" window(s) (aka redraw them every frame), then render the drawn windows

        // #ifdef _IMGUI
        // ImGui new frame
        // (After event loop)
        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplSDL2_NewFrame();
        ImGui::NewFrame();

        // ImGui Draw
        // toggleable bools window
        {
            ImGui::Begin("Bools");

            // ImGui::Checkbox("Show Model Importer Window", &showMaterialDropdownWindow);
            ImGui::TextUnformatted("Hello!\n");

            ImGui::End();
        }

        ImGui::Begin("GameWindow");
        {
            // Using a Child allow to fill all the space of the window.
            // It also alows customization
            ImGui::BeginChild("GameRender");
            // if size isn't window size, set it to window size and resize the texture of the framebuffer (obviously it needs to be bound)
            if (wsize.x != ImGui::GetWindowSize().x || wsize.y != ImGui::GetWindowSize().y)
            {
                wsize = ImGui::GetWindowSize();
                frameBuffer.ResizeTexture(wsize.x, wsize.y);
            }

            // Because I use the texture from OpenGL, I need to invert the V from the UV.
            ImGui::Image((void *)(intptr_t)frameBuffer.textureColor, wsize, ImVec2(0, 1), ImVec2(1, 0));
            ImGui::EndChild();
        }
        ImGui::End();

        if (ImGui::GetIO().WantCaptureMouse)
        {
            mCanSelectObject = false;
        }
        else
        {
            mCanSelectObject = true;
        }
        // #endif

        frameBuffer.Bind();
        // set clear color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // just for testing
        // glViewport(mDisplayManager->GetScreenDimensions().x - 1500, mDisplayManager->GetScreenDimensions().y - 600, 600, 600);
        // glViewport(0, 0, mDisplayManager->GetScreenDimensions().x, mDisplayManager->GetScreenDimensions().y);

        // other openGL render stuff
        mCamera->Update();
        // PrintVector3(mCamera->GetPosition(), "test!");

        // draw with custom framebuffer to texture

        for (auto &meshRenderer : mMeshRenderers)
        {
            BindVertexBuffer(meshRenderer.vertexBufferVAO, meshRenderer.vertexBufferID);
            BindIndexBuffer(meshRenderer.indexBufferID);
            meshRenderer.GetMaterial()->GetShader()->Bind();

            meshRenderer.Render(*mCamera.get());

            meshRenderer.GetMaterial()->GetShader()->Unbind();
            UnbindIndexBuffer();
            UnbindVertexBuffer();
        }

        frameBuffer.Unbind();

        // glViewport(mDisplayManager->GetScreenDimensions().x - 1500, mDisplayManager->GetScreenDimensions().y - 600, 600, 600);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        // draw with normal framebuffer (later disabled)
        for (auto &meshRenderer : mMeshRenderers)
        {
            BindVertexBuffer(meshRenderer.vertexBufferVAO, meshRenderer.vertexBufferID);
            BindIndexBuffer(meshRenderer.indexBufferID);
            meshRenderer.GetMaterial()->GetShader()->Bind();

            meshRenderer.Render(*mCamera.get());

            meshRenderer.GetMaterial()->GetShader()->Unbind();
            UnbindIndexBuffer();
            UnbindVertexBuffer();
        }

        // ImGui rendering
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    }

    // needs to be separate in case we want to do stuff before (collision checking etc.)
    void Renderer::SwapWindow()
    {
        SDL_GL_SwapWindow(mDisplayManager->GetWindow());
    }

    // should happen in a "select objects" script/class
    glm::vec3 Renderer::ClickObject(int x, int y)
    {
        // TODO: need to read from the framebuffer texture
        // frameBuffer.Bind();
        // // if (!mCanSelectObject)
        // // {
        // //     return glm::vec3(-9999999.0f);
        // // } // when we can't select an object, return

        // int dataSize = 1 * 1;
        // float data = 0;
        // glReadPixels(x, y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &data);

        // std::cout << data << " the data!\n";

        frameBuffer.Unbind();

        // get mouse position is x and y argument and convert them to Normalized Device Coords (aka OpenGL screen coordinate system)
        float ndcX = (2.0f * x) / mDisplayManager->GetScreenDimensions().x - 1.0f;
        float ndcY = (2.0f * y) / mDisplayManager->GetScreenDimensions().y - 1.0f;
        glm::vec2 normalizedDeviceCoords = glm::vec2(ndcX, -ndcY);

        // convert to homogeneous clip space (ray points forwards to -z)
        glm::vec4 clipCoords = glm::vec4(normalizedDeviceCoords.x, normalizedDeviceCoords.y, -1.0f, 1.0f);

        // convert to eye space (camera coordinates)
        glm::mat4 inversedProjection = glm::inverse(mCamera->GetProjection());
        glm::vec4 eyeCoords = inversedProjection * clipCoords;
        eyeCoords = glm::vec4(eyeCoords.x, eyeCoords.y, -1.0f, 0.0f);

        // convert to world space
        glm::vec4 inversedView = glm::inverse(mCamera->GetView()) * eyeCoords;
        glm::vec3 worldCoords = glm::vec3(inversedView.x, inversedView.y, inversedView.z);
        // normalize world coords
        worldCoords = glm::normalize(worldCoords);

        // Ray Picking
        // Ray Start and End positions in Normalized Device Coordinates
        glm::vec4 rayStartNDC = glm::vec4(normalizedDeviceCoords.x, normalizedDeviceCoords.y, -1.0f, 1.0f);
        glm::vec4 rayEndNDC = glm::vec4(normalizedDeviceCoords.x, normalizedDeviceCoords.y, 0.0f, 1.0f);

        // calculate ray start world and ray end world positions
        glm::mat4 M = glm::inverse(mCamera->GetProjection() * mCamera->GetView());
        glm::vec4 rayStartWorld = M * rayStartNDC;
        rayStartWorld /= rayStartWorld.w;
        glm::vec4 rayEndWorld = M * rayEndNDC;
        rayEndWorld /= rayEndWorld.w;

        glm::vec3 rayDirWorld(rayEndWorld - rayStartWorld);
        rayDirWorld = glm::normalize(rayDirWorld);

        auto outOrigin = glm::vec3(rayStartWorld);
        auto outDirection = glm::normalize(rayDirWorld);

        // TODO: do collision checking with a bsp-tree or something like it because this is inefficient

        // disable all bounding boxes
        for (auto &meshRenderer : mMeshRenderers)
        {
            meshRenderer.drawBoundingBox = false;
        }

        // check collision for every mesh
        for (size_t i = 0; i < mMeshRenderers.size(); i++)
        {
            for (int j = 0; j < 100; j++)
            {
                float intersectionDistance;            // Output of TestRayOBBIntersection()
                glm::vec3 bbMin = mMeshRenderers.at(i).bbMin; // min size of mesh bb;
                glm::vec3 bbMax = mMeshRenderers.at(i).bbMax; // max size of mesh bb;
                glm::mat4 mTest = mMeshRenderers.at(i).model; // model matrix of mesh

                glm::vec3 intersectionPoint;

                if (TestRayOBBIntersection(
                        outOrigin,
                        outDirection,
                        bbMin,
                        bbMax,
                        mTest,
                        intersectionDistance,
                        intersectionPoint))
                {
                    mMeshRenderers.at(i).drawBoundingBox = true;
                    PrintVector3(mMeshRenderers.at(i).position, " intersected!");
                    mMeshIndex = i;           // set clicked mesh to reference of mesh that was clicked
                    return intersectionPoint; // return intersection point
                }
                else
                {
                    mMeshIndex = -1; // if no mesh clicked, pointer = nullptr
                }
            }
        }
        return glm::vec3(-9999999.0f); // return -9999999 because this can never be a valid intersection point
    }
}