#include "camera.hpp"

namespace OGLE
{
    Camera::Camera(float fov, float width, float height)
    {
        // projection = glm::ortho(0.0f, (float)width, (float)height, 0.0f, -1.0f, 1.0f);
        projection = glm::perspective(fov / 2.0f, width / height, 0.1f, 1000.0f);
        view = glm::mat4(1.0f);
        position = glm::vec3(0.0f);
        Translate(glm::vec3(0.0f));
        Update();
    }

    Camera::~Camera()
    {
    }

    glm::mat4 Camera::GetView()
    {
        return view;
    }

    glm::mat4 Camera::GetViewProj()
    {
        return viewProj;
    }

    glm::vec3 Camera::GetPosition()
    {
        return position;
    }

    glm::mat4 Camera::GetProjection()
    {
        return projection;
    }

    void Camera::Update()
    {
        viewProj = projection * view;
    }

    void Camera::Translate(glm::vec3 v)
    {
        position += v;
        view = glm::translate(view, v * -1.0f);
    }
}