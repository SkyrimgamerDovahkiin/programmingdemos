#pragma once

#include "../Core/config.hpp"

namespace OGLE::Rendering
{
    class DisplayManager
    {
    public:
        DisplayManager();
        ~DisplayManager();

        void Init(int screenWidth, int screenHeight, uint32_t flags = SDL_WINDOW_OPENGL, const char *name = "OpenGL Game");

        glm::vec2 GetScreenDimensions() { return mScreenDimensions;};

        SDL_Window *GetWindow() { return mWindow; };
        SDL_GLContext GetContext() { return mGlContext; };

    private:
        SDL_Window *mWindow;
        SDL_GLContext mGlContext;
        glm::vec2 mScreenDimensions;
    };
}