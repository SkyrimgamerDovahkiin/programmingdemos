#pragma once

#include "Engine/Core/config.hpp"

// custom
#include "defines.hpp"
#include "Engine/Core/material.hpp"
#include "Engine/Rendering/floating_camera.hpp"

using OGLE::Core::Material, OGLE::FloatingCamera;

namespace OGLE
{
    class Mesh
    {
    public:
        // Mesh(const char *filename, Material *material, Shader *shader);
        Mesh(std::shared_ptr<Material> material);
        ~Mesh();

        void SetMaterial(std::shared_ptr<Material> material);
        Material *GetMaterial();

        void Render(FloatingCamera &camera);
        void DrawBoundingBox(FloatingCamera &camera, std::shared_ptr<OGLE::Core::Shader> bbShader);
        void CalculateBoundingBox();

        glm::mat4 model = glm::mat4(1.0f);
        glm::mat4 modelViewProj = glm::mat4(1.0f);
        glm::mat4 bbModel = glm::mat4(1.0f);
        glm::mat4 bbModelViewProj = glm::mat4(1.0f);

        glm::vec3 bbMin; // min size of mesh bb;
        glm::vec3 bbMax; // max size of mesh bb;

        glm::vec3 position = glm::vec3(1.0f);

        uint64_t numIndices = 0;
        uint64_t numIndicesOffset = 0;

        std::vector<Vertex> vertices;

        GLuint vertexBufferID = 0;
        GLuint vertexBufferVAO = 0;
        GLuint indexBufferID = 0;

        bool drawBoundingBox = false;

    private:
        std::shared_ptr<Material> mMaterial;
        // VertexBuffer *vertexBuffer; // change this later so that a vertex buffer can be passed to the constructor and used for rendering
        // IndexBuffer *indexBuffer;   // change this later so that a index buffer can be passed to the constructor and used for rendering
        // Shader *shader;

        // uint64_t numIndices = 0;
    };
}