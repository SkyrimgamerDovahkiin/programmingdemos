#pragma once

#include "Engine/Core/config.hpp"

// vector 3 definitions
struct Vector3
{
    glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
    glm::vec3 down = glm::vec3(0.0f, -1.0f, 0.0f);
    glm::vec3 forward = glm::vec3(0.0f, 0.0f, 1.0f);
    glm::vec3 back = glm::vec3(0.0f, 0.0f, -1.0f);
    glm::vec3 right = glm::vec3(1.0f, 0.0f, 0.0f);
    glm::vec3 left = glm::vec3(-1.0f, 0.0f, 0.0f);
    glm::vec3 zero = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 one = glm::vec3(1.0f, 1.0f, 1.0f);
};

// the vertex
struct Vertex
{
    glm::vec3 positions;
    glm::vec3 normals;
    glm::vec2 uvs;
    glm::vec4 colors;
};

// std::ofstream &operator<<(std::ofstream &stream, Material &material);
// std::ifstream &operator>>(std::ifstream &stream, Material &material);

// the valid GLSL data types
enum GLSLDataType
{
    DATATYPE_BOOL,
    DATATYPE_BVEC2,
    DATATYPE_BVEC3,
    DATATYPE_BVEC4,
    DATATYPE_INT,
    DATATYPE_IVEC2,
    DATATYPE_IVEC3,
    DATATYPE_IVEC4,
    DATATYPE_UINT,
    DATATYPE_UVEC2,
    DATATYPE_UVEC3,
    DATATYPE_UVEC4,
    DATATYPE_FLOAT,
    DATATYPE_VEC2,
    DATATYPE_VEC3,
    DATATYPE_VEC4,
    DATATYPE_DOUBLE,
    DATATYPE_DEC2,
    DATATYPE_DEC3,
    DATATYPE_DEC4,
    DATATYPE_MAT2,
    DATATYPE_MAT3,
    DATATYPE_MAT4,
    DATATYPE_SAMPLER2D,
    DATATYPE_ISAMPLER2D,
    DATATYPE_USAMPLER2D,
};

// CharacterGlyph for text rendering (later)
struct CharacterGlyph
{
    glm::vec2 Size;       // Size of glyph
    glm::vec2 Bearing;    // Offset from baseline to left/top of glyph
    unsigned int Advance; // Offset to advance to next glyph
    float tx;             // x offset of glyph in texture coordinates
};

void PrintVector3(const glm::vec3 &v1, const std::string &s);
void PrintVector4(const glm::vec4 &v1);