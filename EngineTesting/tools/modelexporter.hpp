#pragma once

#include "../Engine/Core/config.hpp"

struct Position
{
    float x, y, z;
};

// my code
struct UV
{
    float x, y;
};

class ModelExporter
{
public:
    ModelExporter();
    ~ModelExporter();
    void Convert(std::string modelDir, std::string outputDir);

private:
    std::vector<Position> positions;
    std::vector<Position> normals;
    std::vector<uint32_t> indices;
    std::vector<UV> uvs;
    void ProcessMesh(aiMesh *mesh, const aiScene *scene);
    void ProcessNode(aiNode *node, const aiScene *scene);
};
