#include "mesh.hpp"

namespace OGLE
{
    Mesh::Mesh(std::shared_ptr<Material> material) : mMaterial(material)
    {
    }

    Mesh::~Mesh()
    {
    }

    void Mesh::SetMaterial(std::shared_ptr<Material> material)
    {
        mMaterial = material;
    }

    Material *Mesh::GetMaterial()
    {
        return mMaterial.get();
    }

    // NOTE: the correct shader should be bound before rendering. the mesh shouldn't take care of that
    void Mesh::Render(FloatingCamera &camera)
    {
        modelViewProj = camera.GetViewProj() * model;

        // should only be done if we want to use normal maps
        if (mMaterial->GetShader()->useNormalMaps)
        {
            glm::mat4 modelView = camera.GetView() * model;
            glm::mat4 invModelView = glm::transpose(glm::inverse(modelView));

            mMaterial->GetShader()->UploadMat4("u_modelView", modelView);
            mMaterial->GetShader()->UploadMat4("u_invModelView", invModelView);
        }

        // do always because there should always be a camera position uniform (if not, this gives an error anyway)
        mMaterial->GetShader()->UploadMat4("u_modelViewProj", modelViewProj);

        mMaterial->UpdateShaderUniforms();
        glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, (void *)(numIndicesOffset * sizeof(GLuint)));
    }

    void Mesh::DrawBoundingBox(FloatingCamera &camera, std::shared_ptr<OGLE::Core::Shader> bbShader)
    {
        glm::vec3 size = glm::vec3(bbMax.x - bbMin.x, bbMax.y - bbMin.y, bbMax.z - bbMin.z);
        glm::vec3 center = glm::vec3((bbMin.x + bbMax.x) / 2, (bbMin.y + bbMax.y) / 2, (bbMin.z + bbMax.z) / 2);
        glm::mat4 transform = glm::translate(glm::mat4(1), center) * glm::scale(glm::mat4(1), size);

        // bbModel = model * transform;
        bbModel = model * transform;
        // /* multiply object's modelViewProj matrix with transform to get bounding box */
        // bbModelViewProj = modelViewProj * transform;//modelViewProj * transform;
        bbModelViewProj = modelViewProj * transform;//modelViewProj * transform;

        if (!drawBoundingBox) { return; }

        bbShader->Bind();
        bbShader->UploadVec4("u_color", glm::vec4(0.075f, 0.6f, 0.0f, 1.0f));
        bbShader->UploadMat4("u_modelViewProj", bbModelViewProj);
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, 0);
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid *)(4 * sizeof(GLuint)));
        glDrawElements(GL_LINES, 8, GL_UNSIGNED_INT, (GLvoid *)(8 * sizeof(GLuint)));
        bbShader->Unbind();
    }

    void Mesh::CalculateBoundingBox()
    {
        // when mesh has no vertices, return
        if (vertices.size() == 0)
        {
            std::cerr << "Mesh vertices vector size is 0!" << std::endl;
            return;
        }

        GLfloat min_x, max_x, min_y, max_y, min_z, max_z;

        min_x = max_x = vertices[0].positions.x;
        min_y = max_y = vertices[0].positions.y;
        min_z = max_z = vertices[0].positions.z;

        for (int i = 0; i < vertices.size(); i++)
        {
            if (vertices[i].positions.x < min_x)
                min_x = vertices[i].positions.x;
            if (vertices[i].positions.x > max_x)
                max_x = vertices[i].positions.x;
            if (vertices[i].positions.y < min_y)
                min_y = vertices[i].positions.y;
            if (vertices[i].positions.y > max_y)
                max_y = vertices[i].positions.y;
            if (vertices[i].positions.z < min_z)
                min_z = vertices[i].positions.z;
            if (vertices[i].positions.z > max_z)
                max_z = vertices[i].positions.z;
        }

        glm::vec3 size = glm::vec3(max_x - min_x, max_y - min_y, max_z - min_z);
        glm::vec3 center = glm::vec3((min_x + max_x) / 2, (min_y + max_y) / 2, (min_z + max_z) / 2);
        glm::mat4 transform = glm::translate(glm::mat4(1), center) * glm::scale(glm::mat4(1), size);

        bbModelViewProj = modelViewProj * transform;

        bbMin = glm::vec3(min_x, min_y, min_z); // set min size of mesh bb;
        bbMax = glm::vec3(max_x, max_y, max_z); // set max size of mesh bb;
    }
}