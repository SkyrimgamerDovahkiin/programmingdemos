#include "Engine/Core/config.hpp"

// custom
#include "defines.hpp"
#include "Engine/Core/vertex_buffer.hpp"
#include "Engine/Core/index_buffer.hpp"
#include "Engine/Core/shader.hpp"
#include "Engine/Core/material.hpp"
#include "Engine/Core/game_object.hpp"
#include "Engine/Rendering/display_manager.hpp"
#include "Engine/Rendering/renderer.hpp"
#include "Engine/Rendering/floating_camera.hpp"
#include "Engine/Input/input_manager.hpp"
#include "mesh.hpp"
#include "tools/modelexporter.hpp"

#define IMGUI_PAYLOAD_TYPE_DND_MATERIAL_LOCATION "DND_MATERIAL_LOCATION"

using OGLE::VertexBuffer,
    OGLE::IndexBuffer,
    OGLE::Core::Shader,
    OGLE::Core::Material,
    OGLE::Core::GameObject,
    OGLE::Core::Transform,
    OGLE::Core::MeshRenderer,
    OGLE::Rendering::DisplayManager,
    OGLE::Rendering::Renderer,
    OGLE::Input::InputManager,
    OGLE::FloatingCamera,
    OGLE::Mesh;

// void GLAPIENTRY OpenGLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
// {
//     std::cout << "[OpenGL Error] " << message << std::endl;
// }

// #ifdef _DEBUG
// void _GLGetError(const char *file, int line, const char *call)
// {
//     while (GLenum error = glGetError())
//     {
//         std::cout << "[OpenGL Error] " << glewGetErrorString(error) << " in " << file << ":" << line << " Call: " << call << std::endl;
//     }
// }
// #endif

std::vector<Vertex> vertices;
std::vector<uint32_t> indices;

uint64_t numVertices = 0;
uint64_t numIndices = 0;

uint64_t maxVertices = 16000000;

int screenWidth = 1920, screenHeight = 1080;
// int screenWidth = 1000, screenHeight = 1000; // for testing purposes

std::shared_ptr<FloatingCamera> camera;
// FloatingCamera camera(90.0f, screenWidth, screenHeight); // the camera

// std::string modelName;
// std::string modelOutputName;
// std::string modelLoadingName;
std::filesystem::path selected;

// the OpenGLShaders
// std::vector<Shader *> oglShaders;

// std::vector<std::pair<std::string, std::shared_ptr<Shader>>> oglShadersByName;
std::vector<std::shared_ptr<Shader>> oglShaders;

// std::vector<Mesh> meshes;

std::vector<std::shared_ptr<GameObject>> gameObjects;
std::vector<std::shared_ptr<Transform>> transforms;
std::vector<std::shared_ptr<MeshRenderer>> meshRenderers;

uint64_t lastModelIndex = 0;

// the curDir for the content explorer
// std::filesystem::path curDir = "Assets";

// char *lastTextureName;

// needed for text input window to show up
bool textInputPopup = false;

int bbIndicesOffset = 0;

glm::vec3 clickedWorldSpacePos;

bool canSelectObject = true; // later make a scene view window

int meshRendererIndex = -1; // the index of the mesh that was clicked in the mesh vector

// NOTE: should only be done AFTER vertex and index buffer was created
// NOTE: change later to choose a vertex buffer/index buffer or create a new one if it's a different model
void CreateGameObject(std::string path, std::shared_ptr<Material> material, glm::vec3 position, VertexBuffer &vertexBuffer, IndexBuffer &indexBuffer)
{
    // get the current path when clicked
    std::string curDir = std::filesystem::current_path();

    uint64_t numVerticesTemp = 0;
    uint64_t numIndicesTemp = 0;
    // uint64_t index = 0;

    std::vector<Vertex> verticesTemp;
    std::vector<uint32_t> indicesTemp;

    verticesTemp.clear();
    indicesTemp.clear();

    selected = path;

    // read model
    std::ifstream input = std::ifstream(path, std::ios::in | std::ios::binary);
    if (!input.is_open())
    {
        std::cout << "Error reading model file!" << std::endl;
    }
    input.read((char *)&numVerticesTemp, sizeof(uint64_t));
    input.read((char *)&numIndicesTemp, sizeof(uint64_t));

    for (uint64_t i = 0; i < numVerticesTemp; i++)
    {
        Vertex vertex;
        input.read((char *)&vertex.positions.x, sizeof(float));
        input.read((char *)&vertex.positions.y, sizeof(float));
        input.read((char *)&vertex.positions.z, sizeof(float));

        input.read((char *)&vertex.normals.x, sizeof(float));
        input.read((char *)&vertex.normals.y, sizeof(float));
        input.read((char *)&vertex.normals.z, sizeof(float));

        // my code
        input.read((char *)&vertex.uvs.x, sizeof(float));
        input.read((char *)&vertex.uvs.y, sizeof(float));

        vertex.colors = glm::vec4(1.0f);

        verticesTemp.push_back(vertex);
    }

    for (uint64_t i = 0; i < numIndicesTemp; i++)
    {
        uint32_t index;
        input.read((char *)&index, sizeof(uint32_t));
        indicesTemp.push_back(index + lastModelIndex);
    }

    lastModelIndex = numVertices + numVerticesTemp; // set last model index to number of vertices + verticesTemp.size() so that the correct index is added to the indices array

    // game object creation
    std::shared_ptr<GameObject> gameObject;
    gameObject = std::make_shared<GameObject>(std::shared_ptr<Transform>(new Transform(gameObject)));
    gameObjects.push_back(std::move(gameObject));
    gameObjects.at(0)->GetTransform().SetPosition(glm::vec3(0.0f));
    gameObjects.at(0)->GetTransform().SetScale(glm::vec3(1.0f));

    std::shared_ptr<MeshRenderer> meshRenderer;

    meshRenderer = std::make_shared<MeshRenderer>(camera,
                                                  vertices,
                                                  indices,
                                                  numIndicesTemp,
                                                  numIndices,
                                                  vertexBuffer.GetBufferID(),
                                                  vertexBuffer.GetBufferVAO(),
                                                  indexBuffer.GetBufferID(),
                                                  oglShaders.at(0),
                                                  gameObject,
                                                  true);

    meshRenderers.push_back(std::move(meshRenderer));
    gameObjects.at(0)->AddMeshRenderer(meshRenderers.at(0));
    meshRenderers.at(0)->SetMaterial(material);
    meshRenderers.at(0)->CalculateBoundingBox();
    meshRenderers.at(0)->SetModel(glm::translate(meshRenderers.at(0)->GetModel(), position));
    meshRenderers.at(0)->SetModelViewProj(camera->GetViewProj() * meshRenderers.at(0)->GetModel());
    gameObjects.at(0)->GetTransform().SetPosition(position);

    // Mesh mesh(material);
    // mesh.SetMaterial(material);
    // mesh.numIndices = numIndicesTemp; // how many indices the mesh has, needed for rendering
    // mesh.vertices = verticesTemp;     // set the vertices, needed for object picking
    // mesh.CalculateBoundingBox();      // calculate mesh's bounding box

    // mesh.vertexBufferID = vertexBuffer.GetBufferID();
    // mesh.vertexBufferVAO = vertexBuffer.GetBufferVAO();
    // mesh.indexBufferID = indexBuffer.GetBufferID();

    // mesh.numIndicesOffset = numIndices;

    // mesh.model = glm::translate(mesh.model, position);
    // mesh.modelViewProj = camera->GetViewProj() * mesh.model;
    // mesh.position = position;
    // meshes.push_back(mesh);

    meshRendererIndex = meshRenderers.size() - 1; // set mesh index

    glBufferSubData(GL_ARRAY_BUFFER, numVertices * sizeof(Vertex), numVerticesTemp * sizeof(Vertex), verticesTemp.data());
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(indicesTemp[0]), numIndicesTemp * sizeof(indicesTemp[0]), indicesTemp.data());

    numVertices += numVerticesTemp;
    numIndices += numIndicesTemp;
}

// generate/fill vertex and index vector for bounding box drawing
void GenerateBuffersBB(std::vector<Vertex> &vertices, std::vector<uint32_t> &indices)
{
    vertices.push_back(Vertex{.positions = glm::vec3(-0.5f, -0.5f, -0.5f), // BottomBottomLeft
                              .normals = glm::vec3(1.0f),
                              .uvs = glm::vec2(1.0f),
                              .colors = glm::vec4(1.0f)});

    vertices.push_back(Vertex{.positions = glm::vec3(0.5f, -0.5f, -0.5f), // BottomBottomRight
                              .normals = glm::vec3(1.0f),
                              .uvs = glm::vec2(1.0f),
                              .colors = glm::vec4(1.0f)});

    vertices.push_back(Vertex{.positions = glm::vec3(0.5f, 0.5f, -0.5f), // BottomTopRight
                              .normals = glm::vec3(1.0f),
                              .uvs = glm::vec2(1.0f),
                              .colors = glm::vec4(1.0f)});

    vertices.push_back(Vertex{.positions = glm::vec3(-0.5f, 0.5f, -0.5f), // BottomTopLeft
                              .normals = glm::vec3(1.0f),
                              .uvs = glm::vec2(1.0f),
                              .colors = glm::vec4(1.0f)});

    vertices.push_back(Vertex{.positions = glm::vec3(-0.5f, -0.5f, 0.5f),
                              .normals = glm::vec3(1.0f),
                              .uvs = glm::vec2(1.0f),
                              .colors = glm::vec4(1.0f)});

    vertices.push_back(Vertex{.positions = glm::vec3(0.5f, -0.5f, 0.5f),
                              .normals = glm::vec3(1.0f),
                              .uvs = glm::vec2(1.0f),
                              .colors = glm::vec4(1.0f)});

    vertices.push_back(Vertex{.positions = glm::vec3(0.5f, 0.5f, 0.5f),
                              .normals = glm::vec3(1.0f),
                              .uvs = glm::vec2(1.0f),
                              .colors = glm::vec4(1.0f)});

    vertices.push_back(Vertex{.positions = glm::vec3(-0.5f, 0.5f, 0.5f),
                              .normals = glm::vec3(1.0f),
                              .uvs = glm::vec2(1.0f),
                              .colors = glm::vec4(1.0f)});

    indices.push_back(0 + bbIndicesOffset);
    indices.push_back(1 + bbIndicesOffset);
    indices.push_back(2 + bbIndicesOffset);
    indices.push_back(3 + bbIndicesOffset);

    indices.push_back(4 + bbIndicesOffset);
    indices.push_back(5 + bbIndicesOffset);
    indices.push_back(6 + bbIndicesOffset);
    indices.push_back(7 + bbIndicesOffset);

    indices.push_back(0 + bbIndicesOffset);
    indices.push_back(4 + bbIndicesOffset);
    indices.push_back(5 + bbIndicesOffset);
    indices.push_back(1 + bbIndicesOffset);

    indices.push_back(2 + bbIndicesOffset);
    indices.push_back(6 + bbIndicesOffset);
    indices.push_back(7 + bbIndicesOffset);
    indices.push_back(3 + bbIndicesOffset);
    bbIndicesOffset += 8;
}

// draw bounding box
void DrawBBox(Mesh &mesh, VertexBuffer &vertexBuffer, IndexBuffer &indexBuffer)
{
    // // recalculation needs to happen when object is rotated or scaled
    // if (mesh.vertices.size() == 0)
    // {
    //     std::cerr << "Mesh vertices vector size is 0!" << std::endl;
    // }

    // GLfloat
    //     min_x,
    //     max_x,
    //     min_y, max_y,
    //     min_z, max_z;

    // min_x = max_x = mesh.vertices[0].positions.x;
    // min_y = max_y = mesh.vertices[0].positions.y;
    // min_z = max_z = mesh.vertices[0].positions.z;

    // for (int i = 0; i < mesh.vertices.size(); i++)
    // {
    //     if (mesh.vertices[i].positions.x < min_x)
    //         min_x = mesh.vertices[i].positions.x;
    //     if (mesh.vertices[i].positions.x > max_x)
    //         max_x = mesh.vertices[i].positions.x;
    //     if (mesh.vertices[i].positions.y < min_y)
    //         min_y = mesh.vertices[i].positions.y;
    //     if (mesh.vertices[i].positions.y > max_y)
    //         max_y = mesh.vertices[i].positions.y;
    //     if (mesh.vertices[i].positions.z < min_z)
    //         min_z = mesh.vertices[i].positions.z;
    //     if (mesh.vertices[i].positions.z > max_z)
    //         max_z = mesh.vertices[i].positions.z;
    // }
    // glm::vec3 size = glm::vec3(max_x - min_x, max_y - min_y, max_z - min_z);
    // glm::vec3 center = glm::vec3((min_x + max_x) / 2, (min_y + max_y) / 2, (min_z + max_z) / 2);
    // glm::mat4 transform = glm::translate(glm::mat4(1), center) * glm::scale(glm::mat4(1), size);

    // // /* multiply object's modelViewProj matrix with transform to get bounding box */
    // glm::mat4 modelViewProj = mesh.modelViewProj * transform;

    // vertexBuffer.Bind(); // must be done in main loop
    // indexBuffer.Bind();
    // oglShaders.at(0)->Bind(); // uses basic shader because a bounding box doesn't need a texture

    // oglShaders.at(0)->UploadVec4("u_color", glm::vec4(0.075f, 0.6f, 0.0f, 1.0f));
    // // oglShaders.at(0)->UploadMat4("u_modelViewProj", modelViewProj);
    // glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, 0);
    // glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid *)(4 * sizeof(GLuint)));
    // glDrawElements(GL_LINES, 8, GL_UNSIGNED_INT, (GLvoid *)(8 * sizeof(GLuint)));

    // oglShaders.at(0)->Unbind();
    // vertexBuffer.Unbind();
    // indexBuffer.Unbind();
}

// code based on http://www.opengl-tutorial.org/miscellaneous/clicking-on-objects/picking-with-custom-ray-obb-function/
bool TestRayOBBIntersection(
    glm::vec3 rayOrigin,         // Ray origin, in world space
    glm::vec3 rayDirection,      // Ray direction (NOT target position!), in world space. Must be normalize()'d.
    glm::vec3 aabbMin,           // Minimum X,Y,Z coords of the mesh when not transformed at all.
    glm::vec3 aabbMax,           // Maximum X,Y,Z coords. Often aabbMin*-1 if your mesh is centered, but it's not always the case.
    glm::mat4 modelMatrix,       // Transformation applied to the mesh (which will thus be also applied to its bounding box)
    float &intersectionDistance, // Output : distance between rayOrigin and the intersection with the OBB
    glm::vec3 &intersectionPoint // the point where the ray intersects the mesh
)
{

    // Intersection method from Real-Time Rendering and Essential Mathematics for Games
    float tMin = 0.0f;
    float tMax = 100000.0f;

    glm::vec3 OBBposition_worldspace(modelMatrix[3].x, modelMatrix[3].y, modelMatrix[3].z);

    glm::vec3 delta = OBBposition_worldspace - rayOrigin;

    // Test intersection with the 2 planes perpendicular to the OBB's X axis
    {
        glm::vec3 xaxis(modelMatrix[0].x, modelMatrix[0].y, modelMatrix[0].z);
        float e = glm::dot(xaxis, delta);
        float f = glm::dot(rayDirection, xaxis);

        if (fabs(f) > 0.001f)
        { // Standard case

            float t1 = (e + aabbMin.x) / f; // Intersection with the "left" plane
            float t2 = (e + aabbMax.x) / f; // Intersection with the "right" plane
            // t1 and t2 now contain distances betwen ray origin and ray-plane intersections

            // We want t1 to represent the nearest intersection,
            // so if it's not the case, invert t1 and t2
            if (t1 > t2)
            {
                float w = t1;
                t1 = t2;
                t2 = w; // swap t1 and t2
            }

            // tMax is the nearest "far" intersection (amongst the X,Y and Z planes pairs)
            if (t2 < tMax)
                tMax = t2;
            // tMin is the farthest "near" intersection (amongst the X,Y and Z planes pairs)
            if (t1 > tMin)
                tMin = t1;

            // And here's the trick :
            // If "far" is closer than "near", then there is NO intersection.
            // See the images in the tutorials for the visual explanation.
            if (tMax < tMin)
                return false;
        }
        else
        { // Rare case : the ray is almost parallel to the planes, so they don't have any "intersection"
            if (-e + aabbMin.x > 0.0f || -e + aabbMax.x < 0.0f)
                return false;
        }
    }

    intersectionPoint.x = tMin;

    // Test intersection with the 2 planes perpendicular to the OBB's Y axis
    // Exactly the same thing than above.
    {
        glm::vec3 yaxis(modelMatrix[1].x, modelMatrix[1].y, modelMatrix[1].z);
        float e = glm::dot(yaxis, delta);
        float f = glm::dot(rayDirection, yaxis);

        if (fabs(f) > 0.001f)
        {

            float t1 = (e + aabbMin.y) / f;
            float t2 = (e + aabbMax.y) / f;

            if (t1 > t2)
            {
                float w = t1;
                t1 = t2;
                t2 = w;
            }

            if (t2 < tMax)
                tMax = t2;
            if (t1 > tMin)
                tMin = t1;
            if (tMin > tMax)
                return false;
        }
        else
        {
            if (-e + aabbMin.y > 0.0f || -e + aabbMax.y < 0.0f)
                return false;
        }
    }

    intersectionPoint.y = tMin;

    // Test intersection with the 2 planes perpendicular to the OBB's Z axis
    // Exactly the same thing than above.
    {
        glm::vec3 zaxis(modelMatrix[2].x, modelMatrix[2].y, modelMatrix[2].z);
        float e = glm::dot(zaxis, delta);
        float f = glm::dot(rayDirection, zaxis);

        if (fabs(f) > 0.001f)
        {

            float t1 = (e + aabbMin.z) / f;
            float t2 = (e + aabbMax.z) / f;

            if (t1 > t2)
            {
                float w = t1;
                t1 = t2;
                t2 = w;
            }

            if (t2 < tMax)
                tMax = t2;
            if (t1 > tMin)
                tMin = t1;
            if (tMin > tMax)
                return false;
        }
        else
        {
            if (-e + aabbMin.z > 0.0f || -e + aabbMax.z < 0.0f)
                return false;
        }
    }

    intersectionPoint.z = tMin;

    intersectionDistance = tMin;
    // intersectionPoint = glm::vec3(tMin);
    return true;
}

// takes mouse position and converts it to world coordinate space and returns them
// code is based on https://www.youtube.com/watch?v=DLKN0jExRIM
// and on https://antongerdelan.net/opengl/raycasting.html
// glm::vec3 ClickedOnObject(int x, int y)
// {
/*if (!canSelectObject)
{
    return glm::vec3(-9999999.0f);
} // when we can't select an object, return

// get mouse position is x and y argument and convert them to Normalized Device Coords (aka OpenGL screen coordinate system)
float ndcX = (2.0f * x) / screenWidth - 1.0f;
float ndcY = (2.0f * y) / screenHeight - 1.0f;
glm::vec2 normalizedDeviceCoords = glm::vec2(ndcX, -ndcY);

// convert to homogeneous clip space (ray points forwards to -z)
glm::vec4 clipCoords = glm::vec4(normalizedDeviceCoords.x, normalizedDeviceCoords.y, -1.0f, 1.0f);

// convert to eye space (camera coordinates)
glm::mat4 inversedProjection = glm::inverse(camera.GetProjection());
glm::vec4 eyeCoords = inversedProjection * clipCoords;
eyeCoords = glm::vec4(eyeCoords.x, eyeCoords.y, -1.0f, 0.0f);

// convert to world space
glm::vec4 inversedView = glm::inverse(camera.GetView()) * eyeCoords;
glm::vec3 worldCoords = glm::vec3(inversedView.x, inversedView.y, inversedView.z);
// normalize world coords
worldCoords = glm::normalize(worldCoords);

// Ray Picking
// Ray Start and End positions in Normalized Device Coordinates
glm::vec4 rayStartNDC = glm::vec4(normalizedDeviceCoords.x, normalizedDeviceCoords.y, -1.0f, 1.0f);
glm::vec4 rayEndNDC = glm::vec4(normalizedDeviceCoords.x, normalizedDeviceCoords.y, 0.0f, 1.0f);

// calculate ray start world and ray end world positions
glm::mat4 M = glm::inverse(camera.GetProjection() * camera.GetView());
glm::vec4 rayStartWorld = M * rayStartNDC;
rayStartWorld /= rayStartWorld.w;
glm::vec4 rayEndWorld = M * rayEndNDC;
rayEndWorld /= rayEndWorld.w;

glm::vec3 rayDirWorld(rayEndWorld - rayStartWorld);
rayDirWorld = glm::normalize(rayDirWorld);

auto outOrigin = glm::vec3(rayStartWorld);
auto outDirection = glm::normalize(rayDirWorld);

// TODO: do collision checking with a bsp-tree or something like it because this is inefficient

// disable all bounding boxes
for (auto &mesh : meshes)
{
    mesh.drawBoundingBox = false;
}

// check collision for every mesh
for (size_t i = 0; i < meshes.size(); i++)
{
    for (int j = 0; j < 100; j++)
    {
        float intersectionDistance;           // Output of TestRayOBBIntersection()
        glm::vec3 bbMin = meshes.at(i).bbMin; // min size of mesh bb;
        glm::vec3 bbMax = meshes.at(i).bbMax; // max size of mesh bb;
        glm::mat4 mTest = meshes.at(i).model; // model matrix of mesh

        glm::vec3 intersectionPoint;

        if (TestRayOBBIntersection(
                outOrigin,
                outDirection,
                bbMin,
                bbMax,
                mTest,
                intersectionDistance,
                intersectionPoint))
        {
            meshes.at(i).drawBoundingBox = true;
            PrintVector3(meshes.at(i).position, " intersected!");
            meshIndex = i;            // set clicked mesh to reference of mesh that was clicked
            return intersectionPoint; // return intersection point
        }
        else
        {
            meshIndex = -1; // if no mesh clicked, pointer = nullptr
        }
    }
}
return glm::vec3(-9999999.0f); // return -9999999 because this can never be a valid intersection point*/
// }

void DragObject(int x, int y)
{
    /*// get mouse position is x and y argument and convert them to Normalized Device Coords (aka OpenGL screen coordinate system)
    float ndcX = (2.0f * x) / screenWidth - 1.0f;
    float ndcY = (2.0f * y) / screenHeight - 1.0f;

    ndcY = -ndcY; // flip ndcY because screen coords y != opengl coords y

    // convert ndc coords to view coords
    // code from https://github.com/emeiri/ogldev/blob/master/tutorial32_youtube/tutorial32.cpp
    float focalLength = camera.GetProjection()[0][0];
    float ar = (float)screenHeight / (float)screenWidth;
    glm::vec3 rayView(ndcX / focalLength, (ndcY * ar) / focalLength, -1.0f);

    // calculate mesh position in view space and intersect it with the ray (later it should be the mesh that was clicked on)
    glm::vec4 objViewSpacePos = camera.GetView() * glm::vec4(meshes.at(meshIndex).model[3]);
    glm::vec4 viewSpaceIntersect = glm::vec4(glm::vec3(rayView) * -objViewSpacePos.z, 1.0f);

    // convert view space coordinates to world space coordinates
    glm::vec4 inversedView = glm::inverse(camera.GetView()) * viewSpaceIntersect;
    glm::vec3 worldCoords = glm::vec3(inversedView.x, inversedView.y, inversedView.z);

    // move mesh
    meshes.at(meshIndex).model[3].x = worldCoords.x;
    meshes.at(meshIndex).model[3].y = worldCoords.y;
    meshes.at(meshIndex).modelViewProj = camera.GetViewProj() * meshes.at(meshIndex).model;
    meshes.at(meshIndex).position = glm::vec3(meshes.at(meshIndex).model[3]);*/
}

/*void ShowModelConverterWindow(ModelExporter modelExporter)
{
    // Window for material access
    {
        ImGui::SetNextWindowSize(ImVec2(500.0f, 250.0f), ImGuiCond_Once);
        ImGui::SetNextWindowPos(ImVec2(screenWidth - 1000.0f, screenHeight - 250.0f), ImGuiCond_Once);

        ImGui::Begin("Model Converter"); // Create window with name

        ImGui::InputText("Model location", &modelName);
        ImGui::SetItemTooltip("Put in the model name. The model location must be in Assets/models");
        // ImGui::SameLine();
        ImGui::InputText("Model output location", &modelOutputName);
        ImGui::SetItemTooltip("Put in the desired model name without file extension. The model will be saved in Assets/models");
        // ImGui::SameLine();
        if (ImGui::Button("Convert", ImVec2(60.0f, 20.0f)))
        {
            modelExporter.Convert(modelName, modelOutputName);
        }

        // static std::string path;
        // ImGui::InputText("OGLE Model location", &path);
        // if (ImGui::Button("Load Model", ImVec2(60.0f, 20.0f)))
        // {
        //     LoadMesh(path);
        // }

        // ImGui::InputText("Model loading location", &modelLoadingName);
        // ImGui::SetItemTooltip("Put in the desired model name without file extension. The model location must be in Assets/models");

        // if (ImGui::Button("Load Model", ImVec2(100.0f, 20.0f)))
        // {
        //     LoadModel(modelLoadingName);
        // }

        ImGui::End();
    }
}*/

// TODO: material should be selected material
void ShowMaterialDropdownWindow()
{
    /*if (meshRendererIndex == -1)
    {
        return;
    } // when no mesh is selected, return
    // Window for material access
    {
        ImGui::SetNextWindowSize(ImVec2(500.0f, 250.0f), ImGuiCond_Once);
        ImGui::SetNextWindowPos(ImVec2(screenWidth - 1000.0f, screenHeight - 250.0f), ImGuiCond_Once);

        ImGui::Begin("Material Dropdown"); // Create window with name

        static int item_current_idx = 0;                                                                       // Here we store our selection data as an index.
        const char *combo_preview_value = meshes.at(meshRendererIndex).GetMaterial()->GetShader()->shaderName.c_str(); // Pass in the preview value visible before opening the combo (it could be anything)

        if (ImGui::BeginCombo("Shaders", combo_preview_value))
        {
            for (int n = 0; n < oglShaders.size(); n++)
            {
                const bool is_selected = (item_current_idx == n);
                // if (ImGui::Selectable(oglShaders[n]->shaderName.c_str(), is_selected))
                if (ImGui::Selectable(oglShaders[n]->shaderName.c_str(), is_selected))
                {
                    item_current_idx = n;

                    meshes.at(meshIndex).GetMaterial()->SetShader(oglShaders[n]);
                    // material->ShaderChanged();
                }

                // Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
                if (is_selected)
                    ImGui::SetItemDefaultFocus();
            }
            ImGui::EndCombo();
        }

        for (auto &it : *meshes.at(meshIndex).GetMaterial()->GetMUniformFloats())
        {
            if (ImGui::InputFloat(it.first.c_str(), &it.second))
            {
                meshes.at(meshIndex).GetMaterial()->SetUniformValue(it.first, it.second);
            }
        }

        for (auto &it : *meshes.at(meshIndex).GetMaterial()->GetMUniformFloat2s())
        {
            if (ImGui::InputFloat2(it.first.c_str(), (float *)&it.second))
            {
                meshes.at(meshIndex).GetMaterial()->SetUniformValue(it.first, it.second);
            }
        }

        for (auto &it : *meshes.at(meshIndex).GetMaterial()->GetMUniformFloat3s())
        {
            if (ImGui::InputFloat3(it.first.c_str(), (float *)&it.second))
            {
                meshes.at(meshIndex).GetMaterial()->SetUniformValue(it.first, it.second);
            }
        }

        for (auto &it : *meshes.at(meshIndex).GetMaterial()->GetMUniformFloat4s())
        {
            if (ImGui::InputFloat4(it.first.c_str(), (float *)&it.second))
            {
                meshes.at(meshIndex).GetMaterial()->SetUniformValue(it.first, it.second);
            }
        }

        ImGui::End();
    }*/
}

// Main code
int main(int, char **)
{
    camera = std::make_shared<FloatingCamera>(90.0f, screenWidth, screenHeight);
    // camera = std::make_shared<FloatingCamera>(90.0f, 600, 600); // just for testing

    std::shared_ptr<DisplayManager> displayManager;
    displayManager = std::make_shared<DisplayManager>();

    displayManager->Init(screenWidth, screenHeight);

    Renderer renderer;
    renderer.Init(camera, displayManager);

    {
        // camera.Translate(glm::vec3(0.0f, 0.0f, 0.0f));
        // camera.Update();

        // sdl init
        // SDL_Window *window;

        // {
        //     SDL_Init(SDL_INIT_EVERYTHING);

        //     SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
        //     SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
        //     SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
        //     SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
        //     SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
        //     SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        // }

        //     // Debug
        // #ifdef _DEBUG
        //     SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
        // #endif

        // uint32_t flags = SDL_WINDOW_OPENGL;
        // window = SDL_CreateWindow("OpenGL Game", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, flags);
        // SDL_GLContext glContext = SDL_GL_CreateContext(window);

        // SDL_GL_SetSwapInterval(1);

        // GLenum err = glewInit();
        // if (err != GLEW_OK)
        // {
        //     std::cout << "Error: " << glewGetErrorString(err) << std::endl;
        //     std::cin.get();
        //     return -1;
        // }

        // std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;

        // #ifdef _DEBUG
        //     glEnable(GL_DEBUG_OUTPUT);
        //     glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        //     glDebugMessageCallback(OpenGLDebugCallback, 0);
        // #endif
    }

    // setup
    // create Vertex buffer
    VertexBuffer vertexBuffer(vertices.data(), maxVertices);

    // create Index buffer (size = maxVertices * 4)
    IndexBuffer indexBuffer(indices.data(), maxVertices / 8);

    glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices * sizeof(Vertex), vertices.data());
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, numIndices * sizeof(indices[0]), indices.data());

    // create vertex buffer for bounding box drawing
    vertexBuffer.Unbind();
    indexBuffer.Unbind();

    std::vector<Vertex> vertices2;
    std::vector<uint32_t> indices2;

    GenerateBuffersBB(vertices2, indices2);

    VertexBuffer vertexBuffer2(vertices2.data(), vertices2.size() * sizeof(Vertex));
    IndexBuffer indexBuffer2(indices2.data(), indices2.size() * sizeof(indices2[0]));

    glBufferSubData(GL_ARRAY_BUFFER, 0, vertices2.size() * sizeof(Vertex), vertices2.data());
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, indices2.size() * sizeof(indices2[0]), indices2.data());

    vertexBuffer2.Unbind();
    indexBuffer2.Unbind();

    // create basic shader
    std::shared_ptr<Shader> basicShader;
    basicShader = std::make_shared<Shader>("Engine/Shaders/basic.OGLEShader");
    basicShader->useNormalMaps = false;
    oglShaders.push_back(basicShader);

    // create texture shader
    std::shared_ptr<Shader> textureShader;
    textureShader = std::make_shared<Shader>("Engine/Shaders/texture.OGLEShader");
    textureShader->useNormalMaps = true;

    // int textureModelViewLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_modelView");
    // int textureInvModelViewLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_invModelView");
    // int textureViewProjMatrixUniformLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_modelViewProj");

    // int diffuseLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_diffuse");
    // int specularLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_specular");
    // int emissiveLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_emissive");
    // int shininessLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_shininess");

    oglShaders.push_back(textureShader);
    // oglShadersByName.push_back(std::pair(textureShader.shaderName, &textureShader));

    // material
    std::shared_ptr<Material> material;
    material = std::make_shared<Material>(oglShaders.at(1));

    // the test mesh 2 with another material
    std::shared_ptr<Material> material2;
    material2 = std::make_shared<Material>(oglShaders.at(0));

    // delta time stuff
    uint64_t perfCounterFrequency = SDL_GetPerformanceFrequency();
    uint64_t lastCounter = SDL_GetPerformanceCounter();
    float delta = 0.0f;

    // Dear ImGui Setup
    {
        // Setup Dear ImGui context
        // IMGUI_CHECKVERSION();
        // ImGui::CreateContext();
        // ImGuiIO &io = ImGui::GetIO();
        // io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; // Enable Keyboard Controls
        // io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;  // Enable Gamepad Controls

        // // Setup Dear ImGui style
        // ImGui::StyleColorsDark();
        // // ImGui::StyleColorsLight();

        // // Setup Platform/Renderer backends
        // ImGui_ImplSDL2_InitForOpenGL(window, glContext);
        // ImGui_ImplOpenGL3_Init();
    }

    // Our state
    // bool show_demo_window = false;
    bool showMaterialDropdownWindow = true;

    // camera speed
    float cameraSpeed = 6.0f;

    // time
    float time = 0.0f;

    // close
    bool close = false;

    // input bools
    bool buttonW = false;
    bool buttonS = false;
    bool buttonA = false;
    bool buttonD = false;
    bool buttonSpace = false;
    bool buttonShift = false;

    // vertexBuffer.Bind();
    // indexBuffer.Bind();

    // the test mesh
    // CreateMesh("Assets/models/golem.ogle", material, glm::vec3(0.0f, 0.0f, 0.0f), vertexBuffer, indexBuffer);

    // CreateMesh("Assets/models/cube.ogle", material2, glm::vec3(5.0f, 0.0f, 0.0f), vertexBuffer, indexBuffer);

    // vertexBuffer.Unbind();
    // indexBuffer.Unbind();

    // model exporter
    ModelExporter modelExporter;

    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);

    InputManager inputManager(&close);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    renderer.SetMeshRenderers(meshRenderers);

    // const Uint8 *keystate = SDL_GetKeyboardState(NULL);
    // Main loop
    while (!close)
    {
        // input
        // check for continuous button presses
        // int x, y;
        // if (SDL_GetMouseState(&x, &y) & SDL_BUTTON_LMASK)
        // {
        //     if (clickedWorldSpacePos != glm::vec3(-9999999.0f))
        //     {
        //     //     DragObject(x, y);
        //     }
        // }

        inputManager.HandleInput();

        // normal button presses
        /*SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            ImGui_ImplSDL2_ProcessEvent(&event); // Forward your event to backend
            if (event.type == SDL_QUIT)
            {
                close = true;
            }
            else if (event.type == SDL_KEYDOWN)
            {
                switch (event.key.keysym.sym)
                {
                case SDLK_w:
                    buttonW = true;
                    break;
                case SDLK_s:
                    buttonS = true;
                    break;
                case SDLK_a:
                    buttonA = true;
                    break;
                case SDLK_d:
                    buttonD = true;
                    break;
                case SDLK_SPACE:
                    buttonSpace = true;
                    break;
                case SDLK_LSHIFT:
                    buttonShift = true;
                    break;
                }
            }
            else if (event.type == SDL_KEYUP)
            {
                switch (event.key.keysym.sym)
                {
                case SDLK_w:
                    buttonW = false;
                    break;
                case SDLK_s:
                    buttonS = false;
                    break;
                case SDLK_a:
                    buttonA = false;
                    break;
                case SDLK_d:
                    buttonD = false;
                    break;
                case SDLK_SPACE:
                    buttonSpace = false;
                    break;
                case SDLK_LSHIFT:
                    buttonShift = false;
                    break;
                }
            }
            else if (event.type == SDL_MOUSEBUTTONDOWN)
            {
                if (event.button.button == SDL_BUTTON_LEFT)
                {
                    clickedWorldSpacePos = renderer.ClickObject(event.motion.x, event.motion.y);
                }
            }

            // Mouse movement, currently disabled
            // else if (event.type == SDL_MOUSEMOTION && SDL_GetRelativeMouseMode())
            // {
            //     camera.OnMouseMoved(event.motion.xrel, event.motion.yrel);
            // }
            // else if (event.type == SDL_MOUSEBUTTONDOWN)
            // {
            //     if (event.button.button == SDL_BUTTON_LEFT)
            //     {
            //         SDL_SetRelativeMouseMode(SDL_TRUE);
            //     }
            //     if (event.button.button == SDL_BUTTON_RIGHT)
            //     {
            //         SDL_SetRelativeMouseMode(SDL_FALSE);
            //     }
            // }
        }

        // camera movement
        {
            if (buttonW)
            {
                renderer.GetCamera()->MoveFront(delta * cameraSpeed);
            }
            if (buttonS)
            {
                renderer.GetCamera()->MoveFront(-delta * cameraSpeed);
            }
            if (buttonA)
            {
                renderer.GetCamera()->MoveSideways(-delta * cameraSpeed);
            }
            if (buttonD)
            {
                renderer.GetCamera()->MoveSideways(delta * cameraSpeed);
            }
            if (buttonSpace)
            {
                renderer.GetCamera()->MoveUp(delta * cameraSpeed);
            }
            if (buttonShift)
            {
                renderer.GetCamera()->MoveUp(-delta * cameraSpeed);
            }
        }*/

        time += delta;

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // renderer.Render();
        {
            // (After event loop)
            // Start the Dear ImGui frame
            /*ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplSDL2_NewFrame();
            ImGui::NewFrame();

            // toggleable bools window
            {
                ImGui::Begin("Bools");

                ImGui::Checkbox("Show Model Importer Window", &showMaterialDropdownWindow);

                ImGui::End();
            }

            ShowMaterialDropdownWindow();

            if (ImGui::GetIO().WantCaptureMouse)
            {
                canSelectObject = false;
            }
            else
            {
                canSelectObject = true;
            }*/

            // if (showModelConverterWindow)
            // {
            //     ShowModelConverterWindow(modelExporter);
            // }

            // set clear color
            // glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

            // glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            /*// render/do other opengl stuff
            vertexBuffer.Bind();
            indexBuffer.Bind();

            camera.Update();

            for (size_t i = 0; i < meshes.size(); i++)
            {
                meshes.at(i).GetMaterial()->GetShader()->Bind();

                meshes.at(i).Render(camera);

                meshes.at(i).GetMaterial()->GetShader()->Unbind();
            }

            indexBuffer.Unbind();
            vertexBuffer.Unbind();

            // draw mesh's bounding box
            vertexBuffer2.Bind();
            indexBuffer2.Bind();
            for (size_t i = 0; i < meshes.size(); i++)
            {
                meshes.at(i).DrawBoundingBox(camera, oglShaders.at(0));
            }
            indexBuffer2.Unbind();
            vertexBuffer2.Unbind();

            // imgui rendering
            ImGui::Render();
            ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());*/

            // SDL_GL_SwapWindow(window);
        }
        renderer.SwapWindow();

        // FPS
        uint64_t endCounter = SDL_GetPerformanceCounter();
        uint64_t counterElapsed = endCounter - lastCounter;
        delta = ((float)counterElapsed) / (float)perfCounterFrequency;
        uint32_t FPS = (uint32_t)((float)perfCounterFrequency / (float)counterElapsed);
        lastCounter = endCounter;

        // std::cout << "FPS: " << FPS << std::endl;
    }

    // glDeleteTextures(1, &materialGrid.diffuseMap);

    // ImGui_ImplOpenGL3_Shutdown();
    // ImGui_ImplSDL2_Shutdown();
    // ImGui::DestroyContext();

    // SDL_GL_DeleteContext(glContext);
    // SDL_DestroyWindow(window);
    // SDL_Quit();

    return 0;
}
