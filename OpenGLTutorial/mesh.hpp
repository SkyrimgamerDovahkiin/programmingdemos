#pragma once

// std
#include <vector>
#include <fstream>
// #include <iostream>

// glm
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// custom
#include "shader.hpp"
#include "vertex_buffer.hpp"
#include "index_buffer.hpp"

namespace OGLE
{
    struct OGLEMaterial
    {
        glm::vec3 diffuse;
        glm::vec3 specular;
        glm::vec3 emissive;
        float shininess;
    };

    struct Material
    {
        OGLEMaterial material;
        GLuint diffuseMap;
        GLuint normalMap;
    };

    class Mesh
    {
    public:
        Mesh(std::vector<Vertex> &vertices, uint64_t numVertices, std::vector<uint32_t> &indices, uint64_t numIndices, Material material, Shader *shader);
        ~Mesh();

        void Render();

    private:
        VertexBuffer *vertexBuffer;
        IndexBuffer *indexBuffer;
        Shader *shader;
        Material material;
        uint64_t numIndices = 0;
        int diffuseLocation;
        int specularLocation;
        int emissiveLocation;
        int shininessLocation;
        int diffuseMapLocation;
        int normalMapLocation;
    };
}