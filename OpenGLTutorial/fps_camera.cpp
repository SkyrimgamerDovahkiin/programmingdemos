#include "fps_camera.hpp"

namespace OGLE
{
    FPSCamera::FPSCamera(float fov, float width, float height) : Camera(fov, width, height)
    {
        yaw = -90.0f;
        pitch = 0.0f;
        OnMouseMoved(0.0f, 0.0f);
        update();
    }

    void FPSCamera::OnMouseMoved(float xRel, float yRel)
    {
        yaw += xRel * mouseSensitivity;
        pitch -= yRel * mouseSensitivity;

        if (pitch < pitchMin)
        {
            pitch = pitchMin;
        }
        if (pitch > pitchMax)
        {
            pitch = pitchMax;
        }

        glm::vec3 front;
        front.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
        front.y = sin(glm::radians(pitch));
        front.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
        lookAt = glm::normalize(front);
        update();
    }

    void FPSCamera::update()
    {
        view = glm::lookAt(position, position + lookAt, vector3.up);
        viewProj = projection * view;
    }

    void FPSCamera::MoveFront(float amount)
    {
        translate(glm::normalize(glm::vec3(1.0f, 0.0f, 1.0f) * lookAt) * amount);
        update();
    }

    void FPSCamera::MoveSideways(float amount)
    {
        translate(glm::normalize(glm::cross(lookAt, vector3.up)) * amount);
        update();
    }
}