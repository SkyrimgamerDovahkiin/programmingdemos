#include "font.hpp"

namespace OGLE
{
    Font::~Font()
    {
        if (fontVertexBufferData)
        {
            delete[] fontVertexBufferData;
        }
    }

    void Font::InitFont(std::string filename)
    {
        uint8_t ttfBuffer[1 << 20];
        uint8_t tmpBitmap[512 * 512];

        fread(ttfBuffer, 1, 1 << 20, fopen(filename.c_str(), "rb"));
        stbtt_BakeFontBitmap(ttfBuffer, 0, 32.0f, tmpBitmap, 512, 512, 32, 96, cdata);

        glGenTextures(1, &fontTexture);
        glBindTexture(GL_TEXTURE_2D, fontTexture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, 512, 512, 0, GL_ALPHA, GL_UNSIGNED_BYTE, tmpBitmap);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glBindTexture(GL_TEXTURE_2D, 0);
        
        // vao/vertexBuffer setup
        glGenVertexArrays(1, &fontVao);
        glBindVertexArray(fontVao);
        glGenBuffers(1, &fontVertexBufferId);
        glBindBuffer(GL_ARRAY_BUFFER, fontVertexBufferId);

        fontVertexBufferCapacity = 20;
        fontVertexBufferData = new FontVertex[fontVertexBufferCapacity * 6];
        
        glBufferData(GL_ARRAY_BUFFER, sizeof(FontVertex) * 6 * fontVertexBufferCapacity, NULL, GL_DYNAMIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(FontVertex), 0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(FontVertex), (const void *)offsetof(FontVertex, texCoords));
        glBindVertexArray(0);
    }

    void Font::DrawString(float x, float y, std::string text, Shader *shader)
    {
        glBindVertexArray(fontVao);
        glBindBuffer(GL_ARRAY_BUFFER, fontVertexBufferId);

        uint32_t len = text.size();
        if (fontVertexBufferCapacity < len)
        {
            fontVertexBufferCapacity = len;
            glBufferData(GL_ARRAY_BUFFER, sizeof(FontVertex) * 6 * fontVertexBufferCapacity, 0, GL_DYNAMIC_DRAW);
            delete[] fontVertexBufferData;
            fontVertexBufferData = new FontVertex[fontVertexBufferCapacity * 6];
        }

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, fontTexture);
        glUniform1i(glGetUniformLocation(shader->GetShaderId(), "u_texture"), 0);

        FontVertex *vData = fontVertexBufferData;
        uint32_t numVertices = 0;

        for (int i = 0; i < text.size(); i++)
        {
            if (text.at(i) >= 32 && text.at(i) < 128)
            {
                stbtt_aligned_quad q;
                stbtt_GetBakedQuad(cdata, 512, 512, text.at(i)-32, &x, &y, &q, 1);

                vData[0].position = glm::vec2(q.x0, q.y0);
                vData[0].texCoords = glm::vec2(q.s0, q.t0);

                vData[1].position = glm::vec2(q.x1, q.y0);
                vData[1].texCoords = glm::vec2(q.s1, q.t0);

                vData[2].position = glm::vec2(q.x1, q.y1);
                vData[2].texCoords = glm::vec2(q.s1, q.t1);

                vData[3].position = glm::vec2(q.x0, q.y1);
                vData[3].texCoords = glm::vec2(q.s0, q.t1);

                vData[4].position = glm::vec2(q.x0, q.y0);
                vData[4].texCoords = glm::vec2(q.s0, q.t0);
                
                vData[5].position = glm::vec2(q.x1, q.y1);
                vData[5].texCoords = glm::vec2(q.s1, q.t1);

                vData += 6;
                numVertices += 6;
            }
        }

        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(FontVertex) * numVertices, fontVertexBufferData);
        glDrawArrays(GL_TRIANGLES, 0, numVertices);
    }
}