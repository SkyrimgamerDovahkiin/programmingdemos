#include "floating_camera.hpp"

namespace OGLE
{
    FloatingCamera::FloatingCamera(float fov, float width, float height) : FPSCamera(fov, width, height)
    {
        
    }

    void FloatingCamera::MoveUp(float amount)
    {
        translate(vector3.up * amount);
    }
}
