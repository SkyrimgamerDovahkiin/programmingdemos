// std
#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>

// stb
#define STB_IMAGE_IMPLEMENTATION
#include "libs/stb_image.h"
#undef STB_IMAGE_IMPLEMENTATION

#define STB_TRUETYPE_IMPLEMENTATION
#include "libs/stb_truetype.h"
#undef STB_TRUETYPE_IMPLEMENTATION

// GLEW/SDL
#define GLEW_STATIC
#include <GL/glew.h>
#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>

// glm
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#ifdef _DEBUG

void _GLGetError(const char *file, int line, const char *call)
{
    while (GLenum error = glGetError())
    {
        std::cout << "[OpenGL Error] " << glewGetErrorString(error) << " in " << file << ":" << line << " Call: " << call << std::endl;
    }
}

#define GLCALL(call) \
    call;            \
    _GLGetError(__FILE__, __LINE__, #call)

#else

#define GLCALL(call) call;

#endif

// custom
#include "defines.hpp"
#include "vertex_buffer.hpp"
#include "index_buffer.hpp"
#include "shader.hpp"
#include "floating_camera.hpp"
#include "mesh.hpp"
#include "model.hpp"
#include "font.hpp"

using OGLE::VertexBuffer, OGLE::Shader, OGLE::IndexBuffer, OGLE::FloatingCamera, OGLE::Mesh, OGLE::Material, OGLE::Model, OGLE::Font;

void GLAPIENTRY OpenGLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
{
    std::cout << "[OpenGL Error] " << message << std::endl;
}

int main(int argc, char **argv)
{
    int screenWidth = 800, screenHeight = 600;

    SDL_Window *window;
    SDL_Init(SDL_INIT_EVERYTHING);

    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetSwapInterval(1);

// Debug
#ifdef _DEBUG
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
#endif

    uint32_t flags = SDL_WINDOW_OPENGL;

    window = SDL_CreateWindow("C++ OpenGL Tutorial", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, flags);
    SDL_GLContext glContext = SDL_GL_CreateContext(window);

    GLenum err = glewInit();
    if (err != GLEW_OK)
    {
        std::cout << "Error: " << glewGetErrorString(err) << std::endl;
        std::cin.get();
        return -1;
    }

    std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;

#ifdef _DEBUG
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(OpenGLDebugCallback, 0);
#endif

    Shader textShader("shaders/text_vert.glsl", "shaders/text_frag.glsl");

    Shader shader("shaders/basic_vert.glsl", "shaders/basic_frag.glsl");
    shader.Bind();
    int directionLocation = GLCALL(glGetUniformLocation(shader.GetShaderId(), "u_directional_light.direction"));
    glm::vec3 sunColor = glm::vec3(0.0f);
    glm::vec3 sunDir = glm::vec3(-1.0f);

    GLCALL(glUniform3fv(glGetUniformLocation(shader.GetShaderId(), "u_directional_light.diffuse"), 1, (float *)&sunColor[0]));
    GLCALL(glUniform3fv(glGetUniformLocation(shader.GetShaderId(), "u_directional_light.specular"), 1, (float *)&sunColor[0]));
    sunColor *= 0.4f;
    GLCALL(glUniform3fv(glGetUniformLocation(shader.GetShaderId(), "u_directional_light.ambient"), 1, (float *)&sunColor[0]));

    glm::vec3 pointLightColor = glm::vec3(0.0f, 0.0f, 0.0f);
    GLCALL(glUniform3fv(glGetUniformLocation(shader.GetShaderId(), "u_point_light.diffuse"), 1, (float *)&pointLightColor[0]));
    GLCALL(glUniform3fv(glGetUniformLocation(shader.GetShaderId(), "u_point_light.specular"), 1, (float *)&pointLightColor[0]));
    pointLightColor *= 0.2f;
    GLCALL(glUniform3fv(glGetUniformLocation(shader.GetShaderId(), "u_point_light.ambient"), 1, (float *)&pointLightColor[0]));
    GLCALL(glUniform1f(glGetUniformLocation(shader.GetShaderId(), "u_point_light.linear"), 0.027f));
    GLCALL(glUniform1f(glGetUniformLocation(shader.GetShaderId(), "u_point_light.quadratic"), 0.0028f));
    glm::vec4 pointLightPos = glm::vec4(0.0f, 0.0f, 10.0f, 1.0f);
    int positionLocation = GLCALL(glGetUniformLocation(shader.GetShaderId(), "u_point_light.position"));

    glm::vec3 spotLightColor = glm::vec3(1.0f);
    GLCALL(glUniform3fv(glGetUniformLocation(shader.GetShaderId(), "u_spot_light.diffuse"), 1, (float *)&spotLightColor[0]));
    GLCALL(glUniform3fv(glGetUniformLocation(shader.GetShaderId(), "u_spot_light.specular"), 1, (float *)&spotLightColor[0]));
    spotLightColor *= 0.2f;
    GLCALL(glUniform3fv(glGetUniformLocation(shader.GetShaderId(), "u_spot_light.ambient"), 1, (float *)&spotLightColor[0]));
    glm::vec3 spotLightPos = glm::vec3(0.0f);
    GLCALL(glUniform3fv(glGetUniformLocation(shader.GetShaderId(), "u_spot_light.position"), 1, (float *)&spotLightPos[0]));
    glm::vec3 spotLightDir = glm::vec3(0.0f, 0.0f, 1.0f);
    GLCALL(glUniform3fv(glGetUniformLocation(shader.GetShaderId(), "u_spot_light.direction"), 1, (float *)&spotLightDir[0]));
    GLCALL(glUniform1f(glGetUniformLocation(shader.GetShaderId(), "u_spot_light.innerCone"), 0.95f));
    GLCALL(glUniform1f(glGetUniformLocation(shader.GetShaderId(), "u_spot_light.outerCone"), 0.80f));

    Font font;
    font.InitFont("fonts/gothica2/Gothica-Book.ttf");

    Model monkey;
    monkey.Init("models/OpenGlTestModel.ogle", &shader);

    uint64_t perfCounterFrequency = SDL_GetPerformanceFrequency();
    uint64_t lastCounter = SDL_GetPerformanceCounter();
    float delta = 0.0f;

    glm::mat4 model = glm::mat4(1.0f);
    model = glm::scale(model, glm::vec3(1.0f));

    FloatingCamera camera(90.0f, screenWidth, screenHeight);
    camera.translate(glm::vec3(0.0f, 0.0f, 5.0f));
    camera.update();

    glm::mat4 modelViewProj = camera.getViewProj() * model;

    int modelViewProjMatrixLocaction = GLCALL(glGetUniformLocation(shader.GetShaderId(), "u_modelViewProj"));
    int modelViewLocaction = GLCALL(glGetUniformLocation(shader.GetShaderId(), "u_modelView"));
    int invModelViewLocaction = GLCALL(glGetUniformLocation(shader.GetShaderId(), "u_invModelView"));

    // int colorUniformLocaction = GLCALL(glGetUniformLocation(shader.GetShaderId(), "u_color"));
    // if (!colorUniformLocation != -1)
    // {
    //     GLCALL(glUniform4f(colorUniformLocation, 1.0f, 0.0f, 1.0f, 1.0f));
    // }

    // int textureUniformLocation = GLCALL(glGetUniformLocation(shader.GetShaderId(), "u_texture"));
    // if (!textureUniformLocation != -1)
    // {
    //     GLCALL(glUniform1i(textureUniformLocation, 0));
    // }

    bool buttonW = false;
    bool buttonS = false;
    bool buttonA = false;
    bool buttonD = false;
    bool buttonSpace = false;
    bool buttonShift = false;

    float cameraSpeed = 6.0f;
    float time = 0.0f;
    bool close = false;
    uint32_t FPS = 0;

    SDL_SetRelativeMouseMode(SDL_TRUE);
    GLCALL(glEnable(GL_CULL_FACE));
    GLCALL(glEnable(GL_DEPTH_TEST));
    while (!close)
    {
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
            {
                close = true;
            }
            else if (event.type == SDL_KEYDOWN)
            {
                switch (event.key.keysym.sym)
                {
                case SDLK_w:
                    buttonW = true;
                    break;
                case SDLK_s:
                    buttonS = true;
                    break;
                case SDLK_a:
                    buttonA = true;
                    break;
                case SDLK_d:
                    buttonD = true;
                    break;
                case SDLK_SPACE:
                    buttonSpace = true;
                    break;
                case SDLK_LSHIFT:
                    buttonShift = true;
                    break;
                case SDLK_ESCAPE:
                    SDL_SetRelativeMouseMode(SDL_FALSE);
                    break;
                }
            }
            else if (event.type == SDL_KEYUP)
            {
                switch (event.key.keysym.sym)
                {
                case SDLK_w:
                    buttonW = false;
                    break;
                case SDLK_s:
                    buttonS = false;
                    break;
                case SDLK_a:
                    buttonA = false;
                    break;
                case SDLK_d:
                    buttonD = false;
                    break;
                case SDLK_SPACE:
                    buttonSpace = false;
                    break;
                case SDLK_LSHIFT:
                    buttonShift = false;
                    break;
                }
            }
            else if (event.type == SDL_MOUSEMOTION)
            {
                if (SDL_GetRelativeMouseMode())
                {
                    camera.OnMouseMoved(event.motion.xrel, event.motion.yrel);
                }
            }
            else if (event.type == SDL_MOUSEBUTTONDOWN)
            {
                if (event.button.button == SDL_BUTTON_LEFT)
                {
                    SDL_SetRelativeMouseMode(SDL_TRUE);
                }
            }
        }

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        time += delta;

        // Wireframe
        // glPolygonMode(GL_FRONT_AND_BA6CK, GL_LINE);

        // if (!colorUniformLocation != -1)
        // {
        //     GLCALL(glUniform4f(colorUniformLocation, sinf(time) * sinf(time), 0.0f, 1.0f, 1.0f));
        // }

        if (buttonW)
        {
            camera.MoveFront(delta * cameraSpeed);
        }
        if (buttonS)
        {
            camera.MoveFront(-delta * cameraSpeed);
        }
        if (buttonA)
        {
            camera.MoveSideways(-delta * cameraSpeed);
        }
        if (buttonD)
        {
            camera.MoveSideways(delta * cameraSpeed);
        }
        if (buttonSpace)
        {
            camera.MoveUp(delta * cameraSpeed);
        }
        if (buttonShift)
        {
            camera.MoveUp(-delta * cameraSpeed);
        }

        camera.update();
        shader.Bind();
        model = glm::rotate(model, 1.0f * delta, glm::vec3(0, 1, 0));
        modelViewProj = camera.getViewProj() * model;
        glm::mat4 modelView = camera.getView() * model;
        glm::mat4 invModelView = glm::transpose(glm::inverse(modelView));

        glm::vec4 transformedSunDir = glm::transpose(glm::inverse(camera.getView())) * glm::vec4(sunDir, 1.0f);
        glUniform3fv(directionLocation, 1, (float *)&transformedSunDir[0]);

        glm::mat4 pointLightMatrix = glm::rotate(glm::mat4(1.0f), -delta, {0.0f, 1.0f, 0.0f});
        pointLightPos = pointLightMatrix * pointLightPos;
        glm::vec3 transformedPointLightPos = (glm::vec3)(camera.getView() * pointLightPos);
        glUniform3fv(positionLocation, 1, (float *)&transformedPointLightPos[0]);

        GLCALL(glUniformMatrix4fv(modelViewProjMatrixLocaction, 1, GL_FALSE, &modelViewProj[0][0]));
        GLCALL(glUniformMatrix4fv(modelViewLocaction, 1, GL_FALSE, &modelView[0][0]));
        GLCALL(glUniformMatrix4fv(invModelViewLocaction, 1, GL_FALSE, &invModelView[0][0]));
        monkey.Render();
        shader.Unbind();

        textShader.Bind();

        int w, h;
        SDL_GetWindowSize(window, &w, &h);
        glm::mat4 ortho = glm::ortho(0.0f, (float)w, (float)h, 0.0f);
        glUniformMatrix4fv(glGetUniformLocation(textShader.GetShaderId(), "u_modelViewProj"), 1, GL_FALSE, &ortho[0][0]);
        glDisable(GL_CULL_FACE);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glDisable(GL_DEPTH_TEST);

        // font.DrawString(20.0f, 20.0f, "CO2-Neutral seit 2007", &textShader);
        std::string fpsString = "FPS: ";
        fpsString.append(std::to_string(FPS));

        font.DrawString(20.0f, 20.0f, fpsString, &textShader);

        textShader.Unbind();
        glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST);

        SDL_GL_SwapWindow(window);

        uint64_t endCounter = SDL_GetPerformanceCounter();
        uint64_t counterElapsed = endCounter - lastCounter;
        delta = ((float)counterElapsed) / (float)perfCounterFrequency;
        FPS = (uint32_t)((float)perfCounterFrequency / (float)counterElapsed);
        lastCounter = endCounter;
    }

    // GLCALL(glDeleteTextures(1, &textureId));

    return 0;
}