#include "mesh.hpp"

namespace OGLE
{
    Mesh::Mesh(std::vector<Vertex>& vertices, uint64_t numVertices, std::vector<uint32_t>& indices, uint64_t numIndices, Material material, Shader *shader)
    {
        this->material = material;
        this->shader = shader;
        this->numIndices = numIndices;

        vertexBuffer = new VertexBuffer(vertices.data(), numVertices);
        indexBuffer = new IndexBuffer(indices.data(), numIndices, sizeof(indices[0]));

        diffuseLocation = glGetUniformLocation(shader->GetShaderId(), "u_material.diffuse");
        specularLocation = glGetUniformLocation(shader->GetShaderId(), "u_material.specular");
        emissiveLocation = glGetUniformLocation(shader->GetShaderId(), "u_material.emissive");
        shininessLocation = glGetUniformLocation(shader->GetShaderId(), "u_material.shininess");
        diffuseMapLocation = glGetUniformLocation(shader->GetShaderId(), "u_diffuse_map");
        normalMapLocation = glGetUniformLocation(shader->GetShaderId(), "u_normal_map");
    }

    Mesh::~Mesh()
    {
        delete vertexBuffer;
        delete indexBuffer;
    }

    void Mesh::Render()
    {
        vertexBuffer->Bind();
        indexBuffer->Bind();
        glUniform3fv(diffuseLocation, 1, &material.material.diffuse[0]);
        glUniform3fv(specularLocation, 1, &material.material.specular[0]);
        glUniform3fv(emissiveLocation, 1, &material.material.emissive[0]);
        glUniform1f(shininessLocation, material.material.shininess);

        glBindTexture(GL_TEXTURE_2D, material.diffuseMap);
        glUniform1i(diffuseMapLocation, 0);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, material.normalMap);
        glActiveTexture(GL_TEXTURE0);
        glUniform1i(normalMapLocation, 1);
        glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, 0);
    }
}