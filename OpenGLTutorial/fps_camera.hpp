#include "camera.hpp"
#include "defines.hpp"

namespace OGLE
{
    class FPSCamera : public Camera
    {
    public:
        FPSCamera(float fov, float width, float height);
        void OnMouseMoved(float xRel, float yRel);
        void update() override;
        void MoveFront(float amount);
        void MoveSideways(float amount);
    protected:
        float yaw;
        float pitch;
        glm::vec3 lookAt;
        const float mouseSensitivity = 0.3f;
        const float pitchMax = 89.0f;
        const float pitchMin = -89.0f;
        Vector3 vector3;
    };
}