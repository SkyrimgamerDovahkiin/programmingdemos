#include "fps_camera.hpp"
#include "defines.hpp"

namespace OGLE
{
    class FloatingCamera : public FPSCamera
    {
    public:
        FloatingCamera(float fov, float width, float height);
        void MoveUp(float amount);
    private:
    };
}