#pragma once

// std
#include <vector>
#include <fstream>
#include <iostream>

// glm
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// custom
#include "mesh.hpp"

//stb
#include "libs/stb_image.h"

namespace OGLE
{
    class Model
    {
    public:
        Model();
        ~Model();

        void Init(const char* filename, Shader* shader);
        void Render();

    private:
        std::vector<Mesh*> meshes;
        std::vector<Material> materials;
    };
}