#include "model.hpp"

namespace OGLE
{
    Model::Model()
    {
    }

    Model::~Model()
    {
        for (Mesh *mesh : meshes)
        {
            delete mesh;
        }
    }

    void Model::Init(const char *filename, Shader *shader)
    {
        uint64_t numMeshes = 0;
        uint64_t numMaterials = 0;
        std::ifstream input = std::ifstream(filename, std::ios::in | std::ios::binary);
        if (!input.is_open())
        {
            std::cout << "File not found!" << std::endl;
            return;
        }

        // Materials
        input.read((char *)&numMaterials, sizeof(uint64_t));
        for (uint64_t i = 0; i < numMaterials; i++)
        {
            Material material = {};
            input.read((char *)&material, sizeof(OGLEMaterial));

            uint64_t diffuseMapNameLength = 0;
            input.read((char *)&diffuseMapNameLength, sizeof(uint64_t));
            std::string diffuseMapName(diffuseMapNameLength, '\0');
            input.read((char *)&diffuseMapName[0], diffuseMapNameLength);

            uint64_t normalMapNameLength = 0;
            input.read((char *)&normalMapNameLength, sizeof(uint64_t));
            std::string normalMapName(normalMapNameLength, '\0');
            input.read((char *)&normalMapName[0], normalMapNameLength);

            assert(diffuseMapNameLength > 0);
            assert(normalMapNameLength > 0);

            int32_t textureWidth = 0;
            int32_t textureHeight = 0;
            int32_t bitsPerPixel = 0;
            glGenTextures(2, &material.diffuseMap);
            stbi_set_flip_vertically_on_load(true);
            {
                auto textureBuffer = stbi_load(diffuseMapName.c_str(), &textureWidth, &textureHeight, &bitsPerPixel, 4);
                assert(textureBuffer);
                assert(material.diffuseMap);

                glBindTexture(GL_TEXTURE_2D, material.diffuseMap);

                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, textureWidth, textureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureBuffer);

                if (textureBuffer)
                {
                    stbi_image_free(textureBuffer);
                }
            }

            {
                auto textureBuffer = stbi_load(normalMapName.c_str(), &textureWidth, &textureHeight, &bitsPerPixel, 4);
                assert(textureBuffer);
                assert(material.normalMap);

                glBindTexture(GL_TEXTURE_2D, material.normalMap);

                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, textureWidth, textureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureBuffer);

                if (textureBuffer)
                {
                    stbi_image_free(textureBuffer);
                }
            }

            glBindTexture(GL_TEXTURE_2D, 0);
            materials.push_back(material);
        }

        // Meshes
        input.read((char *)&numMeshes, sizeof(uint64_t));

        for (uint64_t i = 0; i < numMeshes; i++)
        {
            std::vector<Vertex> vertices;
            uint64_t numVertices = 0;

            std::vector<uint32_t> indices;
            uint64_t numIndices = 0;
            uint64_t materialIndex = 0;

            input.read((char *)&materialIndex, sizeof(uint64_t));
            input.read((char *)&numVertices, sizeof(uint64_t));
            input.read((char *)&numIndices, sizeof(uint64_t));

            for (uint64_t i = 0; i < numVertices; i++)
            {
                Vertex vertex;
                input.read((char *)&vertex.position.x, sizeof(float));
                input.read((char *)&vertex.position.y, sizeof(float));
                input.read((char *)&vertex.position.z, sizeof(float));

                input.read((char *)&vertex.normal.x, sizeof(float));
                input.read((char *)&vertex.normal.y, sizeof(float));
                input.read((char *)&vertex.normal.z, sizeof(float));

                input.read((char *)&vertex.tangent.x, sizeof(float));
                input.read((char *)&vertex.tangent.y, sizeof(float));
                input.read((char *)&vertex.tangent.z, sizeof(float));

                input.read((char *)&vertex.textureCoord.x, sizeof(float));
                input.read((char *)&vertex.textureCoord.y, sizeof(float));
                vertices.push_back(vertex);
            }

            for (uint64_t i = 0; i < numIndices; i++)
            {
                uint32_t index;
                input.read((char *)&index, sizeof(uint32_t));
                indices.push_back(index);
            }

            Mesh *mesh = new Mesh(vertices, numVertices, indices, numIndices, materials[materialIndex], shader);
            meshes.push_back(mesh);
        }
    }

    void Model::Render()
    {
        for (Mesh *mesh : meshes)
        {
            mesh->Render();
        }
    }
}