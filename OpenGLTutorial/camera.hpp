#pragma once

// glm
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace OGLE
{
    class Camera
    {
    public:
        Camera(float fov, float width, float height);
        ~Camera();

        glm::mat4 getViewProj();
        glm::mat4 getView();
        virtual void update();
        virtual void translate(glm::vec3 v);

    protected:
        glm::vec3 position;
        glm::mat4 projection;
        glm::mat4 view;
        glm::mat4 viewProj;
    };
}