#pragma once

// std
#include <iostream>
#include <string>
#include <fstream>

// GLEW
#include <GL/glew.h>

// stb
#include "libs/stb_truetype.h"

// glm
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// custom
#include "shader.hpp"

namespace OGLE
{
    struct FontVertex
    {
        glm::vec2 position;
        glm::vec2 texCoords;
    };

    struct Font
    {
        ~Font();

        void InitFont(std::string filename);
        // void Font::DrawString(float x, float y, const char *text, Shader *shader)
        void DrawString(float x, float y, std::string text, Shader *shader);

    private:
        stbtt_bakedchar cdata[96];
        GLuint fontTexture;
        GLuint fontVao;
        GLuint fontVertexBufferId;
        FontVertex *fontVertexBufferData = 0;
        uint32_t fontVertexBufferCapacity;
    };
}