#pragma once

#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

struct Vector3
{
    glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
    glm::vec3 down = glm::vec3(0.0f, -1.0f, 0.0f);
    glm::vec3 forward = glm::vec3(0.0f, 0.0f, 1.0f);
    glm::vec3 back = glm::vec3(0.0f, 0.0f, -1.0f);
    glm::vec3 right = glm::vec3(1.0f, 0.0f, 0.0f);
    glm::vec3 left = glm::vec3(-1.0f, 0.0f, 0.0f);
    glm::vec3 zero = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 one = glm::vec3(1.0f, 1.0f, 1.0f);
};

struct Vertex
{
    // float x;
    // float y;
    // float z;

    // float u;
    // float v;

    // float r;
    // float g;
    // float b;
    // float a;
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec3 tangent;
    glm::vec2 textureCoord;
};