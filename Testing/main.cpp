// std
#include <iostream>
#include <vector>
#include <unordered_map>

#include <glm/glm.hpp>

// GLEW/SDL
#define GLEW_STATIC
#include <GL/glew.h>
#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>

void GLAPIENTRY OpenGLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
{
    std::cout << "[OpenGL Error] " << message << std::endl;
}

#ifdef _DEBUG

void _GLGetError(const char *file, int line, const char *call)
{
    while (GLenum error = glGetError())
    {
        std::cout << "[OpenGL Error] " << glewGetErrorString(error) << " in " << file << ":" << line << " Call: " << call << std::endl;
    }
}

#endif

// int size = 0;
// int offset = 0;
int size = 0;

int screenWidth = 1000, screenHeight = 1000;

// std::vector<unsigned char> data;

// std::vector<std::pair<std::string, std::vector<unsigned char>>> data;

std::unordered_map<std::string, std::vector<unsigned char>> data;

void to_vector(std::vector<float> vec, std::string loc)
{
    auto test = data.find(loc);
    test->second.resize(vec.size());

    for (size_t i = 0; i < vec.size(); i++)
    {
        memcpy(test->second.data() + i * sizeof(float), &vec.at(i), sizeof(vec.at(i)));
    }
}

auto from_vector(std::string loc, size_t size)
{
    std::vector<float> vec(size, 0.0f);

    auto test = data.find(loc);
    // make sure the vector is the right size
    if (vec.size() != test->second.size())
        throw std::runtime_error{"Size of data in vector and float do not match"};
    // copy the bytes into the float
    for (size_t i = 0; i < vec.size(); i++)
    {
        memcpy(&vec.at(i), test->second.data() + i * sizeof(float), sizeof(vec.at(i)));
    }

    return vec;
}

// Main code
int main(int, char **)
{
    // sdl init
    SDL_Window *window;

    {
        SDL_Init(SDL_INIT_EVERYTHING);

        SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetSwapInterval(1);
    }

    // Debug
#ifdef _DEBUG
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
#endif

    uint32_t flags = SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE;
    window = SDL_CreateWindow("OpenGL Game", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, flags);
    SDL_GLContext glContext = SDL_GL_CreateContext(window);

    GLenum err = glewInit();
    if (err != GLEW_OK)
    {
        std::cout << "Error: " << glewGetErrorString(err) << std::endl;
        std::cin.get();
        return -1;
    }

    std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;

#ifdef _DEBUG
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(OpenGLDebugCallback, 0);
#endif

    // close
    bool close = false;

    // Main loop
    while (!close)
    {
        // input
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
            {
                close = true;
            }
        }

        // set clear color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glViewport(0, 0, screenWidth / 2, screenHeight / 2);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        SDL_GL_SwapWindow(window);
    }

    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
