#include "defines.hpp"

// overwrite operator for writing material
std::ofstream &operator<<(std::ofstream &stream, Material &material)
{
    stream.write(reinterpret_cast<char *>(&material.shaderID), sizeof(float));

    stream.write(reinterpret_cast<char *>(&material.diffuse.x), sizeof(float));
    stream.write(reinterpret_cast<char *>(&material.diffuse.y), sizeof(float));
    stream.write(reinterpret_cast<char *>(&material.diffuse.z), sizeof(float));

    stream.write(reinterpret_cast<char *>(&material.specular.x), sizeof(float));
    stream.write(reinterpret_cast<char *>(&material.specular.y), sizeof(float));
    stream.write(reinterpret_cast<char *>(&material.specular.z), sizeof(float));

    stream.write(reinterpret_cast<char *>(&material.emissive.x), sizeof(float));
    stream.write(reinterpret_cast<char *>(&material.emissive.y), sizeof(float));
    stream.write(reinterpret_cast<char *>(&material.emissive.z), sizeof(float));

    stream.write(reinterpret_cast<char *>(&material.shininess), sizeof(float));

    return stream;
}

// overwrite operator for reading material
std::ifstream &operator>>(std::ifstream &stream, Material &material)
{
    stream.read(reinterpret_cast<char *>(&material.shaderID), sizeof(float));

    stream.read(reinterpret_cast<char *>(&material.diffuse.x), sizeof(float));
    stream.read(reinterpret_cast<char *>(&material.diffuse.y), sizeof(float));
    stream.read(reinterpret_cast<char *>(&material.diffuse.z), sizeof(float));

    stream.read(reinterpret_cast<char *>(&material.specular.x), sizeof(float));
    stream.read(reinterpret_cast<char *>(&material.specular.y), sizeof(float));
    stream.read(reinterpret_cast<char *>(&material.specular.z), sizeof(float));

    stream.read(reinterpret_cast<char *>(&material.emissive.x), sizeof(float));
    stream.read(reinterpret_cast<char *>(&material.emissive.y), sizeof(float));
    stream.read(reinterpret_cast<char *>(&material.emissive.z), sizeof(float));

    stream.read(reinterpret_cast<char *>(&material.shininess), sizeof(float));

    return stream;
}