#include <stdio.h>

void printField(int *field);
/*void moveTurm(int *field, int *turmPosX, int *turmPosY, int *turmNewPosX, int *turmNewPosY);
void moveSpringer(int *field, int *springerPosX, int *springerPosY, int *springerNewPosX, int *springerNewPosY);
void moveBauer(int *field, int *bauerPosX, int *bauerPosY, int *bauerNewPosX, int *bauerNewPosY);
void moveLauefer(int *field, int *laueferPosX, int *laueferPosY, int *laueferNewPosX, int *laueferNewPosY);
void moveDame(int *field, int *damePosX, int *damePosY, int *dameNewPosX, int *dameNewPosY);
void moveKoenig(int *field, int *koenigPosX, int *koenigPosY, int *koenigNewPosX, int *koenigNewPosY);*/

//void moveTurm(int *field, int *turmPosX, int *turmPosY, int *newPosX, int *newPosY);
void moveSpringer(int *field, int *posX, int *posY, int *newPosX, int *newPosY);
/*void moveBauer(int *field, int *bauerPosX, int *bauerPosY, int *newPosX, int *newPosY);
void moveLauefer(int *field, int *laueferPosX, int *laueferPosY, int *newPosX, int *newPosY);
void moveDame(int *field, int *damePosX, int *damePosY, int *newPosX, int *newPosY);
void moveKoenig(int *field, int *koenigPosX, int *koenigPosY, int *newPosX, int *newPosY);*/

int main()
{
    //TODO: Feld richtig gefüllt erzeugen;
    int field[8][8] = {2, 1, 4, 6, 5, 4, 1, 2,
                       3, 3, 3, 3, 3, 3, 3, 3,
                       0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0,
                       3, 3, 3, 3, 3, 3, 3, 3,
                       2, 1, 4, 6, 5, 4, 1, 2},
        //int field[8][8] = {0},
        spielfigur,
        posX,
        posY,
        //turmPosX = 7,
        //turmPosY = 7,
        //springerPosX = 0,
        //springerPosY = 7,
        //bauerPosX = 0,
        //bauerPosY = 6,
        //laueferPosX = 1,
        //laueferPosY = 7,
        //damePosX = 2,
        //damePosY = 7,
        //koenigPosX = 3,
        //koenigPosY = 7,
        newPosX,
        newPosY;
    char temp;

    enum Type
    {
        NONE = 0,
        SPRINGER,
        TURM,
        BAUER,
        LAEUFER,
        DAME,
        KOENIG,
    };

    // setzen der Spielfiguren
    //field[posY][posX] = 1;
    //field[turmPosY][turmPosX] = 2;
    //field[bauerPosY][bauerPosX] = 3;
    //field[laueferPosY][laueferPosX] = 4;
    //field[damePosY][damePosX] = 5;
    //field[koenigPosY][koenigPosX] = 6;
    printf("\nBeenden mit 0\n");
    do
    {
        printField(&field[0][0]);
        printf("\nWelche Figur [1 = springer, 2 = turm, 3 = bauer, 4 = läufer, 5 = dame, 6 = koenig]?: ");
        scanf("%d%c", &spielfigur, &temp);

        switch (spielfigur)
        {
        case SPRINGER:
            printf("\nWelches Feld Y?: ");
            scanf("%d%c", &posY, &temp);
            printf("\nWelches Feld X?: ");
            scanf("%d%c", &posX, &temp);
            if (field[posY][posX] == 1)
            {
                printf("\nWelches Neue Feld Y?: ");
                scanf("%d%c", &newPosY, &temp);
                printf("\nWelches Neue Feld X?: ");
                scanf("%d%c", &newPosX, &temp);
            }
            else
            {
                printf("\nZug ungültig! ");
            }
            
            if (field[newPosY][newPosX] == 0)
            {
                moveSpringer(&field[0][0], &posX, &posY, &newPosX, &newPosY);
            }
            else
            {
                printf("\nZug ungültig! ");
            }
            break;
            /*case TURM:
            printf("\nWelches Feld Y?: ");
            scanf("%d%c", &newPosY, &temp);
            printf("\nWelches Feld X?: ");
            scanf("%d%c", &newPosX, &temp);
            if (field[newPosY][newPosX] == 0)
            {
                moveTurm(&field[0][0], &turmPosX, &turmPosY, &newPosX, &newPosY);
            }
            else
            {
                printf("\nZug ungültig! ");
            }
            break;
        case BAUER:
            printf("\nWelches Feld Y?: ");
            scanf("%d%c", &newPosY, &temp);
            printf("\nWelches Feld X?: ");
            scanf("%d%c", &newPosX, &temp);
            if (field[newPosY][newPosX] == 0)
            {
                moveBauer(&field[0][0], &bauerPosX, &bauerPosY, &newPosX, &newPosY);
            }
            else
            {
                printf("\nZug ungültig! ");
            }
            break;
        case LAEUFER:
            printf("\nWelches Feld Y?: ");
            scanf("%d%c", &newPosY, &temp);
            printf("\nWelches Feld X?: ");
            scanf("%d%c", &newPosX, &temp);

            if (field[newPosY][newPosX] == 0)
            {
                moveLauefer(&field[0][0], &laueferPosX, &laueferPosY, &newPosX, &newPosY);
            }
            else
            {
                printf("\nZug ungültig! ");
            }
            break;
        case DAME:
            printf("\nWelches Feld Y?: ");
            scanf("%d%c", &newPosY, &temp);
            printf("\nWelches Feld X?: ");
            scanf("%d%c", &newPosX, &temp);

            if (field[newPosY][newPosX] == 0)
            {
                moveDame(&field[0][0], &damePosX, &damePosY, &newPosX, &newPosY);
            }
            else
            {
                printf("\nZug ungültig! ");
            }
            break;
        case KOENIG:
            printf("\nWelches Feld Y?: ");
            scanf("%d%c", &newPosY, &temp);
            printf("\nWelches Feld X?: ");
            scanf("%d%c", &newPosX, &temp);

            if (field[newPosY][newPosX] == 0)
            {
                moveKoenig(&field[0][0], &koenigPosX, &koenigPosY, &newPosX, &newPosY);
            }
            else
            {
                printf("\nZug ungültig! ");
            }
            break;*/
        }
    } while (spielfigur != 0);
    return 0;
}

// Ausgabe Spielfeld
void printField(int *field)
{
    printf("\n");
    int i, j;
    // Schleife fuer Zeilen, Y-Achse
    for (i = 0; i < 8; i++)
    {
        // Schleife fuer Spalten, X-Achse
        for (j = 0; j < 8; j++)
        {
            printf("%d ", *(field + i * 8 + j));
        }
        printf("\n");
    }
}
// Spielfigur bewegen

void moveSpringer(int *field, int *posX, int *posY, int *newPosX, int *newPosY)
{
    // alte Position loeschen
    *(field + *posY * 8 + *posX) = 0;
    // neue Position bestimmen
    *posX = *newPosX;
    *posY = *newPosY;
    // Grenzueberschreitung pruefen
    if (*posX < 0)
        *posX = 7;
    if (*posX > 7)
        *posX = 0;
    if (*posY < 0)
        *posY = 7;
    if (*posY > 7)
        *posY = 0;
    // neue Position setzen
    *(field + *posY * 8 + *posX) = 1;
}

/*void moveTurm(int *field, int *turmPosX, int *turmPosY, int *newPosX, int *newPosY)
{
    // alte Position loeschen
    *(field + *turmPosY * 8 + *turmPosX) = 0;
    // neue Position bestimmen
    *turmPosX = *newPosX;
    *turmPosY = *newPosY;
    // Grenzueberschreitung pruefen
    if (*turmPosX < 0)
        *turmPosX = 7;
    if (*turmPosX > 7)
        *turmPosX = 0;
    if (*turmPosY < 0)
        *turmPosY = 7;
    if (*turmPosY > 7)
        *turmPosY = 0;
    // neue Position setzen
    *(field + *turmPosY * 8 + *turmPosX) = 2;
}

void moveBauer(int *field, int *bauerPosX, int *bauerPosY, int *newPosX, int *newPosY)
{
    // alte Position loeschen
    *(field + *bauerPosY * 8 + *bauerPosX) = 0;
    // neue Position bestimmen
    *bauerPosX = *newPosX;
    *bauerPosY = *newPosY;
    // Grenzueberschreitung pruefen
    if (*bauerPosX < 0)
        *bauerPosX = 7;
    if (*bauerPosX > 7)
        *bauerPosX = 0;
    if (*bauerPosY < 0)
        *bauerPosY = 7;
    if (*bauerPosY > 7)
        *bauerPosY = 0;
    // neue Position setzen
    *(field + *bauerPosY * 8 + *bauerPosX) = 3;
}

void moveLauefer(int *field, int *laueferPosX, int *laueferPosY, int *newPosX, int *newPosY)
{
    // alte Position loeschen
    *(field + *laueferPosY * 8 + *laueferPosX) = 0;
    // neue Position bestimmen
    *laueferPosX = *newPosX;
    *laueferPosY = *newPosY;
    // Grenzueberschreitung pruefen
    if (*laueferPosX < 0)
        *laueferPosX = 7;
    if (*laueferPosX > 7)
        *laueferPosX = 0;
    if (*laueferPosY < 0)
        *laueferPosY = 7;
    if (*laueferPosY > 7)
        *laueferPosY = 0;
    // neue Position setzen
    *(field + *laueferPosY * 8 + *laueferPosX) = 4;
}

void moveDame(int *field, int *damePosX, int *damePosY, int *newPosX, int *newPosY)
{
    // alte Position loeschen
    *(field + *damePosY * 8 + *damePosX) = 0;
    // neue Position bestimmen
    *damePosX = *newPosX;
    *damePosY = *newPosY;
    // Grenzueberschreitung pruefen
    if (*damePosX < 0)
        *damePosX = 7;
    if (*damePosX > 7)
        *damePosX = 0;
    if (*damePosY < 0)
        *damePosY = 7;
    if (*damePosY > 7)
        *damePosY = 0;
    // neue Position setzen
    *(field + *damePosY * 8 + *damePosX) = 5;
}

void moveKoenig(int *field, int *koenigPosX, int *koenigPosY, int *newPosX, int *newPosY)
{
    // alte Position loeschen
    *(field + *koenigPosY * 8 + *koenigPosX) = 0;
    // neue Position bestimmen
    *koenigPosX = *newPosX;
    *koenigPosY = *newPosY;
    // Grenzueberschreitung pruefen
    if (*koenigPosX < 0)
        *koenigPosX = 7;
    if (*koenigPosX > 7)
        *koenigPosX = 0;
    if (*koenigPosY < 0)
        *koenigPosY = 7;
    if (*koenigPosY > 7)
        *koenigPosY = 0;
    // neue Position setzen
    *(field + *koenigPosY * 8 + *koenigPosX) = 6;
}*/