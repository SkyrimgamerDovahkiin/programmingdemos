#pragma once
#include <GL/glew.h>

#include "defines.hpp"

class VertexBuffer
{
public:
    VertexBuffer(void *data, uint32_t numVertices);
    ~VertexBuffer();
    void Bind();
    void Unbind();

private:
    GLuint bufferId;
    GLuint vao;
};