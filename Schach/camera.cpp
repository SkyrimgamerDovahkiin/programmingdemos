#include "camera.hpp"

Camera::Camera(int width, int height)
{
    projection = glm::ortho(0.0f, (float)width, (float)height, 0.0f, -1.0f, 1.0f);
    view = glm::mat4(1.0f);
    position = glm::vec3(0.0f);
    // view = glm::translate(view, v * -1.0f);
    Update();
}

Camera::~Camera()
{
}

glm::mat4 Camera::GetViewProj()
{
    return viewProj;
}

glm::vec3 Camera::GetPosition()
{
    return position;
}

void Camera::Update()
{
    viewProj = projection * view;
}

// void Camera::Translate(glm::vec3 v)
// {
//     position += v;
//     view = glm::translate(view, v * -1.0f);
// }