#include <iostream>
#include <vector>
#include <string>

// GLEW/SDL
#define GLEW_STATIC
#include <GL/glew.h>
#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>

// STB
#define STB_IMAGE_IMPLEMENTATION
#include "libs/stb_image.h"

#include "defines.hpp"
#include "create_rect.hpp"
#include "camera.hpp"
#include "shader.hpp"
#include "vertex_buffer.hpp"
#include "index_buffer.hpp"

// OpenGL Error handling
void GLAPIENTRY OpenGLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
{
    std::cout << "[OpenGL Error] " << message << std::endl;
}

// vertices and indices stuff
std::vector<Vertex> vertices;
uint32_t numVertices = 0;
std::vector<uint32_t> indices;
uint32_t numIndices = 0;
uint32_t indicesIndex = 0;
uint32_t maxVertices = 64000000; // 64 MB Vertex Buffer size, indices size gets determined from that

// the chess pieces
std::vector<ChessPiece> chessPieces;

// store the board to check if clicked on
std::vector<Rectangle> boardRects;

// the chess board model and chess board MVP matrix
glm::mat4 boardModel = glm::mat4(1.0f);
glm::mat4 boardModelViewProj = glm::mat4(1.0f);

// screen width and height
int screenWidth = 600, screenHeight = 600;

// the texture width and height
float texWidth = 768.0f, texHeight = 256.0f;

// texture id, currently for testing
GLuint texId = 0;

Camera camera(screenWidth, screenHeight);

void LoadTexture()
{
    int textureWidth = 0;
    int textureHeight = 0;
    int bitsPerPixel = 0;

    // stbi_set_flip_vertically_on_load(true);
    auto textureBuffer = stbi_load("graphics/ChessPieces.png", &textureWidth, &textureHeight, &bitsPerPixel, 4);

    glGenTextures(1, &texId);
    glBindTexture(GL_TEXTURE_2D, texId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, textureWidth, textureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureBuffer);
    glBindTexture(GL_TEXTURE_2D, 0);

    if (textureBuffer)
    {
        stbi_image_free(textureBuffer);
    }
}

bool colorCode = true;

void CreateChessPieces()
{
    ChessPiece chessPiece;
    chessPiece.chessPiece = ChessPieces::WHITE_PAWN;
    chessPiece.rect.position.y = 6 * 75.0f;
    chessPiece.rect.texCoord.x = 2 * 256.0f;
    chessPiece.modelViewProj = camera.GetViewProj() * chessPiece.model;

    chessPieces.push_back(chessPiece);

    CreateRect(chessPiece.rect, vertices, indices, numVertices, numIndices, indicesIndex, chessPiece.rect.texCoord.x, chessPiece.rect.texCoord.y, texWidth, texHeight, 256.0f, 256.0f);
}

int main()
{
    // create chess board
    for (int x = 0; x < 8; x++)
    {
        Rectangle rect = {glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(75.0f), glm::vec2(0.0f), glm::vec3(1.0f)};
        rect.position.y = 0.0f + x * 75.0f;
        for (int y = 0; y < 8; y++)
        {
            rect.position.x = 0.0f + y * 75.0f;
            colorCode = !colorCode;
            if (colorCode)
            {
                rect.texCoord.x = 256.0f;
            }
            else if (!colorCode)
            {
                rect.texCoord.x = 0.0f;
            }
            CreateRect(rect, vertices, indices, numVertices, numIndices, indicesIndex, rect.texCoord.x, rect.texCoord.y, texWidth, texHeight, 256.0f, 256.0f);
            boardRects.push_back(rect);
        }
        colorCode = !colorCode;
    }

    CreateChessPieces();

    boardModelViewProj = camera.GetViewProj() * boardModel;

    // sdl init
    SDL_Window *window;
    {
        SDL_Init(SDL_INIT_EVERYTHING);

        SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    }

// Debug
#ifdef _DEBUG
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
#endif

    uint32_t flags = SDL_WINDOW_OPENGL;
    window = SDL_CreateWindow("Chess", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, flags);
    SDL_GLContext glContext = SDL_GL_CreateContext(window);

    SDL_GL_SetSwapInterval(1); // enable vsync

    // glew initialization
    GLenum err = glewInit();
    if (err != GLEW_OK)
    {
        std::cout << "Error: " << glewGetErrorString(err) << std::endl;
        std::cin.get();
        return -1;
    }

    std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;

#ifdef _DEBUG
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(OpenGLDebugCallback, 0);
#endif

    // create Vertex buffer
    VertexBuffer vertexBuffer(vertices.data(), maxVertices);

    // create Index buffer (size = maxVertices / 6)
    IndexBuffer indexBuffer(indices.data(), maxVertices / 6);

    glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices * sizeof(Vertex), vertices.data());
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, numIndices * sizeof(indices[0]), indices.data());

    // create texture shader
    Shader textureShader("shaders/texture_vert.glsl", "shaders/texture_frag.glsl");

    // texture shader uniforms
    int colorUniformLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_color");
    int textureUniformLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_texture");
    int textureViewProjMatrixUniformLocation = glGetUniformLocation(textureShader.GetShaderId(), "u_modelViewProj");

    // delta time stuff
    uint64_t perfCounterFrequency = SDL_GetPerformanceFrequency();
    uint64_t lastCounter = SDL_GetPerformanceCounter();
    float delta = 0.0f;

    // time
    float time = 0.0f;

    bool close = false;

    // Load Texture(s)
    LoadTexture();

    // blending for highlighting
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // Main Loop
    while (!close)
    {
        // input
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            // enable when including ImGui
            // ImGui_ImplSDL2_ProcessEvent(&event); // Forward your event to backend

            if (event.type == SDL_QUIT)
            {
                close = true;
            }
            else if (event.type == SDL_KEYDOWN)
            {
            }
            else if (event.type == SDL_KEYUP)
            {
            }
            else if (event.type == SDL_MOUSEBUTTONDOWN)
            {
                if (event.button.button == SDL_BUTTON_LEFT)
                {
                    for (auto &chessPiece : chessPieces)
                    {
                        if (chessPiece.IsClicked(event.motion.x, event.motion.y))
                        {
                            std::cout << "Clicked!\n";
                        }
                    }
                    
                    // ClickedOnObject(event.motion.x, event.motion.y);
                }
            }
        }

        time += delta;

        // ImGui frame start (when enabled)

        // set clear color
        glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // render/do other opengl stuff
        vertexBuffer.Bind();
        indexBuffer.Bind();
        textureShader.Bind();

        // draw stuff
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texId);

        glUniform4f(colorUniformLocation, 1.0f, 1.0f, 1.0f, 1.0f);
        glUniform1i(textureUniformLocation, 0);

        // draw normal board
        glUniformMatrix4fv(textureViewProjMatrixUniformLocation, 1, GL_FALSE, &boardModelViewProj[0][0]);
        glDrawElements(GL_TRIANGLES, 384, GL_UNSIGNED_INT, 0);

        // // draw test piece
        glUniformMatrix4fv(textureViewProjMatrixUniformLocation, 1, GL_FALSE, &chessPieces.at(0).modelViewProj[0][0]);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void *)(384 * sizeof(GLuint)));

        textureShader.Unbind();
        indexBuffer.Unbind();
        vertexBuffer.Unbind();

        // imgui rendering (when enabled)
        // ImGui::Render();
        // ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        // swap window
        SDL_GL_SwapWindow(window);

        // FPS
        uint64_t endCounter = SDL_GetPerformanceCounter();
        uint64_t counterElapsed = endCounter - lastCounter;
        delta = ((float)counterElapsed) / (float)perfCounterFrequency;
        uint32_t FPS = (uint32_t)((float)perfCounterFrequency / (float)counterElapsed);
        lastCounter = endCounter;
    }

    glDeleteTextures(1, &texId);

    // imgui shutdown (when enabled)
    // ImGui_ImplOpenGL3_Shutdown();
    // ImGui_ImplSDL2_Shutdown();
    // ImGui::DestroyContext();

    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}