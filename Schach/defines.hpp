#pragma once

// glm
#include <glm/glm.hpp>

struct Vertex
{
    glm::vec3 position;
    glm::vec2 uv;
    glm::vec4 color;
};

struct Rectangle
{
    glm::vec3 position;  // positions
    glm::vec2 dimension; // dimensions
    glm::vec2 texCoord;  // texture coordinates
    glm::vec3 color;     // color
};

enum ChessColor
{
    COL_BLACK,
    COL_WHITE,
};

enum ChessPieces
{
    NIL, // default
    BLACK_KING,
    BLACK_ROOK,
    BLACK_BISHOP,
    BLACK_QUEEN,
    BLACK_KNIGHT,
    BLACK_PAWN,
    WHITE_KING,
    WHITE_ROOK,
    WHITE_BISHOP,
    WHITE_QUEEN,
    WHITE_KNIGHT,
    WHITE_PAWN,
};

// a chess piece
struct ChessPiece
{
    // // positions in an array
    // int posX = 0;
    // int posY = 0;

    // int color = COL_BLACK; // 0 = black, 1 = white
    ChessPieces chessPiece = ChessPieces::NIL;

    Rectangle rect = {glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(75.0f), glm::vec2(0.0f), glm::vec3(1.0f)};

    // the chess piece model and chess piece MVP matrix
    glm::mat4 model = glm::mat4(1.0f);
    glm::mat4 modelViewProj = glm::mat4(1.0f);

    bool IsClicked(float mouseX, float mouseY)
    {
        return ((mouseX >= rect.position.x) && (mouseX <= rect.position.x + rect.dimension.x)) &&
               ((mouseY >= rect.position.y) && (mouseY <= rect.position.y + rect.dimension.y));
    }

    // should show the positions the piece can move to
    void ShowAvailablePositions()
    {

    }
};