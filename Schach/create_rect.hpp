#pragma once

#include "defines.hpp"

void CreateRect(Rectangle rect, std::vector<Vertex> &vertices, std::vector<uint32_t> &indices, uint32_t &numVertices, uint32_t &numIndices, uint32_t &index, float xOffset, float yOffset, float texWidth, float texHeight, float spriteWidth, float spriteHeight)
{
    // x and y is top left corner
    vertices.push_back(Vertex{glm::vec3(rect.position.x, rect.position.y + rect.dimension.y, rect.position.z),
                              glm::vec2(xOffset / texWidth, (yOffset + spriteHeight) / texHeight),
                              glm::vec4(rect.color, 1.0f)}); // bottomLeft
    indices.push_back(index);
    index++;
    vertices.push_back(Vertex{glm::vec3(rect.position.x, rect.position.y, rect.position.z),
                              glm::vec2(xOffset / texWidth, yOffset / texHeight),
                              glm::vec4(rect.color, 1.0f)}); // topLeft
    indices.push_back(index);
    index++;
    vertices.push_back(Vertex{glm::vec3(rect.position.x + rect.dimension.x, rect.position.y + rect.dimension.y, rect.position.z),
                              glm::vec2((xOffset + spriteWidth) / texWidth, (yOffset + spriteHeight) / texHeight),
                              glm::vec4(rect.color, 1.0f)}); // bottomRight
    indices.push_back(index);
    vertices.push_back(Vertex{glm::vec3(rect.position.x + rect.dimension.x, rect.position.y, rect.position.z),
                              glm::vec2((xOffset + spriteWidth) / texWidth, yOffset / texHeight),
                              glm::vec4(rect.color, 1.0f)}); // topRight
    index--;
    indices.push_back(index);
    index++;
    indices.push_back(index);
    index++;
    indices.push_back(index);
    index++;

    numVertices = vertices.size();
    numIndices = indices.size();
}