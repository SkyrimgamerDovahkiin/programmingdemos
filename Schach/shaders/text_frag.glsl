#version 330 core

layout(location = 0) out vec4 f_color;

in vec4 v_color;
in vec2 v_tex_coord;
uniform sampler2D text;

uniform vec3 text_color;

void main()
{    
    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, v_tex_coord).r);
    f_color = sampled * vec4(text_color, 1.0);
}