#pragma once
#include <GL/glew.h>

#include "defines.hpp"

class IndexBuffer
{
public:
    IndexBuffer(void *data, uint32_t size);
    ~IndexBuffer();
    void Bind();
    void Unbind();

private:
    GLuint bufferId;
};