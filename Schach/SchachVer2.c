#include <stdio.h>
#include <stdbool.h>
#include <string.h>

// TODO: auswahl von figur fixen
// TODO: alle figuren überarbeiten,
// aber vorallem Dame, und König überarbeiten;
// schachmatt implementieren

// TODO: schlagen bei den einzelnen Figuren implementieren;
// TODO: beim schlagen: evtl. figur zum schlagen anzeigen;
// TODO: CheckBorders überarbeiten;
// TODO: eventuell checken welcher player dran ist
// (so dass man z.b. nicht sich selbst schlagen kann);
void printField(int *field);
void checkField(int field[8][8]);
void moveTurmP1(int *field, int *posX, int *posY, int *newPosX, int *newPosY);
void moveTurmP2(int *field, int *posX, int *posY, int *newPosX, int *newPosY);
void moveSpringerP1(int *field, int *posX, int *posY, int *newPosX, int *newPosY);
void moveSpringerP2(int *field, int *posX, int *posY, int *newPosX, int *newPosY);
void moveBauerP1(int *field, int *posXB1, int *posYB1, int *newPosX, int *newPosY);
void moveBauerP2(int *field, int *posXB2, int *posYB2, int *newPosX, int *newPosY);
void moveLaueferP1(int *field, int *posX, int *posY, int *newPosX, int *newPosY);
void moveLaueferP2(int *field, int *posX, int *posY, int *newPosX, int *newPosY);
void moveDameP1(int *field, int *posX, int *posY, int *newPosX, int *newPosY);
void moveDameP2(int *field, int *posX, int *posY, int *newPosX, int *newPosY);
void moveKoenigP1(int *field, int *posX, int *posY, int *newPosX, int *newPosY);
void moveKoenigP2(int *field, int *posX, int *posY, int *newPosX, int *newPosY);
void checkCheckmate(int *posX, int *posY);
int check(int x, int y);
int checkBorders(int x, int y);

bool bauerFirstMoveP1 = true;
bool bauerFirstMoveP2 = true;

int pwstatus[8] = {0, 0, 0, 0, 0, 0, 0, 0};
int pbstatus[8] = {0, 0, 0, 0, 0, 0, 0, 0};

/*int field[8][8] = {2, 1, 4, 5, 6, 4, 1, 2,
                   3, 3, 3, 3, 3, 3, 3, 3,
                   0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0,
                   3, 3, 3, 3, 3, 3, 3, 3,
                   2, 1, 4, 5, 6, 4, 1, 2};*/

/* int field[8][8] = {0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 3, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0, 0, 3, 5,
                   0, 0, 0, 0, 0, 3, 0, 0,
                   0, 0, 0, 3, 3, 0, 0, 0,
                   0, 0, 0, 5, 6, 4, 0, 0};*/

// int field[8][8] = {0, 0, 0, 0, 0, 0, 0, 0,
//                    3, 3, 3, 3, 3, 3, 3, 3,
//                    0, 0, 0, 0, 0, 0, 0, 0,
//                    0, 0, 0, 0, 0, 0, 0, 0,
//                    0, 0, 0, 0, 0, 0, 0, 0,
//                    0, 0, 0, 0, 0, 0, 0, 0,
//                    3, 3, 3, 3, 3, 3, 3, 3,
//                    0, 0, 0, 0, 0, 0, 0, 0};

//Typ der Figur
enum Type
{
    NONE = 0,
    SPRINGER,
    TURM,
    BAUER,
    LAEUFER,
    DAME,
    KOENIG,
};

//Farbe der Figur
enum Color
{
    WHITE = 1,
    BLACK,
};

struct Piece
{
    enum Type type;
    enum Color color;
};

struct Piece field[8][8];

int main()
{
    bool zug = false;
    int spielfigur,
        r1,
        c1,
        r2,
        c2,
        posYB1,
        posXB1,
        // posYB2,
        // posXB2,
        posYD1,
        posXD1,
        posYD2,
        posXD2,
        posX,
        posY,
        newPosX,
        newPosY;
    char temp;

    // setzen der Spielfiguren
    printf("\nBeenden mit 0\n");
    do
    {
        printField(&field[0][0]);
        // checkCheckmate(0, 0);

        // später wieder auskommentieren, wenn könig existent
        // checkField(field);
        
        if (!zug)
        {
            printf("\nSpieler 1 ");
        }
        else if (zug)
        {
            printf("\nSpieler 2 ");
        }
        printf("\nWelche Figur [1 = springer, 2 = turm, 3 = bauer, 4 = läufer, 5 = dame, 6 = koenig]?: ");
        scanf("%d%c", &spielfigur, &temp);

        switch (spielfigur)
        {
        /*case SPRINGER:
            if (!zug)
            {
                printf("\nWelches Feld Y?: ");
                scanf("%d%c", &posY, &temp);
                printf("\nWelches Feld X?: ");
                scanf("%d%c", &posX, &temp);
            }
            else if (zug)
            {
                printf("\nWelches Feld Y?: ");
                scanf("%d%c", &posY, &temp);
                printf("\nWelches Feld X?: ");
                scanf("%d%c", &posX, &temp);
            }
            if (field[--posY][--posX] == 1 && !zug)
            {
                moveSpringerP1(&field[0][0], &posX, &posY, &newPosX, &newPosY);
                zug = true;
            }
            else if (field[--posY][--posX] == 1 && zug)
            {
                moveSpringerP2(&field[0][0], &posX, &posY, &newPosX, &newPosY);
                zug = false;
            }
            else
            {
                printf("\nZug ungültig! ");
            }
            break;
        case TURM:
            if (!zug)
            {
                printf("\nWelches Feld Y?: ");
                scanf("%d%c", &posY, &temp);
                printf("\nWelches Feld X?: ");
                scanf("%d%c", &posX, &temp);
            }
            else if (zug)
            {
                printf("\nWelches Feld Y?: ");
                scanf("%d%c", &posY, &temp);
                printf("\nWelches Feld X?: ");
                scanf("%d%c", &posX, &temp);
            }
            if (field[--posY][--posX] == 2 && !zug)
            {
                moveTurmP1(&field[0][0], &posX, &posY, &newPosX, &newPosY);
                zug = true;
            }
            else if (field[--posY][--posX] == 2 && zug)
            {
                moveTurmP2(&field[0][0], &posX, &posY, &newPosX, &newPosY);
                zug = false;
            }
            else
            {
                printf("\n%d%d", posY, posX);
                printf("\nZug ungültig from Turm! ");
            }
            break;*/

        case BAUER:
            printf("\nWelches Feld Y?: ");
            scanf("%d%c", &posY, &temp);
            printf("\nWelches Feld X?: ");
            scanf("%d%c", &posX, &temp);

            if (field[posY][posX].type == 3 && !zug)
            {
                moveBauerP1(&field[0][0], &posX, &posY, &newPosX, &newPosY);
                zug = true;
            }
            else if (field[posY][posX].type == 3 && zug)
            {
                moveBauerP2(&field[0][0], &posX, &posY, &newPosX, &newPosY);
                zug = false;
            }
            else
            {
                printf("\nZug ungültig! ");
            }
            break;

        /*case LAEUFER:
            if (!zug)
            {
                printf("\nWelches Feld Y?: ");
                scanf("%d%c", &posY, &temp);
                printf("\nWelches Feld X?: ");
                scanf("%d%c", &posX, &temp);
            }
            else if (zug)
            {
                printf("\nWelches Feld Y?: ");
                scanf("%d%c", &posY, &temp);
                printf("\nWelches Feld X?: ");
                scanf("%d%c", &posX, &temp);
            }
            if (field[--posY][--posX] == 4 && !zug)
            {
                moveLaueferP1(&field[0][0], &posX, &posY, &newPosX, &newPosY);
                zug = true;
            }
            else if (field[--posY][--posX] == 4 && zug)
            {
                moveLaueferP2(&field[0][0], &posX, &posY, &newPosX, &newPosY);
                zug = false;
            }
            else
            {
                printf("\nZug ungültig! ");
            }
            break;
        case DAME:
            if (!zug)
            {
                printf("\nWelches Feld Y?: ");
                scanf("%d%c", &posY, &temp);
                printf("\nWelches Feld X?: ");
                scanf("%d%c", &posX, &temp);
                (posY) -= 1;
                (posX) -= 1;
            }
            else if (zug)
            {
                printf("\nWelches Feld Y?: ");
                scanf("%d%c", &posY, &temp);
                printf("\nWelches Feld X?: ");
                scanf("%d%c", &posX, &temp);
                // geht scheinbar, mehr test nötig
                (posY) -= 1;
                (posX) -= 1;
            }
            if (field[posY][posX] == 5 && !zug)
            {
                moveDameP1(&field[0][0], &posX, &posY, &newPosX, &newPosY);
                zug = true;
            }
            else if (field[posY][posX] == 5 && zug)
            {
                moveDameP2(&field[0][0], &posX, &posY, &newPosX, &newPosY);
                zug = false;
            }
            else
            {
                // pos zum eingeben == 2, 5
                // eigentliche pos == 1, 4?
                // wenn minus minus weg, dann eigentliche pos stimmt
                // z.b. 0, 3;
                printf("\nZug ungültig! ");
            }
            break;
        case KOENIG:
            if (!zug)
            {
                printf("\nWelches Feld Y?: ");
                scanf("%d%c", &posY, &temp);
                printf("\nWelches Feld X?: ");
                scanf("%d%c", &posX, &temp);
            }
            else if (zug)
            {
                printf("\nWelches Feld Y?: ");
                scanf("%d%c", &posY, &temp);
                printf("\nWelches Feld X?: ");
                scanf("%d%c", &posX, &temp);
            }
            if (field[--posY][--posX] == 6 && !zug)
            {
                moveKoenigP1(&field[0][0], &posX, &posY, &newPosX, &newPosY);
                zug = true;
            }
            else if (field[--posY][--posX] == 6 && zug)
            {
                moveKoenigP1(&field[0][0], &posX, &posY, &newPosX, &newPosY);
                zug = false;
            }
            else
            {
                printf("\nZug ungültig! ");
            }
            break;*/
        }
    } while (spielfigur != 0);
    return 0;
}

// Ausgabe Spielfeld
void printField(int *field)
{
    printf("\n");
    int i, j;
    // Schleife fuer Zeilen, Y-Achse
    for (i = 0; i < 8; i++)
    {
        // Schleife fuer Spalten, X-Achse
        for (j = 0; j < 8; j++)
        {
            printf("%d ", *(field + i * 8 + j));
        }
        printf("\n");
    }
}

void checkField(int field[8][8])
{
    int i, j;
    int n, o;
    int origN, origO;
    bool isBlocked = false;
    // int move;
    // int width;
    // char temp;

    // Schleife fuer Zeilen, Y-Achse
    for (i = 0; i < 8; i++)
    {
        // Schleife fuer Spalten, X-Achse
        for (j = 0; j < 8; j++)
        {
            if (field[i][j] == 6)
            {
                // prüft, wo sich könig befindet
                n = j;
                o = i;
                printf("\n%d, %d, %s", j + 1, i + 1, "König!");
                break;
            }
        }
    }

    origO = o;
    origN = n;

    while (check(n + 1, o - 1) == 0 && n < 7 && o > 0)
    {
        n++;
        o--;
    }
    if (check(n + 1, o - 1) != 0)
    {
        printf("\n%d, %d, %s", n + 1, o - 1, "(Schach!)");
        isBlocked = true;
    }

    if (isBlocked == true)
    {
        n = origN;
        o = origO;
        n + 1;
        o - 1;
        while (check(n + 1, o - 1) == 0 && n < 7 && o > 0)
        {
            n++;
            o--;
        }
        if (check(n + 1, o - 1) != 0)
        {
            printf("\n%d, %d, %s", n + 1, o - 1, "(Schach Matt!)");
        }
    }

    n = origN;
    o = origO;
    isBlocked = false;

    // while (check(n - 1, o + 1) == 0 && n > 0 && o < 7)
    // {
    //     printf("\n%d, %d, %s", n - 1, o + 1, "(2, diagonal links nach unten)");
    //     n--;
    //     o++;
    // }
    // while (check(n - 1, o - 1) == 0 && n > 0 && o > 0)
    // {
    //     printf("\n%d, %d, %s", n - 1, o - 1, "(3, diagonal links nach oben)");
    //     n--;
    //     o--;
    // }
    // n = (int)*posX;
    // o = (int)*posY;
    // while (check(n + 1, o + 1) == 0 && n < 7 && o < 7)
    // {
    //     printf("\n%d, %d, %s", n + 1, o + 1, "(4, diagonal rechts nach unten)");
    //     n++;
    //     o++;
    // }
    // n = (int)*posY;
    // while (check(*posX, n - 1) == 0 && n > 0)
    // {
    //     printf("\n%d, %d, %s", *posX, n - 1, "(5, hoch)");
    //     n--;
    // }
    // n = (int)*posY;
    // while (check(*posX, n + 1) == 0 && n < 7)
    // {
    //     printf("\n%d, %d, %s", *posX, n + 1, "(6, runter)");
    //     n++;
    // }
    // n = (int)*posX;
    // while (check(n - 1, *posY) == 0 && n > 0)
    // {
    //     printf("\n%d, %d, %s", n - 1, *posY, "(7, links)");
    //     n--;
    // }
    // n = (int)*posX;
    // while (check(n + 1, *posY) == 0 && n < 7)
    // {
    //     printf("\n%d, %d, %s", n + 1, *posY, "(8, rechts)");
    //     n++;
    // }

    // *(field + *posY * 8 + *posX) = 0;
}

// check if field is occupied
int check(int x, int y)
{
    switch (field[y][x].type)
    {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
        return 1;
    default:
        return 0;
    }
}

// checks if piece is inside field
//  not functional yet;
int checkBorders(int x, int y)
{
    if (x < 7 && y < 7)
        return 1;
    else if (x > 0 && y > 0)
        return 2;
    else if (x > 0 && y < 7)
        return 3;
    else if (x < 7 && y > 0)
        return 4;
    else if (x <= 7 && y <= 7)
        return 5;
    else if (x >= 0 && y >= 0)
        return 6;
    else if (x >= 0 && y <= 7)
        return 7;
    else if (x <= 7 && y >= 0)
        return 8;
    else
    {
        return 0;
    }
}

// Spielfiguren bewegen
// Springer 1 u. 2 funktioniert (halbwegs)
void moveSpringerP1(int *field, int *posX, int *posY, int *newPosX, int *newPosY)
{
    int move;
    // alte Position loeschen
    *(field + *posY * 8 + *posX) = 0;
    // char *firstMove = "";
    //  neue Position bestimmen
    // TODO: chars (oder strings) machen für die einzelnen moves
    // TODO: prüfen ob Feld besetzt. wenn besetzt, teil auf der position löschen
    printf("Available are: ");
    // Funktionsweise: solange feld im array, springer kann sich hinbewegen
    // wenn feld wo hinbewegt wird voll, dann figur wird gelöscht u.
    // springer bewegt sich hin;
    if (*posX + 2 <= 7 && *posY + 1 <= 7)
    {
        printf("\n%d, %d, %s", *posX + 2, *posY + 1, "(1)");
        // strcpy(firstMove, "1");
    }
    if (*posX + 2 <= 7 && *posY - 1 >= 0)
        printf("\n%d, %d, %s", *posX + 2, *posY - 1, "(2)");
    if (*posX - 2 >= 0 && *posY + 1 <= 7)
        printf("\n%d, %d, %s", *posX - 2, *posY + 1, "(3)");
    if (*posX - 2 >= 0 && *posY - 1 >= 0)
        printf("\n%d, %d, %s", *posX - 2, *posY - 1, "(4)");

    if (*posX + 1 <= 7 && *posY + 2 <= 7)
        printf("\n%d, %d, %s", *posX + 1, *posY + 2, "(5)");
    if (*posX - 1 >= 0 && *posY + 2 <= 7)
        printf("\n%d, %d, %s", *posX - 1, *posY + 2, "(6)");
    if (*posX + 1 <= 7 && *posY - 2 >= 0)
        printf("\n%d, %d, %s", *posX + 1, *posY - 2, "(7)");
    if (*posX - 1 >= 0 && *posY - 2 >= 0)
        printf("\n%d, %d, %s", *posX - 1, *posY - 2, "(8)");

    // richtig hinbewegen
    // TODO: Angeben, welche Zahl beim moven für was zuständig ist;
    printf("\nWohin (1 - 8)? ");
    scanf("%d", &move);
    switch (move)
    {
    case 1:
        *newPosX = ((*posX) + 2);
        *newPosY = ((*posY) + 1);
        if (*newPosX <= 7 && *newPosY <= 7)
        {
            (*posX) += 2;
            (*posY) += 1;
        }
        break;
    case 2:
        *newPosX = ((*posX) + 2);
        *newPosY = ((*posY) - 1);
        if (*newPosX <= 7 && *newPosY >= 0)
        {
            (*posX) += 2;
            (*posY) -= 1;
        }
        break;
    case 3:
        *newPosX = ((*posX) - 2);
        *newPosY = ((*posY) - 1);
        if (*newPosX >= 0 && *newPosY >= 0)
        {
            (*posX) -= 2;
            (*posY) -= 1;
        }
        break;
    case 4:
        *newPosX = ((*posX) - 2);
        *newPosY = ((*posY) + 1);
        if (*newPosX >= 0 && *newPosY <= 7)
        {
            (*posX) -= 2;
            (*posY) += 1;
        }
        break;
    case 5:
        *newPosX = ((*posX) + 1);
        *newPosY = ((*posY) + 2);
        if (*newPosX <= 7 && *newPosY <= 7)
        {
            (*posX) += 1;
            (*posY) += 2;
        }
        break;
    case 6:
        *newPosX = ((*posX) - 1);
        *newPosY = ((*posY) + 2);
        if (*newPosX >= 0 && *newPosY <= 7)
        {
            (*posX) -= 1;
            (*posY) += 2;
        }
        break;
    case 7:
        *newPosX = ((*posX) - 1);
        *newPosY = ((*posY) + 2);
        if (*newPosX <= 7 && *newPosY >= 0)
        {
            (*posX) += 1;
            (*posY) -= 2;
        }
        break;
    case 8:
        *newPosX = ((*posX) - 1);
        *newPosY = ((*posY) - 2);
        if (*newPosX >= 0 && *newPosY >= 0)
        {
            (*posX) -= 1;
            (*posY) -= 2;
        }
        break;
    default:
        printf("\nZug ungültig!: ");
        break;
    }
    // Grenzueberschreitung pruefen
    if (*posX < 0)
        *posX = 0;
    if (*posX > 7)
        *posX = 7;
    if (*posY < 0)
        *posY = 0;
    if (*posY > 7)
        *posY = 7;
    // neue Position setzen
    *(field + *posY * 8 + *posX) = 1;
}

void moveSpringerP2(int *field, int *posX, int *posY, int *newPosX, int *newPosY)
{
    int move;
    // alte Position loeschen
    *(field + *posY * 8 + *posX) = 0;
    // char *firstMove = "";
    //  neue Position bestimmen
    // TODO: chars (oder strings) machen für die einzelnen moves
    // TODO: prüfen ob Feld besetzt. wenn besetzt, teil auf der position löschen
    printf("Available are: ");
    // Funktionsweise: solange feld im array, springer kann sich hinbewegen
    // wenn feld wo hinbewegt wird voll, dann figur wird gelöscht u.
    // springer bewegt sich hin;
    if (*posX - 2 >= 0 && *posY - 1 >= 0)
    {
        printf("\n%d, %d, %s", *posX - 2, *posY - 1, "(1)");
        // strcpy(firstMove, "1");
    }
    if (*posX - 2 <= 0 && *posY + 1 <= 7)
        printf("\n%d, %d, %s", *posX - 2, *posY + 1, "(2)");
    if (*posX + 2 <= 7 && *posY - 1 >= 0)
        printf("\n%d, %d, %s", *posX + 2, *posY - 1, "(3)");
    if (*posX + 2 <= 7 && *posY + 1 <= 7)
        printf("\n%d, %d, %s", *posX + 2, *posY + 1, "(4)");

    if (*posX - 1 >= 0 && *posY - 2 >= 0)
        printf("\n%d, %d, %s", *posX - 1, *posY - 2, "(5)");
    if (*posX + 1 <= 7 && *posY - 2 >= 0)
        printf("\n%d, %d, %s", *posX + 1, *posY - 2, "(6)");
    if (*posX - 1 >= 0 && *posY + 2 <= 7)
        printf("\n%d, %d, %s", *posX - 1, *posY + 2, "(7)");
    if (*posX + 1 <= 7 && *posY + 2 <= 7)
        printf("\n%d, %d, %s", *posX + 1, *posY + 2, "(8)");

    // richtig hinbewegen
    // TODO: Angeben, welche Zahl beim moven für was zuständig ist;
    printf("\nWohin (1 - 8)? ");
    scanf("%d", &move);
    switch (move)
    {
    case 1:
        *newPosX = ((*posX) - 2);
        *newPosY = ((*posY) - 1);
        if (*newPosX >= 0 && *newPosY >= 0)
        {
            (*posX) -= 2;
            (*posY) -= 1;
        }
        break;
    case 2:
        *newPosX = ((*posX) - 2);
        *newPosY = ((*posY) + 1);
        if (*newPosX >= 0 && *newPosY <= 7)
        {
            (*posX) -= 2;
            (*posY) += 1;
        }
        break;
    case 3:
        *newPosX = ((*posX) + 2);
        *newPosY = ((*posY) + 1);
        if (*newPosX <= 7 && *newPosY <= 7)
        {
            (*posX) += 2;
            (*posY) += 1;
        }
        break;
    case 4:
        *newPosX = ((*posX) + 2);
        *newPosY = ((*posY) - 1);
        if (*newPosX <= 7 && *newPosY >= 0)
        {
            (*posX) += 2;
            (*posY) -= 1;
        }
        break;
    case 5:
        *newPosX = ((*posX) - 1);
        *newPosY = ((*posY) - 2);
        if (*newPosX >= 0 && *newPosY >= 0)
        {
            (*posX) -= 1;
            (*posY) -= 2;
        }
        break;
    case 6:
        *newPosX = ((*posX) + 1);
        *newPosY = ((*posY) - 2);
        if (*newPosX <= 7 && *newPosY >= 0)
        {
            (*posX) += 1;
            (*posY) -= 2;
        }
        break;
    case 7:
        *newPosX = ((*posX) + 1);
        *newPosY = ((*posY) - 2);
        if (*newPosX >= 0 && *newPosY <= 7)
        {
            (*posX) -= 1;
            (*posY) += 2;
        }
        break;
    case 8:
        *newPosX = ((*posX) + 1);
        *newPosY = ((*posY) + 2);
        if (*newPosX <= 7 && *newPosY <= 7)
        {
            (*posX) += 1;
            (*posY) += 2;
        }
        break;
    default:
        printf("\nZug ungültig!: ");
        break;
    }
    // Grenzueberschreitung pruefen
    if (*posX < 0)
        *posX = 0;
    if (*posX > 7)
        *posX = 7;
    if (*posY < 0)
        *posY = 0;
    if (*posY > 7)
        *posY = 7;
    // neue Position setzen
    *(field + *posY * 8 + *posX) = 1;
}

// Turm 1 geht (vllt. noch anzeige möglicher moves überarbeiten)
void moveTurmP1(int *field, int *posX, int *posY, int *newPosX, int *newPosY)
{
    int row, width, n;
    char temp;
    // alte Position loeschen
    *(field + *posY * 8 + *posX) = 0;
    // neue mögliche Position anzeigen
    printf("Available are: \n");
    printf("Horizontally: \n");
    n = (int)*posY;

    while (check(*posX, n - 1) == 0 && (n) > 0)
    {
        printf("\n%d, %d, %s", *posX, n - 1, "(3) hoch");
        n--;
    }
    if (check(*posX, n - 1) == 1 && (n) > 0)
    {
        printf("\n%d, %d, %s", *posX, n - 1, "(3) hoch (Figur schlagen)");
    }

    n = (int)*posY;
    while (check(*posX, n + 1) == 0 && (n) < 7)
    {
        printf("\n%d, %d, %s", *posX, n + 1, "(4) runter");
        n++;
    }
    if (check(*posX, n + 1) == 1 && (n) < 7)
    {
        printf("\n%d, %d, %s", *posX, n + 1, "(4) runter (Figur schlagen)");
    }

    printf("\nVertically: \n");

    n = (int)*posX;
    while (check(n - 1, *posY) == 0 && (n) > 0)
    {
        printf("\n%d, %d, %s", n - 1, *posY, "(1) links");
        n--;
    }
    if (check(n - 1, *posY) == 1 && (n) > 0)
    {
        printf("\n%d, %d, %s", n - 1, *posY, "(1) links (Figur schlagen)");
    }

    n = (int)*posX;
    while (check(n + 1, *posY) == 0 && (n) < 7)
    {
        printf("\n%d, %d, %s", n + 1, *posY, "(2) rechts");
        n++;
    }
    if (check(n + 1, *posY) == 1 && (n) < 7)
    {
        printf("\n%d, %d, %s", n + 1, *posY, " (2) rechts (Figur schlagen)");
    }

    // Zur neuen Position hinbewegen
    printf("\nWelche Richtung (1 - 4)? ");
    scanf("%d", &row);
    printf("\nWie weit?");
    scanf("%d%c", &width, &temp);
    switch (row)
    {
    case 1:
        *newPosX = (*posX - width);
        while (*posX > *newPosX)
        {
            (*posX) -= 1;
            if (check(*posX + 1, *posY) == 1)
            {
                printf("\nZug ungültig! (Turm)");
                (*posX) += width;
                break;
            }
        }
        break;
    case 2:
        *newPosX = (*posX + width);
        while (*posX < *newPosX)
        {
            (*posX) += 1;
            if (check(*posX - 1, *posY) == 1)
            {
                printf("\nZug ungültig! (Turm)");
                (*posX) -= width;
                break;
            }
        }
        break;
    case 3:
        *newPosY = (*posY - width);
        // TODO: (vllt.) Figur auf besetztem Feld löschen

        // Schleife überprüft ob feld in erlaubter weite
        // if überprüft wenn feld besetzt, nur max bis dorthin moven
        //  und dann aufhören
        while (*posY > *newPosY)
        {
            (*posY) -= 1;
            if (check(*posX, *posY + 1) == 1)
            {
                printf("\nZug ungültig! (Turm)");
                (*posY) += width;
                break;
            }
        }
        break;
    case 4:
        *newPosY = (*posY + width);
        while (*posY < *newPosY)
        {
            (*posY) += 1;
            if (check(*posX, *posY - 1) == 1)
            {
                printf("\nZug ungültig! (Turm)");
                (*posY) -= width;
                break;
            }
        }
        break;
    default:
        printf("\nZug ungültig!");
        break;
    }
    // Grenzueberschreitung pruefen
    if (*posX < 0)
        *posX = 0;
    if (*posX > 7)
        *posX = 7;
    if (*posY < 0)
        *posY = 0;
    if (*posY > 7)
        *posY = 7;
    // neue Position setzen
    *(field + *posY * 8 + *posX) = 2;
}

// Turm 2 geht (vllt. noch anzeige möglicher moves überarbeiten)
void moveTurmP2(int *field, int *posX, int *posY, int *newPosX, int *newPosY)
{
    int row, width, n;
    char temp;
    // alte Position loeschen
    *(field + *posY * 8 + *posX) = 0;
    // neue mögliche Position anzeigen
    printf("Available are: \n");
    printf("Horizontally: \n");
    n = (int)*posY;
    while (check(*posX, n + 1) == 0 && (n) < 7)
    {
        printf("\n%d, %d, %s", *posX, n + 1, "(3) runter");
        n++;
    }
    if (check(*posX, n + 1) == 1 && (n) < 7)
    {
        printf("\n%d, %d, %s", *posX, n + 1, "(3) runter (Figur schlagen)");
    }

    n = (int)*posY;
    while (check(*posX, n - 1) == 0 && (n) > 0)
    {
        printf("\n%d, %d, %s", *posX, n - 1, "(4) hoch");
        n--;
    }
    if (check(*posX, n - 1) == 1 && (n) > 0)
    {
        printf("\n%d, %d, %s", *posX, n - 1, "(4) hoch (Figur schlagen)");
    }

    printf("\nVertically: \n");
    n = (int)*posX;
    while (check(n + 1, *posY) == 0 && (n) < 7)
    {
        printf("\n%d, %d, %s", n + 1, *posY, "(1) rechts");
        n++;
    }
    if (check(n + 1, *posY) == 1 && (n) < 7)
    {
        printf("\n%d, %d, %s", n + 1, *posY, " (1) rechts (Figur schlagen)");
    }

    n = (int)*posX;
    while (check(n - 1, *posY) == 0 && (n) > 0)
    {
        printf("\n%d, %d, %s", n - 1, *posY, "(2) links");
        n--;
    }
    if (check(n - 1, *posY) == 1 && (n) > 0)
    {
        printf("\n%d, %d, %s", n - 1, *posY, "(2) links (Figur schlagen)");
    }

    // Zur neuen Position hinbewegen
    printf("\nWelche Richtung (1 - 4)? ");
    scanf("%d", &row);
    printf("\nWie weit?");
    scanf("%d%c", &width, &temp);
    switch (row)
    {
    case 1:
        *newPosX = (*posX + width);
        while (*posX < *newPosX)
        {
            (*posX) += 1;
            if (check(*posX - 1, *posY) == 1)
            {
                printf("\nZug ungültig! (Turm)");
                (*posX) -= width;
                break;
            }
        }
        break;
    case 2:
        *newPosX = (*posX - width);
        while (*posX > *newPosX)
        {
            (*posX) -= 1;
            if (check(*posX + 1, *posY) == 1)
            {
                printf("\nZug ungültig! (Turm)");
                (*posX) += width;
                break;
            }
        }
        break;
    case 3:
        *newPosY = (*posY + width);
        while (*posY < *newPosY)
        {
            (*posY) += 1;
            if (check(*posX, *posY - 1) == 1)
            {
                printf("\nZug ungültig! (Turm)");
                (*posY) -= width;
                break;
            }
        }
        break;
    case 4:
        *newPosY = (*posY - width);
        while (*posY > *newPosY)
        {
            (*posY) -= 1;
            if (check(*posX, *posY + 1) == 1)
            {
                printf("\nZug ungültig! (Turm)");
                (*posY) += width;
                break;
            }
        }
        break;
    default:
        printf("\nZug ungültig!");
        break;
    }
    // Grenzueberschreitung pruefen
    if (*posX < 0)
        *posX = 0;
    if (*posX > 7)
        *posX = 7;
    if (*posY < 0)
        *posY = 0;
    if (*posY > 7)
        *posY = 7;
    // neue Position setzen
    *(field + *posY * 8 + *posX) = 2;
}

// Bauer 1 & 2 funktioniert (halbwegs)
void moveBauerP1(int *field, int *posXB1, int *posYB1, int *newPosX, int *newPosY)
{
    // variables
    int noF;
    char temp;
    bool firstFieldEmpty = false;
    bool secondFieldEmpty = false;
    bool moveFirstChoice = false;
    bool moveSecondChoice = false;
    bool moveThirdChoice = false;

    // alte Position loeschen
    *(field + *posYB1 * 8 + *posXB1) = 0;

    // neue Position bestimmen
    pwstatus[*posXB1]++;

    // moves vorher anzeigen
    printf("Available are: \n");

    // first move
    if (pwstatus[*posXB1] == 1)
    {
        // wenn erstes oder 2 feld vor bauer leer, jeweiligen bool auf true setzen
        if ((check(((int)*posXB1), ((int)*posYB1) - 1) == 0) && (check(((int)*posXB1), ((int)*posYB1) - 2) == 0))
        {
            firstFieldEmpty = true;
            printf("\n1 Feld ");
        }
        if ((check(((int)*posXB1), ((int)*posYB1) - 2) == 0) && (check(((int)*posXB1), ((int)*posYB1) - 1) == 0))
        {
            secondFieldEmpty = true;
            printf("\n2 Felder ");
        }

        // jeweiligen zug ausführen, je nach freiem feld. wenn 2 felder frei, fragen wohin bewegt werden soll
        if (secondFieldEmpty == true)
        {
            printf("\nWelcher Zug?: ");
            scanf("%d%c", &noF, &temp);
        }

        if ((noF == 1 && firstFieldEmpty == true) || (firstFieldEmpty == true && secondFieldEmpty == false))
        {
            (*posYB1) -= 1;
        }
        if (noF == 2 && firstFieldEmpty == true && secondFieldEmpty == true)
        {
            (*posYB1) -= 2;
        }
        else
        {
            printf("\nZug ungültig! ");
        }
    }

    // second move
    else if (pwstatus[*posXB1] != 1)
    {
        if (check(((int)*posXB1), ((int)*posYB1) - 1) == 0)
        {
            printf("\nEin Feld vor (1)");
            printf("\n%d, %d ", (int)*posYB1 - 1, (int)*posXB1);
            moveFirstChoice = true;
        }

        if (check((((int)*posXB1) + 1), ((int)*posYB1) - 1) == 1)
        {
            printf("\nDiagonal rechts (2)");
            printf("\n%d, %d ", (int)*posYB1 - 1, (int)*posXB1 + 1);
            moveSecondChoice = true;
        }

        if (check((((int)*posXB1) - 1), ((int)*posYB1) - 1) == 1)
        {
            printf("\nDiagonal links (3)");
            printf("\n%d, %d ", (int)*posYB1 - 1, (int)*posXB1 - 1);
            moveThirdChoice = true;
        }

        printf("\nWelcher Zug?: ");
        scanf("%d%c", &noF, &temp);

        if ((noF == 1 && moveFirstChoice == true))
        {
            (*posYB1) -= 1;
        }
        if (noF == 2 && moveSecondChoice == true)
        {
            (*posYB1) -= 1;
            (*posXB1) += 1;
        }
        if (noF == 3 && moveThirdChoice == true)
        {
            (*posYB1) -= 1;
            (*posXB1) -= 1;
        }
        else
        {
            // else zählt für alle ifs!;
            printf("\nZug ungültig! ");
        }
    }

    // Grenzueberschreitung pruefen
    if (*posXB1 < 0)
        *posXB1 = 0;
    if (*posXB1 > 7)
        *posXB1 = 7;
    if (*posYB1 < 0)
        *posYB1 = 0;
    if (*posYB1 > 7)
        *posYB1 = 7;
    // neue Position setzen
    *(field + *posYB1 * 8 + *posXB1) = 3;
}

void moveBauerP2(int *field, int *posXB2, int *posYB2, int *newPosX, int *newPosY)
{
    // variables
    int noF;
    char temp;
    bool firstFieldEmpty = false;
    bool secondFieldEmpty = false;
    bool moveFirstChoice = false;
    bool moveSecondChoice = false;
    bool moveThirdChoice = false;

    // alte Position loeschen
    *(field + *posYB2 * 8 + *posXB2) = 0;

    // neue Position bestimmen
    pbstatus[*posXB2]++;

    // moves vorher anzeigen
    printf("Available are: \n");

    // first move
    if (pbstatus[*posXB2] == 1)
    {

        // wenn erstes oder 2 feld vor bauer leer, jeweiligen bool auf true setzen
        if ((check(((int)*posXB2), ((int)*posYB2) + 1) == 0))
        {
            firstFieldEmpty = true;
            printf("\n1 Feld ");
        }
        if ((check(((int)*posXB2), ((int)*posYB2) + 2) == 0))
        {
            secondFieldEmpty = true;
            printf("\n2 Felder ");
        }

        // jeweiligen zug ausführen, je nach freiem feld. wenn 2 felder frei, fragen wohin bewegt werden soll
        if (secondFieldEmpty == true)
        {
            printf("\nWelcher Zug?: ");
            scanf("%d%c", &noF, &temp);
        }

        if ((noF == 1 && firstFieldEmpty == true) || (firstFieldEmpty == true && secondFieldEmpty == false))
        {
            (*posYB2) += 1;
        }
        if (noF == 2 && firstFieldEmpty == true && secondFieldEmpty == true)
        {
            (*posYB2) += 2;
        }
        else
        {
            printf("\nZug ungültig! ");
        }
    }

    // second move
    else if (pbstatus[*posXB2] != 1)
    {
        if (check(((int)*posXB2), ((int)*posYB2) + 1) == 0)
        {
            printf("\nEin Feld vor (1)");
            printf("\n%d, %d ", (int)*posYB2 + 1, (int)*posXB2);
            moveFirstChoice = true;
        }

        if (check((((int)*posXB2) + 1), ((int)*posYB2) + 1) == 1)
        {
            printf("\nDiagonal rechts (2)");
            printf("\n%d, %d ", (int)*posYB2 + 1, (int)*posXB2 + 1);
            moveSecondChoice = true;
        }

        if (check((((int)*posXB2) - 1), ((int)*posYB2) + 1) == 1)
        {
            printf("\nDiagonal links (3)");
            printf("\n%d, %d ", (int)*posYB2 + 1, (int)*posXB2 - 1);
            moveThirdChoice = true;
        }

        printf("\nWelcher Zug?: ");
        scanf("%d%c", &noF, &temp);

        if ((noF == 1 && moveFirstChoice == true))
        {
            (*posYB2) += 1;
        }
        if (noF == 2 && moveSecondChoice == true)
        {
            (*posYB2) += 1;
            (*posXB2) += 1;
        }
        if (noF == 3 && moveThirdChoice == true)
        {
            (*posYB2) += 1;
            (*posXB2) -= 1;
        }
        else
        {
            printf("\nZug ungültig second move! ");
        }
    }

    // Grenzueberschreitung pruefen
    if (*posXB2 < 0)
        *posXB2 = 0;
    if (*posXB2 > 7)
        *posXB2 = 7;
    if (*posYB2 < 0)
        *posYB2 = 0;
    if (*posYB2 > 7)
        *posYB2 = 7;
    // neue Position setzen
    *(field + *posYB2 * 8 + *posXB2) = 3;
}

// Läufer 1 u. 2 funktioniert (halbwegs)
void moveLaueferP1(int *field, int *posX, int *posY, int *newPosX, int *newPosY)
{
    // Funktionsweise: kann sich diagonal in jede Richtung bewegen, außer
    // eine andere Figur ist im Weg;
    int n, o;
    int move;
    int width;
    char temp;
    // alte Position loeschen
    *(field + *posY * 8 + *posX) = 0;
    // neue mögliche Position anzeigen
    n = (int)*posX;
    o = (int)*posY;
    while (check(n + 1, o - 1) == 0 && n < 7 && o > 0)
    {
        printf("\n%d, %d, %s", n + 1, o - 1, "(1, diagonal rechts nach oben)");
        n++;
        o--;
    }
    n = (int)*posX;
    o = (int)*posY;
    while (check(n - 1, o + 1) == 0 && n > 0 && o < 7)
    {
        printf("\n%d, %d, %s", n - 1, o + 1, "(2, diagonal links nach unten)");
        n--;
        o++;
    }
    n = (int)*posX;
    o = (int)*posY;
    while (check(n - 1, o - 1) == 0 && n > 0 && o > 0)
    {
        printf("\n%d, %d, %s", n - 1, o - 1, "(3, diagonal links nach oben)");
        n--;
        o--;
    }
    n = (int)*posX;
    o = (int)*posY;
    while (check(n + 1, o + 1) == 0 && n < 7 && o < 7)
    {
        printf("\n%d, %d, %s", n + 1, o + 1, "(4, diagonal rechts nach unten)");
        n++;
        o++;
    }

    // moven
    printf("\nWohin bewegen (1 - 4)?");
    scanf("%d", &move);
    printf("\nWie weit?");
    scanf("%d", &width);
    switch (move)
    {
    case 1:
        *newPosX = ((*posX) + width);
        *newPosY = ((*posY) - width);
        if (check(*newPosX, *newPosY) == 0 && *newPosX <= 7 && *newPosY >= 0)
        {
            (*posX) += width;
            (*posY) -= width;
        }
        break;
    case 2:
        *newPosX = ((*posX) - width);
        *newPosY = ((*posY) + width);
        if (check(*newPosX, *newPosY) == 0 && *newPosX >= 0 && *newPosY <= 7)
        {
            (*posX) -= width;
            (*posY) += width;
        }
        break;
    case 3:
        *newPosX = ((*posX) - width);
        *newPosY = ((*posY) - width);
        if (check(*newPosX, *newPosY) == 0 && *newPosX >= 0 && *newPosY >= 0)
        {
            (*posX) -= width;
            (*posY) -= width;
        }
        break;
    case 4:
        *newPosX = ((*posX) + width);
        *newPosY = ((*posY) + width);
        if (check(*newPosX, *newPosY) == 0 && *newPosX <= 7 && *newPosY <= 7)
        {
            (*posX) += width;
            (*posY) += width;
        }
        break;
    default:
        printf("\n Zug ungültig!");
        break;
    }
    // Grenzueberschreitung pruefen
    if (*posX < 0)
        *posX = 0;
    if (*posX > 7)
        *posX = 7;
    if (*posY < 0)
        *posY = 0;
    if (*posY > 7)
        *posY = 7;
    // neue Position setzen
    *(field + *posY * 8 + *posX) = 4;
}

void moveLaueferP2(int *field, int *posX, int *posY, int *newPosX, int *newPosY)
{
    int n, o;
    int move;
    int width;
    char temp;
    // alte Position loeschen
    *(field + *posY * 8 + *posX) = 0;
    // neue mögliche Position anzeigen
    n = (int)*posX;
    o = (int)*posY;
    while (check(n - 1, o + 1) == 0 && n > 0 && o < 7)
    {
        printf("\n%d, %d, %s", n - 1, o + 1, "(1, diagonal rechts nach oben)");
        n--;
        o++;
    }
    n = (int)*posX;
    o = (int)*posY;
    while (check(n + 1, o - 1) == 0 && n < 7 && o > 0)
    {
        printf("\n%d, %d, %s", n + 1, o - 1, "(2, diagonal links nach unten)");
        n++;
        o--;
    }
    n = (int)*posX;
    o = (int)*posY;
    while (check(n + 1, o + 1) == 0 && n < 7 && o < 7)
    {
        printf("\n%d, %d, %s", n + 1, o + 1, "(3, diagonal links nach oben)");
        n++;
        o++;
    }
    n = (int)*posX;
    o = (int)*posY;
    while (check(n - 1, o - 1) == 0 && n > 0 && o > 0)
    {
        printf("\n%d, %d, %s", n - 1, o - 1, "(4, diagonal rechts nach unten)");
        n--;
        o--;
    }

    // moven
    printf("\nWohin bewegen (1 - 4)?");
    scanf("%d", &move);
    printf("\nWie weit?");
    scanf("%d", &width);
    switch (move)
    {
    case 1:
        *newPosX = ((*posX) - width);
        *newPosY = ((*posY) + width);
        if (check(*newPosX, *newPosY) == 0 && *newPosX >= 0 && *newPosY <= 7)
        {
            (*posX) -= width;
            (*posY) += width;
        }
        break;
    case 2:
        *newPosX = ((*posX) + width);
        *newPosY = ((*posY) - width);
        if (check(*newPosX, *newPosY) == 0 && *newPosX <= 7 && *newPosY >= 0)
        {
            (*posX) += width;
            (*posY) -= width;
        }
        break;
    case 3:
        *newPosX = ((*posX) + width);
        *newPosY = ((*posY) + width);
        if (check(*newPosX, *newPosY) == 0 && *newPosX <= 7 && *newPosY <= 7)
        {
            (*posX) += width;
            (*posY) += width;
        }
        break;
    case 4:
        *newPosX = ((*posX) - width);
        *newPosY = ((*posY) - width);
        if (check(*newPosX, *newPosY) == 0 && *newPosX >= 0 && *newPosY >= 0)
        {
            (*posX) -= width;
            (*posY) -= width;
        }
        break;
    default:
        printf("\n Zug ungültig!");
        break;
    }
    // Grenzueberschreitung pruefen
    if (*posX < 0)
        *posX = 0;
    if (*posX > 7)
        *posX = 7;
    if (*posY < 0)
        *posY = 0;
    if (*posY > 7)
        *posY = 7;
    // neue Position setzen
    *(field + *posY * 8 + *posX) = 4;
}

// Dame 1 funktioniert wahrscheinlich
void moveDameP1(int *field, int *posX, int *posY, int *newPosX, int *newPosY)
{
    // Funktionsweise: Läufer und Turm combined
    // kann nicht über andere figuren springen
    int n, o;
    int move;
    int width;
    char temp;
    // alte Position loeschen
    *(field + *posY * 8 + *posX) = 0;
    // neue mögliche Position anzeigen
    n = (int)*posX;
    o = (int)*posY;
    while (check(n + 1, o - 1) == 0 && n < 7 && o > 0)
    {
        printf("\n%d, %d, %s", n + 1, o - 1, "(1, diagonal rechts nach oben)");
        n++;
        o--;
    }
    n = (int)*posX;
    o = (int)*posY;
    while (check(n - 1, o + 1) == 0 && n > 0 && o < 7)
    {
        printf("\n%d, %d, %s", n - 1, o + 1, "(2, diagonal links nach unten)");
        n--;
        o++;
    }
    n = (int)*posX;
    o = (int)*posY;
    while (check(n - 1, o - 1) == 0 && n > 0 && o > 0)
    {
        printf("\n%d, %d, %s", n - 1, o - 1, "(3, diagonal links nach oben)");
        n--;
        o--;
    }
    n = (int)*posX;
    o = (int)*posY;
    while (check(n + 1, o + 1) == 0 && n < 7 && o < 7)
    {
        printf("\n%d, %d, %s", n + 1, o + 1, "(4, diagonal rechts nach unten)");
        n++;
        o++;
    }
    n = (int)*posY;
    while (check(*posX, n - 1) == 0 && n > 0)
    {
        printf("\n%d, %d, %s", *posX, n - 1, "(5, hoch)");
        n--;
    }
    n = (int)*posY;
    while (check(*posX, n + 1) == 0 && n < 7)
    {
        printf("\n%d, %d, %s", *posX, n + 1, "(6, runter)");
        n++;
    }
    n = (int)*posX;
    while (check(n - 1, *posY) == 0 && n > 0)
    {
        printf("\n%d, %d, %s", n - 1, *posY, "(7, links)");
        n--;
    }
    n = (int)*posX;
    while (check(n + 1, *posY) == 0 && n < 7)
    {
        printf("\n%d, %d, %s", n + 1, *posY, "(8, rechts)");
        n++;
    }
    // moven
    printf("\nWohin bewegen (1 - 8)?");
    scanf("%d", &move);
    printf("\nWie weit?");
    scanf("%d", &width);
    switch (move)
    {
    case 1:
        *newPosX = ((*posX) + width);
        *newPosY = ((*posY) - width);
        if (check(*newPosX, *newPosY) == 0 && *newPosX <= 7 && *newPosY >= 0)
        {
            (*posX) += width;
            (*posY) -= width;
        }
        break;
    case 2:
        *newPosX = ((*posX) - width);
        *newPosY = ((*posY) + width);
        if (check(*newPosX, *newPosY) == 0 && *newPosX >= 0 && *newPosY <= 7)
        {
            (*posX) -= width;
            (*posY) += width;
        }
        break;
    case 3:
        *newPosX = ((*posX) - width);
        *newPosY = ((*posY) - width);
        if (check(*newPosX, *newPosY) == 0 && *newPosX >= 0 && *newPosY >= 0)
        {
            (*posX) -= width;
            (*posY) -= width;
        }
        break;
    case 4:
        *newPosX = ((*posX) + width);
        *newPosY = ((*posY) + width);
        if (check(*newPosX, *newPosY) == 0 && *newPosX <= 7 && *newPosY <= 7)
        {
            (*posX) += width;
            (*posY) += width;
        }
        break;
    case 5:
        *newPosY = (*posY - width);
        if (check(*posX, *newPosY) == 0)
        {
            (*posY) -= width;
        }
        else
        {
            printf("\nZug ungültig!");
        }
        break;
    case 6:
        *newPosY = (*posY + width);
        if (check(*posX, *newPosY) == 0)
        {
            (*posY) += width;
        }
        else
        {
            printf("\nZug ungültig!");
        }
        break;
    case 7:
        *newPosX = (*posX - width);
        if (check(*newPosX, *posY) == 0)
        {
            (*posX) -= width;
        }
        else
        {
            printf("\nZug ungültig!");
        }
        break;
    case 8:
        *newPosX = (*posX + width);
        if (check(*newPosX, *posY) == 0)
        {
            (*posX) += width;
        }
        else
        {
            printf("\nZug ungültig!");
        }
        break;
    default:
        printf("\n Zug ungültig!");
        break;
    }
    // Grenzueberschreitung pruefen
    if (*posX < 0)
        *posX = 0;
    if (*posX > 7)
        *posX = 7;
    if (*posY < 0)
        *posY = 0;
    if (*posY > 7)
        *posY = 7;
    // neue Position setzen
    *(field + *posY * 8 + *posX) = 5;
}

// Dame 2 funktioniert wahrscheinlich
void moveDameP2(int *field, int *posX, int *posY, int *newPosX, int *newPosY)
{
    // Funktionsweise: Läufer und Turm combined
    // kann nicht über andere figuren springen
    int n, o;
    int move;
    int width;
    char temp;
    // alte Position loeschen
    *(field + *posY * 8 + *posX) = 0;
    // neue mögliche Position anzeigen
    n = (int)*posX;
    o = (int)*posY;
    while (check(n - 1, o + 1) == 0 && n > 0 && o < 7)
    {
        printf("\n%d, %d, %s", n - 1, o + 1, "(1, diagonal links nach unten)");
        n--;
        o++;
    }
    n = (int)*posX;
    o = (int)*posY;
    while (check(n + 1, o - 1) == 0 && n < 7 && o > 0)
    {
        printf("\n%d, %d, %s", n + 1, o - 1, "(2, diagonal rechts nach oben)");
        n++;
        o--;
    }
    n = (int)*posX;
    o = (int)*posY;
    while (check(n + 1, o + 1) == 0 && n < 7 && o < 7)
    {
        printf("\n%d, %d, %s", n + 1, o + 1, "(3, diagonal rechts nach unten)");
        n++;
        o++;
    }
    n = (int)*posX;
    o = (int)*posY;
    while (check(n - 1, o - 1) == 0 && n > 0 && o > 0)
    {
        printf("\n%d, %d, %s", n - 1, o - 1, "(4, diagonal links nach oben)");
        n--;
        o--;
    }

    // gerade pos;
    n = (int)*posY;
    while (check(*posX, n + 1) == 0 && n < 7)
    {
        printf("\n%d, %d, %s", *posX, n + 1, "(5, runter)");
        n++;
    }

    n = (int)*posY;
    while (check(*posX, n - 1) == 0 && n > 0)
    {
        printf("\n%d, %d, %s", *posX, n - 1, "(6, hoch)");
        n--;
    }

    n = (int)*posX;
    while (check(n + 1, *posY) == 0 && n < 7)
    {
        printf("\n%d, %d, %s", n + 1, *posY, "(7, rechts)");
        n++;
    }
    n = (int)*posX;
    while (check(n - 1, *posY) == 0 && n > 0)
    {
        printf("\n%d, %d, %s", n - 1, *posY, "(8, links)");
        n--;
    }

    // moven
    printf("\nWohin bewegen (1 - 8)?");
    scanf("%d", &move);
    printf("\nWie weit?");
    scanf("%d", &width);
    switch (move)
    {
    case 1:
        *newPosX = ((*posX) - width);
        *newPosY = ((*posY) + width);
        if (check(*newPosX, *newPosY) == 0 && *newPosX >= 0 && *newPosY <= 7)
        {
            (*posX) -= width;
            (*posY) += width;
        }
        break;
    case 2:
        *newPosX = ((*posX) + width);
        *newPosY = ((*posY) - width);
        if (check(*newPosX, *newPosY) == 0 && *newPosX <= 7 && *newPosY >= 0)
        {
            (*posX) += width;
            (*posY) -= width;
        }
        break;
    case 3:
        *newPosX = ((*posX) + width);
        *newPosY = ((*posY) + width);
        if (check(*newPosX, *newPosY) == 0 && *newPosX <= 7 && *newPosY <= 7)
        {
            (*posX) += width;
            (*posY) += width;
        }
        break;
    case 4:
        *newPosX = ((*posX) - width);
        *newPosY = ((*posY) - width);
        if (check(*newPosX, *newPosY) == 0 && *newPosX >= 0 && *newPosY >= 0)
        {
            (*posX) -= width;
            (*posY) -= width;
        }
        break;
    case 5:
        *newPosX = (*posX + width);
        if (check(*newPosX, *posY) == 0)
        {
            (*posX) += width;
        }
        else
        {
            printf("\nZug ungültig!");
        }
        break;
    case 6:
        *newPosX = (*posX - width);
        if (check(*newPosX, *posY) == 0)
        {
            (*posX) -= width;
        }
        else
        {
            printf("\nZug ungültig!");
        }
        break;
    case 7:
        *newPosY = (*posY + width);
        if (check(*posX, *newPosY) == 0)
        {
            (*posY) += width;
        }
        else
        {
            printf("\nZug ungültig!");
        }
        break;
    case 8:
        *newPosY = (*posY - width);
        if (check(*posX, *newPosY) == 0)
        {
            (*posY) -= width;
        }
        else
        {
            printf("\nZug ungültig!");
        }
        break;
    default:
        printf("\n Zug ungültig!");
        break;
    }
    // Grenzueberschreitung pruefen
    if (*posX < 0)
        *posX = 0;
    if (*posX > 7)
        *posX = 7;
    if (*posY < 0)
        *posY = 0;
    if (*posY > 7)
        *posY = 7;
    // neue Position setzen
    *(field + *posY * 8 + *posX) = 5;
}

void moveKoenigP1(int *field, int *posX, int *posY, int *newPosX, int *newPosY)
{
    checkCheckmate(posX, posY);
    int n, o;
    int move;
    char temp;
    // alte Position loeschen
    *(field + *posY * 8 + *posX) = 0;
    // neue mögliche Position anzeigen
    n = (int)*posX;
    o = (int)*posY;
    if (check(n - 1, o + 1) == 0 && n > 0 && o < 7)
    {
        printf("\n%d, %d, %s", n - 1, o + 1, "(1, diagonal rechts nach oben)");
        n--;
        o++;
    }
    n = (int)*posX;
    o = (int)*posY;
    if (check(n + 1, o - 1) == 0 && n < 7 && o > 0)
    {
        printf("\n%d, %d, %s", n + 1, o - 1, "(2, diagonal links nach unten)");
        n++;
        o--;
    }
    n = (int)*posX;
    o = (int)*posY;
    if (check(n + 1, o + 1) == 0 && n < 7 && o < 7)
    {
        printf("\n%d, %d, %s", n + 1, o + 1, "(3, diagonal links nach oben)");
        n++;
        o++;
    }
    n = (int)*posX;
    o = (int)*posY;
    if (check(n - 1, o - 1) == 0 && n > 0 && o > 0)
    {
        printf("\n%d, %d, %s", n - 1, o - 1, "(4, diagonal rechts nach unten)");
        n--;
        o--;
    }

    // gerade pos;
    n = (int)*posY;
    if (check(*posX, n + 1) == 0 && n < 7)
    {
        printf("\n%d, %d, %s", *posX, n + 1, "(5, hoch)");
        n++;
    }

    n = (int)*posY;
    if (check(*posX, n - 1) == 0 && n > 0)
    {
        printf("\n%d, %d, %s", *posX, n - 1, "(6, runter)");
        n--;
    }

    n = (int)*posX;
    if (check(n + 1, *posY) == 0 && n < 7)
    {
        printf("\n%d, %d, %s", n + 1, *posY, "(7, links)");
        n++;
    }
    n = (int)*posX;
    if (check(n - 1, *posY) == 0 && n > 0)
    {
        printf("\n%d, %d, %s", n - 1, *posY, "(8, rechts)");
        n--;
    }

    // moven
    printf("\nWohin bewegen (1 - 8)?");
    scanf("%d", &move);
    switch (move)
    {
    case 1:
        *newPosX = ((*posX) - 1);
        *newPosY = ((*posY) + 1);
        if (check(*newPosX, *newPosY) == 0 && *newPosX >= 0 && *newPosY <= 7)
        {
            (*posX) -= 1;
            (*posY) += 1;
        }
        break;
    case 2:
        *newPosX = ((*posX) + 1);
        *newPosY = ((*posY) - 1);
        if (check(*newPosX, *newPosY) == 0 && *newPosX <= 7 && *newPosY >= 0)
        {
            (*posX) += 1;
            (*posY) -= 1;
        }
        break;
    case 3:
        *newPosX = ((*posX) + 1);
        *newPosY = ((*posY) + 1);
        if (check(*newPosX, *newPosY) == 0 && *newPosX <= 7 && *newPosY <= 7)
        {
            (*posX) += 1;
            (*posY) += 1;
        }
        break;
    case 4:
        *newPosX = ((*posX) - 1);
        *newPosY = ((*posY) - 1);
        if (check(*newPosX, *newPosY) == 0 && *newPosX >= 0 && *newPosY >= 0)
        {
            (*posX) -= 1;
            (*posY) -= 1;
        }
        break;
    case 5:
        *newPosX = (*posX + 1);
        if (check(*newPosX, *posY) == 0)
        {
            (*posX) += 1;
        }
        else
        {
            printf("\nZug ungültig!");
        }
        break;
    case 6:
        *newPosX = (*posX - 1);
        if (check(*newPosX, *posY) == 0)
        {
            (*posX) -= 1;
        }
        else
        {
            printf("\nZug ungültig!");
        }
        break;
    case 7:
        *newPosY = (*posY + 1);
        if (check(*posX, *newPosY) == 0)
        {
            (*posY) += 1;
        }
        else
        {
            printf("\nZug ungültig!");
        }
        break;
    case 8:
        *newPosY = (*posY - 1);
        if (check(*posX, *newPosY) == 0)
        {
            (*posY) -= 1;
        }
        else
        {
            printf("\nZug ungültig!");
        }
        break;
    default:
        printf("\n Zug ungültig!");
        break;
    }
    // Grenzueberschreitung pruefen
    if (*posX < 0)
        *posX = 0;
    if (*posX > 7)
        *posX = 7;
    if (*posY < 0)
        *posY = 0;
    if (*posY > 7)
        *posY = 7;
    // neue Position setzen
    *(field + *posY * 8 + *posX) = 6;
}

void moveKoenigP2(int *field, int *posX, int *posY, int *newPosX, int *newPosY)
{
    checkCheckmate(posX, posY);
    int n, o;
    int move;
    char temp;
    // alte Position loeschen
    *(field + *posY * 8 + *posX) = 0;
    // neue mögliche Position anzeigen
    n = (int)*posX;
    o = (int)*posY;
    if (check(n - 1, o + 1) == 0 && n > 0 && o < 7)
    {
        printf("\n%d, %d, %s", n - 1, o + 1, "(1, diagonal rechts nach oben)");
        n--;
        o++;
    }
    n = (int)*posX;
    o = (int)*posY;
    if (check(n + 1, o - 1) == 0 && n < 7 && o > 0)
    {
        printf("\n%d, %d, %s", n + 1, o - 1, "(2, diagonal links nach unten)");
        n++;
        o--;
    }
    n = (int)*posX;
    o = (int)*posY;
    if (check(n + 1, o + 1) == 0 && n < 7 && o < 7)
    {
        printf("\n%d, %d, %s", n + 1, o + 1, "(3, diagonal links nach oben)");
        n++;
        o++;
    }
    n = (int)*posX;
    o = (int)*posY;
    if (check(n - 1, o - 1) == 0 && n > 0 && o > 0)
    {
        printf("\n%d, %d, %s", n - 1, o - 1, "(4, diagonal rechts nach unten)");
        n--;
        o--;
    }

    // gerade pos;
    n = (int)*posY;
    if (check(*posX, n + 1) == 0 && n < 7)
    {
        printf("\n%d, %d, %s", *posX, n + 1, "(5, hoch)");
        n++;
    }

    n = (int)*posY;
    if (check(*posX, n - 1) == 0 && n > 0)
    {
        printf("\n%d, %d, %s", *posX, n - 1, "(6, runter)");
        n--;
    }

    n = (int)*posX;
    if (check(n + 1, *posY) == 0 && n < 7)
    {
        printf("\n%d, %d, %s", n + 1, *posY, "(7, links)");
        n++;
    }
    n = (int)*posX;
    if (check(n - 1, *posY) == 0 && n > 0)
    {
        printf("\n%d, %d, %s", n - 1, *posY, "(8, rechts)");
        n--;
    }

    // moven
    printf("\nWohin bewegen (1 - 8)?");
    scanf("%d", &move);
    switch (move)
    {
    case 1:
        *newPosX = ((*posX) - 1);
        *newPosY = ((*posY) + 1);
        if (check(*newPosX, *newPosY) == 0 && *newPosX >= 0 && *newPosY <= 7)
        {
            (*posX) -= 1;
            (*posY) += 1;
        }
        break;
    case 2:
        *newPosX = ((*posX) + 1);
        *newPosY = ((*posY) - 1);
        if (check(*newPosX, *newPosY) == 0 && *newPosX <= 7 && *newPosY >= 0)
        {
            (*posX) += 1;
            (*posY) -= 1;
        }
        break;
    case 3:
        *newPosX = ((*posX) + 1);
        *newPosY = ((*posY) + 1);
        if (check(*newPosX, *newPosY) == 0 && *newPosX <= 7 && *newPosY <= 7)
        {
            (*posX) += 1;
            (*posY) += 1;
        }
        break;
    case 4:
        *newPosX = ((*posX) - 1);
        *newPosY = ((*posY) - 1);
        if (check(*newPosX, *newPosY) == 0 && *newPosX >= 0 && *newPosY >= 0)
        {
            (*posX) -= 1;
            (*posY) -= 1;
        }
        break;
    case 5:
        *newPosX = (*posX + 1);
        if (check(*newPosX, *posY) == 0)
        {
            (*posX) += 1;
        }
        else
        {
            printf("\nZug ungültig!");
        }
        break;
    case 6:
        *newPosX = (*posX - 1);
        if (check(*newPosX, *posY) == 0)
        {
            (*posX) -= 1;
        }
        else
        {
            printf("\nZug ungültig!");
        }
        break;
    case 7:
        *newPosY = (*posY + 1);
        if (check(*posX, *newPosY) == 0)
        {
            (*posY) += 1;
        }
        else
        {
            printf("\nZug ungültig!");
        }
        break;
    case 8:
        *newPosY = (*posY - 1);
        if (check(*posX, *newPosY) == 0)
        {
            (*posY) -= 1;
        }
        else
        {
            printf("\nZug ungültig!");
        }
        break;
    default:
        printf("\n Zug ungültig!");
        break;
    }
    // Grenzueberschreitung pruefen
    if (*posX < 0)
        *posX = 0;
    if (*posX > 7)
        *posX = 7;
    if (*posY < 0)
        *posY = 0;
    if (*posY > 7)
        *posY = 7;
    // neue Position setzen
    *(field + *posY * 8 + *posX) = 6;
}

void checkCheckmate(int *posX, int *posY)
{
    int n, o;
    int move;
    int width;
    char temp;
    // neue mögliche Position anzeigen
    n = (int)*posX;
    o = (int)*posY;
    while (check(n + 1, o - 1) == 0 && n < 7 && o > 0)
    {
        n++;
        o--;
    }
    if (check(n + 1, o - 1) == 1 && n < 7 && o > 0)
    {
        printf("\n Test1!");
    }

    n = (int)*posX;
    o = (int)*posY;
    while (check(n - 1, o + 1) == 0 && n > 0 && o < 7)
    {
        n--;
        o++;
    }
    if (check(n - 1, o + 1) == 1 && n > 0 && o < 7)
    {
        printf("\n Test2!");
    }

    n = (int)*posX;
    o = (int)*posY;
    while (check(n - 1, o - 1) == 0 && n > 0 && o > 0)
    {
        n--;
        o--;
    }
    if (check(n - 1, o - 1) == 1 && n > 0 && o > 0)
    {
        printf("\n Test3!");
    }

    n = (int)*posX;
    o = (int)*posY;
    while (check(n + 1, o + 1) == 0 && n < 7 && o < 7)
    {
        n++;
        o++;
    }
    if (check(n + 1, o + 1) == 1 && n < 7 && o < 7)
    {
        printf("\n Test4!");
    }

    n = (int)*posY;
    while (check(*posX, n - 1) == 0 && n > 0)
    {
        n--;
    }
    if (check(*posX, n - 1) == 1 && n > 0)
    {
        printf("\n Test5!");
    }

    n = (int)*posY;
    while (check(*posX, n + 1) == 0 && n < 7)
    {
        n++;
    }
    if (check(*posX, n + 1) == 1 && n < 7)
    {
        printf("\n Test6!");
    }

    n = (int)*posX;
    while (check(n - 1, *posY) == 0 && n > 0)
    {
        n--;
    }
    if (check(n - 1, *posY) == 1 && n > 0)
    {
        printf("\n Test7!");
    }

    n = (int)*posX;
    while (check(n + 1, *posY) == 0 && n < 7)
    {
        n++;
    }
    if (check(n + 1, *posY) == 1 && n < 7)
    {
        printf("\n Test8!");
    }

    // Grenzueberschreitung pruefen
    if (*posX < 0)
        *posX = 0;
    if (*posX > 7)
        *posX = 7;
    if (*posY < 0)
        *posY = 0;
    if (*posY > 7)
        *posY = 7;
}