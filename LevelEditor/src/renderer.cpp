#include "renderer.hpp"

namespace LevelEditor
{
    Renderer::Renderer(const DisplayManager &dm, uint32_t numVertices, uint32_t numIndices)
        : mVertexBuffer(VertexBuffer(numVertices)),
          mIndexBuffer(IndexBuffer(numIndices))
    {
        mDisplayManager = std::make_unique<DisplayManager>(dm);
        mWindow = mDisplayManager->GetWindow();
    }

    Renderer::~Renderer()
    {
    }

    void Renderer::Init(uint32_t numVertices,
                        uint32_t numIndices,
                        const std::vector<uint32_t> &indices,
                        const std::vector<Vertex> &vertices)
    {
        // buffer vertex buffer data
        glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices * sizeof(Vertex), vertices.data());

        // buffer index buffer data
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, numIndices * sizeof(indices[0]), indices.data());

        // set OpenGL params
        // glEnable(GL_BLEND);
        // glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
    }

    void Renderer::CreateShader(std::string &&shaderName, const char *vertexShaderFilename, const char *fragmentShaderFilename)
    {
        mShaders.try_emplace(shaderName, vertexShaderFilename, fragmentShaderFilename);
    }

    void Renderer::Render(glm::mat4 modelViewProj)
    {
        glClear(GL_COLOR_BUFFER_BIT);

        // TODO: Add vertex/index buffer
        mVertexBuffer.Bind();
        mIndexBuffer.Bind();

        // cursor drawing
        mShaders.at("cursorShader").Bind();

        // draw
        mShaders.at("cursorShader").UploadMat4("u_modelViewProj", modelViewProj);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        mShaders.at("cursorShader").Unbind();

        // level drawing
        mShaders.at("textureShader").Bind();

        // draw

        mShaders.at("textureShader").Unbind();

        mIndexBuffer.Unbind();
        mVertexBuffer.Unbind();

        SDL_GL_SwapWindow(mWindow);
    }

    void Renderer::CalculateFPS(float deltaTime)
    {
        FPS = 1000 / deltaTime;
        std::cout << "FPS: " << FPS << std::endl;
    }
}