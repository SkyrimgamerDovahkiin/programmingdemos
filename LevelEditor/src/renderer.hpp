#pragma once

#include "config.hpp"
#include "display_manager.hpp"
#include "vertex_buffer.hpp"
#include "index_buffer.hpp"
#include "shader.hpp"

namespace LevelEditor
{
    class Renderer
    {
    public:
        Renderer(const DisplayManager &dm, uint32_t numVertices, uint32_t numIndices);
        ~Renderer();

        // sets OpenGL parameters and initialize other stuff
        void Init(uint32_t numVertices,
                  uint32_t numIndices,
                  const std::vector<uint32_t> &indices,
                  const std::vector<Vertex> &vertices);

        // Adds a shader to the list of available shaders
        void CreateShader(std::string &&shaderName, const char *vertexShaderFilename, const char *fragmentShaderFilename);

        void Render(glm::mat4 modelViewProj);

        void CalculateFPS(float deltaTime);

    private:
        std::unique_ptr<DisplayManager> mDisplayManager;
        SDL_Window *mWindow = nullptr;

        std::unordered_map<std::string, Shader> mShaders;

        VertexBuffer mVertexBuffer;
        IndexBuffer mIndexBuffer;

        // fps
        int FPS = 0;
    };
}
