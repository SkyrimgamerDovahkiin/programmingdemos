#include "camera.hpp"

namespace LevelEditor
{
    Camera::Camera(int width, int height)
    {
        mProjection = glm::ortho(0.0f, (float)width, (float)height, 0.0f, -1.0f, 1.0f);
        mView = glm::mat4(1.0f);
        mPosition = glm::vec3(0.0f);
        Translate(glm::vec3(0.0f));
        Update();
    }

    Camera::~Camera()
    {
    }

    void Camera::Update()
    {
        mViewProj = mProjection * mView;
    }

    void Camera::Translate(glm::vec3 v)
    {
        mPosition += v;
        mView = glm::translate(mView, v * -1.0f);
    }
}