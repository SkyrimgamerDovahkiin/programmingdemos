#include "shader.hpp"

namespace LevelEditor
{
    Shader::Shader(const char *vertexShaderFilename, const char *fragmentShaderFilename)
    {
        mShaderId = CreateShader(vertexShaderFilename, fragmentShaderFilename);
    }

    Shader::~Shader()
    {
        glDeleteProgram(mShaderId);
    }

    void Shader::Bind()
    {
        glUseProgram(mShaderId);
    }

    void Shader::Unbind()
    {
        glUseProgram(0);
    }

    GLint Shader::GetVariableLocation(const std::string& varName)
    {
        auto iter = mShaderVariables.find(varName);
        if (iter != mShaderVariables.end())
        {
            return iter->second.location;
        }

        return -1;
    }

    void Shader::UploadMat4(std::string &&name, glm::mat4 _mat4)
    {
        int loc = GetVariableLocation(name);
        glUniformMatrix4fv(loc, 1, GL_FALSE, &_mat4[0][0]);
    }

    GLuint Shader::Compile(const char *shaderSource, GLenum type)
    {
        GLuint id = glCreateShader(type);
        const char *src = shaderSource;
        glShaderSource(id, 1, &src, 0);
        glCompileShader(id);

        int result;
        glGetShaderiv(id, GL_COMPILE_STATUS, &result);
        if (result != GL_TRUE)
        {
            int length = 0;
            glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
            char *message = new char[length];
            glGetShaderInfoLog(id, length, &length, message);
            std::cerr << "Shader compilation error: " << message << std::endl;
            delete[] message;
            return 0;
        }
        return id;
    }

    std::string Shader::Parse(const char *filename)
    {
        std::FILE *file;
        file = std::fopen(filename, "rb");
        if (file == nullptr)
        {
            std::cerr << "File " << filename << " not found" << std::endl;
            return 0;
        }

        std::string contents;
        fseek(file, 0, SEEK_END);
        size_t filesize = ftell(file);
        rewind(file);
        contents.resize(filesize);

        fread(&contents[0], 1, filesize, file);
        fclose(file);

        return contents;
    }

    GLuint Shader::CreateShader(const char *vertexShaderFilename, const char *fragmentShaderFilename)
    {
        std::string vertexShaderSource = Parse(vertexShaderFilename);
        std::string fragmentShaderSource = Parse(fragmentShaderFilename);

        GLuint program = glCreateProgram();
        GLuint vs = Compile(vertexShaderSource.c_str(), GL_VERTEX_SHADER);
        GLuint fs = Compile(fragmentShaderSource.c_str(), GL_FRAGMENT_SHADER);

        glAttachShader(program, vs);
        glAttachShader(program, fs);
        glLinkProgram(program);

#ifdef _RELEASE
        glDetachShader(program, vs);
        glDetachShader(program, fs);

        glDeleteShader(vs);
        glDeleteShader(fs);
#endif

        // get uniforms
        int numUniforms;
        glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &numUniforms);

        int maxCharLength;
        glGetProgramiv(program, GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxCharLength);
        if (numUniforms > 0) // only do this when uniforms exist
        {
            char *buffer = (char *)malloc(sizeof(char) * maxCharLength);

            for (size_t i = 0; i < numUniforms; i++)
            {
                int length, size;
                GLenum dataType;

                glGetActiveUniform(program, (GLuint)i, maxCharLength, &length, &size, &dataType, buffer);
                GLint location = glGetUniformLocation(program, buffer);

                ShaderVariable sv;
                sv.location = location;
                sv.dataType = dataType;

                mShaderVariables.emplace(std::string(buffer, length), sv);
            }

            free(buffer);
        }

        return program;
    }
}