#include <iostream>
#include <cstring>

class String
{
public:
    String() = default;
    String(const char *string)
    {
        printf("Created!\n");
        mSize = strlen(string);
        mData = new char[mSize];
        memcpy(mData, string, mSize);
    }

    String(const String &other)
    {
        printf("Copied!\n");
        mSize = other.mSize;
        mData = new char[mSize];
        memcpy(mData, other.mData, mSize);
    }

    String(String &&other) noexcept
    {
        printf("Moved!\n");
        mSize = other.mSize;
        mData = other.mData;

        other.mSize = 0;
        other.mData = nullptr;
    }

    String &operator=(String &&other) noexcept
    {
        printf("Moved!\n");

        if (this != &other)
        {
            delete[] mData;

            mSize = other.mSize;
            mData = other.mData;

            other.mSize = 0;
            other.mData = nullptr;
        }

        return *this;
    }

    ~String()
    {
        printf("Destroyed!\n");
        delete mData;
    }

    void Print()
    {
        for (uint32_t i = 0; i < mSize; i++)
        {
            printf("%c", mData[i]);
        }
        printf("\n");
    }

private:
    char *mData;
    uint32_t mSize;
};

class Entity
{
public:
    Entity(const String &name)
        : mName(name)
    {
    }

    Entity(String &&name)
        : mName(std::move(name))
    {
    }

    void PrintName()
    {
        mName.Print();
    }

    String mName;
};

int main()
{
    std::cout << "Entities:" << std::endl;
    Entity e("Test");
    e.PrintName();

    std::cout << "Strings:" << std::endl;
    String a = "Tester";
    String dest;

    std::cout << "a:";
    a.Print();
    std::cout << "dest:";
    dest.Print();

    dest = std::move(a);

    std::cout << "a:";
    a.Print();
    std::cout << "dest:";
    dest.Print();
    std::cout << "\n";

    return 0;
}
