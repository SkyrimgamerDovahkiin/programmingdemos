#include "time.hpp"

namespace LevelEditor
{
    Time::Time()
    {
    }

    void Time::Update()
    {
        time = SDL_GetTicks64();

        // delta time calculation
        last = now;
        now = SDL_GetPerformanceCounter();
        deltaTime = (double)((now - last) *1000 / (double)SDL_GetPerformanceFrequency());
    }

    Time::~Time()
    {
    }
}