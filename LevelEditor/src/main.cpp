#include "config.hpp"

#include "shader.hpp"
#include "display_manager.hpp"
#include "renderer.hpp"
#include "camera.hpp"
#include "game_object.hpp"
#include "time.hpp"
// #include "defines.hpp"

using LevelEditor::Shader,
    LevelEditor::DisplayManager,
    LevelEditor::Renderer,
    LevelEditor::Camera,
    LevelEditor::GameObject,
    LevelEditor::Time;

const int SCREEN_WIDTH = 600;
const int SCREEN_HEIGHT = 600;

const int LEVEL_WIDTH = 2400;
const int LEVEL_HEIGHT = 2400;

std::vector<Vertex> vertices;
std::vector<uint32_t> indices;

uint32_t numVertices = 0;
uint32_t numIndices = 0;

uint32_t indicesCount = 0;

uint32_t maxVertices = 16000000;
uint32_t maxIndices = 24000000;

// maybe put into own file later
void CreateVerticesRect(Rectangle rect, float xOffset, float yOffset, float texWidth, float texHeight, float spriteWidth, float spriteHeight)
{
    // x and y is top left corner
    vertices.push_back(Vertex{
        glm::vec3(rect.x, rect.y + rect.height, 0.0f),
        glm::vec2(xOffset / texWidth, (yOffset + spriteHeight) / texHeight)}); // bottomLeft
    indices.push_back(indicesCount);
    indicesCount++;
    vertices.push_back(Vertex{
        glm::vec3(rect.x, rect.y, 0.0f),
        glm::vec2(xOffset / texWidth, yOffset / texHeight)}); // topLeft
    indices.push_back(indicesCount);
    indicesCount++;
    vertices.push_back(Vertex{
        glm::vec3(rect.x + rect.width, rect.y + rect.height, 0.0f),
        glm::vec2((xOffset + spriteWidth) / texWidth, (yOffset + spriteHeight) / texHeight)}); // bottomRight
    indices.push_back(indicesCount);
    vertices.push_back(Vertex{
        glm::vec3(rect.x + rect.width, rect.y, 0.0f),
        glm::vec2((xOffset + spriteWidth) / texWidth, yOffset / texHeight)}); // topRight
    indicesCount--;
    indices.push_back(indicesCount);
    indicesCount++;
    indices.push_back(indicesCount);
    indicesCount++;
    indices.push_back(indicesCount);
    indicesCount++;

    numVertices = vertices.size();
    numIndices = indices.size();
}

int main()
{
    Camera camera(SCREEN_WIDTH, SCREEN_HEIGHT);

    // create objects

    GameObject cursorGameObject;
    cursorGameObject.Move2D(camera.GetViewProj(), glm::vec2(0.0f));

    // create GameObject vertices and indices
    CreateVerticesRect(cursorGameObject.GetRect(), 0, 0, 1, 1, 1, 1);

    GameObject cursorGameObject2;
    cursorGameObject2.Move2D(camera.GetViewProj(), glm::vec2(40.0f));

    CreateVerticesRect(cursorGameObject2.GetRect(), 0, 0, 1, 1, 1, 1);

    Time time;

    DisplayManager displayManager;
    displayManager.Init(SCREEN_WIDTH, SCREEN_HEIGHT);

    Renderer renderer(displayManager, maxIndices, maxVertices);
    renderer.Init(numVertices, numIndices, indices, vertices);

    std::string t = "cursorShader";

    renderer.CreateShader("cursorShader", "shaders/cursor_vert.glsl", "shaders/cursor_frag.glsl");
    renderer.CreateShader("textureShader", "shaders/texture_vert.glsl", "shaders/texture_frag.glsl");

    bool close = false;
    SDL_Event event; // should be done in the input manager

    while (!close)
    {
        time.Update();

        // input
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
            {
                close = true;
            }
            else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_d)
            {
                cursorGameObject.Move2D(camera.GetViewProj(), glm::vec2(20.0f, 0.0f));
            }
            else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_a)
            {
                cursorGameObject.Move2D(camera.GetViewProj(), glm::vec2(-20.0f, 0.0f));
            }
            else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_w)
            {
                cursorGameObject.Move2D(camera.GetViewProj(), glm::vec2(0.0f, -20.0f));
            }
            else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_s)
            {
                cursorGameObject.Move2D(camera.GetViewProj(), glm::vec2(0.0f, 20.0f));
            }
        }

        renderer.Render(cursorGameObject.GetModelViewProj());
        // renderer.CalculateFPS(time.deltaTime);
    }

    return 0;
}