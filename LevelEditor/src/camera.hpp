#pragma once

#include "config.hpp"

namespace LevelEditor
{
    class Camera
    {
    public:
        Camera(int width, int height);
        ~Camera();

        inline glm::mat4 GetViewProj() { return mViewProj; }
        inline glm::vec3 GetPosition() { return mPosition; }

        virtual void Update();
        virtual void Translate(glm::vec3 v);

    private:
        glm::vec3 mPosition;
        glm::mat4 mProjection;
        glm::mat4 mView;
        glm::mat4 mViewProj;
    };
}