#pragma once
#include "config.hpp"

namespace LevelEditor
{
    class IndexBuffer
    {
    public:
        IndexBuffer(uint32_t numIndices);
        ~IndexBuffer();
        void Bind();
        void Unbind();

    private:
        GLuint bufferId;
    };
}