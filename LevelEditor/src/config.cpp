#include "config.hpp"

// GLEW/SDL
#define GLEW_STATIC
#include <GL/glew.h>
#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>

// STB
#define STB_IMAGE_IMPLEMENTATION
#include "../libs/stb_image.h"