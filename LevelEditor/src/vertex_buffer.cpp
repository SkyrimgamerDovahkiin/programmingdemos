#include "vertex_buffer.hpp"

namespace LevelEditor
{
    VertexBuffer::VertexBuffer(uint32_t numVertices)
    {
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);

        glGenBuffers(1, &bufferId);
        glBindBuffer(GL_ARRAY_BUFFER, bufferId);
        glBufferData(GL_ARRAY_BUFFER, numVertices, NULL, GL_DYNAMIC_DRAW);

        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(struct Vertex, position));
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(struct Vertex, texCoords));

        glBindVertexArray(0);
    }

    VertexBuffer::~VertexBuffer()
    {
        glDeleteBuffers(1, &bufferId);
        glDeleteVertexArrays(1, &vao);
    }

    void VertexBuffer::Bind()
    {
        glBindVertexArray(vao);
        glBindBuffer(GL_ARRAY_BUFFER, bufferId);
    }

    void VertexBuffer::Unbind()
    {
        glBindVertexArray(0);
    }
}