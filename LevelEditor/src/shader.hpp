#pragma once

#include "config.hpp"

struct ShaderVariable
{
    GLint location;
    GLenum dataType;

    bool operator==(const ShaderVariable &other) const
    {
        return other.location == location;
    }
};

namespace LevelEditor
{
    class Shader
    {
    public:
        Shader(const char *vertexShaderFilename, const char *fragmentShaderFilename);
        ~Shader();

        void Bind();
        void Unbind();

        inline GLuint &GetShaderId() { return mShaderId; }

        GLint GetVariableLocation(const std::string& varName);
        void UploadMat4(std::string &&name, glm::mat4 _mat4);

    private:
        GLuint Compile(const char *shaderSource, GLenum type);
        std::string Parse(const char *filename);
        GLuint CreateShader(const char *vertexShaderFilename, const char *fragmentShaderFilename);
        GLuint mShaderId;

        // string = name, ShaderVariable = location and data type
        std::unordered_map<std::string, ShaderVariable> mShaderVariables;
    };
}