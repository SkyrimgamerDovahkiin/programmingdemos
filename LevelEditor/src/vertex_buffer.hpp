#pragma once

#include "config.hpp"

#include "defines.hpp"

namespace LevelEditor
{
    class VertexBuffer
    {
    public:
        VertexBuffer(uint32_t numVertices);
        ~VertexBuffer();
        void Bind();
        void Unbind();

    private:
        GLuint bufferId;
        GLuint vao;
    };
}