/* contains all time stuff
such as delta time
and time since start of the game*/

#pragma once

#include "config.hpp"

namespace LevelEditor
{
    class Time
    {
    public:
        Time();
        ~Time();

        // keeps updating delta time and time since start of the game
        void Update();

    public:
        float time;
        float deltaTime;

    private:
        uint64_t last = 0;
        uint64_t now = SDL_GetPerformanceCounter();
    };
}