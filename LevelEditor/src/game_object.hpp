#pragma once

#include "config.hpp"

#include "defines.hpp"

namespace LevelEditor
{
    class GameObject
    {
    public:
        GameObject();
        ~GameObject();

        inline Rectangle GetRect() const { return mRect; }
        inline glm::mat4 GetModelViewProj() const { return mModelViewProjMatrix; }

        // translates object on x and y axis
        void Move2D(glm::mat4 cameraViewProj, glm::vec2 position)
        {
            mModelMatrix = glm::translate(mModelMatrix, glm::vec3(position.x, position.y, 0.0f));
            mModelViewProjMatrix = cameraViewProj * mModelMatrix;
            mRect.x = mModelMatrix[3].x;
            mRect.y = mModelMatrix[3].y;
        }

    private:
        Rectangle mRect = {0, 0, 20, 20}; // contains position and size
        glm::mat4 mModelMatrix = glm::mat4(1.0f);
        glm::mat4 mModelViewProjMatrix = glm::mat4(1.0f);
    };
}
