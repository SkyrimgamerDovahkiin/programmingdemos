#pragma once

#include "config.hpp"

// a vertex
struct Vertex
{
    glm::vec3 position;
    glm::vec2 texCoords;
    glm::vec4 color;
};

// a rectangle
struct Rectangle
{
    int x;               // Rectangle position x
    int y;               // Rectangle position y
    int width;           // Rectangle width
    int height;          // Rectangle height
};