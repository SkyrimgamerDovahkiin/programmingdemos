#pragma once

#include "config.hpp"

namespace LevelEditor
{
    // OpenGL Debug Callback
    void GLAPIENTRY OpenGLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam);

    class DisplayManager
    {
    public:
        DisplayManager();
        ~DisplayManager();

        SDL_Window *GetWindow() const { return mWindow; };

        // Initializes SDL and glew
        void Init(int width, int height, uint32_t flags = SDL_WINDOW_OPENGL, const char* windowTitle = "Level Editor");

    private:
        SDL_Window *mWindow = nullptr;
        SDL_GLContext mGlContext;
    };
}