#version 330 core

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec2 a_texCoord;
layout(location = 2) in vec4 a_color;

out vec4 v_color;
out vec2 v_texCoord;

uniform mat4 u_modelViewProj;
uniform vec4 u_texValues;

void main()
{
    gl_Position = u_modelViewProj * vec4(a_position, 1.0f);
    v_color = a_color;
    v_texCoord = a_texCoord;

    // clamp the texture coords between 0 and 1 so they are correct
    // v_texCoord.x = clamp(a_texCoord.x + u_texValues.x, u_texValues.x, u_texValues.y);
    // v_texCoord.y = clamp(a_texCoord.y + u_texValues.z, u_texValues.z, u_texValues.w);
    // v_texCoord.y = a_texCoord.y;
}