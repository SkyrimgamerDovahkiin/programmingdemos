#ifndef CALCULATOR_H // include guard
#define CALCULATOR_H
#include <deque>
#include <string>

namespace Calculator
{
    class calculator
    {
    public:
        double Solve(double num1, char op, double num2);
        void DissasembleExpression(std::string &expression, std::deque<double> &numbers, std::deque<char> &operators);
        void Calculate(std::deque<double> &numbers, std::deque<char> &operators);
    };

}

#endif /* CALCULATOR_H */