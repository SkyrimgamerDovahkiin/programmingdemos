#include "raylib.h"
#include <iostream>
#include <cstring>
#include <string>
#include <sstream>
#include <algorithm>
#include <deque>
#include "../Program/calculator.hpp"

#define MAX_INPUT_CHARS 100

using namespace Calculator;
using std::cout, std::string, std::deque;

// TODO: Bugfixing/overhauling
// Better Comments

// NOTE: Sometimes buggy with long numbers and commas (rounding errors)

// Button class
class Button
{
public:
    Rectangle buttonRect;
    char expression;

    // Print the button expression and draws it also
    void ButtonExpression(Rectangle bounds, Vector2 pos, Rectangle sourceRec, Texture2D button, bool btnAction, int &letterCount, int &posX, char name[],
                          char nameFirstResult[], char nameSecondResult[], char nameThirdResult[], char nameFourthResult[],
                          bool &showFirstResult, bool &showSecondResult, bool &showThirdResult, bool &showFourthResult)
    {
        Vector2 mousePoint = GetMousePosition();
        calculator calculator_class;
        bool mouseOverBtn = false;

        // Check button state
        if (CheckCollisionPointRec(mousePoint, bounds))
        {
            mouseOverBtn = true;

            if (IsMouseButtonReleased(MOUSE_BUTTON_LEFT))
            {
                btnAction = true;
            }
        }
        else
            mouseOverBtn = false;

        // when button pressed, print according expression or do the operation that should be done
        if (btnAction)
        {
            if (expression == 'D' && letterCount < 15)
            {

                letterCount--;
                if (letterCount < 0)
                    letterCount = 0;
                name[letterCount] = '\0';
            }
            else if (expression == 'D' && (letterCount >= 15 && letterCount < MAX_INPUT_CHARS))
            {
                letterCount--;
                if (letterCount < 0)
                    letterCount = 0;

                // calculate last char length so that the last char is removed but the expression doesn't move
                string str{name[letterCount]};
                const char *expressionPtr = str.c_str();
                posX = posX + 4 + MeasureText(expressionPtr, 40);
                name[letterCount] = '\0';
            }

            if (expression == '=')
            {
                // disassembles the expression and calculates
                string expression = name;
                string expressionForResultStoring = name;

                deque<double> numbers;
                deque<char> operators;
                calculator_class.DissasembleExpression(expression, numbers, operators);
                calculator_class.Calculate(numbers, operators);

                // needed: the expression, the "equals" symbol and the result
                double result = 0.0;
                if (numbers.size() > 0)
                {
                    result = numbers.at(0);
                }
                operators.clear();
                numbers.clear();

                // using ostringstream because the result is more precise
                std::ostringstream streamObj;
                streamObj << result;
                std::string strObj = streamObj.str();

                // copying the contents of the string to char array
                string showInResult = " ";
                if (showFirstResult)
                {
                    showInResult = expressionForResultStoring + "=" + strObj + "\0";
                    std::strcpy(nameFirstResult, showInResult.c_str());
                    showSecondResult = true;
                    showFirstResult = false;
                }
                else if (showSecondResult)
                {
                    // string showInFirstResult = " ";
                    showInResult = expressionForResultStoring + "=" + strObj + "\0";
                    std::strcpy(nameSecondResult, showInResult.c_str());
                    showThirdResult = true;
                    showSecondResult = false;
                }
                else if (showThirdResult)
                {
                    // string showInFirstResult = " ";
                    showInResult = expressionForResultStoring + "=" + strObj + "\0";
                    std::strcpy(nameThirdResult, showInResult.c_str());
                    showFourthResult = true;
                    showThirdResult = false;
                }
                else if (showFourthResult)
                {
                    // string showInFirstResult = " ";
                    showInResult = expressionForResultStoring + "=" + strObj + "\0";
                    std::strcpy(nameFourthResult, showInResult.c_str());
                    showFirstResult = false;
                    showSecondResult = false;
                    showThirdResult = false;
                    showFourthResult = false;
                }

                // resets letter count and clears array
                letterCount = 0;
                char *begin = name;
                char *end = begin + sizeof(name);
                std::fill(begin, end, 0);

                // copying the contents of the string to char array
                letterCount = strObj.size();
                std::strcpy(name, strObj.c_str());
            }
            else if (expression != 'D' && expression != '=' && (letterCount < 15))
            {
                posX = 0;
                name[letterCount] = expression;
                name[letterCount + 1] = '\0'; // Add null terminator at the end of the string.
                letterCount++;
            }
            else if (expression != 'D' && expression != '=' && (letterCount >= 15 && letterCount < MAX_INPUT_CHARS))
            {

                name[letterCount] = expression;
                name[letterCount + 1] = '\0'; // Add null terminator at the end of the string.

                // calculate last char length so that the last char is visible
                string str{name[letterCount]};
                const char *expressionPtr = str.c_str();
                posX = posX - 4 - MeasureText(expressionPtr, 40);

                letterCount++;
            }
        }
        //--------------------------------------------------------------------

        // Draw control
        // convert the char to a const char *
        string str{expression};
        const char *expressionPtr = str.c_str();

        if (mouseOverBtn)
        {
            DrawTextureRec(button, sourceRec, {pos.x, pos.y}, GRAY);
        }
        else
            DrawTextureRec(button, sourceRec, {pos.x, pos.y}, WHITE);
        DrawText(expressionPtr, pos.x + bounds.width / 2, pos.y + bounds.height / 2 - 10, 20, WHITE);
        //--------------------------------------------------------------------
    }
};

int main()
{
    const int screenWidth = 430;
    const int screenHeight = 460;
    calculator calculator_class;

    // array with all available expressions
    char expressionsArray[20] = {
        // numbers
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '0',
        // operators
        '*',
        '/',
        '-',
        '+',
        '(',
        ')',
        // other
        '=', // equals, program should just print result
        'D', // delete, program should delete last char
        ',', // comma, program should read everything after and before as one number
        '.', // dot, program should read everything after and before as one number
    };

    // get changed later. the number is not the actual button number, but one less
    // because the first button gets drawn before the others
    int numButtons = 17;
    // int numButtons = 18;

    int counter = 0;
    // int arrayLocation = 0;

    // the offset/spacing for between the buttons
    const float offset = 10.0f;

    // button dimensions
    const Vector2 buttonDimensions = {60, 45};

    // the initial button pos
    const Vector2 initButtonPos = {10.0f, 295.0f};

    InitWindow(screenWidth, screenHeight, "Calculator");

    char name[MAX_INPUT_CHARS + 1] = "\0"; // NOTE: One extra space required for null terminator char '\0'
    char nameFirstResult[MAX_INPUT_CHARS + 1] = "\0";
    char nameSecondResult[MAX_INPUT_CHARS + 1] = "\0";
    char nameThirdResult[MAX_INPUT_CHARS + 1] = "\0";
    char nameFourthResult[MAX_INPUT_CHARS + 1] = "\0";
    int letterCount = 0;

    Rectangle textBox = {0, 230, screenWidth, 50};
    Rectangle firstResultBox = {0, 175, screenWidth, 50};
    Rectangle secondResultBox = {0, 120, screenWidth, 50};
    Rectangle thirdResultBox = {0, 65, screenWidth, 50};
    Rectangle fourthResultBox = {0, 10, screenWidth, 50};
    bool mouseOnText = false;
    bool mouseOnFirstBox = false;
    bool mouseOnSecondBox = false;
    bool mouseOnThirdBox = false;
    bool mouseOnFourthBox = false;
    bool showFirstResult = true;
    bool showSecondResult = false;
    bool showThirdResult = false;
    bool showFourthResult = false;

    int framesCounter = 0;

    auto workDir = GetWorkingDirectory();
    string spriteLocation = workDir + (string)"/Sprites/ButtonBackground.png";
    Texture2D buttonTex = LoadTexture(spriteLocation.c_str()); // Load button texture

    // float frameHeight = (float)buttonDimensions.y;

    float frameHeight = (float)buttonTex.height;

    Rectangle sourceRec = {0, 0, (float)buttonTex.width, frameHeight};

    // Define button bounds on screen
    // Rectangle btnBounds = {initButtonPos.x, initButtonPos.y, (float)buttonDimensions.x, frameHeight};
    Rectangle btnBounds = {initButtonPos.x, initButtonPos.y, (float)buttonTex.width, frameHeight};

    // int btnState = 0;       // Button state: 0-NORMAL, 1-MOUSE_HOVER, 2-PRESSED
    bool btnAction = false; // Button action should be activated

    Vector2 mousePoint = {0.0f, 0.0f};

    // the xPosition for the scrolling text
    int posX = 0;

    SetTargetFPS(60);

    // Main game loop
    while (!WindowShouldClose()) // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        mousePoint = GetMousePosition();
        btnAction = false;
        counter = 0;

        //----------------------------------------------------------------------------------

        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

        ClearBackground(RAYWHITE);

        // Draw Buttons
        float newBtnPosX = btnBounds.x + buttonDimensions.x + offset;
        float newBtnPosY = btnBounds.y + buttonDimensions.x + offset;

        Rectangle rect = {btnBounds.x, btnBounds.y, buttonDimensions.x, buttonDimensions.y};
        Button button = Button{rect, expressionsArray[counter]};
        button.ButtonExpression(rect, Vector2{initButtonPos.x, initButtonPos.y}, sourceRec, buttonTex, btnAction, letterCount, posX, name,
                                nameFirstResult, nameSecondResult, nameThirdResult, nameFourthResult,
                                showFirstResult, showSecondResult, showThirdResult, showFourthResult);
        counter++;

        for (int i = 0; i < numButtons; i++)
        {
            if (i == 0)
            {
                newBtnPosX = initButtonPos.x + buttonDimensions.x + offset;
                newBtnPosY = initButtonPos.y;
            }
            else if (i == 5)
            {
                newBtnPosX = initButtonPos.x;
                newBtnPosY = newBtnPosY + buttonDimensions.y + offset;
            }
            else if (i == 11)
            {
                newBtnPosX = initButtonPos.x;
                newBtnPosY = newBtnPosY + buttonDimensions.y + offset;
            }
            else
            {
                newBtnPosX = newBtnPosX + buttonDimensions.x + offset;
            }
            rect = {newBtnPosX, newBtnPosY, buttonDimensions.x, buttonDimensions.y};
            button = Button{rect, expressionsArray[counter]};
            button.ButtonExpression(rect, Vector2{newBtnPosX, newBtnPosY}, sourceRec, buttonTex, btnAction, letterCount, posX, name,
                                    nameFirstResult, nameSecondResult, nameThirdResult, nameFourthResult,
                                    showFirstResult, showSecondResult, showThirdResult, showFourthResult);
            counter++;
        }

        if (CheckCollisionPointRec(GetMousePosition(), textBox))
            mouseOnText = true;
        else
            mouseOnText = false;

        if (CheckCollisionPointRec(GetMousePosition(), firstResultBox))
            mouseOnFirstBox = true;
        else
            mouseOnFirstBox = false;

        if (CheckCollisionPointRec(GetMousePosition(), secondResultBox))
            mouseOnSecondBox = true;
        else
            mouseOnSecondBox = false;

        if (CheckCollisionPointRec(GetMousePosition(), thirdResultBox))
            mouseOnThirdBox = true;
        else
            mouseOnThirdBox = false;

        if (CheckCollisionPointRec(GetMousePosition(), fourthResultBox))
            mouseOnFourthBox = true;
        else
            mouseOnFourthBox = false;

        if (mouseOnText)
        {
            // Draw input line
            // Set the window's cursor to the I-Beam
            SetMouseCursor(MOUSE_CURSOR_IBEAM);
        }
        else
        {
            SetMouseCursor(MOUSE_CURSOR_DEFAULT);
        }
        // Get char pressed (unicode character) on the queue
        int key = GetCharPressed();

        // Check if more characters have been pressed on the same frame
        while (key > 0)
        {
            // NOTE: Only allow keys that are in the expressionsArray
            for (unsigned int i = 0; i < sizeof(expressionsArray); i++)
            {
                if (key == expressionsArray[i] && (letterCount < 15))
                {
                    posX = 0;
                    name[letterCount] = (char)key;
                    name[letterCount + 1] = '\0'; // Add null terminator at the end of the string.
                    letterCount++;
                }
                else if (key == expressionsArray[i] && (letterCount >= 15 && letterCount < MAX_INPUT_CHARS))
                {
                    name[letterCount] = (char)key;
                    name[letterCount + 1] = '\0'; // Add null terminator at the end of the string.

                    // calculate last char length so that the last char is visible
                    string str{name[letterCount]};
                    const char *expressionPtr = str.c_str();
                    posX = posX - 4 - MeasureText(expressionPtr, 40);
                    letterCount++;
                }

                if (key == ',' || key == '.')
                {
                    key = '.';
                }
                else if (key == 'D' || key == '=')
                {
                    key = '\0';
                }
            }

            key = GetCharPressed(); // Check next character in the queue
        }

        if (IsKeyPressed(KEY_ENTER))
        {
            // disassembles the expression and calculates
            string expression = name;
            string expressionForResultStoring = name;

            deque<double> numbers;
            deque<char> operators;
            calculator_class.DissasembleExpression(expression, numbers, operators);
            calculator_class.Calculate(numbers, operators);

            // needed: the expression, the "equals" symbol and the result
            double result = 0.0;
            if (numbers.size() > 0)
            {
                result = numbers.at(0);
            }
            operators.clear();
            numbers.clear();

            // using ostringstream because the result is more precise
            std::ostringstream streamObj;
            streamObj << result;
            std::string strObj = streamObj.str();

            // copying the contents of the string to char array
            string showInResult = " ";
            if (showFirstResult)
            {
                showInResult = expressionForResultStoring + "=" + strObj + "\0";
                std::strcpy(nameFirstResult, showInResult.c_str());
                showSecondResult = true;
                showFirstResult = false;
            }
            else if (showSecondResult)
            {
                // string showInFirstResult = " ";
                showInResult = expressionForResultStoring + "=" + strObj + "\0";
                std::strcpy(nameSecondResult, showInResult.c_str());
                showThirdResult = true;
                showSecondResult = false;
            }
            else if (showThirdResult)
            {
                // string showInFirstResult = " ";
                showInResult = expressionForResultStoring + "=" + strObj + "\0";
                std::strcpy(nameThirdResult, showInResult.c_str());
                showFourthResult = true;
                showThirdResult = false;
            }
            else if (showFourthResult)
            {
                // string showInFirstResult = " ";
                showInResult = expressionForResultStoring + "=" + strObj + "\0";
                std::strcpy(nameFourthResult, showInResult.c_str());
                showFirstResult = false;
                showSecondResult = false;
                showThirdResult = false;
                showFourthResult = false;
            }

            // resets letter count and clears array
            letterCount = 0;
            char *begin = name;
            char *end = begin + sizeof(name);
            std::fill(begin, end, 0);

            // copying the contents of the string to char array
            letterCount = strObj.size();
            std::strcpy(name, strObj.c_str());
        }

        if (IsKeyPressed(KEY_BACKSPACE) && letterCount < 15)
        {

            letterCount--;
            if (letterCount < 0)
                letterCount = 0;
            name[letterCount] = '\0';
        }
        else if (IsKeyPressed(KEY_BACKSPACE) && letterCount >= 15)
        {
            letterCount--;
            if (letterCount < 0)
                letterCount = 0;

            // calculate last char length so that the last char is removed but the expression doesn't move
            string str{name[letterCount]};
            const char *expressionPtr = str.c_str();
            posX = posX + 4 + MeasureText(expressionPtr, 40);
            name[letterCount] = '\0';
        }

        framesCounter++;

        // NOTE: need to find a better solution for resetting the counter
        if (framesCounter > 100000)
            framesCounter = 0;

        // draws the text box and the "name" text
        // when letter count >= 15, text get drawn at posX because otherwise the
        // last char wouldn't be visible
        DrawRectangleRec(textBox, BLACK);

        DrawRectangleRec(firstResultBox, RAYWHITE);
        DrawRectangleRec(secondResultBox, RAYWHITE);
        DrawRectangleRec(thirdResultBox, RAYWHITE);
        DrawRectangleRec(fourthResultBox, RAYWHITE);

        if (letterCount < 15)
        {
            DrawText(name, (int)textBox.x + 5, (int)textBox.y + 8, 40, RAYWHITE);
            if (((framesCounter / 20) % 2) == 0)
                DrawText("_", (int)textBox.x + 8 + MeasureText(name, 40), (int)textBox.y + 12, 40, RAYWHITE);
        }

        else if (letterCount >= 15)
        {
            DrawText(name, posX, (int)textBox.y + 8, 40, RAYWHITE);
            if (((framesCounter / 20) % 2) == 0)
                DrawText("_", posX + 8 + MeasureText(name, 40), (int)textBox.y + 12, 40, RAYWHITE);
        }

        if (mouseOnFirstBox && strlen(nameFirstResult) != 0)
        {
            DrawRectangleRec(firstResultBox, GRAY);
        }
        else if (mouseOnSecondBox && strlen(nameSecondResult) != 0)
        {
            DrawRectangleRec(secondResultBox, GRAY);
        }
        else if (mouseOnThirdBox && strlen(nameThirdResult) != 0)
        {
            DrawRectangleRec(thirdResultBox, GRAY);
        }
        else if (mouseOnFourthBox && strlen(nameFourthResult) != 0)
        {
            DrawRectangleRec(fourthResultBox, GRAY);
        }
        // Draws the result boxes text. if the mouse is over one and the mouse button is pressed, it copies the text before the "=" into the array
        DrawText(nameFirstResult, (int)firstResultBox.x + 5, (int)firstResultBox.y + 8, 40, BLACK);
        DrawText(nameSecondResult, (int)secondResultBox.x + 5, (int)secondResultBox.y + 8, 40, BLACK);
        DrawText(nameThirdResult, (int)thirdResultBox.x + 5, (int)thirdResultBox.y + 8, 40, BLACK);
        DrawText(nameFourthResult, (int)fourthResultBox.x + 5, (int)fourthResultBox.y + 8, 40, BLACK);

        if (mouseOnFirstBox && IsMouseButtonPressed(MOUSE_BUTTON_LEFT) && strlen(nameFirstResult) != 0)
        {
            letterCount = 0;
            char *begin = name;
            char *end = begin + sizeof(name);
            std::fill(begin, end, 0);

            for (long unsigned int i = 0; i < sizeof(nameFirstResult); i++)
            {
                name[i] = nameFirstResult[i];
                if (name[i] == '=')
                {
                    name[i] = '\0';
                    letterCount = (int)i;
                    break;
                }
            }
        }
        else if (mouseOnSecondBox && IsMouseButtonPressed(MOUSE_BUTTON_LEFT) && strlen(nameSecondResult) != 0)
        {
            letterCount = 0;
            char *begin = name;
            char *end = begin + sizeof(name);
            std::fill(begin, end, 0);

            for (long unsigned int i = 0; i < sizeof(nameSecondResult); i++)
            {
                name[i] = nameSecondResult[i];
                if (name[i] == '=')
                {
                    name[i] = '\0';
                    letterCount = (int)i;
                    break;
                }
            }
        }
        else if (mouseOnThirdBox && IsMouseButtonPressed(MOUSE_BUTTON_LEFT) && strlen(nameThirdResult) != 0)
        {
            letterCount = 0;
            char *begin = name;
            char *end = begin + sizeof(name);
            std::fill(begin, end, 0);

            for (long unsigned int i = 0; i < sizeof(nameThirdResult); i++)
            {
                name[i] = nameThirdResult[i];
                if (name[i] == '=')
                {
                    name[i] = '\0';
                    letterCount = (int)i;
                    break;
                }
            }
        }
        else if (mouseOnFourthBox && IsMouseButtonPressed(MOUSE_BUTTON_LEFT) && strlen(nameFourthResult) != 0)
        {
            letterCount = 0;
            char *begin = name;
            char *end = begin + sizeof(name);
            std::fill(begin, end, 0);

            for (long unsigned int i = 0; i < sizeof(nameFourthResult); i++)
            {
                name[i] = nameFourthResult[i];
                if (name[i] == '=')
                {
                    name[i] = '\0';
                    letterCount = (int)i;
                    break;
                }
            }
        }
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    UnloadTexture(buttonTex); // Unload button texture
    // buttons.clear();

    CloseWindow(); // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}

bool IsAnyKeyPressed()
{
    bool keyPressed = false;
    int key = GetKeyPressed();

    if ((key >= 32) && (key <= 126))
        keyPressed = true;

    return keyPressed;
}