# Open GL Game
Repository for a small game with a basic Level Editor made with OpenGL (it currently has no name). Currently Work in Progress

This game is inspired by https://www.youtube.com/watch?v=quJfPJqBrtI&list=PLNQqmNlNaclFpSZMDsboyksPaekA4-US1&index=1 but instead of using X11, it is developed with C++, OpenGL, glew and glfw.

I'm also going to develop a small Level Editor to make it easier to create Levels

## How to compile
The code in this repository can be debugged with VS Code
To do this, you need to follow some steps

NOTE: you can use any compiler you want to, but i'm only going to provide instructions for the one(s) i chose because then, everything should work with VS Code
Also, the default launch.json, tasks.json and c_cpp_properties should work on Linux (Ubuntu/Linux Mint) and Windows 10 ootb if you chose the default paths for everything.

How to compile for Linux:
- install glfw, glew and glm libs for your distro
- change launch.json, tasks.json and c_cpp_properties to fit your directories/compiler
- go to the "Run and Debug" tab in VS Code, select "Launch Linux" and if you set your paths correctly, everything should work


How to compile for Windows:
NOTE: launch.json, tasks.json and c_cpp_properties use 32-bit g++ and gdb and also 32-bit
lib paths by default. If you want to compile for a different architecture, you need to install them and set the directories in the files

NOTE: you can also choose to do this without MSYS2, but it's easier with MSYS2

- To compile with MinGW/g++ you need to
- Install MSYS2
- Install g++ with MSYS2
- Install glfw, glew and glm with MSYS2
- change launch.json, tasks.json and c_cpp_properties to fit your directories/compiler
- go to the "Run and Debug" tab in VS Code, select "Launch Windows" and if you set your paths correctly, everything should work