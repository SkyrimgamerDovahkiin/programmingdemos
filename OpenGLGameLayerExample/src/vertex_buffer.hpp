#pragma once

// glew
#include <GL/glew.h>

// custom
#include "defines.hpp"

class VertexBuffer
{
public:
    VertexBuffer(const void* data, unsigned int size, unsigned int hasData);
    ~VertexBuffer();

    void Bind() const;
    void Unbind() const;

private:
    GLuint mRendererID;

};