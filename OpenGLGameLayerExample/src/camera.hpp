#pragma once

// glm
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "events/mouse_event.hpp"
#include "events/window_event.hpp"

class Camera
{
public:
    Camera(float width, float height);
    ~Camera();

    inline glm::mat4 GetViewProj() { return mViewProj; }
    inline glm::vec3 GetPosition() { return mPosition; }
    inline float GetAspectRatio() { return mAspectRatio; }
    inline float GetZoomLevel() { return mZoomLevel; }

    void Update();
    void Translate(glm::vec3 v); // maybe add different move methods later?

    void OnEvent(Event &e);
    bool Zoom(MouseScrolledEvent &e);
    bool OnWindowResized(WindowResizeEvent &e);

private:
    glm::vec3 mPosition;
    glm::mat4 mProjection;
    glm::mat4 mView;
    glm::mat4 mViewProj;

    float mAspectRatio;
    float mZoomLevel;

    // for zoom/resizing
    float mScreenWidth;
    float mScreenHeight;
};