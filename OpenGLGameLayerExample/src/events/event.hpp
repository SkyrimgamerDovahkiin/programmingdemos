#pragma once

#include <string>
#include <sstream>
#include <functional>

enum class EventType
{
    None = 0, // base
    WindowResized, WindowClose,
    KeyPressed, KeyReleased,
    MouseButtonPressed, MouseButtonReleased, MouseMoved, MouseScrolled
};

// TODO: add support for multiple event catergories
// (for example, a key press, which belongs to "EventCategoryKeyboard", is also an input event).
enum EventCategory
{
    None = 0, // base
    EventCategoryWindow,
    EventCategoryKeyboard,
    EventCategoryMouse,
    EventCategoryApplication,
};

// for easier implementation of the catergories;
#define EVENT_CLASS_CATEGORY(category) virtual int GetCategoryFlags() const override { return category; }

// base event.
// NOTE: currently, events are blocking, meaning that they have to be dealt with immediatly.
// Maybe use an event queue later (if needed).
class Event
{
public:
    virtual EventType GetEventType() const = 0;
    virtual const char *GetName() const = 0;
    virtual int GetCategoryFlags() const = 0;

    // for printing other stuff to console, such as the keycode for a press event for example
    virtual std::string ToString() const { return GetName(); }

    bool Handled = false;

    inline bool IsInCategory(EventCategory category)
    {
        return GetCategoryFlags() & category;
    }
};

// event dispatching

// this event dispatcher first compares the type of the event we want to dispatch
// with the event type specified with T and then dispatches the event

class EventDispatcher
{
public:
    EventDispatcher(Event &event)
        : mEvent(event)
    {
    }

    template <typename T>
    bool Dispatch(std::function<bool(T&)> func)
    {
        if (mEvent.GetEventType() == T::GetStaticType())
        {
            mEvent.Handled = func(static_cast<T &>(mEvent));
            return true;
        }
        return false;
    }

private:
    Event &mEvent;
};