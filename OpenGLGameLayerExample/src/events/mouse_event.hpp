#pragma once

#include "event.hpp"

class MouseScrolledEvent : public Event
{
public:
    MouseScrolledEvent(float xOffset, float yOffset)
        : mXOffset(xOffset), mYOffset(yOffset) {}

    inline float GetXOffset() const { return mXOffset; }
    inline float GetYOffset() const { return mYOffset; }

    std::string ToString() const override
    {
        std::stringstream ss;
        ss << "MouseScrolledEvent: " << GetXOffset() << ", " << GetYOffset();
        return ss.str();
    }

    static EventType GetStaticType() { return EventType::MouseScrolled; }
    virtual EventType GetEventType() const override { return GetStaticType(); }
    virtual const char *GetName() const override { return "MouseScrolled"; }

    EVENT_CLASS_CATEGORY(EventCategoryMouse)
private:
    float mXOffset, mYOffset;
};

class MouseButtonEvent : public Event
{
public:
    inline int GetMouseButton() const { return mButton; }

    EVENT_CLASS_CATEGORY(EventCategoryMouse)
protected:
    MouseButtonEvent(int button)
        : mButton(button) {}

    int mButton;
};

class MouseButtonPressedEvent : public MouseButtonEvent
{
public:
    MouseButtonPressedEvent(int button)
        : MouseButtonEvent(button) {}

    std::string ToString() const override
    {
        std::stringstream ss;
        ss << "MouseButtonPressedEvent: " << mButton;
        return ss.str();
    }

    static EventType GetStaticType() { return EventType::MouseButtonPressed; }
    virtual EventType GetEventType() const override { return GetStaticType(); }
    virtual const char* GetName() const override { return "MouseButtonPressed"; }
};

class MouseButtonReleasedEvent : public MouseButtonEvent
{
public:
    MouseButtonReleasedEvent(int button)
        : MouseButtonEvent(button) {}

    std::string ToString() const override
    {
        std::stringstream ss;
        ss << "MouseButtonReleasedEvent: " << mButton;
        return ss.str();
    }

    static EventType GetStaticType() { return EventType::MouseButtonReleased; }
    virtual EventType GetEventType() const override { return GetStaticType(); }
    virtual const char* GetName() const override { return "MouseButtonReleased"; }
};