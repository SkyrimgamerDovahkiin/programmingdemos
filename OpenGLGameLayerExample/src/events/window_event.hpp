#pragma once

#include "event.hpp"

class WindowResizeEvent : public Event
{
public:
    WindowResizeEvent(uint32_t width, uint32_t height)
        : mWidth(width), mHeight(height) {}

    inline float GetWidth() const { return mWidth; }
    inline float GetHeight() const { return mHeight; }

    std::string ToString() const override
    {
        std::stringstream ss;
        ss << "WindowResizeEvent: " << mWidth << ", " << mHeight;
        return ss.str();
    }

    static EventType GetStaticType() { return EventType::WindowResized; }
    virtual EventType GetEventType() const override { return GetStaticType(); }
    virtual const char *GetName() const override { return "WindowResized"; }

    EVENT_CLASS_CATEGORY(EventCategoryWindow)
private:
    uint32_t mWidth, mHeight;
};

class WindowCloseEvent : public Event
{
public:
    WindowCloseEvent() {}

    static EventType GetStaticType() { return EventType::WindowClose; }
    virtual EventType GetEventType() const override { return GetStaticType(); }
    virtual const char* GetName() const override { return "WindowClose"; }

    EVENT_CLASS_CATEGORY(EventCategoryApplication)
};