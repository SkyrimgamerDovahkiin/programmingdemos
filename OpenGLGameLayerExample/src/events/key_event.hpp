#pragma once

#include "event.hpp"

class KeyEvent : public Event
{
public:
    inline int GetKeyCode() const { return mKeyCode; }

    EVENT_CLASS_CATEGORY(EventCategoryKeyboard)

protected:
    KeyEvent(int keycode)
        : mKeyCode(keycode) {}

    int mKeyCode;
};

class KeyPressedEvent : public KeyEvent
{
public:
    KeyPressedEvent(int keycode)
        : KeyEvent(keycode) {}

    static EventType GetStaticType() { return EventType::KeyPressed; } // needed for comparison when dispatching
    EventType GetEventType() const override { return GetStaticType(); }
    const char *GetName() const override { return "KeyPressed"; }

    std::string ToString() const override
    {
        std::stringstream ss;
        ss << "KeyPressedEvent:\n   Key Code: " << mKeyCode << "\n";
        return ss.str();
    }
};

class KeyReleasedEvent : public KeyEvent
{
public:
    KeyReleasedEvent(int keycode)
        : KeyEvent(keycode) {}

    static EventType GetStaticType() { return EventType::KeyReleased; } // needed for comparison when dispatching
    EventType GetEventType() const override { return GetStaticType(); }
    const char *GetName() const override { return "KeyReleased"; }

    std::string ToString() const override
    {
        std::stringstream ss;
        ss << "KeyReleasedEvent:\n   Key Code: " << mKeyCode << "\n";
        return ss.str();
    }
};