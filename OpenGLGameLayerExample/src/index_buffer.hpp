#pragma once

// glew
#include <GL/glew.h>

class IndexBuffer
{
public:
    IndexBuffer(const unsigned int* data, unsigned int size, unsigned int hasData);
    ~IndexBuffer();

    void Bind() const;
    void Unbind() const;

    // TODO: change later so that GetCount only returns the number of indices that actually exist
    // inline unsigned int GetCount() const { return mCount; }

private:
    GLuint mRendererID;
    // unsigned int mCount;

};