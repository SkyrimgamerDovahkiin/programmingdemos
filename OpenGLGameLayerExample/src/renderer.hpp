#pragma once

// glew
#include <GL/glew.h>

#include "vertex_array.hpp"
#include "index_buffer.hpp"
#include "shader.hpp"

class Renderer
{
public:
    void Clear() const;
    void Draw(const VertexArray &va, const Shader &shader, GLint indicesCount, GLint offset) const;
};