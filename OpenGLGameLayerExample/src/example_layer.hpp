#pragma once

// std
#include <iostream>

// glew
#include <GL/glew.h>

// glfw
#include <GLFW/glfw3.h>

// // glm
// #include <glm/glm.hpp>
// #include <glm/ext/matrix_transform.hpp>
// #include <glm/gtc/type_ptr.hpp>

// // imgui
// #include "imgui.h"
// #include "imgui_impl_glfw.h"
// #include "imgui_impl_opengl3.h"

// // custom
// #include "renderer.hpp"
// #include "vertex_buffer.hpp"
// #include "index_buffer.hpp"
// #include "vertex_array.hpp"
#include "shader.hpp"
// #include "texture.hpp"
#include "camera.hpp"
// #include "create_rect.hpp"

// // events
// #include "events/key_event.hpp"
// #include "events/mouse_event.hpp"

class ExampleLayer
{
public:
    ExampleLayer();
    ~ExampleLayer();

    void OnAttach();
    void OnDetach();
    void OnEvent(Event &event);
    // void OnUpdate(GLCore::Timestep ts);
    void OnUpdate();
    // void OnImGuiRender();

private:
    Shader *mShader;
    Camera mCamera;

    GLuint mQuadVA, mQuadVB, mQuadIB;

    glm::vec4 mSquareBaseColor = {0.8f, 0.2f, 0.3f, 1.0f};
    glm::vec4 mSquareAlternateColor = {0.2f, 0.3f, 0.8f, 1.0f};
    glm::vec4 mSquareColor = mSquareBaseColor;
};