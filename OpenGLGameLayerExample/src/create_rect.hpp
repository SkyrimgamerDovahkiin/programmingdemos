#pragma once

// std
#include <vector>

// glew
#include <GL/glew.h>

// custom
#include "defines.hpp"

void CreateRect(Rectangle rect, std::vector<Vertex> &vertices, std::vector<GLuint> &indices, GLuint &index)
{
    Vertex vertex = {glm::vec3(rect.position.x, rect.position.y, 0.0f), glm::vec2(0.0f, 0.0f)};
    vertices.push_back(vertex);

    vertex = {glm::vec3(rect.position.x + rect.dimension.x, rect.position.y, 0.0f), glm::vec2(1.0f, 0.0f)};
    vertices.push_back(vertex);

    vertex = {glm::vec3(rect.position.x + rect.dimension.x, rect.position.y + rect.dimension.y, 0.0f), glm::vec2(1.0f, 1.0f)};
    vertices.push_back(vertex);

    vertex = {glm::vec3(rect.position.x, rect.position.y + rect.dimension.y, 0.0f), glm::vec2(0.0f, 1.0f)};
    vertices.push_back(vertex);

    indices.insert(indices.end(), {index, index + 1, index + 2, index + 2, index + 3, index});
    index = index + 4;
};