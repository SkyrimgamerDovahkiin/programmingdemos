#pragma once

// std
#include <string>
#include <memory>

// custom
#include "events/event.hpp"
#include "events/window_event.hpp"
#include "example_layer.hpp"

// for passing to glfw. should be static because we should only be needing 1 window for now
struct WindowData
{
    std::function<void(Event &)> EventCallback;

    WindowData() = default;
    WindowData(const WindowData &) = delete;
    WindowData &operator=(const WindowData &) = delete;
};

class ExampleApp
{
public:
    ExampleApp(const std::string &name = "OpenGL Examples", uint32_t width = 600, uint32_t height = 600);
    ~ExampleApp();

    void Run();

    void OnEvent(Event &e);

    void PushLayer(ExampleLayer *layer);

    inline GLFWwindow &GetWindow() { return *mWindow; }

    inline static ExampleApp &Get() { return *sInstance; }

    WindowData mWindowData; // test

private:
    bool OnWindowClose(WindowCloseEvent &e);

private:
    GLFWwindow* mWindow;
    // ImGuiLayer *mImGuiLayer;
    bool mRunning = true;
    ExampleLayer* exLayer;
    float mLastFrameTime = 0.0f;

private:
    static ExampleApp *sInstance;
};