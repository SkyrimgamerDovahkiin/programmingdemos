#pragma once

// glm
#include <glm/glm.hpp>

struct Vertex
{
    glm::vec3 position;
    glm::vec2 texCoords;
};

struct Rectangle
{
    glm::vec2 position; // x and y position
    glm::vec2 dimension; // width/height
};