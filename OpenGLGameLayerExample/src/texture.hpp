#pragma once

// std
#include <string>

// glew
#include <GL/glew.h>

class Texture
{
public:
    Texture(const std::string& path);
    ~Texture();

    void Bind(unsigned int slot = 0) const;
    void Unbind() const;

    inline int GetWidth() const { return mWidth; }
    inline int GetHeight() const { return mHeight; }

private:
    GLuint mRendererID;
    std::string mFilePath;
    unsigned char *mLocalBuffer;
    GLint mWidth, mHeight, mBPP;
};