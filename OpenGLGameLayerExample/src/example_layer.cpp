#include "example_layer.hpp"

void GLAPIENTRY OpenGLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
{
    std::cout << "[OpenGL Error] " << message << std::endl;
}

ExampleLayer::ExampleLayer()
    : mCamera(600, 600)
{
}

ExampleLayer::~ExampleLayer()
{
}

void ExampleLayer::OnAttach()
{
    glDebugMessageCallback(OpenGLDebugCallback, nullptr);
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // shader creation
    mShader = new Shader("res/shaders/Basic.shader");

    glCreateVertexArrays(1, &mQuadVA);
    glBindVertexArray(mQuadVA);

    float vertices[] = {
        -0.5f, -0.5f, 0.0f,
        0.5f, -0.5f, 0.0f,
        0.5f, 0.5f, 0.0f,
        -0.5f, 0.5f, 0.0f};

    glCreateBuffers(1, &mQuadVB);
    glBindBuffer(GL_ARRAY_BUFFER, mQuadVB);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);

    uint32_t indices[] = {0, 1, 2, 2, 3, 0};
    glCreateBuffers(1, &mQuadIB);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mQuadIB);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
}

void ExampleLayer::OnDetach()
{
    glDeleteVertexArrays(1, &mQuadVA);
    glDeleteBuffers(1, &mQuadVB);
    glDeleteBuffers(1, &mQuadIB);
}

void ExampleLayer::OnEvent(Event &event)
{
    mCamera.OnEvent(event);

    EventDispatcher dispatcher(event);
    dispatcher.Dispatch<MouseButtonPressedEvent>(
        [&](MouseButtonPressedEvent &e)
        {
            mSquareColor = mSquareAlternateColor;
            return false;
        });
    dispatcher.Dispatch<MouseButtonReleasedEvent>(
        [&](MouseButtonReleasedEvent &e)
        {
            mSquareColor = mSquareBaseColor;
            return false;
        });
}

// void ExampleLayer::OnUpdate(Timestep ts)
void ExampleLayer::OnUpdate()
{
    mCamera.Update();

    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    mShader->Bind();

    glm::mat4 model = glm::mat4(1.0f);
    glm::mat4 mvp = mCamera.GetViewProj() * model;

    mShader->SetUniformMat4f("u_MVP", mvp);

    // get color uniform

    glBindVertexArray(mQuadVA);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
}