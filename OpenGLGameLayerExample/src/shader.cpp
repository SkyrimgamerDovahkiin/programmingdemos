#include "shader.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

Shader::Shader(const std::string &filepath)
    : mFilePath(filepath), mRendererID(0)
{
    ShaderProgramSource source = ParseShader(filepath);
    mRendererID = CreateShader(source.VertexSource, source.FragmentSource);
    GetUniforms();
}

Shader::~Shader()
{
    glDeleteProgram(mRendererID);
}

ShaderProgramSource Shader::ParseShader(const std::string &filepath)
{
    std::ifstream stream(filepath);

    enum class ShaderType
    {
        NONE = -1,
        VERTEX = 0,
        FRAGMENT = 1
    };

    std::string line;
    std::stringstream ss[2];
    ShaderType type = ShaderType::NONE;
    while (getline(stream, line))
    {
        if (line.find("#shader") != std::string::npos)
        {
            if (line.find("vertex") != std::string::npos)
            {
                type = ShaderType::VERTEX;
            }
            if (line.find("fragment") != std::string::npos)
            {
                type = ShaderType::FRAGMENT;
            }
        }
        else
        {
            ss[(int)type] << line << '\n';
        }
    }

    return {ss[0].str(), ss[1].str()};
}

GLuint Shader::CompileShader(GLenum type, const std::string &source)
{
    GLuint id = glCreateShader(type);
    const char *src = source.c_str();
    glShaderSource(id, 1, &src, nullptr);
    glCompileShader(id);

    GLint result;
    glGetShaderiv(id, GL_COMPILE_STATUS, &result);
    if (result == GL_FALSE)
    {
        GLint length;
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
        char *message = (char *)alloca(length * sizeof(char));
        glGetShaderInfoLog(id, length, &length, message);
        std::cout << "Failed to compile " << (type == GL_VERTEX_SHADER ? "vertex" : "fragment") << " shader!" << std::endl;
        std::cout << message << std::endl;
        glDeleteShader(id);
        return 0;
    }

    return id;
}

GLuint Shader::CreateShader(const std::string &vertexShader, const std::string &fragmentShader)
{
    GLuint program = glCreateProgram();
    GLuint vs = CompileShader(GL_VERTEX_SHADER, vertexShader);
    GLuint fs = CompileShader(GL_FRAGMENT_SHADER, fragmentShader);

    glAttachShader(program, vs);
    glAttachShader(program, fs);

    glLinkProgram(program);
    glValidateProgram(program);

#ifdef _RELEASE
    glDetachShader(program, vs);
    glDetachShader(program, fs);

    glDeleteShader(vs);
    glDeleteShader(fs);
#endif

    return program;
}

void Shader::Bind() const
{
    glUseProgram(mRendererID);
}

void Shader::Unbind() const
{
    glUseProgram(0);
}

void Shader::GetUniforms()
{
    GLint numUniforms;
    glGetProgramiv(mRendererID, GL_ACTIVE_UNIFORMS, &numUniforms);

    GLint maxCharLength;
    glGetProgramiv(mRendererID, GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxCharLength);

    if (numUniforms > 0) // only do this when uniforms exist
    {
        GLchar *buffer = (GLchar *)malloc(sizeof(GLchar) * maxCharLength);

        for (size_t i = 0; i < numUniforms; i++)
        {
            GLint length, size;
            GLenum dataType;

            glGetActiveUniform(mRendererID, (GLuint)i, maxCharLength, &length, &size, &dataType, buffer);
            GLint location = glGetUniformLocation(mRendererID, buffer);

            mUniformLocationCache[std::string(buffer, length)] = location;
        }

        free(buffer);
    }
}

void Shader::SetUniform1i(const std::string &name, GLint value)
{
    glUniform1i(GetUniformLocation(name), value);
}

void Shader::SetUniform4f(const std::string &name, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3)
{
    glUniform4f(GetUniformLocation(name), v0, v1, v2, v3);
}

void Shader::SetUniformMat4f(const std::string &name, const glm::mat4 &matrix)
{
    glUniformMatrix4fv(GetUniformLocation(name), 1, GL_FALSE, &matrix[0][0]);
}

int Shader::GetUniformLocation(const std::string &name)
{
    auto it = mUniformLocationCache.find(name);

    if (it != mUniformLocationCache.end())
    {
        return it->second;
    }
    else
    {
        std::cout << "Uniform location does either not exist or is unused!" << std::endl;
        return -1;
    }
}
