#pragma once

#include "vertex_buffer.hpp"

class VertexBufferLayout;

class VertexArray
{
public:
    VertexArray();
    ~VertexArray();

    void AddBuffer(const VertexBuffer& vb);
    
    void Bind() const;
    void Unbind() const;

private:
    GLuint mRendererID;
};