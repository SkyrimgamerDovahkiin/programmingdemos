#include <memory>

#include "example_app.hpp"

int main()
{
    std::unique_ptr<ExampleApp> app = std::make_unique<ExampleApp>();
    app->Run();

    return 0;
}

// // std
// // #include <iostream>
// // #include <fstream>
// // #include <string>
// // #include <sstream>
// // #include <signal.h>

// // // glew
// // #include <GL/glew.h>

// // // glfw
// // #include <GLFW/glfw3.h>

// // // glm
// // #include <glm/glm.hpp>
// // #include <glm/ext/matrix_transform.hpp>
// // #include <glm/gtc/type_ptr.hpp>

// // // imgui
// // #include "imgui.h"
// // #include "imgui_impl_glfw.h"
// // #include "imgui_impl_opengl3.h"

// // // custom
// // #include "renderer.hpp"
// // #include "vertex_buffer.hpp"
// // #include "index_buffer.hpp"
// // #include "vertex_array.hpp"
// // #include "shader.hpp"
// // #include "texture.hpp"
// // #include "camera.hpp"
// // #include "create_rect.hpp"

// // // events
// // #include "events/key_event.hpp"
// // #include "events/mouse_event.hpp"

// void GLAPIENTRY OpenGLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
// {
//     std::cout << "[OpenGL Error] " << message << std::endl;
// }

// const int SCREEN_WIDTH = 1500;
// const int SCREEN_HEIGHT = 600;

// Camera camera(SCREEN_WIDTH, SCREEN_HEIGHT);

// glm::dvec2 cursorPos(0.0f);

// std::vector<Vertex> vertices;
// std::vector<GLuint> indices;
// GLuint indicesIndex = 0;

// GLuint maxVertices = 16000000;
// GLuint maxIndices = maxVertices / 5; // because a uint32_t is only 4 bytes. also need to change this to a GL type later

// GLuint numVertices = 0;
// GLuint numIndices = 0;

// static void CursorPositionCallback(GLFWwindow *window, double xpos, double ypos)
// {
//     cursorPos.x = xpos;
//     cursorPos.y = ypos;
// }

// // TODO: do only if left mouse button is pressed
// bool DoSomethingWithMouse(MouseButtonPressedEvent &e)
// {
//     std::vector<Vertex> verticesToPlace;
//     std::vector<GLuint> indicesToPlace;

//     // TODO: this need to be applied to the cursor rect later
//     float camAR = camera.GetAspectRatio();
//     float camZoom = camera.GetZoomLevel();

//     float zoomWidth = camAR * camZoom;
//     float zoomHeight = camZoom;

//     // the differences between the orig width/height and the zoom that need to be applied to the cursor
//     float diffX = SCREEN_WIDTH / zoomWidth;
//     float diffY = SCREEN_HEIGHT / zoomHeight;

//     float cursorPosZoomedX = cursorPos.x / diffX;
//     int remainderX = (int)cursorPosZoomedX % 20;
//     int realX = cursorPosZoomedX + camera.GetPosition().x + 20 - remainderX - 20;

//     float cursorPosZoomedY = cursorPos.y / diffY;
//     int remainderY = (int)cursorPosZoomedY % 20;
//     int realY = cursorPosZoomedY + camera.GetPosition().y + 20 - remainderY - 20;

//     Rectangle rect = {glm::vec2(realX, realY), glm::vec2(20.0f, 20.0f)};
//     CreateRect(rect, verticesToPlace, indicesToPlace, indicesIndex);

//     std::cout << "rect placed!\n";

//     glBufferSubData(GL_ARRAY_BUFFER, numVertices * sizeof(Vertex), 4 * sizeof(Vertex), verticesToPlace.data());
//     glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(indices[0]), 6 * sizeof(indices[0]), indicesToPlace.data());

//     numVertices += verticesToPlace.size();
//     numIndices += indicesToPlace.size();

//     return false;
// }

// void OnMouseClicked(Event &e)
// {
//     EventDispatcher dispatcher(e);
//     dispatcher.Dispatch<MouseButtonPressedEvent>(std::bind(&DoSomethingWithMouse, std::placeholders::_1));
// }

// // put OnEvent somewhere else so that camera doesn't need to be global
// void OnEvent(Event &e)
// {
//     camera.OnEvent(e);
//     OnMouseClicked(e);
// }

// // for passing to glfw. should be static because we should only be needing 1 window for now
// struct WindowData
// {
//     std::function<void(Event &)> EventCallback;

//     WindowData() = default;
//     WindowData(const WindowData &) = delete;
//     WindowData &operator=(const WindowData &) = delete;
// };

// int main(void)
// {
//     GLFWwindow *window;
//     {
//         /* Initialize the library */
//         if (!glfwInit())
//             return -1;

//         glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
//         glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
//         glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
//         glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, 1);
//         // glfwWindowHint(GLFW_RESIZABLE, 0); // disable resizing

//         /* Create a windowed mode window and its OpenGL context */
//         window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Hello World", NULL, NULL);
//         if (!window)
//         {
//             glfwTerminate();
//             return -1;
//         }

//         /* Make the window's context current */
//         glfwMakeContextCurrent(window);

//         glfwSwapInterval(1); // enable v-sync
//     }

//     // window data, should only exist once per window
//     WindowData windowData;
//     glfwSetWindowUserPointer(window, &windowData);

//     // 1. OnKeyPressed (or whatever function we want to use) needs to be bound
//     // the function also needs to have a event reference as an argument
//     // 2. windowData.EventCallback needs to be assigned to the bound function
//     // 3. windowData.EventCallback can then be used with arguments

//     windowData.EventCallback = std::bind(OnEvent, std::placeholders::_1);

//     if (glewInit() != GLEW_OK)
//     {
//         std::cout << "Error: Failed to initialize GLEW" << std::endl;
//     }

//     {
//         // print out OpenGL version
//         std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;

//         // print out supported OpenGL extensions
//         // GLint numExtensions = 0;
//         // glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

//         // for (int i = 0; i < numExtensions; ++i)
//         // {
//         //     std::cout << "OpenGL extensions: " << glGetStringi(GL_EXTENSIONS, i) << std::endl;
//         // }
//     }

//     // If debug enabled, enable OpenGL debug
// #ifdef _DEBUG
//     glEnable(GL_DEBUG_OUTPUT);
//     glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
//     glDebugMessageCallback(OpenGLDebugCallback, 0);
// #endif

//     // Camera camera(SCREEN_WIDTH, SCREEN_HEIGHT);

//     Rectangle cursor = {glm::vec2(0.0f, 0.0f), glm::vec2(20.0f, 20.0f)};
//     CreateRect(cursor, vertices, indices, indicesIndex);
//     // Rectangle rr = {glm::vec2(0.0f, 0.0f), glm::vec2(20.0f, 20.0f)};
//     // CreateRect(rr, vertices, indices, indicesIndex);
//     Rectangle rect = {glm::vec2(400.0f, 100.0f), glm::vec2(20.0f, 20.0f)};
//     CreateRect(rect, vertices, indices, indicesIndex);
//     rect = {glm::vec2(600.0f, 100.0f), glm::vec2(20.0f, 20.0f)};
//     CreateRect(rect, vertices, indices, indicesIndex);

//     numVertices = vertices.size();
//     numIndices = indices.size();

//     // TODO: add function to create rect at mouse position when clicked

//     // if we want to use transparent images (also needed for text later)
//     glEnable(GL_BLEND);
//     glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

//     // glfw callbacks
//     // glfwSetKeyCallback(window, [](GLFWwindow *window, int key, int scancode, int action, int mods)
//     // {
//     //     WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

//     //     switch (action)
//     //     {
//     //         case GLFW_PRESS:
//     //         {
//     //             KeyPressedEvent event(key);
//     //             data.EventCallback(event);
//     //             break;
//     //         }
//     //         case GLFW_RELEASE:
//     //         {
//     //             KeyReleasedEvent event(key);
//     //             data.EventCallback(event);
//     //             break;
//     //         }
//     //         case GLFW_REPEAT:
//     //         {
//     //             // TODO: repeat event (if needed at all)
//     //             break;
//     //         }
//     //     }
//     // });

//     glfwSetMouseButtonCallback(window, [](GLFWwindow *window, int button, int action, int mods)
//                                {
// 			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

// 			switch (action)
// 			{
// 				case GLFW_PRESS:
// 				{
// 					MouseButtonPressedEvent event(button);
// 					data.EventCallback(event);
// 					break;
// 				}
// 				case GLFW_RELEASE:
// 				{
// 					MouseButtonReleasedEvent event(button);
// 					data.EventCallback(event);
// 					break;
// 				}
// 			} });

//     glfwSetScrollCallback(window, [](GLFWwindow *window, double xOffset, double yOffset)
//                           {
//         WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

//         MouseScrolledEvent event((float)xOffset, (float)yOffset);
//         data.EventCallback(event); });

//     glfwSetWindowSizeCallback(window, [](GLFWwindow *window, int width, int height)
//                               {
//         WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

//         WindowResizeEvent event(width, height);
//         data.EventCallback(event);

//         glViewport(0, 0, width, height); });

//     glfwSetCursorPosCallback(window, CursorPositionCallback);

//     /* ----------------------------------------------------- */
//     // NOTE: if Vertex Array is used and bound and an Vertex Buffer or Index Buffer is bound after that,
//     // the VB or IB "belongs" to the Vertex Array. If both VB and IB are used,
//     // the IB needs to be created AFTER defining the VB layout
//     /* ----------------------------------------------------- */

//     // debug
//     // std::cout << sizeof(Vertex) << std::endl;
//     // std::cout << sizeof(GLuint) << std::endl;

//     VertexArray va;
//     // VertexBuffer vb(vertices.data(), vertices.size() * sizeof(Vertex));
//     VertexBuffer vb(vertices.data(), maxVertices, 0);
//     // glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_DYNAMIC_DRAW);
//     // glBufferData(GL_ARRAY_BUFFER, 300000, 0, GL_STATIC_DRAW);

//     va.AddBuffer(vb);

//     // IndexBuffer ib(indices.data(), indices.size());
//     IndexBuffer ib(indices.data(), maxIndices, 0);
//     // glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(indices[0]), indices.data(), GL_DYNAMIC_DRAW);
//     // glBufferData(GL_ELEMENT_ARRAY_BUFFER, 300000, 0, GL_STATIC_DRAW);

//     // Camera camera(SCREEN_WIDTH, SCREEN_HEIGHT);

//     Shader shader("res/shaders/Basic.shader");
//     shader.Bind();
//     // shader.SetUniform4f("u_color", 0.8f, 0.3f, 0.8f, 1.0f);

//     Texture texture("res/textures/TestImage.png");
//     texture.Bind();
//     shader.SetUniform1i("u_texture", 0);

//     va.Unbind();
//     vb.Bind();
//     ib.Bind();
//     glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices * sizeof(Vertex), vertices.data());
//     glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, numIndices * sizeof(indices[0]), indices.data());

//     va.Unbind();
//     shader.Unbind();
//     vb.Unbind();
//     ib.Unbind();

//     Renderer renderer;

//     // Setup Dear ImGui context
//     IMGUI_CHECKVERSION();
//     ImGui::CreateContext();
//     ImGuiIO &io = ImGui::GetIO();

//     // we currently have no keyboard/mouse control
//     // io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; // Enable Keyboard Controls
//     // io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;  // Enable Gamepad Controls

//     // Setup Platform/Renderer backends
//     ImGui_ImplGlfw_InitForOpenGL(window, true); // Second param install_callback=true will install GLFW callbacks and chain to existing ones.
//     ImGui_ImplOpenGL3_Init();
//     ImGui::StyleColorsDark();

//     glm::vec3 translationA(0.0f, 0.0f, 0.0f);
//     glm::vec3 translationB(0.0f, 0.0f, 0.0f);

//     float r = 0.0f;
//     float increment = 0.05f;

//     /* Loop until the user closes the window */
//     while (!glfwWindowShouldClose(window))
//     {
//         vb.Bind();
//         ib.Bind();

//         glfwGetCursorPos(window, &cursorPos.x, &cursorPos.y); // update cursor position

//         /* Render here */
//         renderer.Clear();

//         // Start the Dear ImGui frame
//         ImGui_ImplOpenGL3_NewFrame();
//         ImGui_ImplGlfw_NewFrame();
//         ImGui::NewFrame();

//         camera.Update();

//         // cursor
//         {
//             // TODO: this need to be applied to the cursor rect later
//             float camAR = camera.GetAspectRatio();
//             float camZoom = camera.GetZoomLevel();

//             float zoomWidth = camAR * camZoom;
//             float zoomHeight = camZoom;

//             // the differences between the orig width/height and the zoom that need to be applied to the cursor
//             float diffX = SCREEN_WIDTH / zoomWidth;
//             float diffY = SCREEN_HEIGHT / zoomHeight;

//             float cursorPosZoomedX = cursorPos.x / diffX;
//             int remainderX = (int)cursorPosZoomedX % 20;
//             int realX = cursorPosZoomedX + camera.GetPosition().x + 20 - remainderX - 20;

//             float cursorPosZoomedY = cursorPos.y / diffY;
//             int remainderY = (int)cursorPosZoomedY % 20;
//             int realY = cursorPosZoomedY + camera.GetPosition().y + 20 - remainderY - 20;
//             // int realY = cursorPos.y + camera.GetPosition().y + diffHeight + 20 - remainderY - 20;
//             // int realY = cursorPos.y + diffHeight + camera.GetPosition().y;

//             // Debug
//             // std::cout << "Cursor: " << cursorPos.x << ", " << realX << "\n";

//             glm::mat4 model = glm::translate(glm::mat4(1.0f), glm::vec3(realX, realY, 0.0f));
//             glm::mat4 mvp = camera.GetViewProj() * model;

//             // bind shader
//             shader.Bind();

//             // TODO: do this with the renderer (maybe create material setup?)
//             shader.SetUniformMat4f("u_MVP", mvp);
//             renderer.Draw(va, shader, 6, 0);
//         }

//         // other rects
//         {
//             glm::mat4 model = glm::translate(glm::mat4(1.0f), translationB);
//             glm::mat4 mvp = camera.GetViewProj() * model;

//             // bind shader
//             shader.Bind();

//             shader.SetUniformMat4f("u_MVP", mvp);
//             // renderer.Draw(va, shader, ib.GetCount() - 6, 6);
//             renderer.Draw(va, shader, numIndices - 6, 6);
//         }

//         if (r > 1.0f)
//         {
//             increment = -0.05f;
//         }
//         else if (r < 0.0f)
//         {
//             increment = 0.05f;
//         }

//         r += increment;

//         // Show a simple ImGui window.
//         {
//             ImGui::Begin("Hello, world!");
//             ImGui::SliderFloat3("Translation A", &translationA.x, 0.0f, (float)SCREEN_WIDTH); // Edit 1 float using a slider from 0.0f to 1.0f
//             ImGui::SliderFloat3("Translation B", &translationB.x, 0.0f, (float)SCREEN_WIDTH); // Edit 1 float using a slider from 0.0f to 1.0f
//             ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / io.Framerate, io.Framerate);
//             ImGui::End();
//         }

//         // ImGui Rendering
//         ImGui::Render();
//         ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

//         /* Swap front and back buffers */
//         glfwSwapBuffers(window);

//         /* Poll for and process events */
//         glfwPollEvents();

//         vb.Unbind();
//         ib.Unbind();
//     }

//     ImGui_ImplOpenGL3_Shutdown();
//     ImGui_ImplGlfw_Shutdown();
//     ImGui::DestroyContext();

//     glfwTerminate();

//     return 0;
// }