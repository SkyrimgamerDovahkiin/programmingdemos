#pragma once

#include <string>
#include <unordered_map>

// glm
#include <glm/glm.hpp>

// glew
#include <GL/glew.h>

struct ShaderProgramSource
{
    std::string VertexSource;
    std::string FragmentSource;
};

class Shader
{
public:
    Shader(const std::string &filepath);
    ~Shader();

    void Bind() const;
    void Unbind() const;

    void GetUniforms();

    // Set uniforms
    void SetUniform1i(const std::string &name, GLint value);
    void SetUniform4f(const std::string &name, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);
    void SetUniformMat4f(const std::string &name, const glm::mat4 &matrix);

private:
    ShaderProgramSource ParseShader(const std::string &filepath);
    GLuint CompileShader(GLenum type, const std::string &source);
    GLuint CreateShader(const std::string &vertexShader, const std::string &fragmentShader);
    int GetUniformLocation(const std::string &name);

private:
    GLuint mRendererID;
    std::string mFilePath;
    std::unordered_map<std::string, GLint> mUniformLocationCache;
};