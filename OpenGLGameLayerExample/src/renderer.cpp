#include "renderer.hpp"

#include <iostream>

void Renderer::Clear() const
{
    glClear(GL_COLOR_BUFFER_BIT);
}

void Renderer::Draw(const VertexArray &va, const Shader &shader, GLint indicesCount, GLint offset) const
{
    // bind shader
    shader.Bind();

    // bind vertex array
    va.Bind();

    glDrawElements(GL_TRIANGLES, indicesCount, GL_UNSIGNED_INT, (GLvoid *)(offset * sizeof(GLuint)));
}
