#include "camera.hpp"

#include <iostream>

Camera::Camera(float width, float height)
    : mAspectRatio(width / height),
      mZoomLevel(height),
      mScreenWidth(width),
      mScreenHeight(height)
{
    mProjection = glm::ortho(0.0f, width, height, 0.0f, -1.0f, 1.0f);

    mView = glm::mat4(1.0f);
    mPosition = glm::vec3(0.0f);
    Translate(glm::vec3(0.0f));
    Update();
}

Camera::~Camera()
{
}

void Camera::Update()
{
    mViewProj = mProjection * mView;
}

void Camera::Translate(glm::vec3 v)
{
    mPosition += v;
    mView = glm::translate(mView, v * -1.0f);
}

void Camera::OnEvent(Event &e)
{
    EventDispatcher dispatcher(e);
    dispatcher.Dispatch<MouseScrolledEvent>(std::bind(&Camera::Zoom, this, std::placeholders::_1));
    dispatcher.Dispatch<WindowResizeEvent>(std::bind(&Camera::OnWindowResized, this, std::placeholders::_1));
}

bool Camera::Zoom(MouseScrolledEvent &e)
{
    // TODO: aspect ratio needs to change when window is resized
    mZoomLevel -= e.GetYOffset() * 5.0f;

    // limit zoom so that we can basically just zoom out until it reaches the screen size
    if (mZoomLevel >= mScreenHeight)
    {
        mZoomLevel = mScreenHeight;
    }

    auto right = mAspectRatio * mZoomLevel;
    auto bottom = mZoomLevel;

    mProjection = glm::ortho(0.0f, mAspectRatio * mZoomLevel, mZoomLevel, 0.0f, -1.0f, 1.0f);
    mViewProj = mProjection * mView;

    return false;
}

bool Camera::OnWindowResized(WindowResizeEvent &e)
{
    std::cout << e.ToString() << std::endl;

    mAspectRatio = (float)e.GetWidth() / (float)e.GetHeight();

    mProjection = glm::ortho(0.0f, mAspectRatio * mZoomLevel, mZoomLevel, 0.0f, -1.0f, 1.0f);
    mViewProj = mProjection * mView;

    return false;
}