#include "example_app.hpp"

ExampleApp *ExampleApp::sInstance = nullptr;

ExampleApp::ExampleApp(const std::string &name, uint32_t width, uint32_t height)
{
    // if (!s_Instance)
    // {
    //     // Initialize core
    //     Log::Init();
    // }

    // GLCORE_ASSERT(!s_Instance, "Application already exists!");
    sInstance = this;

    // last
    exLayer = new ExampleLayer();

    {
        /* Initialize the library */
        if (!glfwInit())
            return;

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, 1);
        // glfwWindowHint(GLFW_RESIZABLE, 0); // disable resizing

        /* Create a windowed mode window and its OpenGL context */
        mWindow = glfwCreateWindow(width, height, name.c_str(), NULL, NULL);
        if (!mWindow)
        {
            glfwTerminate();
            return;
        }

        /* Make the window's context current */
        glfwMakeContextCurrent(mWindow);

        glfwSwapInterval(1); // enable v-sync
    }

    if (glewInit() != GLEW_OK)
    {
        std::cout << "Error: Failed to initialize GLEW" << std::endl;
    }
    std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;

    // mWindow = std::unique_ptr<Window>(Window::Create({name, width, height}));
    // maybe create window data
    // mWindowData->EventCallback = std::bind(&ExampleApp::OnEvent, this, std::placeholders::_1);
    mWindowData.EventCallback = std::bind(&ExampleApp::OnEvent, this, std::placeholders::_1);
    // mWindow->SetEventCallback(std::bind(&OnEvent, this, std::placeholders::_1));

    exLayer->OnAttach();

    glfwSetWindowUserPointer(mWindow, &mWindowData);

    glfwSetWindowCloseCallback(mWindow, [](GLFWwindow *window)
                               {
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
			WindowCloseEvent event;
			data.EventCallback(event); }); // there is the problem somehow

}

ExampleApp::~ExampleApp()
{
}

void ExampleApp::OnEvent(Event &e)
{
    EventDispatcher dispatcher(e);
    dispatcher.Dispatch<WindowCloseEvent>(std::bind(&ExampleApp::OnWindowClose, this, std::placeholders::_1));

    exLayer->OnEvent(e);
}

void ExampleApp::Run()
{
    while (mRunning)
    {
        // float time = (float)glfwGetTime();
        // mLastFrameTime = time;

        exLayer->OnUpdate();

        glfwPollEvents();
        glfwSwapBuffers(mWindow);

        // m_ImGuiLayer->Begin();
        // for (Layer *layer : mLayerStack)
        //     layer->OnImGuiRender();
        // m_ImGuiLayer->End();

        // mWindow->OnUpdate();
    }
}

bool ExampleApp::OnWindowClose(WindowCloseEvent &e)
{
    mRunning = false;
    return true;
}