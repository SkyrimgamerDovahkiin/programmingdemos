#include "vertex_array.hpp"

VertexArray::VertexArray()
{
    glGenVertexArrays(1, &mRendererID);
}

VertexArray::~VertexArray()
{
    glDeleteVertexArrays(1, &mRendererID);
}

void VertexArray::AddBuffer(const VertexBuffer &vb)
{
    Bind();
    vb.Bind();
    
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(struct Vertex, position));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(struct Vertex, texCoords));
}

void VertexArray::Bind() const
{
    glBindVertexArray(mRendererID);
}

void VertexArray::Unbind() const
{
    glBindVertexArray(0);
}
