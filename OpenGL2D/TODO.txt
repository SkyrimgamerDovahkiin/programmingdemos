// Engine Goals:
// be an ECS engine. If not possible, use normal OOP. Also use OOP for the beginning.
// able to create 2D Idle games. 3D will maybe (if i have time) be added later
// Simple implementation of everything, nothing overly complex

// Features:
// be able to create UI
// be able to create "Game Objects"
// be able to import images (PNG, maybe more formats later)

// First Version:
// Have a menu with Buttons (which are maybe entities) that isn't moveable in an 800 x 600 Window
// Needed:
// Class/Entity "Text" with a Text and a Transform that displays something like a score
// Class/Entity "Button" with a Sprite and a Transform
// The Button needs to do things (System)