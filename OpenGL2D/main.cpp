// std
#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>

// GLEW/SDL
#define GLEW_STATIC
#include <GL/glew.h>
#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>

// glm
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// stb
#define STB_IMAGE_IMPLEMENTATION
#include "libs/stb_image.h"

// custom
#include "defines.hpp"
#include "vertex_buffer.hpp"
#include "index_buffer.hpp"
#include "shader.hpp"
#include "sprite_renderer.hpp"
#include "camera.hpp"
#include "rectangle.hpp"

using OGLE::VertexBuffer, OGLE::Shader, OGLE::IndexBuffer, OGLE::Camera;

void GLAPIENTRY OpenGLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam)
{
    std::cout << "[OpenGL Error] " << message << std::endl;
}

#ifdef _DEBUG

void _GLGetError(const char *file, int line, const char *call)
{
    while (GLenum error = glGetError())
    {
        std::cout << "[OpenGL Error] " << glewGetErrorString(error) << " in " << file << ":" << line << " Call: " << call << std::endl;
    }
}

#define GLCALL(call) \
    call;            \
    _GLGetError(__FILE__, __LINE__, #call)

#else

#define GLCALL(call) call;

#endif

void CreateRect(Rectangle rect, std::vector<Vertex> &vertices, std::vector<uint32_t> &indices, uint32_t &numVertices, uint32_t &numIndices, uint32_t &index)
{
    float vertexPosYMinus = -(rect.height / 2) + rect.y;
    float vertexPosYPlus = (rect.height / 2) + rect.y;
    float vertexPosXMinus = -(rect.width / 2) + rect.x;
    float vertexPosXPlus = (rect.width / 2) + rect.x;

    vertices.push_back(Vertex{vertexPosXMinus, vertexPosYMinus, 0.0f, rect.color.x, rect.color.y, rect.color.z, 1.0f});
    indices.push_back(index);
    index++;
    vertices.push_back(Vertex{vertexPosXMinus, vertexPosYPlus, 0.0f, rect.color.x, rect.color.y, rect.color.z, 1.0f});
    indices.push_back(index);
    index++;
    vertices.push_back(Vertex{vertexPosXPlus, vertexPosYMinus, 0.0f, rect.color.x, rect.color.y, rect.color.z, 1.0f});
    indices.push_back(index);
    vertices.push_back(Vertex{vertexPosXPlus, vertexPosYPlus, 0.0f, rect.color.x, rect.color.y, rect.color.z, 1.0f});
    index--;
    indices.push_back(index);
    index++;
    indices.push_back(index);
    index++;
    indices.push_back(index);
    index++;

    std::cout << index << std::endl;

    numVertices = vertices.size();
    numIndices = indices.size();
}

int main(int argc, char **argv)
{
    int screenWidth = 800, screenHeight = 600;

    SDL_Window *window;
    SDL_Init(SDL_INIT_EVERYTHING);

    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetSwapInterval(1);

// Debug
#ifdef _DEBUG
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
#endif

    uint32_t flags = SDL_WINDOW_OPENGL;

    window = SDL_CreateWindow("C++ OpenGL Tutorial", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, flags);
    SDL_GLContext glContext = SDL_GL_CreateContext(window);

    GLenum err = glewInit();
    if (err != GLEW_OK)
    {
        std::cout << "Error: " << glewGetErrorString(err) << std::endl;
        std::cin.get();
        return -1;
    }

    std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;

#ifdef _DEBUG
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(OpenGLDebugCallback, 0);
#endif

    Rectangle groundRect = {-350.0f, -250.0f, 1600.0f, 100.0f, glm::vec3(1.00f, 0.26f, 0.00f)};
    Rectangle playerRect = {-350.0f, -150.0f, 50.0f, 50.0f, glm::vec3(0.04f, 0.00f, 1.00f)};
    Rectangle obstacle = {-100.0f, -100.0f, 150.0f, 70.0f, glm::vec3(1.00f, 0.00f, 0.85f)};
    Rectangle obstacle2 = {100.0f, -60.0f, 210.0f, 90.0f, glm::vec3(0.00f, 1.00f, 0.52f)};

    std::vector<Vertex> vertices;
    uint32_t numVertices = 0;

    std::vector<uint32_t> indices;
    uint32_t numIndices = 0;
    uint32_t index = 0;

    CreateRect(groundRect, vertices, indices, numVertices, numIndices, index);
    CreateRect(playerRect, vertices, indices, numVertices, numIndices, index);
    CreateRect(obstacle, vertices, indices, numVertices, numIndices, index);
    CreateRect(obstacle2, vertices, indices, numVertices, numIndices, index);

    IndexBuffer indexBuffer(indices.data(), numIndices, sizeof(indices[0]));

    VertexBuffer vertexBuffer(vertices.data(), numVertices);
    vertexBuffer.Unbind();

    Shader shader("shaders/basic_vert.glsl", "shaders/basic_frag.glsl");
    shader.Bind();

    // FPS Counter
    uint64_t perfCounterFrequency = SDL_GetPerformanceFrequency();
    uint64_t lastCounter = SDL_GetPerformanceCounter();
    float delta = 0.0f;

    glm::mat4 model = glm::mat4(1.0f);
    // model = glm::scale(model, glm::vec3(1.2f));

    // 2DCamera camera(screenWidth, screenHeight);
    Camera camera(screenWidth, screenHeight);
    camera.translate(glm::vec3(0.0f, 0.0f, 0.0f));
    camera.update();

    glm::mat4 modelViewProj = camera.getViewProj() * model;

    int modelViewProjMatrixLocaction = GLCALL(glGetUniformLocation(shader.GetShaderId(), "u_modelViewProj"));

    // Wireframe
    // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    float time = 0;

    bool close = false;
    // GLCALL(glEnable(GL_CULL_FACE));
    // GLCALL(glFrontFace(GL_CCW));
    while (!close)
    {
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        time += delta;

        camera.update();
        // model = glm::rotate(model, 1.0f * delta, glm::vec3(0, 1, 0));
        modelViewProj = camera.getViewProj() * model;

        vertexBuffer.Bind();
        indexBuffer.Bind();
        GLCALL(glUniformMatrix4fv(modelViewProjMatrixLocaction, 1, GL_FALSE, &modelViewProj[0][0]));
        GLCALL(glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, 0));
        indexBuffer.Unbind();
        vertexBuffer.Unbind();

        SDL_GL_SwapWindow(window);

        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
            {
                close = true;
            }
        }

        // FPS Counter, maybe print to text later
        uint64_t endCounter = SDL_GetPerformanceCounter();
        uint64_t counterElapsed = endCounter - lastCounter;
        delta = ((float)counterElapsed) / (float)perfCounterFrequency;
        uint32_t FPS = (uint32_t)((float)perfCounterFrequency / (float)counterElapsed);
        // std::cout << FPS << std::endl;
        lastCounter = endCounter;
    }

    return 0;
}
