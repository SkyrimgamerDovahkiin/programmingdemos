#pragma once

// glm
#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace OGLE
{
    class Camera
    {
    public:
        Camera(int width, int height);
        ~Camera();

        glm::mat4 getViewProj();
        virtual void update();
        virtual void translate(glm::vec3 v);

    protected:
        glm::vec3 position;
        glm::mat4 projection;
        glm::mat4 view;
        glm::mat4 viewProj;
    };
}