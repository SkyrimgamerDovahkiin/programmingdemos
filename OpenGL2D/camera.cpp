#include "camera.hpp"

namespace OGLE
{
    Camera::Camera(int width, int height) //(float fov, float width, float height)
    {
        projection = glm::ortho(-((float)width / 2.0f), (float)width / 2.0f, -((float)height / 2.0f), (float)height / 2.0f, -1.0f, 1.0f);
        // perspective(fov / 2.0f, width / height, 0.1f, 1000.0f);
        view = glm::mat4(1.0f);
        position = glm::vec3(0.0f);
        update();
    }

    Camera::~Camera()
    {
    }

    glm::mat4 Camera::getViewProj()
    {
        return viewProj;
    }

    void Camera::update()
    {
        viewProj = projection * view;
    }

    void Camera::translate(glm::vec3 v)
    {
        position += v;
        view = glm::translate(view, v * -1.0f);
    }
}